-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.5.45 - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para prestamos_ol
CREATE DATABASE IF NOT EXISTS `prestamos_ol` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `prestamos_ol`;

-- Volcando estructura para tabla prestamos_ol.cuotas
CREATE TABLE IF NOT EXISTS `cuotas` (
  `cod` int(20) NOT NULL AUTO_INCREMENT,
  `codprestamo` varchar(10) DEFAULT NULL,
  `numcuota` varchar(10) DEFAULT NULL,
  `fechapagar` varchar(12) DEFAULT NULL,
  `fechapagado` varchar(12) DEFAULT NULL,
  `importetotal` varchar(20) DEFAULT NULL,
  `importepagado` varchar(20) DEFAULT NULL,
  `observacion` varchar(360) DEFAULT NULL,
  `finalizada` int(1) DEFAULT '0',
  `a1` varchar(240) DEFAULT '',
  `a2` varchar(240) DEFAULT '',
  `a3` varchar(240) DEFAULT '',
  `importeinteres` varchar(20) DEFAULT '0.0',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla prestamos_ol.cuotas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `cuotas` DISABLE KEYS */;
/*!40000 ALTER TABLE `cuotas` ENABLE KEYS */;

-- Volcando estructura para tabla prestamos_ol.opciones
CREATE TABLE IF NOT EXISTS `opciones` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `num_whatsapp` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `num_comunicarse` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `novedades` varchar(240) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `permitir_login` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla prestamos_ol.opciones: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `opciones` DISABLE KEYS */;
INSERT IGNORE INTO `opciones` (`cod`, `num_whatsapp`, `num_comunicarse`, `novedades`, `permitir_login`) VALUES
	(1, '541150469127', '1150469127', 'probando las novedades', 1);
/*!40000 ALTER TABLE `opciones` ENABLE KEYS */;

-- Volcando estructura para tabla prestamos_ol.planes
CREATE TABLE IF NOT EXISTS `planes` (
  `cod` int(12) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) DEFAULT NULL,
  `importeprestar` varchar(16) DEFAULT NULL,
  `importedevolver` varchar(16) DEFAULT NULL,
  `importeinteres` varchar(16) DEFAULT NULL,
  `calculapor` varchar(4) DEFAULT NULL,
  `tipo` varchar(12) DEFAULT NULL,
  `cantcuotas` varchar(4) DEFAULT NULL,
  `interesporc` varchar(5) DEFAULT NULL,
  `porccobrador` varchar(4) DEFAULT NULL,
  `a1` varchar(240) DEFAULT '',
  `a2` varchar(240) DEFAULT '',
  `a3` varchar(240) DEFAULT '',
  `a4` varchar(20) DEFAULT '',
  `orden` int(10) DEFAULT '0',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla prestamos_ol.planes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `planes` DISABLE KEYS */;
/*!40000 ALTER TABLE `planes` ENABLE KEYS */;

-- Volcando estructura para tabla prestamos_ol.prestamos
CREATE TABLE IF NOT EXISTS `prestamos` (
  `cod` int(20) NOT NULL AUTO_INCREMENT,
  `plan` varchar(50) DEFAULT NULL,
  `observacion` varchar(360) DEFAULT NULL,
  `codcuotas` varchar(4000) DEFAULT NULL,
  `fechagenerado` varchar(12) DEFAULT NULL,
  `codcliente` varchar(6) DEFAULT NULL,
  `finalizado` int(1) DEFAULT '0',
  `codcobrador` varchar(30) DEFAULT '',
  `a1` varchar(240) DEFAULT '',
  `a2` varchar(240) DEFAULT '',
  `a3` varchar(240) DEFAULT '',
  `a4` varchar(240) DEFAULT '',
  `a5` varchar(240) DEFAULT '',
  `a6` varchar(240) DEFAULT '',
  `nom_cobrador` varchar(40) DEFAULT '',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla prestamos_ol.prestamos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `prestamos` DISABLE KEYS */;
/*!40000 ALTER TABLE `prestamos` ENABLE KEYS */;

-- Volcando estructura para tabla prestamos_ol.presta_banned
CREATE TABLE IF NOT EXISTS `presta_banned` (
  `userid` int(11) NOT NULL,
  `until` int(11) NOT NULL,
  `by` int(11) NOT NULL,
  `reason` varchar(255) NOT NULL,
  UNIQUE KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla prestamos_ol.presta_banned: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `presta_banned` DISABLE KEYS */;
/*!40000 ALTER TABLE `presta_banned` ENABLE KEYS */;

-- Volcando estructura para tabla prestamos_ol.presta_groups
CREATE TABLE IF NOT EXISTS `presta_groups` (
  `groupid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `color` varchar(50) NOT NULL,
  `canban` int(11) NOT NULL,
  `canhideavt` int(11) NOT NULL,
  `canedit` int(11) NOT NULL,
  PRIMARY KEY (`groupid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla prestamos_ol.presta_groups: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `presta_groups` DISABLE KEYS */;
INSERT IGNORE INTO `presta_groups` (`groupid`, `name`, `type`, `priority`, `color`, `canban`, `canhideavt`, `canedit`) VALUES
	(1, 'Guest', 0, 1, '', 0, 0, 0),
	(2, 'Member', 1, 1, '#08c', 0, 0, 0),
	(3, 'Moderator', 2, 1, 'green', 1, 1, 0),
	(4, 'Administrator', 3, 1, '#F0A02D', 1, 1, 1);
/*!40000 ALTER TABLE `presta_groups` ENABLE KEYS */;

-- Volcando estructura para tabla prestamos_ol.presta_privacy
CREATE TABLE IF NOT EXISTS `presta_privacy` (
  `userid` int(11) NOT NULL,
  `email` int(11) NOT NULL,
  UNIQUE KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla prestamos_ol.presta_privacy: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `presta_privacy` DISABLE KEYS */;
INSERT IGNORE INTO `presta_privacy` (`userid`, `email`) VALUES
	(1, 0);
/*!40000 ALTER TABLE `presta_privacy` ENABLE KEYS */;

-- Volcando estructura para tabla prestamos_ol.presta_settings
CREATE TABLE IF NOT EXISTS `presta_settings` (
  `site_name` varchar(255) NOT NULL DEFAULT 'Demo Site',
  `url` varchar(300) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `max_ban_period` int(11) NOT NULL DEFAULT '10',
  `register` int(11) NOT NULL DEFAULT '1',
  `email_validation` int(11) NOT NULL DEFAULT '0',
  `captcha` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla prestamos_ol.presta_settings: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `presta_settings` DISABLE KEYS */;
INSERT IGNORE INTO `presta_settings` (`site_name`, `url`, `admin_email`, `max_ban_period`, `register`, `email_validation`, `captcha`) VALUES
	('Créditos Oeste', 'http://localhost/facundo', 'nor.reply@gmail.com', 10, 0, 0, 0);
/*!40000 ALTER TABLE `presta_settings` ENABLE KEYS */;

-- Volcando estructura para tabla prestamos_ol.presta_users
CREATE TABLE IF NOT EXISTS `presta_users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `key` varchar(50) NOT NULL DEFAULT '',
  `validated` varchar(100) NOT NULL DEFAULT '1',
  `groupid` int(11) NOT NULL DEFAULT '2',
  `lastactive` int(11) NOT NULL,
  `showavt` int(11) NOT NULL DEFAULT '1',
  `banned` int(11) NOT NULL DEFAULT '0',
  `regtime` int(11) NOT NULL,
  `fbprofile` varchar(60) NOT NULL DEFAULT '',
  `codpc` int(10) NOT NULL,
  `localidad` varchar(60) NOT NULL DEFAULT '',
  `token` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla prestamos_ol.presta_users: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `presta_users` DISABLE KEYS */;
INSERT IGNORE INTO `presta_users` (`userid`, `username`, `display_name`, `password`, `email`, `key`, `validated`, `groupid`, `lastactive`, `showavt`, `banned`, `regtime`, `fbprofile`, `codpc`, `localidad`, `token`) VALUES
	(1, 'admin_co', 'Administrador', '!pass_co!', '', '', '1', 4, 1573684258, 1, 0, 0, '', 0, '', '');
/*!40000 ALTER TABLE `presta_users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

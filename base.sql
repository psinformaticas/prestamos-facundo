-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.5.45 - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para base_fc
CREATE DATABASE IF NOT EXISTS `base_fc` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci */;
USE `base_fc`;

-- Volcando estructura para tabla base_fc.app_fallidas_prestamos
CREATE TABLE IF NOT EXISTS `app_fallidas_prestamos` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `codprest` int(11) DEFAULT '0',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla base_fc.app_fallidas_prestamos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `app_fallidas_prestamos` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_fallidas_prestamos` ENABLE KEYS */;

-- Volcando estructura para tabla base_fc.app_fallidas_usuarios
CREATE TABLE IF NOT EXISTS `app_fallidas_usuarios` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) COLLATE utf8_spanish_ci DEFAULT '',
  `accion` varchar(20) COLLATE utf8_spanish_ci DEFAULT '',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla base_fc.app_fallidas_usuarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `app_fallidas_usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_fallidas_usuarios` ENABLE KEYS */;

-- Volcando estructura para tabla base_fc.clientes
CREATE TABLE IF NOT EXISTS `clientes` (
  `cod` int(12) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) DEFAULT NULL,
  `fechanac` varchar(20) DEFAULT NULL,
  `dni` varchar(20) DEFAULT NULL,
  `direccion` varchar(80) DEFAULT NULL,
  `localidad` varchar(40) DEFAULT NULL,
  `telfijo` varchar(80) DEFAULT NULL,
  `telcel` varchar(80) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `observacion` varchar(360) DEFAULT NULL,
  `codprestamos` varchar(5000) DEFAULT NULL,
  `fechaAlta` varchar(12) DEFAULT NULL,
  `ref1` varchar(400) DEFAULT NULL,
  `ref2` varchar(400) DEFAULT NULL,
  `ref3` varchar(400) DEFAULT NULL,
  `ref4` varchar(400) DEFAULT NULL,
  `a1` varchar(240) DEFAULT '',
  `a2` varchar(240) DEFAULT '',
  `a3` varchar(240) DEFAULT '',
  `ultimopago` varchar(60) DEFAULT '01/01/2099-00:00-0',
  `esclientenuevo` varchar(30) DEFAULT '',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla base_fc.clientes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

-- Volcando estructura para tabla base_fc.cobradores
CREATE TABLE IF NOT EXISTS `cobradores` (
  `cod` int(12) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) DEFAULT NULL,
  `codint` varchar(80) DEFAULT NULL,
  `telefono` varchar(120) DEFAULT NULL,
  `obs` varchar(460) DEFAULT NULL,
  `a1` varchar(240) DEFAULT '',
  `a2` varchar(240) DEFAULT '',
  `a3` varchar(240) DEFAULT '',
  `a4` varchar(240) DEFAULT '',
  `nombre_app` varchar(40) DEFAULT '',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla base_fc.cobradores: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `cobradores` DISABLE KEYS */;
/*!40000 ALTER TABLE `cobradores` ENABLE KEYS */;

-- Volcando estructura para tabla base_fc.cuotas
CREATE TABLE IF NOT EXISTS `cuotas` (
  `cod` int(20) NOT NULL AUTO_INCREMENT,
  `codprestamo` varchar(10) DEFAULT NULL,
  `numcuota` varchar(10) DEFAULT NULL,
  `fechapagar` varchar(12) DEFAULT NULL,
  `fechapagado` varchar(12) DEFAULT NULL,
  `importetotal` varchar(20) DEFAULT NULL,
  `importepagado` varchar(20) DEFAULT NULL,
  `observacion` varchar(360) DEFAULT NULL,
  `finalizada` int(1) DEFAULT '0',
  `a1` varchar(240) DEFAULT '',
  `a2` varchar(240) DEFAULT '',
  `a3` varchar(240) DEFAULT '',
  `importeinteres` varchar(20) DEFAULT '0.0',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla base_fc.cuotas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `cuotas` DISABLE KEYS */;
/*!40000 ALTER TABLE `cuotas` ENABLE KEYS */;

-- Volcando estructura para tabla base_fc.localidades
CREATE TABLE IF NOT EXISTS `localidades` (
  `cod` int(10) NOT NULL AUTO_INCREMENT,
  `codint` varchar(30) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `a1` varchar(200) DEFAULT '',
  `a2` varchar(200) DEFAULT '',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla base_fc.localidades: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `localidades` DISABLE KEYS */;
/*!40000 ALTER TABLE `localidades` ENABLE KEYS */;

-- Volcando estructura para tabla base_fc.logs
CREATE TABLE IF NOT EXISTS `logs` (
  `cod` int(20) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(20) DEFAULT NULL,
  `hora` varchar(10) DEFAULT NULL,
  `usuario` varchar(30) DEFAULT NULL,
  `accion` varchar(120) DEFAULT NULL,
  `detalles` varchar(400) DEFAULT NULL,
  `a1` varchar(120) DEFAULT '',
  `a2` varchar(120) DEFAULT '',
  `a3` varchar(120) DEFAULT '',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla base_fc.logs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;

-- Volcando estructura para tabla base_fc.opciones
CREATE TABLE IF NOT EXISTS `opciones` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) DEFAULT NULL,
  `direccion` varchar(40) DEFAULT NULL,
  `telefono` varchar(40) DEFAULT NULL,
  `interesdia` varchar(6) DEFAULT NULL,
  `mostrarpagados` varchar(2) DEFAULT NULL,
  `mostrarvencidos` varchar(2) DEFAULT NULL,
  `serial` varchar(40) DEFAULT NULL,
  `extra1` varchar(20) DEFAULT NULL,
  `extra2` varchar(20) DEFAULT NULL,
  `extra3` varchar(20) DEFAULT NULL,
  `extra4` varchar(20) DEFAULT NULL,
  `extra5` varchar(20) DEFAULT NULL,
  `extra6` varchar(20) DEFAULT NULL,
  `ultimobackup` varchar(30) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla base_fc.opciones: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `opciones` DISABLE KEYS */;
INSERT IGNORE INTO `opciones` (`id`, `nombre`, `direccion`, `telefono`, `interesdia`, `mostrarpagados`, `mostrarvencidos`, `serial`, `extra1`, `extra2`, `extra3`, `extra4`, `extra5`, `extra6`, `ultimobackup`) VALUES
	(1, '', '', '', '1.8', 'si', 'si', 'zw9c95gw', 'porcentaje', 'no', '', '', '', '', '26/02/2020 23:59');
/*!40000 ALTER TABLE `opciones` ENABLE KEYS */;

-- Volcando estructura para tabla base_fc.pagos_parciales
CREATE TABLE IF NOT EXISTS `pagos_parciales` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `cod_cliente` int(11) NOT NULL DEFAULT '0',
  `cod_cuota` int(11) NOT NULL DEFAULT '0',
  `cod_prestamo` int(11) NOT NULL DEFAULT '0',
  `importe` double NOT NULL DEFAULT '0',
  `usuario` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `fecha` date NOT NULL,
  `obj_prestamo` blob NOT NULL,
  `cobrador` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla base_fc.pagos_parciales: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pagos_parciales` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagos_parciales` ENABLE KEYS */;

-- Volcando estructura para tabla base_fc.planes
CREATE TABLE IF NOT EXISTS `planes` (
  `cod` int(12) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) DEFAULT NULL,
  `importeprestar` varchar(16) DEFAULT NULL,
  `importedevolver` varchar(16) DEFAULT NULL,
  `importeinteres` varchar(16) DEFAULT NULL,
  `calculapor` varchar(4) DEFAULT NULL,
  `tipo` varchar(12) DEFAULT NULL,
  `cantcuotas` varchar(4) DEFAULT NULL,
  `interesporc` varchar(16) DEFAULT NULL,
  `porccobrador` varchar(4) DEFAULT NULL,
  `a1` varchar(240) DEFAULT '',
  `a2` varchar(240) DEFAULT '',
  `a3` varchar(240) DEFAULT '',
  `a4` varchar(20) DEFAULT '',
  `orden` int(10) DEFAULT '0',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla base_fc.planes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `planes` DISABLE KEYS */;
/*!40000 ALTER TABLE `planes` ENABLE KEYS */;

-- Volcando estructura para tabla base_fc.prestamos
CREATE TABLE IF NOT EXISTS `prestamos` (
  `cod` int(20) NOT NULL AUTO_INCREMENT,
  `plan` varchar(50) DEFAULT NULL,
  `observacion` varchar(360) DEFAULT NULL,
  `codcuotas` varchar(4000) DEFAULT NULL,
  `fechagenerado` varchar(12) DEFAULT NULL,
  `codcliente` varchar(6) DEFAULT NULL,
  `finalizado` int(1) DEFAULT '0',
  `codcobrador` varchar(30) DEFAULT '',
  `a1` varchar(240) DEFAULT '',
  `a2` varchar(240) DEFAULT '',
  `a3` varchar(240) DEFAULT '',
  `a4` varchar(240) DEFAULT '',
  `a5` varchar(240) DEFAULT '',
  `a6` varchar(240) DEFAULT '',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla base_fc.prestamos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `prestamos` DISABLE KEYS */;
/*!40000 ALTER TABLE `prestamos` ENABLE KEYS */;

-- Volcando estructura para tabla base_fc.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `cod` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `tipo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla base_fc.usuarios: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT IGNORE INTO `usuarios` (`cod`, `nombre`, `password`, `tipo`) VALUES
	(1, 'Administrador', '', 'administrador');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

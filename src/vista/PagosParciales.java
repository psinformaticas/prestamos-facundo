/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vista;

import controlador.Conector;
import controlador.ConectorApp;
import controlador.Funciones;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableColumnModel;
import modelo.Cliente;
import modelo.Cobrador;
import modelo.Cuota;
import modelo.Log;
import modelo.PagoParcial;
import modelo.TMLog;
import modelo.TMPagoParcial;
import modelo.TableMouseListenerClientes;
import modelo.Usuario;
import static vista.PagosParciales.pagos;
import static vista.PagosParciales.tablaPagos;
import static vista.Principal.tablaClientes;

class PopupActionListenerPagosParciales implements ActionListener {
  public void actionPerformed(ActionEvent actionEvent) {
      
      String codS = (String) tablaPagos.getValueAt(tablaPagos.getSelectedRow(), 0);
      
      PagoParcial pago = new PagoParcial();
      for (PagoParcial p: pagos) {
          if (p.getCod() == Integer.parseInt(codS)) {
              pago = p;
          }
      }
      
      
      
      if (actionEvent.getActionCommand().equals("Ver estado anterior")) {
          String cuotasStr = "<html>Cód.&nbsp;&nbsp;&nbsp;&nbsp;Núm.&nbsp;&nbsp;&nbsp;&nbsp;Importe&nbsp;&nbsp;&nbsp;&nbsp;Pagado&nbsp;&nbsp;&nbsp;&nbsp;Venc.&nbsp;&nbsp;&nbsp;&nbsp;Fecha Pagado<br>";
          for (Cuota c: pago.getPrestamo().getCuotas()) {
              cuotasStr += Integer.toString(c.getCod())+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+c.getNumCuota()+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+c.getImporteTotal()+"&nbsp;&nbsp;&nbsp;&nbsp;"+c.getImportePagado()+"&nbsp;&nbsp;&nbsp;&nbsp;"+c.getFechaPagar()+"&nbsp;&nbsp;&nbsp;&nbsp;"+c.getFechaPagado()+"<br>";
          }
          cuotasStr += "</html>";
          JOptionPane.showMessageDialog(null, cuotasStr, "Estado de cuotas anterior a este pago", 1);
      }
      
      if (actionEvent.getActionCommand().equals("Eliminar registro de pago parcial")) {
          int dialogResult = JOptionPane.showConfirmDialog(null, "Desea eliminar este registro?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
          if(dialogResult == JOptionPane.YES_OPTION){
            Conector con = new Conector();
            con.conectar();
            con.eliminarPagoParcialPorCod(pago.getCod());
            con.cerrar();
          }
          
      }
      
      if (actionEvent.getActionCommand().equals("Restaurar estado anterior")) {
            int dialogResult = JOptionPane.showConfirmDialog(null, "Desea restaurar el estado anterior para este préstamo?", "Confirmar restauración", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if(dialogResult == JOptionPane.YES_OPTION){
                Conector con = new Conector();
                con.conectar();
                for (Cuota c: pago.getPrestamo().getCuotas()) {
                    con.actualizarCuota(c);
                }
                con.cerrar();
                
                try {
                    ConectorApp conApp = new ConectorApp();
                    conApp.conectar();
                    for (Cuota c: pago.getPrestamo().getCuotas()) {
                        conApp.actualizarCuota(c);
                    }
                    conApp.cerrar();
                } catch (Exception e) {
                    
                }
                
                JOptionPane.showMessageDialog(null, "Se ha restaurado el estado anterior del préstamo", "INFO", 1);
            }
          
          
      }
  }
}

public class PagosParciales extends javax.swing.JFrame {

    /** Creates new form Principal */
    private static TMPagoParcial modeloPagos;
    public static ArrayList<PagoParcial> pagos;
    static Conector con;
    
    public static JPopupMenu popupPagos;
    public static JMenuItem itemVerEstado;
    public static JMenuItem itemRestaurar;
    public static JMenuItem itemEliminar;
    
    public PagosParciales() {
        initComponents();
        setLocationRelativeTo(null);
    
        pagos = new ArrayList<>();
        comboClientes.removeAllItems();
        ArrayList<String> nombreClientes = new ArrayList<String>();;
        
        for (Cliente cli: Principal.clientes) {
            nombreClientes.add(cli.getNombre().toLowerCase());
        }
        
        Collections.sort(nombreClientes);
        
        for (String nom: nombreClientes) {
            comboClientes.addItem(nom);
        }
        
        //Texto predictivo comboChoferes
        JTextField campoCli = (JTextField) comboClientes.getEditor().getEditorComponent();
        //campoDestinos.setText("");
        campoCli.addKeyListener(new ComboKeyHandler(comboClientes));
        
        
        comboCobradores.removeAllItems();
        ArrayList<String> nombresCobradores = new ArrayList<String>();;
        
        for (Cobrador cob: Principal.cobradores) {
            nombresCobradores.add(cob.getCodInt());
        }
        
        Collections.sort(nombresCobradores);
        
        for (String nom: nombresCobradores) {
            comboCobradores.addItem(nom);
        }
        
        setResizable(false);
        setVisible (true);
        
        inicio.setDate(Funciones.sumarMeses(new Date(), -1));
        fin.setDate(new Date());
        
        modeloPagos = new TMPagoParcial(new ArrayList<>());
        tablaPagos.setModel(modeloPagos);
        
        con = new Conector();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bg = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        btnActualizar = new javax.swing.JButton();
        comboClientes = new javax.swing.JComboBox<>();
        jLabel43 = new javax.swing.JLabel();
        inicio = new org.jdesktop.swingx.JXDatePicker();
        fin = new org.jdesktop.swingx.JXDatePicker();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        todos = new javax.swing.JCheckBox();
        comboCobradores = new javax.swing.JComboBox<>();
        jLabel44 = new javax.swing.JLabel();
        todosCob = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        total = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaPagos = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setSize(new java.awt.Dimension(782, 566));

        bg.setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(0, 102, 204));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 34)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("PAGOS PARCIALES");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 986, Short.MAX_VALUE)
                .addGap(30, 30, 30))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(11, 11, 11))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Buscador");

        btnActualizar.setText("Actualizar Listado");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        comboClientes.setEditable(true);
        comboClientes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Sin especificar" }));
        comboClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboClientesActionPerformed(evt);
            }
        });

        jLabel43.setText("Cliente:");

        jLabel2.setText("Fecha Inicio:");

        jLabel3.setText("Fecha Fin:");

        todos.setText("Todos");
        todos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                todosActionPerformed(evt);
            }
        });

        comboCobradores.setEditable(true);
        comboCobradores.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Sin especificar" }));
        comboCobradores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCobradoresActionPerformed(evt);
            }
        });

        jLabel44.setText("Cobrador");

        todosCob.setText("Todos");
        todosCob.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                todosCobActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(comboClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(todos))
                            .addComponent(jLabel43))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(comboCobradores, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(todosCob))
                            .addComponent(jLabel44))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(inicio, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(fin, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addComponent(btnActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel43)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(comboClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(inicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(fin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnActualizar)
                            .addComponent(todos)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel44)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(comboCobradores, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(todosCob))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel5.setText("Total:");

        total.setEditable(false);
        total.setBackground(new java.awt.Color(255, 255, 255));
        total.setText("0.0");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(total, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(total, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        tablaPagos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tablaPagos);

        javax.swing.GroupLayout bgLayout = new javax.swing.GroupLayout(bg);
        bg.setLayout(bgLayout);
        bgLayout.setHorizontalGroup(
            bgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(bgLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        bgLayout.setVerticalGroup(
            bgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bgLayout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void actualizarLista() {
        String cliSel = (String) comboClientes.getSelectedItem();
        Cliente cli = Funciones.getClientePorNombre(Principal.clientes, cliSel);
        
        String cobSel = (String) comboCobradores.getSelectedItem();
        Cobrador cob = Funciones.getCobradorPorRuta(Principal.cobradores, cobSel);
        
        pagos.clear();
        con.conectar();
        if (todos.isSelected()) {
            
            if (todosCob.isSelected()) {
                pagos = con.getPagosParcialesCond(inicio.getDate(), fin.getDate(), 0, 0, 0);
            } else {
                pagos = con.getPagosParcialesCond(inicio.getDate(), fin.getDate(), 0, 0, Integer.parseInt(cob.getCodInt()));
            }
        } else {
            if (todosCob.isSelected()) {
                pagos = con.getPagosParcialesCond(inicio.getDate(), fin.getDate(), cli.getCod(), 0, 0);
            } else {
                pagos = con.getPagosParcialesCond(inicio.getDate(), fin.getDate(), cli.getCod(), 0, Integer.parseInt(cob.getCodInt()));
            }
            
        }
        con.cerrar();
        modeloPagos = new TMPagoParcial(pagos);
        tablaPagos.setModel(modeloPagos);
        
        double tot = 0.0;
        for (PagoParcial p: pagos) {
            tot += p.getImporte();
        }
        
        tot = Funciones.formatearDecimales(tot, 2);
        total.setText("$"+Double.toString(tot));
        
        //DEFINE ANCHOS
        TableColumnModel cmLogs = tablaPagos.getColumnModel();
        cmLogs.getColumn(0).setPreferredWidth(70);
        cmLogs.getColumn(1).setPreferredWidth(45);
        cmLogs.getColumn(2).setPreferredWidth(20);
        cmLogs.getColumn(3).setPreferredWidth(150);
        cmLogs.getColumn(4).setPreferredWidth(150);
        
        ActionListener alUsers = new PopupActionListenerPagosParciales();
        popupPagos = new JPopupMenu();
        itemVerEstado = new JMenuItem("Ver estado anterior");
        itemVerEstado.addActionListener(alUsers);
        itemRestaurar = new JMenuItem("Restaurar estado anterior");
        itemRestaurar.addActionListener(alUsers);
        itemEliminar = new JMenuItem("Eliminar registro de pago parcial");
        itemEliminar.addActionListener(alUsers);
        
        popupPagos.add(itemVerEstado);
        popupPagos.add(itemRestaurar);
        popupPagos.add(itemEliminar);
        
        tablaPagos.setComponentPopupMenu(popupPagos);
        tablaPagos.addMouseListener(new TableMouseListenerClientes(tablaPagos));
    }
    
    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        actualizarLista();
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void comboClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboClientesActionPerformed
    }//GEN-LAST:event_comboClientesActionPerformed

    private void todosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_todosActionPerformed
        if (todos.isSelected()) {
            comboClientes.setEnabled(false);
        } else {
            comboClientes.setEnabled(true);
        }
    }//GEN-LAST:event_todosActionPerformed

    private void comboCobradoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCobradoresActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboCobradoresActionPerformed

    private void todosCobActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_todosCobActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_todosCobActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PagosParciales.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PagosParciales.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PagosParciales.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PagosParciales.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PagosParciales().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bg;
    private javax.swing.JButton btnActualizar;
    public static javax.swing.JComboBox<String> comboClientes;
    public static javax.swing.JComboBox<String> comboCobradores;
    public static org.jdesktop.swingx.JXDatePicker fin;
    public static org.jdesktop.swingx.JXDatePicker inicio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable tablaPagos;
    public static javax.swing.JCheckBox todos;
    public static javax.swing.JCheckBox todosCob;
    public static javax.swing.JTextField total;
    // End of variables declaration//GEN-END:variables

}

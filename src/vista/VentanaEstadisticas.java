/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.Funciones;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.TableColumnModel;
import modelo.Cliente;
import modelo.Cobrador;
import modelo.Cuota;
import modelo.Localidad;
import modelo.Plan;
import modelo.Prestamo;
import modelo.TMStatsCob;
import modelo.TMStatsDias;
import modelo.TMStatsLocalidades;
import modelo.TMStatsPlan;

/**
 *
 * @author Will
 */
public class VentanaEstadisticas extends javax.swing.JFrame {

    /**
     * Creates new form VerAuto
     */
    
    TMStatsPlan modeloPlanes;
    TMStatsCob modeloCobradores;
    TMStatsDias modeloDias;
    TMStatsLocalidades modeloLocs;
    
    public VentanaEstadisticas() {
        initComponents();
        setLocationRelativeTo(null);
      
        cargarStatsPlanes();
        cargarStatsCob();
        cargarStatsDias();
        cargarStatsLoc();
        cargarStatsPlataEnCalle();
    }
    
    private void cargarStatsPlanes() {
        ArrayList<String> statsPlanes = new ArrayList<>();
        
        
        for (Plan plan: Principal.planes) {
            int cont = 0;
            for (Prestamo prestamo: Principal.todosLosPrestamos) {
                if (plan.getNombre().toLowerCase().equals(prestamo.getPlan().toLowerCase())) {
                    cont++;
                }
            }
            statsPlanes.add(plan.getNombre()+"-i-"+Integer.toString(cont));
        }
        
        
        
        modeloPlanes = new TMStatsPlan(statsPlanes);
        tablaPlanes.setModel(modeloPlanes);
        
        //DEFINE ANCHOS
        TableColumnModel tPlanesCm = tablaPlanes.getColumnModel();
        tPlanesCm.getColumn(0).setPreferredWidth(170);
        tPlanesCm.getColumn(1).setPreferredWidth(40);
    }
    
    private void cargarStatsCob() {
        ArrayList<String> statsCob = new ArrayList<>();
        
        
        for (Cobrador cob: Principal.cobradores) {
            int cont = 0;
            for (Cliente cli: Principal.clientes) {
                if (cob.getCodInt().equals(cli.getA1())) {
                    cont++;
                }
            }
            statsCob.add(Funciones.capitalize(cob.getNombre())+"-i-"+Integer.toString(cont));
        }
        
        
        
        modeloCobradores = new TMStatsCob(statsCob);
        tablaCobradores.setModel(modeloCobradores);
        
        //DEFINE ANCHOS
        TableColumnModel tPlanesCm = tablaCobradores.getColumnModel();
        tPlanesCm.getColumn(0).setPreferredWidth(170);
        tPlanesCm.getColumn(1).setPreferredWidth(40);
    }
    
    private void cargarStatsDias() {
        ArrayList<String> statsDias = new ArrayList<>();

        int cont1a10 = 0;
        int cont11a20 = 0;
        int cont21a30 = 0;
        int contmasde30 = 0;

        for (Cuota c: Principal.todasLasCuotas) {
            if (!c.yaSePago()) {
                System.out.println(c.getDiasAtrasoHastaHoy());
                if (c.getDiasAtrasoHastaHoy()>=1 && c.getDiasAtrasoHastaHoy()<=10) {
                    cont1a10++;
                }
                if (c.getDiasAtrasoHastaHoy()>=11 && c.getDiasAtrasoHastaHoy()<=20) {
                    cont11a20++;
                }
                if (c.getDiasAtrasoHastaHoy()>=21 && c.getDiasAtrasoHastaHoy()<=30) {
                    cont21a30++;
                }
                if (c.getDiasAtrasoHastaHoy()>30) {
                    contmasde30++;
                }
            }
        }
        statsDias.add("1 a 10 DÍAS-i-"+Integer.toString(cont1a10));
        statsDias.add("11 a 20 DÍAS-i-"+Integer.toString(cont11a20));
        statsDias.add("21 a 30 DÍAS-i-"+Integer.toString(cont21a30));
        statsDias.add("MÁS DE 30 DÍAS-i-"+Integer.toString(contmasde30));

        
        
        modeloDias = new TMStatsDias(statsDias);
        tablaDias.setModel(modeloDias);
        
        //DEFINE ANCHOS
        TableColumnModel tPlanesCm = tablaCobradores.getColumnModel();
        tPlanesCm.getColumn(0).setPreferredWidth(170);
        tPlanesCm.getColumn(1).setPreferredWidth(40);
    }
    
    private void cargarStatsLoc() {
        ArrayList<String> statsLoc = new ArrayList<>();
        
        
        for (Localidad loc: Principal.localidades) {
            int cont = 0;
            int contActivos = 0;
            int contInactivos = 0;
            
            for (Cliente cli: Principal.clientes) {
                if (loc.getCodInt().equals(cli.getLocalidad())) {
                    cont++;
                    if (cli.tieneTodasLasCuotasPagasSinContarInt()) {
                        contInactivos++;
                    } else {
                        contActivos++;
                    }
                }
            }
            statsLoc.add(Funciones.capitalize(loc.getNombre())+"-i-"+Integer.toString(cont)+"-i-"+Integer.toString(contActivos)+"-i-"+Integer.toString(contInactivos));
        }
        
        
        
        modeloLocs = new TMStatsLocalidades(statsLoc);
        tablaLocalidades.setModel(modeloLocs);
        
        //DEFINE ANCHOS
        TableColumnModel tPlanesCm = tablaLocalidades.getColumnModel();
        tPlanesCm.getColumn(0).setPreferredWidth(110);
        tPlanesCm.getColumn(1).setPreferredWidth(30);
           tPlanesCm.getColumn(2).setPreferredWidth(30);
              tPlanesCm.getColumn(3).setPreferredWidth(30);
    }
    
//    private void cargarStatsPlataEnCalle() {
//        
//        double totalEnCalleD = 0.0;
//        for (Cliente cli: Principal.clientes) {
//            totalEnCalleD += Funciones.getDeudaTotalPorCliente(cli, false);
//        }
//        
//        totalEnCalleD = Funciones.formatearDecimales(totalEnCalleD,2);
//        
//        plataEnCalle.setText("$"+Double.toString(totalEnCalleD));
//        
//        int cont = 0;
//        for (Prestamo prestamo: Principal.todosLosPrestamos) {
////            if (!Funciones.prestamoTieneTodasCuotasPagasYFaltaPagarInt(prestamo)); {
//            if (!Funciones.prestamoTieneTodasCuotasPagasSinContarInt(prestamo)); {
//                cont++;
//            }
//        }
//        credEnCalle.setText(Integer.toString(cont));
//    }
    
    private void cargarStatsPlataEnCalle() {
        
        double totalEnCalleD = 0.0;
        for (Cuota c: Principal.todasLasCuotas) {
            if (!c.yaSePago()) {
                totalEnCalleD += c.getImporteAdeudadoD();
            }
        }
        
        totalEnCalleD = Funciones.formatearDecimales(totalEnCalleD,2);
        
        plataEnCalle.setText("$"+Double.toString(totalEnCalleD));
        
        int cont = 0;
        Cuota cuotaNum999 = new Cuota();
        cuotaNum999.setNumCuota("999");
        for (Prestamo prestamo: Principal.todosLosPrestamos) {
          if (prestamo.tieneCuotasAnterioresAEstaSinPagar(cuotaNum999)) {
              cont++;
          }
        }
        credEnCalle.setText(Integer.toString(cont));
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        labelDominio = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaPlanes = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaCobradores = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tablaDias = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaLocalidades = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        credEnCalle = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        plataEnCalle = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        labelDominio.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        labelDominio.setText("Estadísticas");

        jButton1.setText("Guardar y cerrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Arial", 0, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_estadisticas.png"))); // NOI18N
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        tablaPlanes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tablaPlanes);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Cantidad de créditos");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Clientes por cobrador");

        tablaCobradores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tablaCobradores);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Días de demora");

        tablaDias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(tablaDias);

        tablaLocalidades.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane5.setViewportView(tablaLocalidades);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Clientes por localidad");

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Créditos en calle");

        credEnCalle.setEditable(false);
        credEnCalle.setBackground(new java.awt.Color(255, 255, 255));
        credEnCalle.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        credEnCalle.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Plata en calle");

        plataEnCalle.setEditable(false);
        plataEnCalle.setBackground(new java.awt.Color(255, 255, 255));
        plataEnCalle.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        plataEnCalle.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(plataEnCalle)
                    .addComponent(credEnCalle)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(plataEnCalle, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(credEnCalle, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(7, 7, 7)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(labelDominio))
                            .addComponent(jSeparator1)
                            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jButton1)))
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE))
                                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 249, Short.MAX_VALUE)
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addContainerGap(25, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelDominio, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaEstadisticas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaEstadisticas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaEstadisticas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaEstadisticas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaEstadisticas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField credEnCalle;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel labelDominio;
    private javax.swing.JTextField plataEnCalle;
    private javax.swing.JTable tablaCobradores;
    private javax.swing.JTable tablaDias;
    private javax.swing.JTable tablaLocalidades;
    private javax.swing.JTable tablaPlanes;
    // End of variables declaration//GEN-END:variables
}

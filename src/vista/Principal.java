/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import aplicacion.ActFallidaUsuario;
import aplicacion.AplicacionControlador;
import aplicacion.ColorearTablaUsuariosApp;
import aplicacion.TMUserApp;
import aplicacion.TableMouseListenerCobradoresApp;
import aplicacion.TableMouseListenerUsersApp;
import aplicacion.WorkersApp;
import auxiliares.FuncionesCovir;
import ayuda.AyudaPpal;
import org.jdesktop.swingx.JXDatePicker;
import java.util.ArrayList;
import modelo.*;
import controlador.*;
import static controlador.FuncionesArchivos.copiarCarpeta;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import reportes.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.DefaultRowSorter;
import javax.swing.JOptionPane;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPasswordField;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import static vista.Principal.clientes;
import static vista.Principal.cod;
import static vista.Principal.fotoCli;
import static vista.Principal.lbEstadoApp;
import static vista.Principal.tablaClientes;
import static vista.Principal.tablaClientesApp;
import static vista.Principal.tablaCobradores;
import workers.Ventana;
import workers.WorkerActRutaPrestamos;
import workers.WorkerCarga;
import workers.WorkerImpPlanes;
import workers.WorkerImpVarios;
import workers.WorkerMoverVencimientos;
import workers.WorkerPlanillaXLSCuotasFecha;
import workers.WorkerRefresh;
import workers.WorkerSincronizarCuotas;
import workers.Workers;

/**
 *
 * @author Will
 */

class PopupActionListenerClientes implements ActionListener {
  public void actionPerformed(ActionEvent actionEvent) {
      
      int codSInt = (int) tablaClientes.getValueAt(tablaClientes.getSelectedRow(), 0);
      Cliente c = Funciones.getClientePorCod(Principal.clientes, codSInt);
      
      if (actionEvent.getActionCommand().equals("Generar Sobre")) {
          try {
              TestExcel.rellenarXlsSobre(c);
          } catch (IOException ex) {
              Logger.getLogger(PopupActionListenerClientes.class.getName()).log(Level.SEVERE, null, ex);
          }
      }
      
      if (actionEvent.getActionCommand().equals("Generar Cartel Deudor")) {
               //PDF
                InputStream inputStream = null;
                JasperPrint jasperPrint= null;
                DeudorDataSource datasource = new DeudorDataSource();

                datasource.setNombre(c.getNombre());
                datasource.setDireccion(c.getDireccion());
                datasource.setDeuda("$"+Double.toString(Funciones.getDeudaTotalPorCliente(c, true)));

                Image img = fotoCli.getImage();

                BufferedImage bi = new BufferedImage(img.getWidth(null),img.getHeight(null),BufferedImage.TYPE_4BYTE_ABGR);

                Graphics2D g2 = bi.createGraphics();
                g2.drawImage(img, 0, 0, null);
                g2.dispose();
                try {
                    ImageIO.write(bi, "jpg", new File("fotocartel.jpg"));
                } catch (IOException ex) {
                    Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
                }


               try {
                    inputStream = new FileInputStream ("reporteDeudor.jrxml");
                } catch (FileNotFoundException ex) {
                   javax.swing.JOptionPane.showMessageDialog(null,"Error al leer el fichero de carga reporte\n "+ex.getMessage());
                }

                try{
                    JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
                    JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                    jasperPrint = JasperFillManager.fillReport(jasperReport, null, datasource);

                    //JasperExportManager.exportReportToPdfFile(jasperPrint, "src/agencia/reporte01.pdf");
                    //JasperViewer.viewReport(jasperPrint, false);
                    JasperViewer jv = new JasperViewer(jasperPrint, false);
                    jv.setTitle("Deudor");
                    jv.setVisible(true);

                }catch (JRException e){
                    javax.swing.JOptionPane.showMessageDialog(null,"Error al cargar fichero jrml jasper report "+e.getMessage());
                }
      }
      
      
  }
}

class PopupActionListenerUsuariosApp implements ActionListener {
  public void actionPerformed(ActionEvent actionEvent) {
      
      int codSInt = (int) tablaClientesApp.getValueAt(tablaClientesApp.getSelectedRow(), 0);
      String dni = (String) tablaClientesApp.getValueAt(tablaClientesApp.getSelectedRow(), 5);
      Cliente c = Funciones.getClientePorCod(Principal.clientes, codSInt);
      
      
      if (actionEvent.getActionCommand().equals("Crear Usuario App")) {
        int dialogResult = JOptionPane.showConfirmDialog(null, "Desea crear un usuario para: \n\n"+c.getNombre()+"?", "Confirmar creación de usuario", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            if (AplicacionControlador.getUsrAppPorUsername(dni) == null) {
                try {
                    WorkersApp.crearUsuario(c);
                } catch (Exception e) {
                    lbEstadoApp.setText("Error al crear usuario para "+c.getNombre());
                }
                
            } else {
                JOptionPane.showMessageDialog(null, "El cliente ya tiene usuario en la App", "Error", 1);
            }
        }
      }
      
      if (actionEvent.getActionCommand().equals("Eliminar Usuario App")) {
        int dialogResult = JOptionPane.showConfirmDialog(null, "Desea eliminar el usuario de la aplicación para: \n\n"+c.getNombre()+"?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            try {
                WorkersApp.eliminarUsuario(c.getDni());
            } catch (Exception e) {
                    lbEstadoApp.setText("Error al eliminar usuario para "+c.getNombre());
            }
            
        }
      }
      
      
      if (actionEvent.getActionCommand().equals("Blanquear contraseña")) {
          int dialogResult = JOptionPane.showConfirmDialog(null, "Desea blanquear la contraseña para: \n\n"+c.getDni()+"?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
             try {
                WorkersApp.blanquearContraseña(c.getDni());
            } catch (Exception e) {
                    lbEstadoApp.setText("Error al blanquear contraseña de "+c.getNombre());
            }
        }
      }
      
  }
}

class PopupActionListenerCobradoresApp implements ActionListener {
  public void actionPerformed(ActionEvent actionEvent) {
      
      int codSInt = (int) tablaCobradores.getValueAt(tablaCobradores.getSelectedRow(), 0);
      String dni = (String) tablaCobradores.getValueAt(tablaCobradores.getSelectedRow(), 5);
      Cobrador c = Funciones.getCobradorPorCod(Principal.cobradores, codSInt);
      
      
      if (actionEvent.getActionCommand().equals("Crear usuario APP")) {
        int dialogResult = JOptionPane.showConfirmDialog(null, "Desea crear un usuario para: \n\n"+c.getNombreApp()+"?", "Confirmar creación de usuario", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            if (AplicacionControlador.getUsrAppPorUsername(dni) == null) {
                try {
                    WorkersApp.crearUsuarioCobrador(c);
                } catch (Exception e) {
                    lbEstadoApp.setText("Error al crear usuario para "+c.getNombreApp());
                }
                
            } else {
                JOptionPane.showMessageDialog(null, "El cobrador ya tiene usuario en la App", "Error", 1);
            }
        }
      }
      
      if (actionEvent.getActionCommand().equals("Eliminar usuario APP")) {
        int dialogResult = JOptionPane.showConfirmDialog(null, "Desea eliminar el usuario de la aplicación para: \n\n"+c.getNombre()+"?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            try {
                WorkersApp.eliminarUsuario(c.getCodInt());
            } catch (Exception e) {
                    lbEstadoApp.setText("Error al eliminar usuario para "+c.getNombre());
            }
            
        }
      }
      
      
      if (actionEvent.getActionCommand().equals("Blanquear contraseña")) {
          int dialogResult = JOptionPane.showConfirmDialog(null, "Desea blanquear la contraseña para: \n\n"+c.getNombreApp()+"?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
             try {
                WorkersApp.blanquearContraseña(c.getCodInt());
            } catch (Exception e) {
                    lbEstadoApp.setText("Error al blanquear contraseña de "+c.getNombre());
            }
        }
      }
      
  }
}

public class Principal extends javax.swing.JFrame {

    public static TMCliente modeloClientes;
    public static TMUserApp modeloUsers;
    public static TMCobrador modeloCobradores;
    public static TMVencimiento modeloVencimientosDiarios;
    public static TMVencimiento modeloCuotasPrestamo;
    public static TMVencimiento modeloVencimientosSemanales;
    public static ArrayList<Cobrador> cobradores;
    public static ArrayList<Cliente> clientes;
    public static ArrayList<Localidad> localidades;
    public static ArrayList<Plan> planes;
    public static ArrayList<Prestamo> todosLosPrestamos;

    public static ArrayList<Cuota> todasLasCuotas;
    public static ArrayList<Cuota> cuotasDelDia;
    public static ArrayList<Cuota> cuotasPrestamo;
    public static ArrayList<Cuota> cuotasDeLaSemana;
    public static ArrayList<Prestamo> prestamosCliente;
    public static ArrayList<Prestamo> prestamosCliente2;
    public static Conector con;
    public static Prestamo prestamoEnPrestamos;
    public static Serial serial;
    
    public static ListadoPrestamos lPrestamos;
    public static ListadoPorXDias listaDias;
    public static ModifCobrador mCobrador;
    public static ListadoPorCob lCobrador;
    public static ListadoPorLocalidad lLocalidades;
    
    public static ColorearVenc colorearVenc;
    public static ButtonColumn columnaPago;
    public static ButtonColumn columnaPagoSemanal;
    public static ButtonColumn columnaPagoCuota;
    public static Cliente cliAux;
    public static String usuario;
    public static String tipo;
    public static List<Usuario> usuarios;
    public static TMUsuario modeloUsuarios;
    public static ButtonColumn columnaEditar;
    public static String zaracatunga;
    public static String serialretail;
    private Referencias referencias;
    public static GuardarPago guardarPago;
    private AgregarPrestamo agregarPrestamo;
    public static Boolean activado;
    public static Opciones opciones;
    public static OpcionesExtra opcionesExtra;
    public static AvisoDemo avisoDemo;
    
    public static JPopupMenu popupMenuClientes;
    public static JMenuItem menuItemCartel;
    public static JMenuItem menuItemSobre;
    
    public static Localidades locWindow;
    
    public static Supp welcomeWindow;
    public static ObsA4 obsA4Window;
    public static AyudaPpal helpWindow;
    
    public static Totales totales;
    public static Ventana loadWindow;
    public static WorkerCarga worker;
    
    public static WorkerImpPlanes workerIP;
    
    //FOTO CLI
    public static ImageIcon fotoCli;
    public static VerFotoCli fotoCliWindow;
    
    //MYSQL
    public static RutaMySQL rutaMySQL;
    
    public static Historial histWindow;
    public static HistorialCobranzas histCobWindow;
    
    public static CheckAgregarPrestamo checkClienteWindow;
    
    public static OpcionesInteres optIntWindow;
    public static BKPAuto bkpAutoWindow;
    public static VentanaEstadisticas statsWindow;
    public static Listado1CuotaPendiente lista1CuotaWindow;
    public static ListadoTodoPagoNoRenovaron noRenovaronWindow;
    public static ListadoPrestamosOtorgados otorgadosWindow;
    public static GenerarCarton genCartonWindow;
    
    public static RutaDrive rutaDrive;
    
    public static DefaultListModel modeloPlanes;
    
    public static boolean adminPsi;
    
    //*******************
    //*  APLICACION WEB *
    //*******************
    
    public static ArrayList<UsuarioApp> usuariosApp;
    public static ArrayList<UsuarioApp> cobradoresApp;
    public static ColorearTablaUsuariosApp colorearUsers;
    
    public static JPopupMenu popupMenuUsuariosApp;
    public static JMenuItem menuItemCrearUser;
    public static JMenuItem menuItemEliminarUser;
    public static JMenuItem menuItemBlanquearPass;
    
    public static JPopupMenu popupMenuUsuariosAppCob;
    public static JMenuItem menuItemCrearUserCob;
    public static JMenuItem menuItemEliminarUserCob;
    public static JMenuItem menuItemBlanquearPassCob;

    public static boolean webEnabled;
    
    public static ArrayList<Integer> actFallidas;
    public static ArrayList<ActFallidaUsuario> actUserFallidas;
    
    public static ConfigApp cfgAppWindow;
    
    public static PagosParciales pagosWindow;
    //**********************
    //* FIN APLICACION WEB *
    //*****c*****************
    public Principal() {
        initComponents();
        setLocationRelativeTo(null);
    }
    
    public Principal(String usu, String tip, boolean admPsi) {
        initComponents();
        setLocationRelativeTo(null);
        
        dispose();
        setResizable(false);
        setVisible (true);
        
        adminPsi = admPsi;
        
        usuario = usu;
        tipo = tip;
        jLabel38.setText(Funciones.capitalize(usu));
        con = new Conector();
        
        activado = false;
        zaracatunga = "zw9c95gw";
        
        todosLosPrestamos = new ArrayList<>();
        todasLasCuotas = new ArrayList<>();
        cuotasDelDia = new ArrayList<>();
        cuotasDeLaSemana = new ArrayList<>();
        cuotasPrestamo = new ArrayList<>();
        colorearVenc = new ColorearVenc();
        
        
        inicioTotal.setDate(Funciones.sumarMeses(Funciones.stringADate(Funciones.devolverFechaActualStr()), -1));
        finTotal.setDate(Funciones.stringADate(Funciones.devolverFechaActualStr()));
        
        ocultarFinalizados.setSelected(Funciones.ocultaFinalizados());
        
        for (int i = 1; i<190; i++) {
            comboCuotas.addItem(Integer.toString(i));
        }
        
        //BGROUPS
        ButtonGroup bgTipoPlan = new ButtonGroup();
        ButtonGroup bgTipoInt = new ButtonGroup();
        ButtonGroup bgTipoPlanB = new ButtonGroup();
        ButtonGroup bgDiasPrest = new ButtonGroup();
        ButtonGroup bgTipoTicket = new ButtonGroup();
        bgTipoPlan.add(esPrestamo);
        bgTipoPlan.add(esProducto);
        bgTipoInt.add(imp);
        bgTipoInt.add(porc);
        bgTipoPlanB.add(pTodos);
        bgTipoPlanB.add(pPrestamos);
        bgTipoPlanB.add(pProductos);
        bgDiasPrest.add(diario);
        bgDiasPrest.add(semanal);
        bgDiasPrest.add(mensual);
        bgDiasPrest.add(pers);
        bgTipoTicket.add(ticket80mm);
        bgTipoTicket.add(hojaa4);
        
        cargarMenuClientes();
        
        lbPers.setVisible(false);
        diasPers.setVisible(false);
        
        //LOG INICIO DE SESION
        con.conectar();
        con.guardarLogLogin();
        con.cerrar();
        
        workerCarga();
//        cargarOpciones();
//        comprobarActivado();
//        comprobarPermisos();
//        cargarValoresIniciales();
//        cargarUsuarios();
//        recalcularValorPlan();
//        cargarClientes();
//        cargarPlanes();
//        cargarPrestamosYCuotas();
//        cargarClientesPrestamos();
        
        mostrarInputProd(false);
        setFotoInicial();
        
        opcionesExtra = Funciones.leerOptExtra();
        //ABRE WELCOME
        if (opcionesExtra.getX5().equals("SI")) {
            abrirWelcome();
        }
        
        //SI NO ES SERVIDOR
        if (con.getOpt().getTipo().equals("TERMINAL")) {
            itemGuardarCopia.setEnabled(false);
            itemRestaurarCopia.setEnabled(false);
            itemAbrirCarpeta.setEnabled(false);
            itemSeleccionarNube.setEnabled(false);
            itemSubirADrive.setEnabled(false);
            itemBajarDeDrive.setEnabled(false);
            itemAbrirCarpetaDrive.setEnabled(false);
            itemBackupAuto.setEnabled(false);
        }
        
        new Reloj();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PanelPrincipal = new javax.swing.JTabbedPane();
        PanelInicio = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JSeparator();
        jPanel23 = new javax.swing.JPanel();
        jPanel26 = new javax.swing.JPanel();
        jLabel134 = new javax.swing.JLabel();
        panelCarga = new javax.swing.JPanel();
        lbCargando1 = new javax.swing.JLabel();
        lbCargando2 = new javax.swing.JLabel();
        lbCargando3 = new javax.swing.JLabel();
        jPanel24 = new javax.swing.JPanel();
        jPanel25 = new javax.swing.JPanel();
        jLabel59 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        valorIntAtraso = new javax.swing.JTextField();
        lbTipoInt = new javax.swing.JLabel();
        mostrarPagas = new javax.swing.JCheckBox();
        morosos = new javax.swing.JCheckBox();
        btnGuardarOpt1 = new javax.swing.JButton();
        comboTipoInt = new javax.swing.JComboBox<>();
        mAnt = new javax.swing.JCheckBox();
        diasVInicial = new javax.swing.JTextField();
        dAnt = new javax.swing.JLabel();
        diasVFinal = new javax.swing.JTextField();
        jLabel99 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jPanel29 = new javax.swing.JPanel();
        jPanel30 = new javax.swing.JPanel();
        botonImprimirPrestamo1 = new javax.swing.JButton();
        botonImprimirPrestamo2 = new javax.swing.JButton();
        parcialesCheck = new javax.swing.JCheckBox();
        jPanel21 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jSeparator8 = new javax.swing.JSeparator();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaVencDiarios = new javax.swing.JTable();
        lbVencHoy = new javax.swing.JLabel();
        btnListaCobrador = new javax.swing.JButton();
        btnListaZona = new javax.swing.JButton();
        jLabel54 = new javax.swing.JLabel();
        lbVencsHoy = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        lbCobrarHoy = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        lbCobradoHoy = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jSeparator9 = new javax.swing.JSeparator();
        jScrollPane4 = new javax.swing.JScrollPane();
        tablaVencSemanales = new javax.swing.JTable();
        btnLista30 = new javax.swing.JButton();
        btnLista60 = new javax.swing.JButton();
        PanelClientes = new javax.swing.JPanel();
        panelIzqClientes = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        botonBlanquear = new javax.swing.JButton();
        botonActualizar = new javax.swing.JButton();
        botonGuardar = new javax.swing.JButton();
        botonEliminar = new javax.swing.JButton();
        botonReferencias = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        botonAgregarPrestamo = new javax.swing.JButton();
        btnVertodos = new javax.swing.JButton();
        lbFoto = new javax.swing.JLabel();
        panelDatos = new javax.swing.JPanel();
        telcel = new javax.swing.JTextField();
        fechaNac = new org.jdesktop.swingx.JXDatePicker();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        telfijo = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        obs = new javax.swing.JTextArea();
        email = new javax.swing.JTextField();
        comboRutas = new javax.swing.JComboBox<>();
        comboLoc = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        direccion = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel104 = new javax.swing.JLabel();
        cod = new javax.swing.JTextField();
        dni = new javax.swing.JTextField();
        nombre = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        comboPrestamos = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lbUltPago = new javax.swing.JLabel();
        btnCartel = new javax.swing.JButton();
        clienteNuevo = new javax.swing.JCheckBox();
        jPanel5 = new javax.swing.JPanel();
        nombreBusqueda = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        dniBusqueda = new javax.swing.JTextField();
        jLabel57 = new javax.swing.JLabel();
        actListado1 = new javax.swing.JButton();
        jScrollPane7 = new javax.swing.JScrollPane();
        tablaClientes = new javax.swing.JTable();
        lbInfoTablaClientes = new javax.swing.JLabel();
        PanelPlanes = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        listaPlanes = new java.awt.List();
        jLabel20 = new javax.swing.JLabel();
        buscaPlan = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        actListado2 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        pTodos = new javax.swing.JRadioButton();
        pPrestamos = new javax.swing.JRadioButton();
        pProductos = new javax.swing.JRadioButton();
        jPanel41 = new javax.swing.JPanel();
        btnBajarPlan = new javax.swing.JButton();
        btnSubirPlan = new javax.swing.JButton();
        btnGuardarOrden = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        esPrestamo = new javax.swing.JRadioButton();
        esProducto = new javax.swing.JRadioButton();
        lbPInicial = new javax.swing.JLabel();
        pInicial = new javax.swing.JTextField();
        lbPPesos = new javax.swing.JLabel();
        lbSoloProd = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        mensual = new javax.swing.JCheckBox();
        diario = new javax.swing.JCheckBox();
        semanal = new javax.swing.JCheckBox();
        pers = new javax.swing.JCheckBox();
        diasPers = new javax.swing.JTextField();
        lbPers = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel32 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        nomPlan = new javax.swing.JTextField();
        importeDevolver = new javax.swing.JTextField();
        importePrestar = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        interesPorc = new javax.swing.JTextField();
        codPlan = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        interesDinero = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        lbFinan = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel118 = new javax.swing.JLabel();
        porcCobrador = new javax.swing.JTextField();
        jLabel119 = new javax.swing.JLabel();
        dineroCobrador = new javax.swing.JTextField();
        jLabel120 = new javax.swing.JLabel();
        impTotal = new javax.swing.JTextField();
        lbImpTotalPesos = new javax.swing.JLabel();
        lbImpTotal = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel61 = new javax.swing.JLabel();
        interesMensual = new javax.swing.JTextField();
        jLabel66 = new javax.swing.JLabel();
        botonEliminarPlan = new javax.swing.JButton();
        botonGuardarPlan = new javax.swing.JButton();
        botonBlanquearPlan = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JSeparator();
        jPanel14 = new javax.swing.JPanel();
        comboCuotas = new javax.swing.JComboBox<>();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        valorCuota = new javax.swing.JTextField();
        botonActualizarPlan = new javax.swing.JButton();
        jPanel37 = new javax.swing.JPanel();
        jLabel124 = new javax.swing.JLabel();
        porc = new javax.swing.JCheckBox();
        imp = new javax.swing.JCheckBox();
        jButton2 = new javax.swing.JButton();
        jPanel43 = new javax.swing.JPanel();
        cantPlanes = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel93 = new javax.swing.JLabel();
        cantPlanesPers = new javax.swing.JTextField();
        PanelPrestamos = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        jLabel43 = new javax.swing.JLabel();
        comboClientes = new javax.swing.JComboBox<>();
        listaPrestamos = new java.awt.List();
        jLabel44 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jLabel46 = new javax.swing.JLabel();
        lbTotalPrestado = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        lbCantPrestamos = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        lbTotalAdeudado = new javax.swing.JLabel();
        botonImprimirPrestamo = new javax.swing.JButton();
        botonEliminarPrestamo = new javax.swing.JButton();
        btnRefinanciar = new javax.swing.JButton();
        actListado3 = new javax.swing.JButton();
        botonImprimirComprobante = new javax.swing.JButton();
        btnCarton = new javax.swing.JButton();
        jPanel20 = new javax.swing.JPanel();
        jPanel28 = new javax.swing.JPanel();
        jLabel62 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        jSeparator11 = new javax.swing.JSeparator();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaCuotasPrestamo = new javax.swing.JTable();
        jLabel94 = new javax.swing.JLabel();
        lbPagado = new javax.swing.JLabel();
        botonReciboA4 = new javax.swing.JButton();
        botonAnular = new javax.swing.JButton();
        ocultarPagasPrestamo = new javax.swing.JCheckBox();
        botonCambiarVenc = new javax.swing.JButton();
        jPanel31 = new javax.swing.JPanel();
        jLabel91 = new javax.swing.JLabel();
        jLabel92 = new javax.swing.JLabel();
        jSeparator19 = new javax.swing.JSeparator();
        jScrollPane8 = new javax.swing.JScrollPane();
        obsPrestamo = new javax.swing.JTextArea();
        guardarObsBtn = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        planPrest = new javax.swing.JLabel();
        codPrest = new javax.swing.JLabel();
        codPrest1 = new javax.swing.JLabel();
        codPrest2 = new javax.swing.JLabel();
        cuotasPrest = new javax.swing.JLabel();
        codPrest4 = new javax.swing.JLabel();
        valorCuotaPrest = new javax.swing.JLabel();
        codPrest6 = new javax.swing.JLabel();
        totalPrest = new javax.swing.JLabel();
        codPrest8 = new javax.swing.JLabel();
        pagadoPrest = new javax.swing.JLabel();
        codPrest10 = new javax.swing.JLabel();
        adeudadoPrest = new javax.swing.JLabel();
        codPrest3 = new javax.swing.JLabel();
        codPrest5 = new javax.swing.JLabel();
        fechaPrest = new javax.swing.JLabel();
        valorImportePrest = new javax.swing.JLabel();
        codPrest11 = new javax.swing.JLabel();
        descontadoPrest = new javax.swing.JLabel();
        codPrest12 = new javax.swing.JLabel();
        sobrePrest = new javax.swing.JLabel();
        jPanel36 = new javax.swing.JPanel();
        planPrest1 = new javax.swing.JLabel();
        cobAsign = new javax.swing.JLabel();
        modifCob = new javax.swing.JButton();
        jPanel42 = new javax.swing.JPanel();
        planPrest2 = new javax.swing.JLabel();
        btnCobrarPrestamo = new javax.swing.JButton();
        PanelCobradores = new javax.swing.JPanel();
        panelIzqClientes1 = new javax.swing.JPanel();
        codCob = new javax.swing.JTextField();
        nombreCob = new javax.swing.JTextField();
        jLabel100 = new javax.swing.JLabel();
        jLabel101 = new javax.swing.JLabel();
        telCob = new javax.swing.JTextField();
        jLabel105 = new javax.swing.JLabel();
        jLabel108 = new javax.swing.JLabel();
        codIntCob = new javax.swing.JTextField();
        jLabel110 = new javax.swing.JLabel();
        jLabel111 = new javax.swing.JLabel();
        jSeparator28 = new javax.swing.JSeparator();
        jPanel33 = new javax.swing.JPanel();
        botonBlanquearCob = new javax.swing.JButton();
        botonActualizarCob = new javax.swing.JButton();
        botonGuardarCob = new javax.swing.JButton();
        botonEliminarCob = new javax.swing.JButton();
        jScrollPane9 = new javax.swing.JScrollPane();
        obsCob = new javax.swing.JTextArea();
        jLabel112 = new javax.swing.JLabel();
        jSeparator29 = new javax.swing.JSeparator();
        jPanel27 = new javax.swing.JPanel();
        btnVertodosCob = new javax.swing.JButton();
        muestraFin = new javax.swing.JCheckBox();
        globales = new javax.swing.JCheckBox();
        nombreApp = new javax.swing.JTextField();
        jLabel132 = new javax.swing.JLabel();
        jPanel34 = new javax.swing.JPanel();
        nombreBusqueda1 = new javax.swing.JTextField();
        jLabel113 = new javax.swing.JLabel();
        jLabel114 = new javax.swing.JLabel();
        actListado4 = new javax.swing.JButton();
        jPanel35 = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        tablaCobradores = new javax.swing.JTable();
        PanelOpciones = new javax.swing.JPanel();
        panelOpt1 = new javax.swing.JPanel();
        jLabel64 = new javax.swing.JLabel();
        jLabel65 = new javax.swing.JLabel();
        jSeparator12 = new javax.swing.JSeparator();
        botonActivar = new javax.swing.JButton();
        jPanel40 = new javax.swing.JPanel();
        jButton7 = new javax.swing.JButton();
        jSeparator33 = new javax.swing.JSeparator();
        recibosA4 = new javax.swing.JCheckBox();
        jSeparator34 = new javax.swing.JSeparator();
        jLabel23 = new javax.swing.JLabel();
        leyendaComprobante = new javax.swing.JTextField();
        jButton8 = new javax.swing.JButton();
        jLabel34 = new javax.swing.JLabel();
        ticket80mm = new javax.swing.JRadioButton();
        hojaa4 = new javax.swing.JRadioButton();
        jSeparator37 = new javax.swing.JSeparator();
        panelOpt2 = new javax.swing.JPanel();
        panelAvisoOPC = new javax.swing.JPanel();
        jLabel76 = new javax.swing.JLabel();
        jLabel77 = new javax.swing.JLabel();
        panelDB1 = new javax.swing.JPanel();
        jLabel67 = new javax.swing.JLabel();
        jSeparator15 = new javax.swing.JSeparator();
        jLabel68 = new javax.swing.JLabel();
        jSeparator16 = new javax.swing.JSeparator();
        jLabel33 = new javax.swing.JLabel();
        jLabel71 = new javax.swing.JLabel();
        jLabel72 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        listaUsuarios = new javax.swing.JTable();
        nombreUsrNew = new javax.swing.JTextField();
        password = new javax.swing.JPasswordField();
        comboTipo = new javax.swing.JComboBox<>();
        botonAgregarUsuario = new javax.swing.JButton();
        panelDB2 = new javax.swing.JPanel();
        jLabel69 = new javax.swing.JLabel();
        jSeparator17 = new javax.swing.JSeparator();
        jLabel70 = new javax.swing.JLabel();
        jSeparator18 = new javax.swing.JSeparator();
        nombreEmpresa = new javax.swing.JTextField();
        jLabel73 = new javax.swing.JLabel();
        dirEmpresa = new javax.swing.JTextField();
        jLabel74 = new javax.swing.JLabel();
        jLabel75 = new javax.swing.JLabel();
        telEmpresa = new javax.swing.JTextField();
        btnGuardarDatos = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jPanel32 = new javax.swing.JPanel();
        ocultarFinalizados = new javax.swing.JCheckBox();
        marcarFinalizados = new javax.swing.JCheckBox();
        jLabel60 = new javax.swing.JLabel();
        jSeparator10 = new javax.swing.JSeparator();
        habImpresion = new javax.swing.JCheckBox();
        jSeparator30 = new javax.swing.JSeparator();
        ocultaDir = new javax.swing.JCheckBox();
        ocultaTel = new javax.swing.JCheckBox();
        PanelImportar = new javax.swing.JPanel();
        panelOpt4 = new javax.swing.JPanel();
        panelDB6 = new javax.swing.JPanel();
        jLabel115 = new javax.swing.JLabel();
        jSeparator31 = new javax.swing.JSeparator();
        jLabel116 = new javax.swing.JLabel();
        jSeparator32 = new javax.swing.JSeparator();
        jPanel38 = new javax.swing.JPanel();
        planPrest3 = new javax.swing.JLabel();
        modifCob2 = new javax.swing.JButton();
        jLabel102 = new javax.swing.JLabel();
        jLabel103 = new javax.swing.JLabel();
        jPanel39 = new javax.swing.JPanel();
        planPrest4 = new javax.swing.JLabel();
        modifCob3 = new javax.swing.JButton();
        jLabel109 = new javax.swing.JLabel();
        jLabel117 = new javax.swing.JLabel();
        jLabel121 = new javax.swing.JLabel();
        jLabel122 = new javax.swing.JLabel();
        jLabel123 = new javax.swing.JLabel();
        panelOpt3 = new javax.swing.JPanel();
        jLabel106 = new javax.swing.JLabel();
        jLabel107 = new javax.swing.JLabel();
        jSeparator25 = new javax.swing.JSeparator();
        PanelEstadisticas = new javax.swing.JPanel();
        panelStats1 = new javax.swing.JPanel();
        jLabel79 = new javax.swing.JLabel();
        jLabel80 = new javax.swing.JLabel();
        jSeparator20 = new javax.swing.JSeparator();
        panelStats2 = new javax.swing.JPanel();
        panelDB3 = new javax.swing.JPanel();
        jLabel81 = new javax.swing.JLabel();
        jSeparator21 = new javax.swing.JSeparator();
        jLabel82 = new javax.swing.JLabel();
        jSeparator22 = new javax.swing.JSeparator();
        jLabel78 = new javax.swing.JLabel();
        jLabel85 = new javax.swing.JLabel();
        jLabel86 = new javax.swing.JLabel();
        jLabel87 = new javax.swing.JLabel();
        jLabel88 = new javax.swing.JLabel();
        totalPrestadoLB = new javax.swing.JLabel();
        cuotasTotLB = new javax.swing.JLabel();
        prestamosTotLB = new javax.swing.JLabel();
        totalCobrarLB = new javax.swing.JLabel();
        totalDevueltoLB = new javax.swing.JLabel();
        jLabel89 = new javax.swing.JLabel();
        totalDevueltoMesLB = new javax.swing.JLabel();
        panelDB4 = new javax.swing.JPanel();
        jLabel83 = new javax.swing.JLabel();
        jSeparator23 = new javax.swing.JSeparator();
        jLabel84 = new javax.swing.JLabel();
        jSeparator24 = new javax.swing.JSeparator();
        jLabel90 = new javax.swing.JLabel();
        clientesRegLB = new javax.swing.JLabel();
        panelDB5 = new javax.swing.JPanel();
        jLabel95 = new javax.swing.JLabel();
        jSeparator26 = new javax.swing.JSeparator();
        jSeparator27 = new javax.swing.JSeparator();
        jLabel96 = new javax.swing.JLabel();
        inicioTotal = new org.jdesktop.swingx.JXDatePicker();
        finTotal = new org.jdesktop.swingx.JXDatePicker();
        jLabel97 = new javax.swing.JLabel();
        jLabel98 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        PanelAplicacion = new javax.swing.JPanel();
        panelStats3 = new javax.swing.JPanel();
        jLabel125 = new javax.swing.JLabel();
        jLabel126 = new javax.swing.JLabel();
        jSeparator43 = new javax.swing.JSeparator();
        jPanel46 = new javax.swing.JPanel();
        jLabel130 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lbInfo = new javax.swing.JTextArea();
        btnActualizarApp = new javax.swing.JButton();
        jPanel48 = new javax.swing.JPanel();
        jLabel131 = new javax.swing.JLabel();
        lbInfoLinea1 = new javax.swing.JLabel();
        lbInfoLinea2 = new javax.swing.JLabel();
        lbInfoLinea3 = new javax.swing.JLabel();
        jPanel49 = new javax.swing.JPanel();
        jLabel133 = new javax.swing.JLabel();
        jButton9 = new javax.swing.JButton();
        panelStats4 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel44 = new javax.swing.JPanel();
        jPanel45 = new javax.swing.JPanel();
        nombreBusquedaUser = new javax.swing.JTextField();
        jLabel127 = new javax.swing.JLabel();
        jLabel128 = new javax.swing.JLabel();
        dniBusquedaUser = new javax.swing.JTextField();
        jLabel129 = new javax.swing.JLabel();
        actListado5 = new javax.swing.JButton();
        jScrollPane11 = new javax.swing.JScrollPane();
        tablaClientesApp = new javax.swing.JTable();
        jPanel47 = new javax.swing.JPanel();
        btnGenerarUsuarios = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        panelEstadoApp = new javax.swing.JPanel();
        lbEstadoApp = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jLabel36 = new javax.swing.JLabel();
        lbFecha = new javax.swing.JLabel();
        lbHora = new javax.swing.JLabel();
        lbEstadoAppAbajo = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        menuArchivo = new javax.swing.JMenu();
        jSeparator13 = new javax.swing.JPopupMenu.Separator();
        itemSalir = new javax.swing.JMenuItem();
        menuAdmin = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        itemHistCobranzas = new javax.swing.JMenuItem();
        jSeparator40 = new javax.swing.JPopupMenu.Separator();
        itemHistActividad = new javax.swing.JMenuItem();
        itemPagosParciales = new javax.swing.JMenuItem();
        menuBase = new javax.swing.JMenu();
        itemGuardarCopia = new javax.swing.JMenuItem();
        itemRestaurarCopia = new javax.swing.JMenuItem();
        itemAbrirCarpeta = new javax.swing.JMenuItem();
        jSeparator36 = new javax.swing.JPopupMenu.Separator();
        itemSeleccionarNube = new javax.swing.JMenuItem();
        itemSubirADrive = new javax.swing.JMenuItem();
        itemBajarDeDrive = new javax.swing.JMenuItem();
        itemAbrirCarpetaDrive = new javax.swing.JMenuItem();
        jSeparator39 = new javax.swing.JPopupMenu.Separator();
        itemBackupAuto = new javax.swing.JMenuItem();
        jSeparator35 = new javax.swing.JPopupMenu.Separator();
        itemBorrarClie = new javax.swing.JMenuItem();
        itemBorrarPrest = new javax.swing.JMenuItem();
        itemBorrarPlanes = new javax.swing.JMenuItem();
        itemBorrarCObr = new javax.swing.JMenuItem();
        itemBorrarLoc = new javax.swing.JMenuItem();
        jSeparator14 = new javax.swing.JPopupMenu.Separator();
        itemMantBD = new javax.swing.JMenuItem();
        itemVaciarHist = new javax.swing.JMenuItem();
        jSeparator44 = new javax.swing.JPopupMenu.Separator();
        itemSincro = new javax.swing.JMenuItem();
        jSeparator45 = new javax.swing.JPopupMenu.Separator();
        itemAdelantar = new javax.swing.JMenuItem();
        menuReportes = new javax.swing.JMenu();
        itemPlanillaSMS = new javax.swing.JMenuItem();
        menuGenerarPlanillaCuotas = new javax.swing.JMenu();
        itemPlanillaCuotasFechaAdeudadas = new javax.swing.JMenuItem();
        itemPlanillaCuotasFechaTodas = new javax.swing.JMenuItem();
        jSeparator42 = new javax.swing.JPopupMenu.Separator();
        itemTodasLasCuotasAdeudadas = new javax.swing.JMenuItem();
        jSeparator41 = new javax.swing.JPopupMenu.Separator();
        menuCli = new javax.swing.JMenu();
        itemResta1Cuota = new javax.swing.JMenuItem();
        itemFinalizaronYNoRenovaron = new javax.swing.JMenuItem();
        menuPrestamos = new javax.swing.JMenu();
        itemPrestamosPorPeriodo = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        itemActivar = new javax.swing.JMenuItem();
        jSeparator38 = new javax.swing.JPopupMenu.Separator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        setResizable(false);

        PanelPrincipal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PanelPrincipalMouseClicked(evt);
            }
        });

        jPanel18.setBackground(new java.awt.Color(255, 255, 255));

        jLabel48.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_inicio.png"))); // NOI18N

        jLabel49.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel49.setText("Pantalla de inicio");

        jPanel23.setBackground(new java.awt.Color(255, 255, 255));
        jPanel23.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel26.setBackground(new java.awt.Color(204, 204, 255));

        jLabel134.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel134.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel134.setText("ESTADO");

        javax.swing.GroupLayout jPanel26Layout = new javax.swing.GroupLayout(jPanel26);
        jPanel26.setLayout(jPanel26Layout);
        jPanel26Layout.setHorizontalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel134, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel26Layout.setVerticalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel26Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel134, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panelCarga.setBackground(new java.awt.Color(255, 102, 0));

        lbCargando1.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lbCargando1.setForeground(new java.awt.Color(255, 255, 255));
        lbCargando1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbCargando1.setText(" ");

        lbCargando2.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lbCargando2.setForeground(new java.awt.Color(255, 255, 255));
        lbCargando2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbCargando2.setText(" ");

        lbCargando3.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lbCargando3.setForeground(new java.awt.Color(255, 255, 255));
        lbCargando3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbCargando3.setText(" ");

        javax.swing.GroupLayout panelCargaLayout = new javax.swing.GroupLayout(panelCarga);
        panelCarga.setLayout(panelCargaLayout);
        panelCargaLayout.setHorizontalGroup(
            panelCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCargaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbCargando2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbCargando1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbCargando3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelCargaLayout.setVerticalGroup(
            panelCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCargaLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(lbCargando1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbCargando2, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE)
                .addGap(5, 5, 5)
                .addComponent(lbCargando3, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel26, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelCarga, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addComponent(jPanel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panelCarga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jPanel24.setBackground(new java.awt.Color(255, 255, 255));
        jPanel24.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel25.setBackground(new java.awt.Color(204, 204, 255));

        jLabel59.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel59.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel59.setText("Opciones");
        jLabel59.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel25Layout = new javax.swing.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel59, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel25Layout.setVerticalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel59, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
        );

        jLabel56.setText("Interés por día de atraso:");

        valorIntAtraso.setText("0");

        lbTipoInt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        lbTipoInt.setText("$");

        mostrarPagas.setBackground(new java.awt.Color(255, 255, 255));
        mostrarPagas.setText("Mostrar cuotas pagas");
        mostrarPagas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mostrarPagasMouseClicked(evt);
            }
        });
        mostrarPagas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mostrarPagasActionPerformed(evt);
            }
        });

        morosos.setBackground(new java.awt.Color(255, 255, 255));
        morosos.setText("Mostrar vencimientos anteriores");
        morosos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                morososMouseClicked(evt);
            }
        });
        morosos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                morososActionPerformed(evt);
            }
        });

        btnGuardarOpt1.setText("Guardar");
        btnGuardarOpt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarOpt1ActionPerformed(evt);
            }
        });

        comboTipoInt.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Efectivo", "Porcentaje" }));
        comboTipoInt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboTipoIntActionPerformed(evt);
            }
        });

        mAnt.setBackground(new java.awt.Color(255, 255, 255));
        mAnt.setText("Mostrar entre");
        mAnt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mAntActionPerformed(evt);
            }
        });

        diasVInicial.setText("90");

        dAnt.setText("dìas anteriores");

        diasVFinal.setText("30");

        jLabel99.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel99.setText("y");

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel24Layout.createSequentialGroup()
                        .addComponent(mostrarPagas)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel24Layout.createSequentialGroup()
                        .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel24Layout.createSequentialGroup()
                                .addComponent(dAnt)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnGuardarOpt1))
                            .addGroup(jPanel24Layout.createSequentialGroup()
                                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(comboTipoInt, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel56, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addComponent(lbTipoInt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(valorIntAtraso, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel24Layout.createSequentialGroup()
                                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(morosos, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel24Layout.createSequentialGroup()
                                        .addComponent(mAnt)
                                        .addGap(18, 18, 18)
                                        .addComponent(diasVInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(6, 6, 6)
                                        .addComponent(jLabel99, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(diasVFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jLabel56)
                .addGap(9, 9, 9)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(valorIntAtraso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbTipoInt))
                    .addComponent(comboTipoInt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(mostrarPagas)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(morosos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mAnt)
                    .addComponent(diasVInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(diasVFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel99))
                .addGap(5, 5, 5)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnGuardarOpt1)
                    .addComponent(dAnt))
                .addGap(5, 5, 5))
        );

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_close.png"))); // NOI18N
        jButton1.setText("Cerrar sesión");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jPanel29.setBackground(new java.awt.Color(255, 255, 255));
        jPanel29.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel30.setBackground(new java.awt.Color(204, 204, 255));

        javax.swing.GroupLayout jPanel30Layout = new javax.swing.GroupLayout(jPanel30);
        jPanel30.setLayout(jPanel30Layout);
        jPanel30Layout.setHorizontalGroup(
            jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel30Layout.setVerticalGroup(
            jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 16, Short.MAX_VALUE)
        );

        botonImprimirPrestamo1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_print.png"))); // NOI18N
        botonImprimirPrestamo1.setText("<html><center>Vencimientos<br>Día</center></html>");
        botonImprimirPrestamo1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonImprimirPrestamo1.setIconTextGap(0);
        botonImprimirPrestamo1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonImprimirPrestamo1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonImprimirPrestamo1ActionPerformed(evt);
            }
        });

        botonImprimirPrestamo2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_print.png"))); // NOI18N
        botonImprimirPrestamo2.setText("<html><center>Vencimientos<br>7 Días</center></html>");
        botonImprimirPrestamo2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonImprimirPrestamo2.setIconTextGap(0);
        botonImprimirPrestamo2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonImprimirPrestamo2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonImprimirPrestamo2ActionPerformed(evt);
            }
        });

        parcialesCheck.setBackground(new java.awt.Color(255, 255, 255));
        parcialesCheck.setText("Imprimir parciales y atrasadas");

        javax.swing.GroupLayout jPanel29Layout = new javax.swing.GroupLayout(jPanel29);
        jPanel29.setLayout(jPanel29Layout);
        jPanel29Layout.setHorizontalGroup(
            jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel29Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel29Layout.createSequentialGroup()
                        .addComponent(parcialesCheck)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel29Layout.createSequentialGroup()
                        .addComponent(botonImprimirPrestamo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                        .addComponent(botonImprimirPrestamo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel29Layout.setVerticalGroup(
            jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel29Layout.createSequentialGroup()
                .addComponent(jPanel30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(botonImprimirPrestamo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonImprimirPrestamo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(parcialesCheck)
                .addGap(5, 5, 5))
        );

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator7)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addComponent(jLabel48)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel49)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel48)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jPanel29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jPanel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel21.setBackground(new java.awt.Color(204, 204, 255));

        jPanel19.setBackground(new java.awt.Color(255, 255, 255));

        jLabel50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_venc.png"))); // NOI18N

        jLabel51.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel51.setText("Vencimientos del día");

        tablaVencDiarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaVencDiarios.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tablaVencDiarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaVencDiariosMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tablaVencDiarios);

        lbVencHoy.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbVencHoy.setText("(00/00/0000)");

        btnListaCobrador.setText("Listado por Cobrador");
        btnListaCobrador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListaCobradorActionPerformed(evt);
            }
        });

        btnListaZona.setText("Listado por Localidad");
        btnListaZona.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListaZonaActionPerformed(evt);
            }
        });

        jLabel54.setText("Vencimientos hoy:");

        lbVencsHoy.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbVencsHoy.setText("0");

        jLabel55.setText("Total a cobrar hoy:");

        lbCobrarHoy.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbCobrarHoy.setText("$0");

        jLabel58.setText("Total canceladas hoy:");

        lbCobradoHoy.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbCobradoHoy.setText("$0");

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addComponent(jLabel50)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel19Layout.createSequentialGroup()
                                .addComponent(jLabel58)
                                .addGap(18, 18, 18)
                                .addComponent(lbCobradoHoy, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel19Layout.createSequentialGroup()
                                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel19Layout.createSequentialGroup()
                                        .addComponent(jLabel51)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lbVencHoy))
                                    .addGroup(jPanel19Layout.createSequentialGroup()
                                        .addComponent(jLabel54, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lbVencsHoy, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel19Layout.createSequentialGroup()
                                        .addComponent(jLabel55)
                                        .addGap(31, 31, 31)
                                        .addComponent(lbCobrarHoy, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGap(73, 73, 73)
                                .addComponent(btnListaZona, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnListaCobrador, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 17, Short.MAX_VALUE))))
                    .addComponent(jSeparator8))
                .addContainerGap())
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnListaCobrador, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel50, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnListaZona, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel51, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbVencHoy, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(5, 5, 5)
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel54)
                            .addComponent(lbVencsHoy))))
                .addGap(5, 5, 5)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel55)
                    .addComponent(lbCobrarHoy))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel58)
                    .addComponent(lbCobradoHoy))
                .addGap(1, 1, 1)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel22.setBackground(new java.awt.Color(255, 255, 255));

        jLabel52.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_venc.png"))); // NOI18N

        jLabel53.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel53.setText("Vencimientos de los próximos 7 días");

        tablaVencSemanales.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaVencSemanales.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tablaVencSemanales.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaVencSemanalesMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tablaVencSemanales);

        btnLista30.setText("Listado a 30 días");
        btnLista30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLista30ActionPerformed(evt);
            }
        });

        btnLista60.setText("Listado a 60 días");
        btnLista60.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLista60ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addGroup(jPanel22Layout.createSequentialGroup()
                        .addComponent(jLabel52)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel53)
                        .addGap(62, 62, 62)
                        .addComponent(btnLista30, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnLista60, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                        .addGap(21, 21, 21))
                    .addComponent(jSeparator9, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel53, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel52, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnLista30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnLista60, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(80, Short.MAX_VALUE))
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout PanelInicioLayout = new javax.swing.GroupLayout(PanelInicio);
        PanelInicio.setLayout(PanelInicioLayout);
        PanelInicioLayout.setHorizontalGroup(
            PanelInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelInicioLayout.createSequentialGroup()
                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PanelInicioLayout.setVerticalGroup(
            PanelInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jPanel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        PanelPrincipal.addTab("Inicio", PanelInicio);

        PanelClientes.setBackground(new java.awt.Color(204, 204, 255));
        PanelClientes.setPreferredSize(new java.awt.Dimension(1220, 604));

        panelIzqClientes.setBackground(new java.awt.Color(255, 255, 255));

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_clientes.png"))); // NOI18N

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel11.setText("Datos del cliente");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        botonBlanquear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_limpiar.png"))); // NOI18N
        botonBlanquear.setText("Limpiar");
        botonBlanquear.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonBlanquear.setIconTextGap(0);
        botonBlanquear.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonBlanquear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBlanquearActionPerformed(evt);
            }
        });

        botonActualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_actualizar.png"))); // NOI18N
        botonActualizar.setText("Actualizar");
        botonActualizar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonActualizar.setIconTextGap(0);
        botonActualizar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonActualizarActionPerformed(evt);
            }
        });

        botonGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_guardar.png"))); // NOI18N
        botonGuardar.setText("Guardar");
        botonGuardar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonGuardar.setIconTextGap(0);
        botonGuardar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarActionPerformed(evt);
            }
        });

        botonEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_close.png"))); // NOI18N
        botonEliminar.setText("Eliminar");
        botonEliminar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonEliminar.setIconTextGap(0);
        botonEliminar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarActionPerformed(evt);
            }
        });

        botonReferencias.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        botonReferencias.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_refmini.png"))); // NOI18N
        botonReferencias.setText("Referencias");
        botonReferencias.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonReferencias.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonReferencias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonReferenciasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botonActualizar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(botonBlanquear, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(botonGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(botonEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(botonReferencias, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(botonBlanquear)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonActualizar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonGuardar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonEliminar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonReferencias, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        botonAgregarPrestamo.setText("Agregar Préstamo");
        botonAgregarPrestamo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarPrestamoActionPerformed(evt);
            }
        });

        btnVertodos.setText("Ver todos los préstamos");
        btnVertodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVertodosActionPerformed(evt);
            }
        });

        lbFoto.setToolTipText("Ver / Modificar Foto");
        lbFoto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbFotoMouseClicked(evt);
            }
        });

        panelDatos.setBackground(new java.awt.Color(255, 255, 255));

        jLabel3.setText("Localidad:");

        jLabel5.setText("Tel. Celular:");

        jLabel4.setText("Tel. Fijo:");

        jLabel8.setText("DNI:");

        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        obs.setColumns(20);
        obs.setLineWrap(true);
        obs.setRows(5);
        obs.setWrapStyleWord(true);
        obs.setPreferredSize(new java.awt.Dimension(292, 94));
        jScrollPane2.setViewportView(obs);

        comboRutas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboRutasActionPerformed(evt);
            }
        });

        comboLoc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboLocActionPerformed(evt);
            }
        });

        jLabel10.setText("Fecha Nac:");

        jLabel7.setText("Cód:");

        jLabel104.setText("Ruta:");

        cod.setEditable(false);

        jButton5.setText("+");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jLabel6.setText("E-mail:");

        jLabel1.setText("Nombre:");

        jLabel9.setText("Préstamos:");

        comboPrestamos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPrestamosActionPerformed(evt);
            }
        });

        jLabel13.setText("Observaciones");

        jLabel2.setText("Dirección:");

        lbUltPago.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lbUltPago.setText(" ");

        btnCartel.setText("Cartel DEUDOR");
        btnCartel.setEnabled(false);
        btnCartel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCartelActionPerformed(evt);
            }
        });

        clienteNuevo.setBackground(new java.awt.Color(255, 255, 51));
        clienteNuevo.setText("Cliente Nuevo");
        clienteNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clienteNuevoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelDatosLayout = new javax.swing.GroupLayout(panelDatos);
        panelDatos.setLayout(panelDatosLayout);
        panelDatosLayout.setHorizontalGroup(
            panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDatosLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(clienteNuevo)))
                .addGap(14, 14, 14))
            .addGroup(panelDatosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbUltPago, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGroup(panelDatosLayout.createSequentialGroup()
                                    .addGap(4, 4, 4)
                                    .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel1)
                                        .addComponent(jLabel7)
                                        .addComponent(jLabel10)))
                                .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel104, javax.swing.GroupLayout.Alignment.TRAILING)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addComponent(comboLoc, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(nombre)
                            .addComponent(dni)
                            .addComponent(direccion)
                            .addComponent(telfijo)
                            .addComponent(telcel)
                            .addComponent(email)
                            .addComponent(comboRutas, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(comboPrestamos, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addComponent(fechaNac, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(panelDatosLayout.createSequentialGroup()
                                .addComponent(cod, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnCartel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        panelDatosLayout.setVerticalGroup(
            panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDatosLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(btnCartel))
                .addGap(12, 12, 12)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fechaNac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(direccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(comboLoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(telfijo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(telcel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel104)
                    .addComponent(comboRutas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(comboPrestamos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(clienteNuevo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addComponent(lbUltPago))
        );

        javax.swing.GroupLayout panelIzqClientesLayout = new javax.swing.GroupLayout(panelIzqClientes);
        panelIzqClientes.setLayout(panelIzqClientesLayout);
        panelIzqClientesLayout.setHorizontalGroup(
            panelIzqClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIzqClientesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelIzqClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator2)
                    .addComponent(jSeparator1)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelIzqClientesLayout.createSequentialGroup()
                        .addComponent(botonAgregarPrestamo)
                        .addGap(62, 62, 62)
                        .addComponent(btnVertodos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10, 10, 10))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelIzqClientesLayout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel11)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelIzqClientesLayout.createSequentialGroup()
                        .addComponent(panelDatos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelIzqClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIzqClientesLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(lbFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelIzqClientesLayout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        panelIzqClientesLayout.setVerticalGroup(
            panelIzqClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIzqClientesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelIzqClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIzqClientesLayout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIzqClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelDatos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelIzqClientesLayout.createSequentialGroup()
                        .addComponent(lbFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11)))
                .addGap(5, 5, 5)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIzqClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(botonAgregarPrestamo, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                    .addComponent(btnVertodos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        nombreBusqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nombreBusquedaActionPerformed(evt);
            }
        });
        nombreBusqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                nombreBusquedaKeyReleased(evt);
            }
        });

        jLabel14.setText("Buscar por nombre:");

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel15.setText("Búsqueda");

        dniBusqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dniBusquedaActionPerformed(evt);
            }
        });
        dniBusqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dniBusquedaKeyReleased(evt);
            }
        });

        jLabel57.setText("Buscar por DNI:");

        actListado1.setText("Actualizar Listado");
        actListado1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actListado1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(actListado1, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addComponent(nombreBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel57)
                            .addComponent(dniBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 177, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel57)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dniBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(actListado1))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(nombreBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        tablaClientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaClientesMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tablaClientesMouseEntered(evt);
            }
        });
        jScrollPane7.setViewportView(tablaClientes);

        lbInfoTablaClientes.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lbInfoTablaClientes.setText(" ");

        javax.swing.GroupLayout PanelClientesLayout = new javax.swing.GroupLayout(PanelClientes);
        PanelClientes.setLayout(PanelClientesLayout);
        PanelClientesLayout.setHorizontalGroup(
            PanelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelClientesLayout.createSequentialGroup()
                .addComponent(panelIzqClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane7)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbInfoTablaClientes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(73, Short.MAX_VALUE))
        );
        PanelClientesLayout.setVerticalGroup(
            PanelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelClientesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 423, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbInfoTablaClientes)
                .addContainerGap(19, Short.MAX_VALUE))
            .addComponent(panelIzqClientes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        PanelPrincipal.addTab("Clientes", PanelClientes);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_planes.png"))); // NOI18N

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel19.setText("Planes");

        listaPlanes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listaPlanesMouseClicked(evt);
            }
        });

        jLabel20.setText("Planes existentes:");

        buscaPlan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                buscaPlanKeyReleased(evt);
            }
        });

        jLabel18.setText("Buscar plan por nombre:");

        actListado2.setText("Actualizar Listado");
        actListado2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actListado2ActionPerformed(evt);
            }
        });

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        pTodos.setBackground(new java.awt.Color(255, 255, 255));
        pTodos.setSelected(true);
        pTodos.setText("Todos");
        pTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pTodosActionPerformed(evt);
            }
        });

        pPrestamos.setBackground(new java.awt.Color(255, 255, 255));
        pPrestamos.setText("Préstamos");
        pPrestamos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pPrestamosActionPerformed(evt);
            }
        });

        pProductos.setBackground(new java.awt.Color(255, 255, 255));
        pProductos.setText("Productos");
        pProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pProductosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pTodos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pPrestamos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pProductos)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pTodos)
                    .addComponent(pPrestamos)
                    .addComponent(pProductos))
                .addGap(5, 5, 5))
        );

        jPanel41.setBackground(new java.awt.Color(255, 255, 255));
        jPanel41.setBorder(javax.swing.BorderFactory.createTitledBorder("Ordenar Planes"));

        btnBajarPlan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/btn_bajar.png"))); // NOI18N
        btnBajarPlan.setText("Bajar");
        btnBajarPlan.setEnabled(false);
        btnBajarPlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBajarPlanActionPerformed(evt);
            }
        });

        btnSubirPlan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/btn_subir.png"))); // NOI18N
        btnSubirPlan.setText("Subir");
        btnSubirPlan.setEnabled(false);
        btnSubirPlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubirPlanActionPerformed(evt);
            }
        });

        btnGuardarOrden.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/btn_guardar16.png"))); // NOI18N
        btnGuardarOrden.setText("Guardar Orden");
        btnGuardarOrden.setEnabled(false);
        btnGuardarOrden.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarOrdenActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel41Layout = new javax.swing.GroupLayout(jPanel41);
        jPanel41.setLayout(jPanel41Layout);
        jPanel41Layout.setHorizontalGroup(
            jPanel41Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel41Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel41Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnGuardarOrden, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel41Layout.createSequentialGroup()
                        .addComponent(btnSubirPlan, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnBajarPlan, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel41Layout.setVerticalGroup(
            jPanel41Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel41Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel41Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSubirPlan, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBajarPlan, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(btnGuardarOrden)
                .addGap(5, 5, 5))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buscaPlan, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(listaPlanes, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(actListado2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel16)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel19))
                            .addComponent(jLabel20)
                            .addComponent(jLabel18))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel41, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel16)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel18)
                .addGap(5, 5, 5)
                .addComponent(buscaPlan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jLabel20)
                .addGap(5, 5, 5)
                .addComponent(listaPlanes, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(actListado2)
                .addGap(5, 5, 5))
        );

        jPanel3.setBackground(new java.awt.Color(204, 204, 255));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel7.setBackground(new java.awt.Color(229, 255, 252));

        jLabel30.setText("Tipo de plan:");

        esPrestamo.setBackground(new java.awt.Color(229, 255, 252));
        esPrestamo.setSelected(true);
        esPrestamo.setText("Préstamo");
        esPrestamo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                esPrestamoActionPerformed(evt);
            }
        });

        esProducto.setBackground(new java.awt.Color(229, 255, 252));
        esProducto.setText("Producto");
        esProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                esProductoActionPerformed(evt);
            }
        });

        lbPInicial.setText("Pago inicial:");

        pInicial.setEditable(false);
        pInicial.setText("0");
        pInicial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pInicialKeyReleased(evt);
            }
        });

        lbPPesos.setText("$");

        lbSoloProd.setText("(Solo productos)");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel30)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(esPrestamo)
                        .addGap(18, 18, 18)
                        .addComponent(esProducto))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(pInicial, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbPInicial, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbPPesos, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbSoloProd)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel30)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(esPrestamo)
                    .addComponent(esProducto))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbPInicial)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbPPesos)
                    .addComponent(lbSoloProd))
                .addContainerGap())
        );

        jPanel8.setBackground(new java.awt.Color(255, 255, 204));

        mensual.setBackground(new java.awt.Color(255, 255, 204));
        mensual.setText("Mensual");
        mensual.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mensualMouseClicked(evt);
            }
        });
        mensual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mensualActionPerformed(evt);
            }
        });

        diario.setBackground(new java.awt.Color(255, 255, 204));
        diario.setText("Diario");
        diario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                diarioMouseClicked(evt);
            }
        });
        diario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                diarioActionPerformed(evt);
            }
        });

        semanal.setBackground(new java.awt.Color(255, 255, 204));
        semanal.setText("Semanal");
        semanal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                semanalMouseClicked(evt);
            }
        });
        semanal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                semanalActionPerformed(evt);
            }
        });

        pers.setBackground(new java.awt.Color(255, 255, 204));
        pers.setText("Personalizado");
        pers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                persActionPerformed(evt);
            }
        });

        lbPers.setText("días");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(diasPers, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbPers))
                    .addComponent(diario)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(mensual, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(semanal, javax.swing.GroupLayout.Alignment.LEADING))
                    .addComponent(pers))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(diario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(semanal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mensual)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pers)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(diasPers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbPers))
                .addContainerGap())
        );

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel31.setText("Datos del plan");

        jLabel32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_datospl.png"))); // NOI18N

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));

        importeDevolver.setBackground(new java.awt.Color(153, 255, 153));
        importeDevolver.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                importeDevolverKeyReleased(evt);
            }
        });

        importePrestar.setBackground(new java.awt.Color(255, 204, 204));
        importePrestar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                importePrestarKeyReleased(evt);
            }
        });

        jLabel26.setText("$");

        interesPorc.setEditable(false);
        interesPorc.setBackground(new java.awt.Color(255, 255, 204));
        interesPorc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                interesPorcKeyReleased(evt);
            }
        });

        codPlan.setEditable(false);

        jLabel29.setText("$");

        jLabel27.setText("$");

        jLabel28.setText("%");

        interesDinero.setEditable(false);
        interesDinero.setBackground(new java.awt.Color(255, 255, 204));

        jLabel35.setText("Cód:");

        jLabel22.setText("Nombre:");

        lbFinan.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        lbFinan.setText("Importe a prestar:");

        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel24.setText("Importe a devolver:");

        jLabel25.setText("Interés total:");

        jLabel118.setText("Porcentaje Cobrador:");

        porcCobrador.setBackground(new java.awt.Color(234, 237, 255));
        porcCobrador.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                porcCobradorKeyReleased(evt);
            }
        });

        jLabel119.setText("%");

        dineroCobrador.setEditable(false);
        dineroCobrador.setBackground(new java.awt.Color(234, 237, 255));

        jLabel120.setText("$");

        impTotal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                impTotalKeyReleased(evt);
            }
        });

        lbImpTotalPesos.setText("$");

        lbImpTotal.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        lbImpTotal.setText("Importe total:");

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel61.setText("Interés mensual:");

        interesMensual.setBackground(new java.awt.Color(255, 255, 121));
        interesMensual.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        interesMensual.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                interesMensualKeyReleased(evt);
            }
        });

        jLabel66.setText("%");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel61)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(interesMensual, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel66, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(interesMensual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel66)
                    .addComponent(jLabel61))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbFinan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbImpTotal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                        .addContainerGap(27, Short.MAX_VALUE)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel118)
                            .addComponent(jLabel35)
                            .addComponent(jLabel22)
                            .addComponent(jLabel25))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(dineroCobrador, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel120, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(nomPlan, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(importeDevolver, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(interesPorc, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(interesDinero, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(codPlan, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(porcCobrador, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel119, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(impTotal, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(importePrestar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 61, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbImpTotalPesos, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codPlan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nomPlan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(impTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbImpTotalPesos)
                    .addComponent(lbImpTotal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(importePrestar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26)
                    .addComponent(lbFinan))
                .addGap(6, 6, 6)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(importeDevolver, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27)
                    .addComponent(jLabel24))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(interesPorc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28)
                    .addComponent(jLabel25))
                .addGap(6, 6, 6)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(interesDinero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(porcCobrador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel119))
                    .addComponent(jLabel118))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dineroCobrador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel120))
                .addGap(18, 18, 18)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        botonEliminarPlan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_close.png"))); // NOI18N
        botonEliminarPlan.setText("Eliminar");
        botonEliminarPlan.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonEliminarPlan.setIconTextGap(0);
        botonEliminarPlan.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonEliminarPlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarPlanActionPerformed(evt);
            }
        });

        botonGuardarPlan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_guardar.png"))); // NOI18N
        botonGuardarPlan.setText("Guardar");
        botonGuardarPlan.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonGuardarPlan.setIconTextGap(0);
        botonGuardarPlan.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonGuardarPlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarPlanActionPerformed(evt);
            }
        });

        botonBlanquearPlan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_limpiar.png"))); // NOI18N
        botonBlanquearPlan.setText("Limpiar");
        botonBlanquearPlan.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonBlanquearPlan.setIconTextGap(0);
        botonBlanquearPlan.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonBlanquearPlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBlanquearPlanActionPerformed(evt);
            }
        });

        jPanel14.setBackground(new java.awt.Color(255, 255, 204));

        comboCuotas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCuotasActionPerformed(evt);
            }
        });

        jLabel39.setText("Cuotas:");

        jLabel40.setText("Valor:");

        valorCuota.setEditable(false);
        valorCuota.setPreferredSize(new java.awt.Dimension(56, 20));

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel40))
                    .addComponent(jLabel39))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(valorCuota, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(comboCuotas, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(15, 15, 15))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel39)
                    .addComponent(comboCuotas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel40)
                    .addComponent(valorCuota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23))
        );

        botonActualizarPlan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_actualizar.png"))); // NOI18N
        botonActualizarPlan.setText("Actualizar");
        botonActualizarPlan.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonActualizarPlan.setIconTextGap(0);
        botonActualizarPlan.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonActualizarPlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonActualizarPlanActionPerformed(evt);
            }
        });

        jPanel37.setBackground(new java.awt.Color(247, 255, 247));

        jLabel124.setText("Calcular interés por:");

        porc.setBackground(new java.awt.Color(247, 255, 247));
        porc.setText("Porcentaje");
        porc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                porcActionPerformed(evt);
            }
        });

        imp.setBackground(new java.awt.Color(247, 255, 247));
        imp.setText("Importe");
        imp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                impActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel37Layout = new javax.swing.GroupLayout(jPanel37);
        jPanel37.setLayout(jPanel37Layout);
        jPanel37Layout.setHorizontalGroup(
            jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel37Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel37Layout.createSequentialGroup()
                        .addComponent(porc)
                        .addGap(18, 18, 18)
                        .addComponent(imp))
                    .addComponent(jLabel124))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel37Layout.setVerticalGroup(
            jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel37Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel124)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(porc)
                    .addComponent(imp))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/btn_calcu_32.png"))); // NOI18N
        jButton2.setText("Calculadora");
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(botonBlanquearPlan, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonGuardarPlan, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonActualizarPlan, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonEliminarPlan, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(93, 93, 93))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator4)
                            .addComponent(jSeparator5)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(jLabel32)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel31))
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jPanel37, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                                                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel32)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                        .addComponent(jPanel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(botonEliminarPlan, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(botonGuardarPlan, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(botonBlanquearPlan, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(botonActualizarPlan, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        jPanel43.setBackground(new java.awt.Color(255, 255, 255));
        jPanel43.setBorder(javax.swing.BorderFactory.createTitledBorder("Info"));

        jLabel21.setText("Planes normales:");

        jLabel93.setText("Planes personalizados:");

        javax.swing.GroupLayout jPanel43Layout = new javax.swing.GroupLayout(jPanel43);
        jPanel43.setLayout(jPanel43Layout);
        jPanel43Layout.setHorizontalGroup(
            jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel43Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel21)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cantPlanes, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(jLabel93)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cantPlanesPers, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel43Layout.setVerticalGroup(
            jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel43Layout.createSequentialGroup()
                .addGroup(jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel93)
                        .addComponent(cantPlanesPers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel21)
                        .addComponent(cantPlanes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(29, 29, 29))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel43, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(274, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel43, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout PanelPlanesLayout = new javax.swing.GroupLayout(PanelPlanes);
        PanelPlanes.setLayout(PanelPlanesLayout);
        PanelPlanesLayout.setHorizontalGroup(
            PanelPlanesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelPlanesLayout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PanelPlanesLayout.setVerticalGroup(
            PanelPlanesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        PanelPrincipal.addTab("Planes", PanelPlanes);

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));

        jLabel41.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_prestamos.png"))); // NOI18N

        jLabel42.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel42.setText("Préstamos");

        jLabel43.setText("Cliente:");

        comboClientes.setEditable(true);
        comboClientes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Sin especificar" }));
        comboClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboClientesActionPerformed(evt);
            }
        });

        listaPrestamos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listaPrestamosMouseClicked(evt);
            }
        });
        listaPrestamos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listaPrestamosActionPerformed(evt);
            }
        });

        jLabel44.setText("Préstamos");

        jPanel15.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel46.setText("Total Prestado:");

        lbTotalPrestado.setText("$0");

        jLabel45.setText("Prestamos del cliente:");

        lbCantPrestamos.setText("0");

        jLabel47.setText("Total Adeudado:");

        lbTotalAdeudado.setText("$0");

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addComponent(jLabel45)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbCantPrestamos, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addComponent(jLabel46)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbTotalPrestado, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addComponent(jLabel47)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbTotalAdeudado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel45)
                    .addComponent(lbCantPrestamos))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel46)
                    .addComponent(lbTotalPrestado))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel47)
                    .addComponent(lbTotalAdeudado))
                .addContainerGap())
        );

        botonImprimirPrestamo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_print.png"))); // NOI18N
        botonImprimirPrestamo.setText("Imprimir");
        botonImprimirPrestamo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonImprimirPrestamo.setIconTextGap(0);
        botonImprimirPrestamo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonImprimirPrestamo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonImprimirPrestamoActionPerformed(evt);
            }
        });

        botonEliminarPrestamo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_close.png"))); // NOI18N
        botonEliminarPrestamo.setText("Eliminar");
        botonEliminarPrestamo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonEliminarPrestamo.setIconTextGap(0);
        botonEliminarPrestamo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonEliminarPrestamo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarPrestamoActionPerformed(evt);
            }
        });

        btnRefinanciar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_finan.png"))); // NOI18N
        btnRefinanciar.setText("Refinanciar");
        btnRefinanciar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRefinanciar.setIconTextGap(0);
        btnRefinanciar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRefinanciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefinanciarActionPerformed(evt);
            }
        });

        actListado3.setText("Actualizar Listado");
        actListado3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actListado3ActionPerformed(evt);
            }
        });

        botonImprimirComprobante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_print.png"))); // NOI18N
        botonImprimirComprobante.setText("Comprobante");
        botonImprimirComprobante.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonImprimirComprobante.setIconTextGap(0);
        botonImprimirComprobante.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonImprimirComprobante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonImprimirComprobanteActionPerformed(evt);
            }
        });

        btnCarton.setText("Generar Cartón");
        btnCarton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCartonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator6)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel41)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel42))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel16Layout.createSequentialGroup()
                                .addComponent(jLabel44)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(actListado3, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(listaPrestamos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(comboClientes, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel16Layout.createSequentialGroup()
                                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel16Layout.createSequentialGroup()
                                        .addComponent(botonImprimirPrestamo, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(botonEliminarPrestamo, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel43)
                                    .addGroup(jPanel16Layout.createSequentialGroup()
                                        .addComponent(btnRefinanciar, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(botonImprimirComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(btnCarton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel41)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel43)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel44)
                            .addComponent(actListado3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(listaPrestamos, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(botonImprimirPrestamo)
                            .addComponent(botonEliminarPrestamo))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRefinanciar))
                    .addComponent(botonImprimirComprobante))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCarton)
                .addContainerGap(51, Short.MAX_VALUE))
        );

        jPanel20.setBackground(new java.awt.Color(204, 204, 255));
        jPanel20.setPreferredSize(new java.awt.Dimension(890, 492));

        jPanel28.setBackground(new java.awt.Color(255, 255, 255));

        jLabel62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_venc.png"))); // NOI18N

        jLabel63.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel63.setText("Cuotas");

        tablaCuotasPrestamo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaCuotasPrestamo.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tablaCuotasPrestamo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaCuotasPrestamoMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tablaCuotasPrestamo);

        jLabel94.setText("Seleccione una cuota para ver fecha de pago.");

        lbPagado.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        botonReciboA4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_print.png"))); // NOI18N
        botonReciboA4.setText("Recibo A4");
        botonReciboA4.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        botonReciboA4.setIconTextGap(0);
        botonReciboA4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonReciboA4ActionPerformed(evt);
            }
        });

        botonAnular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_close.png"))); // NOI18N
        botonAnular.setText("Anular Pago");
        botonAnular.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        botonAnular.setIconTextGap(0);
        botonAnular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAnularActionPerformed(evt);
            }
        });

        ocultarPagasPrestamo.setBackground(new java.awt.Color(255, 255, 255));
        ocultarPagasPrestamo.setText("Ocultar cuotas pagas");
        ocultarPagasPrestamo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ocultarPagasPrestamoActionPerformed(evt);
            }
        });

        botonCambiarVenc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_camven.png"))); // NOI18N
        botonCambiarVenc.setText("Cambiar Venc.");
        botonCambiarVenc.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        botonCambiarVenc.setIconTextGap(0);
        botonCambiarVenc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCambiarVencActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel28Layout = new javax.swing.GroupLayout(jPanel28);
        jPanel28.setLayout(jPanel28Layout);
        jPanel28Layout.setHorizontalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel28Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 841, Short.MAX_VALUE)
                    .addComponent(jSeparator11)
                    .addGroup(jPanel28Layout.createSequentialGroup()
                        .addComponent(jLabel62)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel63)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(ocultarPagasPrestamo)
                        .addGap(18, 18, 18)
                        .addComponent(botonReciboA4, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonCambiarVenc, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonAnular, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel28Layout.createSequentialGroup()
                        .addComponent(jLabel94)
                        .addGap(18, 18, 18)
                        .addComponent(lbPagado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel28Layout.setVerticalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel28Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel62)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel28Layout.createSequentialGroup()
                            .addGap(8, 8, 8)
                            .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel63, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(ocultarPagasPrestamo)
                                .addComponent(botonReciboA4))))
                    .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(botonAnular)
                        .addComponent(botonCambiarVenc)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator11, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lbPagado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel94, javax.swing.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel31.setBackground(new java.awt.Color(255, 255, 255));
        jPanel31.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel91.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_datospl.png"))); // NOI18N

        jLabel92.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel92.setText("Observaciones");

        obsPrestamo.setColumns(20);
        obsPrestamo.setRows(5);
        jScrollPane8.setViewportView(obsPrestamo);

        guardarObsBtn.setText("Guardar");
        guardarObsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guardarObsBtnActionPerformed(evt);
            }
        });

        jLabel17.setText("360 caract.");

        javax.swing.GroupLayout jPanel31Layout = new javax.swing.GroupLayout(jPanel31);
        jPanel31.setLayout(jPanel31Layout);
        jPanel31Layout.setHorizontalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel31Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator19)
                    .addGroup(jPanel31Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(guardarObsBtn))
                    .addGroup(jPanel31Layout.createSequentialGroup()
                        .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel31Layout.createSequentialGroup()
                                .addComponent(jLabel91)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel92))
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel31Layout.setVerticalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel31Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel91)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel31Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel92, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator19, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(guardarObsBtn))
                .addContainerGap())
        );

        jPanel17.setBackground(new java.awt.Color(255, 255, 255));
        jPanel17.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        planPrest.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        planPrest.setText("NINGÚN PRESTAMO SELECCIONADO");

        codPrest.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        codPrest.setText("0");

        codPrest1.setText("Código:");

        codPrest2.setText("Cuotas:");

        cuotasPrest.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cuotasPrest.setText("0");

        codPrest4.setText("Valor Cuota:");

        valorCuotaPrest.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        valorCuotaPrest.setText("$0");

        codPrest6.setText("Importe total:");

        totalPrest.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        totalPrest.setText("$0");

        codPrest8.setText("Importe pagado:");

        pagadoPrest.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        pagadoPrest.setText("$0");

        codPrest10.setText("Importe adeudado:");

        adeudadoPrest.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        adeudadoPrest.setText("$0");

        codPrest3.setText("Fecha:");

        codPrest5.setText("Importe prestado:");

        fechaPrest.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        fechaPrest.setText("01/01/2099");

        valorImportePrest.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        valorImportePrest.setText("$0");

        codPrest11.setText("Importe desc.:");

        descontadoPrest.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        descontadoPrest.setForeground(new java.awt.Color(255, 0, 51));
        descontadoPrest.setText("$0");

        codPrest12.setText("Importe en sobre:");

        sobrePrest.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        sobrePrest.setForeground(new java.awt.Color(0, 153, 0));
        sobrePrest.setText("$0");

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(planPrest, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addComponent(codPrest2, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cuotasPrest, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addComponent(codPrest10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(adeudadoPrest, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel17Layout.createSequentialGroup()
                                .addComponent(codPrest1, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(codPrest, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel17Layout.createSequentialGroup()
                                .addComponent(codPrest4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(valorCuotaPrest, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel17Layout.createSequentialGroup()
                                    .addComponent(codPrest6)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(totalPrest, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel17Layout.createSequentialGroup()
                                    .addComponent(codPrest8)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(pagadoPrest, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel17Layout.createSequentialGroup()
                                .addComponent(codPrest5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(valorImportePrest, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel17Layout.createSequentialGroup()
                                .addComponent(codPrest3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fechaPrest, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel17Layout.createSequentialGroup()
                                .addComponent(codPrest11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(descontadoPrest, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(0, 25, Short.MAX_VALUE))
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addComponent(codPrest12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sobrePrest, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(planPrest)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codPrest1)
                    .addComponent(codPrest))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codPrest3)
                    .addComponent(fechaPrest))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codPrest5)
                    .addComponent(valorImportePrest))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codPrest2)
                    .addComponent(cuotasPrest))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codPrest4)
                    .addComponent(valorCuotaPrest))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codPrest6)
                    .addComponent(totalPrest))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codPrest8)
                    .addComponent(pagadoPrest))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codPrest10)
                    .addComponent(adeudadoPrest))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codPrest11)
                    .addComponent(descontadoPrest))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codPrest12)
                    .addComponent(sobrePrest))
                .addContainerGap(51, Short.MAX_VALUE))
        );

        jPanel36.setBackground(new java.awt.Color(255, 255, 255));
        jPanel36.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        planPrest1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        planPrest1.setText("COBRADOR ASIGNADO");

        cobAsign.setText("Sin especificar");

        modifCob.setText("Modificar");
        modifCob.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifCobActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel36Layout = new javax.swing.GroupLayout(jPanel36);
        jPanel36.setLayout(jPanel36Layout);
        jPanel36Layout.setHorizontalGroup(
            jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel36Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(planPrest1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cobAsign, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel36Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(modifCob, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel36Layout.setVerticalGroup(
            jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel36Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(planPrest1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cobAsign)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(modifCob)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel42.setBackground(new java.awt.Color(255, 255, 255));
        jPanel42.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        planPrest2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        planPrest2.setText("COBRAR MÁS DE UNA CUOTA");

        btnCobrarPrestamo.setText("Cobrar");
        btnCobrarPrestamo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCobrarPrestamoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel42Layout = new javax.swing.GroupLayout(jPanel42);
        jPanel42.setLayout(jPanel42Layout);
        jPanel42Layout.setHorizontalGroup(
            jPanel42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel42Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(planPrest2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCobrarPrestamo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel42Layout.setVerticalGroup(
            jPanel42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel42Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(planPrest2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCobrarPrestamo, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel20Layout.createSequentialGroup()
                        .addComponent(jPanel31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel36, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel42, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(31, Short.MAX_VALUE))
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel20Layout.createSequentialGroup()
                        .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel42, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel20Layout.createSequentialGroup()
                                .addGap(118, 118, 118)
                                .addComponent(jPanel36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel17, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel31, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout PanelPrestamosLayout = new javax.swing.GroupLayout(PanelPrestamos);
        PanelPrestamos.setLayout(PanelPrestamosLayout);
        PanelPrestamosLayout.setHorizontalGroup(
            PanelPrestamosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelPrestamosLayout.createSequentialGroup()
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        PanelPrestamosLayout.setVerticalGroup(
            PanelPrestamosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel20, javax.swing.GroupLayout.DEFAULT_SIZE, 607, Short.MAX_VALUE)
        );

        PanelPrincipal.addTab("Préstamos", PanelPrestamos);

        PanelCobradores.setBackground(new java.awt.Color(204, 204, 255));
        PanelCobradores.setPreferredSize(new java.awt.Dimension(1220, 604));

        panelIzqClientes1.setBackground(new java.awt.Color(255, 255, 255));

        codCob.setEditable(false);

        jLabel100.setText("Nombre:");

        jLabel101.setText("Teléfono:");

        jLabel105.setText("Cód:");

        jLabel108.setText("Ruta:");

        jLabel110.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_prestamos.png"))); // NOI18N

        jLabel111.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel111.setText("Datos del cobrador");

        jPanel33.setBackground(new java.awt.Color(255, 255, 255));

        botonBlanquearCob.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_limpiar.png"))); // NOI18N
        botonBlanquearCob.setText("Limpiar");
        botonBlanquearCob.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonBlanquearCob.setIconTextGap(0);
        botonBlanquearCob.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonBlanquearCob.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBlanquearCobActionPerformed(evt);
            }
        });

        botonActualizarCob.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_actualizar.png"))); // NOI18N
        botonActualizarCob.setText("Actualizar");
        botonActualizarCob.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonActualizarCob.setIconTextGap(0);
        botonActualizarCob.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonActualizarCob.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonActualizarCobActionPerformed(evt);
            }
        });

        botonGuardarCob.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_guardar.png"))); // NOI18N
        botonGuardarCob.setText("Guardar");
        botonGuardarCob.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonGuardarCob.setIconTextGap(0);
        botonGuardarCob.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonGuardarCob.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarCobActionPerformed(evt);
            }
        });

        botonEliminarCob.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_close.png"))); // NOI18N
        botonEliminarCob.setText("Eliminar");
        botonEliminarCob.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonEliminarCob.setIconTextGap(0);
        botonEliminarCob.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonEliminarCob.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarCobActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel33Layout = new javax.swing.GroupLayout(jPanel33);
        jPanel33.setLayout(jPanel33Layout);
        jPanel33Layout.setHorizontalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botonActualizarCob, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel33Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(botonBlanquearCob, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(botonGuardarCob, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonEliminarCob, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel33Layout.setVerticalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addComponent(botonBlanquearCob)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonActualizarCob)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonGuardarCob)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonEliminarCob)
                .addGap(96, 96, 96))
        );

        obsCob.setColumns(20);
        obsCob.setRows(5);
        obsCob.setPreferredSize(new java.awt.Dimension(230, 94));
        jScrollPane9.setViewportView(obsCob);

        jLabel112.setText("Observaciones");

        jPanel27.setBackground(new java.awt.Color(255, 255, 255));
        jPanel27.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnVertodosCob.setText("Ver todos los préstamos");
        btnVertodosCob.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVertodosCobActionPerformed(evt);
            }
        });

        muestraFin.setBackground(new java.awt.Color(255, 255, 255));
        muestraFin.setText("Mostrar finalizados");

        globales.setBackground(new java.awt.Color(255, 255, 255));
        globales.setText("Mostrar préstamos globales");

        javax.swing.GroupLayout jPanel27Layout = new javax.swing.GroupLayout(jPanel27);
        jPanel27.setLayout(jPanel27Layout);
        jPanel27Layout.setHorizontalGroup(
            jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel27Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnVertodosCob, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(muestraFin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel27Layout.createSequentialGroup()
                        .addComponent(globales, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 27, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel27Layout.setVerticalGroup(
            jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel27Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnVertodosCob, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(globales)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(muestraFin))
        );

        jLabel132.setText("Nombre App:");

        javax.swing.GroupLayout panelIzqClientes1Layout = new javax.swing.GroupLayout(panelIzqClientes1);
        panelIzqClientes1.setLayout(panelIzqClientes1Layout);
        panelIzqClientes1Layout.setHorizontalGroup(
            panelIzqClientes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelIzqClientes1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelIzqClientes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator28, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIzqClientes1Layout.createSequentialGroup()
                        .addGroup(panelIzqClientes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane9, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelIzqClientes1Layout.createSequentialGroup()
                                .addGroup(panelIzqClientes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelIzqClientes1Layout.createSequentialGroup()
                                        .addGroup(panelIzqClientes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(panelIzqClientes1Layout.createSequentialGroup()
                                                .addGap(41, 41, 41)
                                                .addComponent(jLabel105)
                                                .addGap(8, 8, 8))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIzqClientes1Layout.createSequentialGroup()
                                                .addGroup(panelIzqClientes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel108, javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addComponent(jLabel100, javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addComponent(jLabel101, javax.swing.GroupLayout.Alignment.TRAILING))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                        .addGroup(panelIzqClientes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(panelIzqClientes1Layout.createSequentialGroup()
                                                .addComponent(codCob, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 140, Short.MAX_VALUE))
                                            .addComponent(telCob)
                                            .addComponent(codIntCob)
                                            .addComponent(nombreCob)))
                                    .addComponent(jPanel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(panelIzqClientes1Layout.createSequentialGroup()
                                        .addComponent(jLabel112, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(panelIzqClientes1Layout.createSequentialGroup()
                                        .addGap(16, 16, 16)
                                        .addComponent(jLabel132)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(nombreApp)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPanel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelIzqClientes1Layout.createSequentialGroup()
                                .addComponent(jLabel110)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel111)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(8, 8, 8))
                    .addComponent(jSeparator29))
                .addContainerGap())
        );
        panelIzqClientes1Layout.setVerticalGroup(
            panelIzqClientes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIzqClientes1Layout.createSequentialGroup()
                .addGroup(panelIzqClientes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelIzqClientes1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel110))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelIzqClientes1Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jLabel111, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator28, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIzqClientes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelIzqClientes1Layout.createSequentialGroup()
                        .addGroup(panelIzqClientes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(codCob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel105))
                        .addGap(14, 14, 14)
                        .addGroup(panelIzqClientes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nombreCob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel100))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelIzqClientes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(codIntCob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel108))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelIzqClientes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(telCob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel101))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelIzqClientes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nombreApp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel132))
                        .addGap(28, 28, 28)
                        .addComponent(jPanel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(49, 49, 49)
                        .addComponent(jLabel112))
                    .addComponent(jPanel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator29, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel34.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        nombreBusqueda1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nombreBusqueda1ActionPerformed(evt);
            }
        });
        nombreBusqueda1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                nombreBusqueda1KeyReleased(evt);
            }
        });

        jLabel113.setText("Buscar por nombre:");

        jLabel114.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel114.setText("Búsqueda");

        actListado4.setText("Actualizar Listado");
        actListado4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actListado4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel34Layout = new javax.swing.GroupLayout(jPanel34);
        jPanel34.setLayout(jPanel34Layout);
        jPanel34Layout.setHorizontalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addComponent(jLabel114)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(actListado4, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel113)
                            .addComponent(nombreBusqueda1, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel34Layout.setVerticalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel114, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(actListado4))
                .addGap(18, 18, 18)
                .addComponent(jLabel113)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(nombreBusqueda1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        tablaCobradores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaCobradores.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaCobradoresMouseClicked(evt);
            }
        });
        jScrollPane10.setViewportView(tablaCobradores);

        javax.swing.GroupLayout jPanel35Layout = new javax.swing.GroupLayout(jPanel35);
        jPanel35.setLayout(jPanel35Layout);
        jPanel35Layout.setHorizontalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 758, Short.MAX_VALUE)
        );
        jPanel35Layout.setVerticalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout PanelCobradoresLayout = new javax.swing.GroupLayout(PanelCobradores);
        PanelCobradores.setLayout(PanelCobradoresLayout);
        PanelCobradoresLayout.setHorizontalGroup(
            PanelCobradoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelCobradoresLayout.createSequentialGroup()
                .addComponent(panelIzqClientes1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelCobradoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel35, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(44, Short.MAX_VALUE))
        );
        PanelCobradoresLayout.setVerticalGroup(
            PanelCobradoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelCobradoresLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(panelIzqClientes1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        PanelPrincipal.addTab("Cobradores", PanelCobradores);

        panelOpt1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel64.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_opt.png"))); // NOI18N

        jLabel65.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel65.setText("Opciones");

        botonActivar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_keys.png"))); // NOI18N
        botonActivar.setText("Activar");
        botonActivar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonActivar.setIconTextGap(0);
        botonActivar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonActivar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonActivarActionPerformed(evt);
            }
        });

        jPanel40.setBackground(new java.awt.Color(255, 255, 255));
        jPanel40.setBorder(javax.swing.BorderFactory.createTitledBorder("Recibos"));

        jButton7.setText("Observación general en Recibo A4");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        recibosA4.setBackground(new java.awt.Color(255, 255, 255));
        recibosA4.setText("Generar Recibos A4 XLS");
        recibosA4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recibosA4ActionPerformed(evt);
            }
        });

        jLabel23.setText("Leyenda en comprobantes:");

        jButton8.setText("Guardar");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jLabel34.setText("Tipo de recibo para pago de cuota:");

        ticket80mm.setBackground(new java.awt.Color(255, 255, 255));
        ticket80mm.setText("Ticket 80mm");
        ticket80mm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ticket80mmActionPerformed(evt);
            }
        });

        hojaa4.setBackground(new java.awt.Color(255, 255, 255));
        hojaa4.setText("Hoja A4");
        hojaa4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hojaa4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel40Layout = new javax.swing.GroupLayout(jPanel40);
        jPanel40.setLayout(jPanel40Layout);
        jPanel40Layout.setHorizontalGroup(
            jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel40Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(recibosA4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator34)
                    .addComponent(leyendaComprobante)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel40Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton8))
                    .addGroup(jPanel40Layout.createSequentialGroup()
                        .addGroup(jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel23)
                            .addGroup(jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(jPanel40Layout.createSequentialGroup()
                                    .addComponent(ticket80mm)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(hojaa4))
                                .addComponent(jSeparator33)
                                .addComponent(jButton7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(0, 12, Short.MAX_VALUE))
                    .addComponent(jSeparator37))
                .addContainerGap())
        );
        jPanel40Layout.setVerticalGroup(
            jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel40Layout.createSequentialGroup()
                .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator37, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(jLabel34)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ticket80mm)
                    .addComponent(hojaa4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator33, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(recibosA4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator34, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel23)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(leyendaComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton8)
                .addGap(0, 13, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelOpt1Layout = new javax.swing.GroupLayout(panelOpt1);
        panelOpt1.setLayout(panelOpt1Layout);
        panelOpt1Layout.setHorizontalGroup(
            panelOpt1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelOpt1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelOpt1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelOpt1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(botonActivar, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelOpt1Layout.createSequentialGroup()
                        .addGroup(panelOpt1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel40, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jSeparator12)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelOpt1Layout.createSequentialGroup()
                                .addComponent(jLabel64)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel65)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(15, 15, 15))
        );
        panelOpt1Layout.setVerticalGroup(
            panelOpt1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOpt1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelOpt1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel64)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelOpt1Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel65, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator12, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botonActivar, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        panelOpt2.setBackground(new java.awt.Color(204, 204, 255));

        panelAvisoOPC.setBackground(new java.awt.Color(255, 255, 255));

        jLabel76.setFont(new java.awt.Font("Tahoma", 1, 32)); // NOI18N
        jLabel76.setForeground(new java.awt.Color(255, 0, 0));
        jLabel76.setText("Su usuario no le permite ingresar a esta sección");

        jLabel77.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/candado.png"))); // NOI18N

        javax.swing.GroupLayout panelAvisoOPCLayout = new javax.swing.GroupLayout(panelAvisoOPC);
        panelAvisoOPC.setLayout(panelAvisoOPCLayout);
        panelAvisoOPCLayout.setHorizontalGroup(
            panelAvisoOPCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAvisoOPCLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel77)
                .addGap(18, 18, 18)
                .addComponent(jLabel76)
                .addContainerGap(23, Short.MAX_VALUE))
        );
        panelAvisoOPCLayout.setVerticalGroup(
            panelAvisoOPCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAvisoOPCLayout.createSequentialGroup()
                .addGroup(panelAvisoOPCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAvisoOPCLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel77))
                    .addGroup(panelAvisoOPCLayout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(jLabel76)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelDB1.setBackground(new java.awt.Color(255, 255, 255));
        panelDB1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel67.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel67.setText("Usuarios");

        jLabel68.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_users.png"))); // NOI18N

        jLabel33.setText("Nombre");

        jLabel71.setText("Contraseña");

        jLabel72.setText("Tipo");

        listaUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane6.setViewportView(listaUsuarios);

        comboTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Limitado", "Administrador" }));

        botonAgregarUsuario.setText("Agregar usuario");
        botonAgregarUsuario.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonAgregarUsuario.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonAgregarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarUsuarioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelDB1Layout = new javax.swing.GroupLayout(panelDB1);
        panelDB1.setLayout(panelDB1Layout);
        panelDB1Layout.setHorizontalGroup(
            panelDB1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDB1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDB1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDB1Layout.createSequentialGroup()
                        .addComponent(jLabel68)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel67)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jSeparator15)
                    .addComponent(jSeparator16)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDB1Layout.createSequentialGroup()
                .addContainerGap(63, Short.MAX_VALUE)
                .addGroup(panelDB1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel71)
                    .addComponent(jLabel72)
                    .addComponent(jLabel33))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelDB1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(comboTipo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(password)
                    .addComponent(nombreUsrNew, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(botonAgregarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
        );
        panelDB1Layout.setVerticalGroup(
            panelDB1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDB1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDB1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel68)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDB1Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel67, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addComponent(jSeparator15, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDB1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDB1Layout.createSequentialGroup()
                        .addGroup(panelDB1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nombreUsrNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel33))
                        .addGap(14, 14, 14)
                        .addGroup(panelDB1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel71))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelDB1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(comboTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel72)))
                    .addComponent(botonAgregarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jSeparator16, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        panelDB2.setBackground(new java.awt.Color(255, 255, 255));
        panelDB2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel69.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel69.setText("Datos de la Empresa");

        jLabel70.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_settings.png"))); // NOI18N

        jLabel73.setText("Empresa:");

        jLabel74.setText("Dirección:");

        jLabel75.setText("Teléfono:");

        btnGuardarDatos.setText("Guardar");
        btnGuardarDatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarDatosActionPerformed(evt);
            }
        });

        jButton4.setText("Cargar Logo");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton6.setText("Borrar Logo");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelDB2Layout = new javax.swing.GroupLayout(panelDB2);
        panelDB2.setLayout(panelDB2Layout);
        panelDB2Layout.setHorizontalGroup(
            panelDB2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDB2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDB2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDB2Layout.createSequentialGroup()
                        .addComponent(jLabel70)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel69)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jSeparator17)
                    .addComponent(jSeparator18)
                    .addGroup(panelDB2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(panelDB2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel73, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel75, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel74, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelDB2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dirEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nombreEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(telEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelDB2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                            .addComponent(btnGuardarDatos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        panelDB2Layout.setVerticalGroup(
            panelDB2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDB2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDB2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel70)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDB2Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel69, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addComponent(jSeparator17, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelDB2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombreEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel73)
                    .addComponent(jButton4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDB2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(telEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel75)
                    .addComponent(jButton6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelDB2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dirEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel74)
                    .addComponent(btnGuardarDatos))
                .addGap(18, 18, 18)
                .addComponent(jSeparator18, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel32.setBackground(new java.awt.Color(255, 255, 255));
        jPanel32.setBorder(javax.swing.BorderFactory.createTitledBorder("Generales"));

        ocultarFinalizados.setBackground(new java.awt.Color(255, 255, 255));
        ocultarFinalizados.setText("Ocultar préstamos finalizados");
        ocultarFinalizados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ocultarFinalizadosActionPerformed(evt);
            }
        });

        marcarFinalizados.setBackground(new java.awt.Color(255, 255, 255));
        marcarFinalizados.setText("Marcar préstamos finalizados");
        marcarFinalizados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                marcarFinalizadosActionPerformed(evt);
            }
        });

        jLabel60.setText("al completar las cuotas.");

        habImpresion.setBackground(new java.awt.Color(255, 255, 255));
        habImpresion.setText("Habilitar impresión");
        habImpresion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                habImpresionActionPerformed(evt);
            }
        });

        ocultaDir.setBackground(new java.awt.Color(255, 255, 255));
        ocultaDir.setText("Ocultar direcciones en informes");
        ocultaDir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ocultaDirActionPerformed(evt);
            }
        });

        ocultaTel.setBackground(new java.awt.Color(255, 255, 255));
        ocultaTel.setText("Ocultar teléfonos en informes");
        ocultaTel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ocultaTelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel32Layout = new javax.swing.GroupLayout(jPanel32);
        jPanel32.setLayout(jPanel32Layout);
        jPanel32Layout.setHorizontalGroup(
            jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel32Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator10)
                    .addComponent(jSeparator30)
                    .addComponent(ocultaDir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel32Layout.createSequentialGroup()
                        .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(habImpresion)
                            .addGroup(jPanel32Layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addComponent(jLabel60))
                            .addComponent(marcarFinalizados)
                            .addComponent(ocultarFinalizados))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(ocultaTel, javax.swing.GroupLayout.DEFAULT_SIZE, 389, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel32Layout.setVerticalGroup(
            jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel32Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(marcarFinalizados)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel60)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ocultarFinalizados)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(habImpresion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator30, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ocultaDir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ocultaTel)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelOpt2Layout = new javax.swing.GroupLayout(panelOpt2);
        panelOpt2.setLayout(panelOpt2Layout);
        panelOpt2Layout.setHorizontalGroup(
            panelOpt2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOpt2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelOpt2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panelDB2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelDB1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelOpt2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelOpt2Layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelAvisoOPC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );
        panelOpt2Layout.setVerticalGroup(
            panelOpt2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOpt2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelOpt2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelOpt2Layout.createSequentialGroup()
                        .addComponent(jPanel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(117, 117, 117)
                        .addComponent(panelDB2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(panelDB1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelOpt2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelOpt2Layout.createSequentialGroup()
                    .addGap(233, 233, 233)
                    .addComponent(panelAvisoOPC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(233, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout PanelOpcionesLayout = new javax.swing.GroupLayout(PanelOpciones);
        PanelOpciones.setLayout(PanelOpcionesLayout);
        PanelOpcionesLayout.setHorizontalGroup(
            PanelOpcionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelOpcionesLayout.createSequentialGroup()
                .addComponent(panelOpt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panelOpt2, javax.swing.GroupLayout.PREFERRED_SIZE, 913, Short.MAX_VALUE))
        );
        PanelOpcionesLayout.setVerticalGroup(
            PanelOpcionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelOpt1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelOpt2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        PanelPrincipal.addTab("Opciones", PanelOpciones);

        panelOpt4.setBackground(new java.awt.Color(204, 204, 255));

        panelDB6.setBackground(new java.awt.Color(255, 255, 255));
        panelDB6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel115.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel115.setText("Base de datos");

        jLabel116.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_db.png"))); // NOI18N

        jPanel38.setBackground(new java.awt.Color(255, 255, 255));
        jPanel38.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        planPrest3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        planPrest3.setText("IMPORTAR PLANES");

        modifCob2.setText("Importar XLS");
        modifCob2.setVerifyInputWhenFocusTarget(false);
        modifCob2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifCob2ActionPerformed(evt);
            }
        });

        jLabel102.setText("Solo válido con archivos guardados como:");

        jLabel103.setText("Excel 97-2003 (.xls)");

        javax.swing.GroupLayout jPanel38Layout = new javax.swing.GroupLayout(jPanel38);
        jPanel38.setLayout(jPanel38Layout);
        jPanel38Layout.setHorizontalGroup(
            jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel38Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(planPrest3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel38Layout.createSequentialGroup()
                        .addGroup(jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel102, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel103)
                            .addComponent(modifCob2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel38Layout.setVerticalGroup(
            jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel38Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(planPrest3, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(modifCob2, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel102)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel103)
                .addContainerGap())
        );

        jPanel39.setBackground(new java.awt.Color(255, 255, 255));
        jPanel39.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        planPrest4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        planPrest4.setText("IMPORTAR CLIENTES, LOCALIDADES Y COBRADORES");

        modifCob3.setText("Importar XLS");
        modifCob3.setVerifyInputWhenFocusTarget(false);
        modifCob3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifCob3ActionPerformed(evt);
            }
        });

        jLabel109.setText("Solo válido con archivos guardados como:");

        jLabel117.setText("Excel 97-2003 (.xls)");

        javax.swing.GroupLayout jPanel39Layout = new javax.swing.GroupLayout(jPanel39);
        jPanel39.setLayout(jPanel39Layout);
        jPanel39Layout.setHorizontalGroup(
            jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel39Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(planPrest4, javax.swing.GroupLayout.DEFAULT_SIZE, 312, Short.MAX_VALUE)
                    .addGroup(jPanel39Layout.createSequentialGroup()
                        .addGroup(jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel109)
                            .addComponent(jLabel117))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(modifCob3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel39Layout.setVerticalGroup(
            jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel39Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(planPrest4, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(modifCob3, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel109)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel117)
                .addContainerGap())
        );

        jLabel121.setText("No realice modificaciones en la base de datos al momento de importar datos a la misma.");

        jLabel122.setText("Asegurese de que la terminal que va a realizar la importación es la única que está utilizando el programa al momento");

        jLabel123.setText("de importar.");

        javax.swing.GroupLayout panelDB6Layout = new javax.swing.GroupLayout(panelDB6);
        panelDB6.setLayout(panelDB6Layout);
        panelDB6Layout.setHorizontalGroup(
            panelDB6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDB6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35))
            .addGroup(panelDB6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDB6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator31)
                    .addComponent(jSeparator32)
                    .addGroup(panelDB6Layout.createSequentialGroup()
                        .addGroup(panelDB6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelDB6Layout.createSequentialGroup()
                                .addComponent(jLabel116)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel115))
                            .addComponent(jLabel121)
                            .addComponent(jLabel122)
                            .addComponent(jLabel123))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelDB6Layout.setVerticalGroup(
            panelDB6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDB6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDB6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel116)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDB6Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel115, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addComponent(jSeparator31, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(panelDB6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel121)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel122)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel123)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator32, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout panelOpt4Layout = new javax.swing.GroupLayout(panelOpt4);
        panelOpt4.setLayout(panelOpt4Layout);
        panelOpt4Layout.setHorizontalGroup(
            panelOpt4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOpt4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelDB6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(230, Short.MAX_VALUE))
        );
        panelOpt4Layout.setVerticalGroup(
            panelOpt4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOpt4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelDB6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelOpt3.setBackground(new java.awt.Color(255, 255, 255));

        jLabel106.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_opt.png"))); // NOI18N

        jLabel107.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel107.setText("Importar");

        javax.swing.GroupLayout panelOpt3Layout = new javax.swing.GroupLayout(panelOpt3);
        panelOpt3.setLayout(panelOpt3Layout);
        panelOpt3Layout.setHorizontalGroup(
            panelOpt3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOpt3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelOpt3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator25, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelOpt3Layout.createSequentialGroup()
                        .addComponent(jLabel106)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel107)))
                .addContainerGap(15, Short.MAX_VALUE))
        );
        panelOpt3Layout.setVerticalGroup(
            panelOpt3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOpt3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelOpt3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel106)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelOpt3Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel107, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator25, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(532, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout PanelImportarLayout = new javax.swing.GroupLayout(PanelImportar);
        PanelImportar.setLayout(PanelImportarLayout);
        PanelImportarLayout.setHorizontalGroup(
            PanelImportarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelImportarLayout.createSequentialGroup()
                .addComponent(panelOpt3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panelOpt4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PanelImportarLayout.setVerticalGroup(
            PanelImportarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelOpt3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelOpt4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        PanelPrincipal.addTab("Importar", PanelImportar);

        panelStats1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel79.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_estadisticas.png"))); // NOI18N

        jLabel80.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel80.setText("Estadísticas");

        javax.swing.GroupLayout panelStats1Layout = new javax.swing.GroupLayout(panelStats1);
        panelStats1.setLayout(panelStats1Layout);
        panelStats1Layout.setHorizontalGroup(
            panelStats1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStats1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelStats1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator20, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelStats1Layout.createSequentialGroup()
                        .addComponent(jLabel79)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel80)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelStats1Layout.setVerticalGroup(
            panelStats1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStats1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelStats1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel79)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelStats1Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel80, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator20, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(532, Short.MAX_VALUE))
        );

        panelStats2.setBackground(new java.awt.Color(204, 204, 255));

        panelDB3.setBackground(new java.awt.Color(255, 255, 255));
        panelDB3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel81.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel81.setText("Préstamos");

        jLabel82.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_prestamos.png"))); // NOI18N

        jLabel78.setText("Préstamos Totales:");

        jLabel85.setText("Cuotas Totales:");

        jLabel86.setText("Total Prestado:");

        jLabel87.setText("Total Devuelto:");

        jLabel88.setText("Total por Cobrar:");

        totalPrestadoLB.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        totalPrestadoLB.setText("$0");

        cuotasTotLB.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cuotasTotLB.setText("0");

        prestamosTotLB.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        prestamosTotLB.setText("0");

        totalCobrarLB.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        totalCobrarLB.setText("$0");

        totalDevueltoLB.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        totalDevueltoLB.setText("$0");

        jLabel89.setText("Total devuelto este mes:");

        totalDevueltoMesLB.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        totalDevueltoMesLB.setText("$0");

        javax.swing.GroupLayout panelDB3Layout = new javax.swing.GroupLayout(panelDB3);
        panelDB3.setLayout(panelDB3Layout);
        panelDB3Layout.setHorizontalGroup(
            panelDB3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDB3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDB3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator21)
                    .addComponent(jSeparator22)
                    .addGroup(panelDB3Layout.createSequentialGroup()
                        .addGroup(panelDB3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelDB3Layout.createSequentialGroup()
                                .addComponent(jLabel82)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel81))
                            .addGroup(panelDB3Layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addGroup(panelDB3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel86, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel89, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel85, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel78, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel88, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel87, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(panelDB3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cuotasTotLB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(prestamosTotLB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(totalPrestadoLB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(totalDevueltoLB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(totalCobrarLB, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                                    .addComponent(totalDevueltoMesLB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(0, 102, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelDB3Layout.setVerticalGroup(
            panelDB3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDB3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDB3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel82)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDB3Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel81, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addComponent(jSeparator21, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDB3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDB3Layout.createSequentialGroup()
                        .addComponent(jLabel78, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel85, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel86, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel87, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel88, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(panelDB3Layout.createSequentialGroup()
                        .addComponent(prestamosTotLB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cuotasTotLB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(totalPrestadoLB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(totalDevueltoLB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(totalCobrarLB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDB3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel89, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(totalDevueltoMesLB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator22, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        panelDB4.setBackground(new java.awt.Color(255, 255, 255));
        panelDB4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel83.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel83.setText("Clientes");

        jLabel84.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_users.png"))); // NOI18N

        jLabel90.setText("Clientes registrados");

        clientesRegLB.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        clientesRegLB.setText("0");

        javax.swing.GroupLayout panelDB4Layout = new javax.swing.GroupLayout(panelDB4);
        panelDB4.setLayout(panelDB4Layout);
        panelDB4Layout.setHorizontalGroup(
            panelDB4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDB4Layout.createSequentialGroup()
                .addGroup(panelDB4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDB4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelDB4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelDB4Layout.createSequentialGroup()
                                .addComponent(jLabel84)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel83)
                                .addGap(0, 255, Short.MAX_VALUE))
                            .addComponent(jSeparator23)
                            .addComponent(jSeparator24)))
                    .addGroup(panelDB4Layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addComponent(jLabel90)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(clientesRegLB, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelDB4Layout.setVerticalGroup(
            panelDB4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDB4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDB4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel84)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDB4Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel83, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addComponent(jSeparator23, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDB4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel90, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(clientesRegLB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(109, 109, 109)
                .addComponent(jSeparator24, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        panelDB5.setBackground(new java.awt.Color(255, 255, 255));
        panelDB5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel95.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel95.setText("Totales entre fechas");

        jLabel96.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_prestamos.png"))); // NOI18N

        inicioTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inicioTotalActionPerformed(evt);
            }
        });

        finTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finTotalActionPerformed(evt);
            }
        });

        jLabel97.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel97.setText("A");

        jLabel98.setText("Período:");

        jButton3.setText("Calcular");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelDB5Layout = new javax.swing.GroupLayout(panelDB5);
        panelDB5.setLayout(panelDB5Layout);
        panelDB5Layout.setHorizontalGroup(
            panelDB5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDB5Layout.createSequentialGroup()
                .addGroup(panelDB5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDB5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelDB5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelDB5Layout.createSequentialGroup()
                                .addComponent(jLabel96)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel95)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jSeparator26)
                            .addComponent(jSeparator27)))
                    .addGroup(panelDB5Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(panelDB5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel98)
                            .addGroup(panelDB5Layout.createSequentialGroup()
                                .addComponent(inicioTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel97, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(finTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 6, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelDB5Layout.setVerticalGroup(
            panelDB5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDB5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDB5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel95, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel96))
                .addGap(7, 7, 7)
                .addComponent(jSeparator26, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel98)
                .addGap(10, 10, 10)
                .addGroup(panelDB5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(inicioTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(finTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel97))
                .addGap(18, 18, 18)
                .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, 53, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jSeparator27, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout panelStats2Layout = new javax.swing.GroupLayout(panelStats2);
        panelStats2.setLayout(panelStats2Layout);
        panelStats2Layout.setHorizontalGroup(
            panelStats2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStats2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(panelStats2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelDB5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelStats2Layout.createSequentialGroup()
                        .addComponent(panelDB3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(panelDB4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(125, Short.MAX_VALUE))
        );
        panelStats2Layout.setVerticalGroup(
            panelStats2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStats2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelStats2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelDB4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelDB3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addComponent(panelDB5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(58, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout PanelEstadisticasLayout = new javax.swing.GroupLayout(PanelEstadisticas);
        PanelEstadisticas.setLayout(PanelEstadisticasLayout);
        PanelEstadisticasLayout.setHorizontalGroup(
            PanelEstadisticasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelEstadisticasLayout.createSequentialGroup()
                .addComponent(panelStats1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panelStats2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PanelEstadisticasLayout.setVerticalGroup(
            PanelEstadisticasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelStats1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelStats2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        PanelPrincipal.addTab("Estadísticas", PanelEstadisticas);

        panelStats3.setBackground(new java.awt.Color(255, 255, 255));

        jLabel125.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_app.png"))); // NOI18N

        jLabel126.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel126.setText("Aplicación WEB");

        jPanel46.setBackground(new java.awt.Color(255, 255, 255));
        jPanel46.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel130.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel130.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel130.setText("PENDIENTES");

        lbInfo.setEditable(false);
        lbInfo.setColumns(20);
        lbInfo.setLineWrap(true);
        lbInfo.setRows(5);
        lbInfo.setWrapStyleWord(true);
        jScrollPane1.setViewportView(lbInfo);

        btnActualizarApp.setText("Actualizar");
        btnActualizarApp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarAppActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel46Layout = new javax.swing.GroupLayout(jPanel46);
        jPanel46.setLayout(jPanel46Layout);
        jPanel46Layout.setHorizontalGroup(
            jPanel46Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel46Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel46Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel130, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addComponent(btnActualizarApp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel46Layout.setVerticalGroup(
            jPanel46Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel46Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jLabel130)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnActualizarApp)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel48.setBackground(new java.awt.Color(255, 255, 255));
        jPanel48.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel131.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel131.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel131.setText("INFORMACIÓN");

        lbInfoLinea1.setText(" ");

        lbInfoLinea2.setText(" ");

        lbInfoLinea3.setText(" ");

        javax.swing.GroupLayout jPanel48Layout = new javax.swing.GroupLayout(jPanel48);
        jPanel48.setLayout(jPanel48Layout);
        jPanel48Layout.setHorizontalGroup(
            jPanel48Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel48Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel48Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel131, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbInfoLinea1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbInfoLinea2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbInfoLinea3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel48Layout.setVerticalGroup(
            jPanel48Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel48Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jLabel131)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbInfoLinea1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbInfoLinea2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbInfoLinea3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel49.setBackground(new java.awt.Color(255, 255, 255));
        jPanel49.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel133.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel133.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel133.setText("AJUSTES APP");

        jButton9.setText("Configuración");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel49Layout = new javax.swing.GroupLayout(jPanel49);
        jPanel49.setLayout(jPanel49Layout);
        jPanel49Layout.setHorizontalGroup(
            jPanel49Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel49Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel49Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel133, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel49Layout.setVerticalGroup(
            jPanel49Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel49Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jLabel133)
                .addGap(18, 18, 18)
                .addComponent(jButton9)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelStats3Layout = new javax.swing.GroupLayout(panelStats3);
        panelStats3.setLayout(panelStats3Layout);
        panelStats3Layout.setHorizontalGroup(
            panelStats3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStats3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelStats3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel48, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelStats3Layout.createSequentialGroup()
                        .addGroup(panelStats3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jSeparator43)
                            .addGroup(panelStats3Layout.createSequentialGroup()
                                .addComponent(jLabel125)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel126, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel46, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel49, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelStats3Layout.setVerticalGroup(
            panelStats3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStats3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelStats3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel125)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelStats3Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel126, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator43, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel49, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jPanel48, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel46, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41))
        );

        panelStats4.setBackground(new java.awt.Color(204, 204, 255));

        jPanel45.setBackground(new java.awt.Color(255, 255, 255));
        jPanel45.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        nombreBusquedaUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nombreBusquedaUserActionPerformed(evt);
            }
        });
        nombreBusquedaUser.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                nombreBusquedaUserKeyReleased(evt);
            }
        });

        jLabel127.setText("Buscar por nombre:");

        jLabel128.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel128.setText("Búsqueda");

        dniBusquedaUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dniBusquedaUserActionPerformed(evt);
            }
        });
        dniBusquedaUser.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dniBusquedaUserKeyReleased(evt);
            }
        });

        jLabel129.setText("Buscar por DNI:");

        actListado5.setText("Actualizar Listado");
        actListado5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actListado5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel45Layout = new javax.swing.GroupLayout(jPanel45);
        jPanel45.setLayout(jPanel45Layout);
        jPanel45Layout.setHorizontalGroup(
            jPanel45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel45Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel45Layout.createSequentialGroup()
                        .addComponent(jLabel128)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(actListado5, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel45Layout.createSequentialGroup()
                        .addGroup(jPanel45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel127)
                            .addComponent(nombreBusquedaUser, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel129)
                            .addComponent(dniBusquedaUser, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 177, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel45Layout.setVerticalGroup(
            jPanel45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel45Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel45Layout.createSequentialGroup()
                        .addComponent(jLabel129)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dniBusquedaUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel45Layout.createSequentialGroup()
                        .addGroup(jPanel45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel128, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(actListado5))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel127)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(nombreBusquedaUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        tablaClientesApp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaClientesApp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaClientesAppMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tablaClientesAppMouseEntered(evt);
            }
        });
        jScrollPane11.setViewportView(tablaClientesApp);

        jPanel47.setBackground(new java.awt.Color(255, 255, 255));
        jPanel47.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnGenerarUsuarios.setText("<html><center>Generar Usuarios<br>para todos los Clientes</center></html>");
        btnGenerarUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerarUsuariosActionPerformed(evt);
            }
        });

        jButton10.setText("Borrar TODOS los usuarios");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel47Layout = new javax.swing.GroupLayout(jPanel47);
        jPanel47.setLayout(jPanel47Layout);
        jPanel47Layout.setHorizontalGroup(
            jPanel47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel47Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnGenerarUsuarios)
                    .addComponent(jButton10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel47Layout.setVerticalGroup(
            jPanel47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel47Layout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addComponent(btnGenerarUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelEstadoApp.setBackground(new java.awt.Color(0, 153, 0));
        panelEstadoApp.setBorder(javax.swing.BorderFactory.createTitledBorder("Estado"));

        lbEstadoApp.setBackground(new java.awt.Color(255, 255, 255));
        lbEstadoApp.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        lbEstadoApp.setForeground(new java.awt.Color(255, 255, 255));
        lbEstadoApp.setText(" ");

        javax.swing.GroupLayout panelEstadoAppLayout = new javax.swing.GroupLayout(panelEstadoApp);
        panelEstadoApp.setLayout(panelEstadoAppLayout);
        panelEstadoAppLayout.setHorizontalGroup(
            panelEstadoAppLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEstadoAppLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbEstadoApp, javax.swing.GroupLayout.DEFAULT_SIZE, 644, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelEstadoAppLayout.setVerticalGroup(
            panelEstadoAppLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lbEstadoApp, javax.swing.GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel44Layout = new javax.swing.GroupLayout(jPanel44);
        jPanel44.setLayout(jPanel44Layout);
        jPanel44Layout.setHorizontalGroup(
            jPanel44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel44Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane11)
                    .addComponent(jPanel45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelEstadoApp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(62, Short.MAX_VALUE))
        );
        jPanel44Layout.setVerticalGroup(
            jPanel44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel44Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel47, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel44Layout.createSequentialGroup()
                        .addComponent(jPanel45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panelEstadoApp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Usuarios", jPanel44);

        javax.swing.GroupLayout panelStats4Layout = new javax.swing.GroupLayout(panelStats4);
        panelStats4.setLayout(panelStats4Layout);
        panelStats4Layout.setHorizontalGroup(
            panelStats4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        panelStats4Layout.setVerticalGroup(
            panelStats4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        javax.swing.GroupLayout PanelAplicacionLayout = new javax.swing.GroupLayout(PanelAplicacion);
        PanelAplicacion.setLayout(PanelAplicacionLayout);
        PanelAplicacionLayout.setHorizontalGroup(
            PanelAplicacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelAplicacionLayout.createSequentialGroup()
                .addComponent(panelStats3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panelStats4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PanelAplicacionLayout.setVerticalGroup(
            PanelAplicacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelStats3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelStats4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        PanelPrincipal.addTab("Aplicación", PanelAplicacion);

        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel36.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel36.setText("Prontas Soluciones Informáticas");

        lbFecha.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbFecha.setText("01/01/2099");

        lbHora.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbHora.setText("00:00");

        lbEstadoAppAbajo.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lbEstadoAppAbajo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbEstadoAppAbajo.setText(" ");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 499, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbEstadoAppAbajo, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbHora, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel36)
                .addComponent(lbFecha)
                .addComponent(lbHora)
                .addComponent(lbEstadoAppAbajo))
        );

        jPanel12.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel37.setText("Usuario:");

        jLabel38.setText("Nombre");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jLabel37)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel38)
                .addContainerGap(80, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel37)
                .addComponent(jLabel38))
        );

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        menuArchivo.setText("Archivo");
        menuArchivo.add(jSeparator13);

        itemSalir.setText("Salir");
        itemSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemSalirActionPerformed(evt);
            }
        });
        menuArchivo.add(itemSalir);

        jMenuBar1.add(menuArchivo);

        menuAdmin.setText("Administrador");

        jMenuItem2.setText("Ventana de estadísticas");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAdmin.add(jMenuItem2);

        itemHistCobranzas.setText("Historial de cobranzas");
        itemHistCobranzas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemHistCobranzasActionPerformed(evt);
            }
        });
        menuAdmin.add(itemHistCobranzas);
        menuAdmin.add(jSeparator40);

        itemHistActividad.setText("Historial de actividad (usuarios)");
        itemHistActividad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemHistActividadActionPerformed(evt);
            }
        });
        menuAdmin.add(itemHistActividad);

        itemPagosParciales.setText("Pagos parciales");
        itemPagosParciales.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemPagosParcialesActionPerformed(evt);
            }
        });
        menuAdmin.add(itemPagosParciales);

        jMenuBar1.add(menuAdmin);

        menuBase.setText("Base de Datos");

        itemGuardarCopia.setText("Guardar copia de seguridad (Local)");
        itemGuardarCopia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemGuardarCopiaActionPerformed(evt);
            }
        });
        menuBase.add(itemGuardarCopia);

        itemRestaurarCopia.setText("Restaurar copia de seguridad (Local)");
        itemRestaurarCopia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemRestaurarCopiaActionPerformed(evt);
            }
        });
        menuBase.add(itemRestaurarCopia);

        itemAbrirCarpeta.setText("Abrir carpeta \"backup\" (Local)");
        itemAbrirCarpeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemAbrirCarpetaActionPerformed(evt);
            }
        });
        menuBase.add(itemAbrirCarpeta);
        menuBase.add(jSeparator36);

        itemSeleccionarNube.setText("Seleccionar carpeta de Drive (Nube)");
        itemSeleccionarNube.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemSeleccionarNubeActionPerformed(evt);
            }
        });
        menuBase.add(itemSeleccionarNube);

        itemSubirADrive.setText("Copiar BACKUPS a Carpeta Drive (Nube)");
        itemSubirADrive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemSubirADriveActionPerformed(evt);
            }
        });
        menuBase.add(itemSubirADrive);

        itemBajarDeDrive.setText("Restaurar BACKUPS de Carpeta Drive (Nube)");
        itemBajarDeDrive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemBajarDeDriveActionPerformed(evt);
            }
        });
        menuBase.add(itemBajarDeDrive);

        itemAbrirCarpetaDrive.setText("Abrir carpeta Drive (Nube)");
        itemAbrirCarpetaDrive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemAbrirCarpetaDriveActionPerformed(evt);
            }
        });
        menuBase.add(itemAbrirCarpetaDrive);
        menuBase.add(jSeparator39);

        itemBackupAuto.setText("Configurar backup automático (diario)");
        itemBackupAuto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemBackupAutoActionPerformed(evt);
            }
        });
        menuBase.add(itemBackupAuto);
        menuBase.add(jSeparator35);

        itemBorrarClie.setText("Borrar Clientes");
        itemBorrarClie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemBorrarClieActionPerformed(evt);
            }
        });
        menuBase.add(itemBorrarClie);

        itemBorrarPrest.setText("Borrar Préstamos");
        itemBorrarPrest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemBorrarPrestActionPerformed(evt);
            }
        });
        menuBase.add(itemBorrarPrest);

        itemBorrarPlanes.setText("Borrar Planes");
        itemBorrarPlanes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemBorrarPlanesActionPerformed(evt);
            }
        });
        menuBase.add(itemBorrarPlanes);

        itemBorrarCObr.setText("Borrar Cobradores");
        itemBorrarCObr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemBorrarCObrActionPerformed(evt);
            }
        });
        menuBase.add(itemBorrarCObr);

        itemBorrarLoc.setText("Borrar Localidades");
        itemBorrarLoc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemBorrarLocActionPerformed(evt);
            }
        });
        menuBase.add(itemBorrarLoc);
        menuBase.add(jSeparator14);

        itemMantBD.setText("Realizar mantenimiento de Base de Datos");
        itemMantBD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemMantBDActionPerformed(evt);
            }
        });
        menuBase.add(itemMantBD);

        itemVaciarHist.setText("Vaciar Historial de actividades");
        itemVaciarHist.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemVaciarHistActionPerformed(evt);
            }
        });
        menuBase.add(itemVaciarHist);
        menuBase.add(jSeparator44);

        itemSincro.setText("Sincronizar cuotas con base de datos online");
        itemSincro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemSincroActionPerformed(evt);
            }
        });
        menuBase.add(itemSincro);
        menuBase.add(jSeparator45);

        itemAdelantar.setText("Adelantar vencimientos de cuotas");
        itemAdelantar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemAdelantarActionPerformed(evt);
            }
        });
        menuBase.add(itemAdelantar);

        jMenuBar1.add(menuBase);

        menuReportes.setText("Reportes");

        itemPlanillaSMS.setText("Generar planilla para envío de SMS");
        itemPlanillaSMS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemPlanillaSMSActionPerformed(evt);
            }
        });
        menuReportes.add(itemPlanillaSMS);

        menuGenerarPlanillaCuotas.setText("Generar Planilla de cuotas a la fecha");

        itemPlanillaCuotasFechaAdeudadas.setText("Solo cuotas adeudadas a la fecha");
        itemPlanillaCuotasFechaAdeudadas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemPlanillaCuotasFechaAdeudadasActionPerformed(evt);
            }
        });
        menuGenerarPlanillaCuotas.add(itemPlanillaCuotasFechaAdeudadas);

        itemPlanillaCuotasFechaTodas.setText("Todas las cuotas a la fecha");
        itemPlanillaCuotasFechaTodas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemPlanillaCuotasFechaTodasActionPerformed(evt);
            }
        });
        menuGenerarPlanillaCuotas.add(itemPlanillaCuotasFechaTodas);
        menuGenerarPlanillaCuotas.add(jSeparator42);

        itemTodasLasCuotasAdeudadas.setText("Todas las cuotas adeudadas (inc. sin vencer)");
        itemTodasLasCuotasAdeudadas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemTodasLasCuotasAdeudadasActionPerformed(evt);
            }
        });
        menuGenerarPlanillaCuotas.add(itemTodasLasCuotasAdeudadas);

        menuReportes.add(menuGenerarPlanillaCuotas);
        menuReportes.add(jSeparator41);

        menuCli.setText("Clientes");

        itemResta1Cuota.setText("1 Sola cuota adeudada");
        itemResta1Cuota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemResta1CuotaActionPerformed(evt);
            }
        });
        menuCli.add(itemResta1Cuota);

        itemFinalizaronYNoRenovaron.setText("Pagaron todo y no renovaron");
        itemFinalizaronYNoRenovaron.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemFinalizaronYNoRenovaronActionPerformed(evt);
            }
        });
        menuCli.add(itemFinalizaronYNoRenovaron);

        menuReportes.add(menuCli);

        menuPrestamos.setText("Préstamos");

        itemPrestamosPorPeriodo.setText("Otorgados por período");
        itemPrestamosPorPeriodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemPrestamosPorPeriodoActionPerformed(evt);
            }
        });
        menuPrestamos.add(itemPrestamosPorPeriodo);

        menuReportes.add(menuPrestamos);

        jMenuBar1.add(menuReportes);

        jMenu1.setText("Ayuda");

        itemActivar.setText("Activar");
        itemActivar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemActivarActionPerformed(evt);
            }
        });
        jMenu1.add(itemActivar);
        jMenu1.add(jSeparator38);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PanelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 1195, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(PanelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 633, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("vista/img/icono.png"));
        return retValue;
    }
    
    //*******************
    //*  APLICACION WEB *
    //*******************
    public static void cargarActFallidas() {
        con.conectar();
        actFallidas = con.getActFallidasPrestamos();
        actUserFallidas = con.getActFallidasUsuarios();
        con.cerrar();

        int cantFallidas = actFallidas.size() + actUserFallidas.size();
        if (cantFallidas == 0) {
            lbInfo.setText("No hay actualizaciones pendientes en la aplicación");
            btnActualizarApp.setEnabled(false);
        } else if (cantFallidas == 1) {
            lbInfo.setText("Hay 1 actualización pendiente en la aplicación");
            btnActualizarApp.setEnabled(true);
        } else {
            lbInfo.setText("Hay "+cantFallidas+" actualizaciones pendientes en la aplicación");
            btnActualizarApp.setEnabled(true);
        }
    }
            
    public static void actualizarPendientes() {
        
        try {
            for (Integer i: actFallidas) {
                Prestamo p = Funciones.getPrestamoPorCod(todosLosPrestamos, i);
                if (p.getCod() != 999999) {
                    AplicacionControlador.guardarPrestamo(p);
                }
                con.conectar();
                con.eliminarActPorCodPrest(i);
                con.cerrar();
            }
            
            for (ActFallidaUsuario act: actUserFallidas) {
                if (act.getAccion().equalsIgnoreCase("generar")) {
                    AplicacionControlador.generarUsuarioPorCliente(Funciones.getClientePorDni(act.getDni()), false);
                    con.conectar();
                    con.eliminarActUserPorUsuario(act.getDni());
                    con.cerrar();
                }
                
                if (act.getAccion().equalsIgnoreCase("eliminar")) {
                    AplicacionControlador.eliminarUsuarioApp(act.getDni());
                    con.conectar();
                    con.eliminarActUserPorUsuario(act.getDni());
                    con.cerrar();
                }
             
                if (act.getAccion().equalsIgnoreCase("actualizar")) {
                    AplicacionControlador.actualizarUsuarioPorCliente(Funciones.getClientePorDni(act.getDni()), false);
                    con.conectar();
                    con.eliminarActUserPorUsuario(act.getDni());
                    con.cerrar();
                }
            }
        } catch (Exception e) {
            
        }
            
        
        cargarActFallidas();
    }
        
    public static void cargarUsuariosApp() {
        WorkersApp.cargarUsuariosApp();
    }
    
    public static void cargarMenuUsuariosApp(){
        ActionListener alUsers = new PopupActionListenerUsuariosApp();
        popupMenuUsuariosApp = new JPopupMenu();
        menuItemCrearUser = new JMenuItem("Crear Usuario App");
        menuItemCrearUser.addActionListener(alUsers);
        menuItemEliminarUser = new JMenuItem("Eliminar Usuario App");
        menuItemEliminarUser.addActionListener(alUsers);
        menuItemBlanquearPass = new JMenuItem("Blanquear contraseña");
        menuItemBlanquearPass.addActionListener(alUsers);
        
        popupMenuUsuariosApp.add(menuItemCrearUser);
        popupMenuUsuariosApp.add(menuItemEliminarUser);
        popupMenuUsuariosApp.add(menuItemBlanquearPass);
        
        ActionListener alCobradores = new PopupActionListenerCobradoresApp();
        popupMenuUsuariosAppCob = new JPopupMenu();
        menuItemCrearUserCob = new JMenuItem("Crear usuario APP");
        menuItemCrearUserCob.addActionListener(alCobradores);
        menuItemEliminarUserCob = new JMenuItem("Eliminar usuario APP");
        menuItemEliminarUserCob.addActionListener(alCobradores);
        menuItemBlanquearPassCob = new JMenuItem("Blanquear contraseña");
        menuItemBlanquearPassCob.addActionListener(alCobradores);
        
        //popupMenuUsuariosAppCob.add(menuItemCrearUserCob);
        //popupMenuUsuariosAppCob.add(menuItemEliminarUserCob);
        //popupMenuUsuariosAppCob.add(menuItemBlanquearPassCob);
        
    }

    public static void busquedaDeUsuarios() {
        ArrayList<Cliente> clisAux  = new ArrayList<>();
        
        if (nombreBusquedaUser.getText().equals("") && !dniBusquedaUser.getText().equals("")) {
            for (Cliente c: clientes) {
                if (c.getDni().toLowerCase().contains(dniBusquedaUser.getText().toLowerCase())) {
                    clisAux.add(c);
                }
            }
            modeloUsers = new TMUserApp(clisAux);
        } else if (dniBusquedaUser.getText().equals("") && !nombreBusquedaUser.getText().equals("")) {
            for (Cliente c: clientes) {
                if (c.getNombre().toLowerCase().contains(nombreBusquedaUser.getText().toLowerCase())) {
                    clisAux.add(c);
                }
            }
            modeloUsers = new TMUserApp(clisAux);
        } else {
            modeloUsers = new TMUserApp(clientes);
        }
        
        
        
        
        
        tablaClientesApp.setModel(modeloUsers);

        //DEFINE ANCHOS
        TableColumnModel columnModelClientes = tablaClientesApp.getColumnModel();
        columnModelClientes.getColumn(0).setPreferredWidth(50);
        columnModelClientes.getColumn(1).setPreferredWidth(130);
        columnModelClientes.getColumn(2).setPreferredWidth(120);
        columnModelClientes.getColumn(3).setPreferredWidth(150);
        columnModelClientes.getColumn(4).setPreferredWidth(100);
        columnModelClientes.getColumn(5).setPreferredWidth(120);


        //ORDENAR CUOTAS PRESTAMOS
        TableRowSorter<TableModel> elQueOrdenaClientes = new TableRowSorter<TableModel>(modeloUsers);
        tablaClientesApp.setRowSorter(elQueOrdenaClientes);

        tablaClientesApp.setAutoCreateRowSorter(true);
        DefaultRowSorter sorterClientes = ((DefaultRowSorter)tablaClientesApp.getRowSorter()); 
        ArrayList listaClientes = new ArrayList();
        listaClientes.add( new RowSorter.SortKey(1, SortOrder.ASCENDING) );
        sorterClientes.setSortKeys(listaClientes);
        sorterClientes.sort();
        cargarMenuUsuariosApp();


        tablaClientesApp.setComponentPopupMenu(popupMenuUsuariosApp);
        tablaClientesApp.addMouseListener(new TableMouseListenerUsersApp(tablaClientesApp));

        tablaClientesApp.setDefaultRenderer(Object.class, colorearUsers);

    }
    
    //************************
    //*  FIN APLICACION WEB  *
    //************************
    
        
    public static void cargarMenuClientes(){
        ActionListener alClientes = new PopupActionListenerClientes();
        popupMenuClientes = new JPopupMenu();
        menuItemCartel = new JMenuItem("Generar Cartel Deudor");
        menuItemCartel.addActionListener(alClientes);
        menuItemSobre = new JMenuItem("Generar Sobre");
        menuItemSobre.addActionListener(alClientes);
        
        popupMenuClientes.add(menuItemCartel);
        popupMenuClientes.add(menuItemSobre);
        
    }
    
    public static void setFotoInicial() {
          fotoCli = FuncionesImagenes.getImageIconResized("res/nofoto.png", lbFoto.getWidth(), lbFoto.getHeight());
          lbFoto.setIcon(fotoCli);
    }
    
    public static void abrirWelcome() {
        if (welcomeWindow != null) {//si existe una venta, la cierra.
                welcomeWindow.dispose();
            }
        
            welcomeWindow = new Supp();
            welcomeWindow.setTitle("Pantalla de bienvenida");
            welcomeWindow.setAlwaysOnTop(true);
            welcomeWindow.setVisible(true);
    }
    
    public static void setFotoPorCli(int cod) {
        File tempFile;
        boolean existe;
        
        if (con.getOpt().getTipo().equals("SERVIDOR")) {
            try {
                tempFile = new File("res/fotos/"+Integer.toString(cod)+".jpg");
            } catch (Exception e) {
                tempFile = new File("res/nofoto.png");
            }
        } else {
            try {
                tempFile = new File("\\\\"+con.getOpt().getSrv()+"\\prestav2\\res\\fotos\\"+Integer.toString(cod)+".jpg");
            } catch (Exception e) {
                tempFile = new File("res/nofoto.png");
            }
        }
        
        
        
        existe = tempFile.exists();
        
        if (existe) {
            if (con.getOpt().getTipo().equals("SERVIDOR")) {
                try {
                    fotoCli = FuncionesImagenes.getImageIconResized("res/fotos/"+Integer.toString(cod)+".jpg", lbFoto.getWidth(), lbFoto.getHeight());
                } catch (Exception e) {
                    fotoCli = FuncionesImagenes.getImageIconResized("res/nofoto.png", lbFoto.getWidth(), lbFoto.getHeight());
                }
            } else {
                try {
                    fotoCli = FuncionesImagenes.getImageIconResized("\\\\"+con.getOpt().getSrv()+"\\prestav2\\res\\fotos\\"+Integer.toString(cod)+".jpg", lbFoto.getWidth(), lbFoto.getHeight());
                } catch (Exception e) {
                    fotoCli = FuncionesImagenes.getImageIconResized("res/nofoto.png", lbFoto.getWidth(), lbFoto.getHeight());
                }
            }
        } else {
            fotoCli = FuncionesImagenes.getImageIconResized("res/nofoto.png", lbFoto.getWidth(), lbFoto.getHeight());
        }

        
        lbFoto.setIcon(fotoCli);
    }
    
    public static void workerCarga() {
        WorkerCarga cargando = new WorkerCarga();
                
            //loadWindow = new Ventana();
            //loadWindow.setVisible(true);
            //cargando.setProgreso(Ventana.barra);
            cargando.execute();
    }
    
    public static void workerActRutaPrestamos(ArrayList<Prestamo> ps, String ruta) {
        WorkerActRutaPrestamos ac = new WorkerActRutaPrestamos();
            ac.setRuta(ruta);
            ac.setPrestamos(ps);
            loadWindow = new Ventana();
            loadWindow.setVisible(true);
            ac.setProgreso(Ventana.barra);
            ac.execute();
    }
    
    public static void workerImpPlanes(ArrayList<Plan> planes) {
        WorkerImpPlanes loadimp = new WorkerImpPlanes();
            loadimp.setPlanes(planes);
            loadWindow = new Ventana();
            loadWindow.setVisible(true);
            loadimp.setProgreso(Ventana.barra);
            loadimp.execute();
    }
    
    public static void workerImpPlanes(ArrayList<Localidad> ls, ArrayList<Cobrador> cobs, ArrayList<Cliente> cs) {
        WorkerImpVarios loadimpx = new WorkerImpVarios();
            loadimpx.setCobradores(cobs);
            loadimpx.setLocalidades(ls);
            loadimpx.setClientes(cs);
            
            loadWindow = new Ventana();
            loadWindow.setVisible(true);
            loadimpx.setProgreso(Ventana.barra);
            loadimpx.execute();
    }
    
    public static void workerRefresh() {
        WorkerRefresh actu = new WorkerRefresh();
                
            actu.execute();
    }
    
    //INICIALIZA Y CARGA VALORES
    public static void mostrarCliente(Cliente c) {
        botonGuardar.setEnabled(false);
        botonEliminar.setEnabled(true);
        botonActualizar.setEnabled(true);
        botonReferencias.setEnabled(true);
        botonAgregarPrestamo.setEnabled(true);
        btnVertodos.setEnabled(true);
        cod.setText(Integer.toString(c.getCod()));
        nombre.setText(Funciones.capitalize(c.getNombre()));
        fechaNac.setDate(Funciones.stringADate(c.getFechaNac()));
        dni.setText(c.getDni());
        direccion.setText(Funciones.capitalize(c.getDireccion()));

        if (c.getEsClienteNuevo().equalsIgnoreCase("si")) {
            clienteNuevo.setSelected(true);
            clienteNuevo.setBackground(new Color(255,255,51));
        } else {
            clienteNuevo.setSelected(false);
            clienteNuevo.setBackground(null);
        }
        
        if (c.getLocalidad().equals("xxx")) {
            comboLoc.setSelectedItem("XXX - Sin especificar");
        }
        comboLoc.setSelectedItem(Funciones.getLocalidadPorCodInt(c.getLocalidad()).getCodInt()+" - "+Funciones.capitalize(Funciones.getLocalidadPorCodInt(c.getLocalidad()).getNombre()));

        if (c.getA1().equals("xxx")) {
            comboRutas.setSelectedItem("XXX - Sin especificar");
        }
        Cobrador c1 = Funciones.getCobradorPorRuta(cobradores, c.getA1());
        comboRutas.setSelectedItem(c1.getCodInt()+" - "+Funciones.capitalize(c1.getNombre()));
        
        telfijo.setText(c.getTelefono().toUpperCase());
        telcel.setText(c.getCelular().toUpperCase());
        email.setText(c.getEmail());
        obs.setText(c.getObservaciones());
        
        comboPrestamos.removeAllItems();
        
        for (Prestamo prestaX: Funciones.getPrestamosPorCli(todosLosPrestamos, Integer.toString(c.getCod()))) {
            comboPrestamos.addItem(prestaX.getPlan().toUpperCase());
        }
        
        if (c.getUltimoPagoFecha().contains("01/01/2099")) {
            lbUltPago.setText("No se registra último pago");    
        } else {
            lbUltPago.setText("Últ. pago: "+c.getUltimoPagoFecha()+" / $"+c.getUltimoPagoImporte());
        }
        
        
        btnCartel.setEnabled(true);
        setFotoPorCli(c.getCod());
        
        
    }
    
    public static void mostrarCobrador(Cobrador c) {
        botonGuardarCob.setEnabled(false);
        botonEliminarCob.setEnabled(true);
        botonActualizarCob.setEnabled(true);
        btnVertodosCob.setEnabled(true);
        codCob.setText(Integer.toString(c.getCod()));
        nombreCob.setText(Funciones.capitalize(c.getNombre()));
        codIntCob.setText(c.getCodInt());
        obsCob.setText(c.getObs());
        nombreApp.setText(c.getNombreApp());
        
    }
    
    public static void cargarOpciones() {
        Conector c = new Conector();
        c.conectar();
        opciones = c.getOpciones();
        c.cerrar();
        
        if (opciones.getExtra2().equals("no") || opciones.getExtra2().equals("")) {
            ocultarPagasPrestamo.setSelected(false);
        } else {
            ocultarPagasPrestamo.setSelected(true);
        }
                
                
        //CARGA TIPO INTERES
        if (opciones.getExtra1().equals("") || opciones.getExtra1().equals("efectivo")) {
            comboTipoInt.setSelectedItem("Efectivo");
            lbTipoInt.setText("$");
        } else {
            comboTipoInt.setSelectedItem("Porcentaje");
            lbTipoInt.setText("%");
        }
        
        valorIntAtraso.setText(Double.toString(opciones.getInteresdia()));
        
        nombreEmpresa.setText(Funciones.capitalize(opciones.getNombre()));
        telEmpresa.setText(opciones.getTelefono().toUpperCase());
        dirEmpresa.setText(Funciones.capitalize(opciones.getDireccion()));
        
        leyendaComprobante.setText(opciones.getExtra3());
        
        if (opciones.getMostrarpagados().equals("si")) {
            mostrarPagas.setSelected(true);    
        } else {
            mostrarPagas.setSelected(false);    
        }
        
        if (opciones.getMostrarvencidos().equals("si")) {
            morosos.setSelected(true);    
        } else {
            morosos.setSelected(false);    
        }
                
    }
    
    public static void guardarOpciones() {
        String sel = (String) comboTipoInt.getSelectedItem();
        if (sel.equals("Efectivo")) {
            opciones.setExtra1("efectivo");
        } else {
            opciones.setExtra1("porcentaje");
        }
        
        opciones.setInteresdia(Double.parseDouble(valorIntAtraso.getText()));
        
        opciones.setNombre(nombreEmpresa.getText());
        opciones.setTelefono(telEmpresa.getText());
        opciones.setDireccion(dirEmpresa.getText());

        
        if (mostrarPagas.isSelected()) {
            opciones.setMostrarpagados("si");
        } else {
            opciones.setMostrarpagados("no");  
        }
        
        if (morosos.isSelected()) {
            opciones.setMostrarvencidos("si");
        } else {
            opciones.setMostrarvencidos("no");  
        }
        
        
        
        con.conectar();
        con.guardarOpciones(opciones);
        con.cerrar();
                
    }
    
    public static void cargarOpcionesExtra() {
        opcionesExtra = Funciones.leerOptExtra();
        
        if (opcionesExtra.getMarcaFinalizados().equals("SI")) {
            marcarFinalizados.setSelected(true);
        } else {
            marcarFinalizados.setSelected(false);
        }
        if (opcionesExtra.getImpresion().equals("SI")) {
            habImpresion.setSelected(true);
        } else {
            habImpresion.setSelected(false);
        }
        if (opcionesExtra.getX1().equals("SI")) {
            ocultaDir.setSelected(true);
        } else {
            ocultaDir.setSelected(false);
        }
        if (opcionesExtra.getX2().equals("SI")) {
            recibosA4.setSelected(true);
        } else {
            recibosA4.setSelected(false);
        }
        if (opcionesExtra.getX3().equals("SI")) {
            ocultaTel.setSelected(true);
        } else {
            ocultaTel.setSelected(false);
        }
        if (opcionesExtra.getX4().equals("80MM")) {
            ticket80mm.setSelected(true);
        } else {
            hojaa4.setSelected(true);
        }
    }
    
    public static void guardarOpcionesExtra() {
        
        
        if (marcarFinalizados.isSelected()) {
            opcionesExtra.setMarcaFinalizados("SI");
        } else {
            opcionesExtra.setMarcaFinalizados("NO");
        }
        if (habImpresion.isSelected()) {
            opcionesExtra.setImpresion("SI");
        } else {
            opcionesExtra.setImpresion("NO");
        }
        if (ocultaDir.isSelected()) {
            opcionesExtra.setX1("SI");
        } else {
            opcionesExtra.setX1("NO");
        }
        if (recibosA4.isSelected()) {
            opcionesExtra.setX2("SI");
        } else {
            opcionesExtra.setX2("NO");
        }
        if (ocultaTel.isSelected()) {
            opcionesExtra.setX3("SI");
        } else {
            opcionesExtra.setX3("NO");
        }
        
        if (ocultaTel.isSelected()) {
            opcionesExtra.setX3("SI");
        } else {
            opcionesExtra.setX3("NO");
        }
        
        if (ticket80mm.isSelected()) {
            opcionesExtra.setX4("80MM");
        } else {
            opcionesExtra.setX4("A4");
        }
        Funciones.guardaOptExtra(opcionesExtra);
    }
    
    public static void comprobarActivado() {
        activado = true;
        if(activado) {
            botonActivar.setVisible(false);
        } else {
//            if (avisoDemo != null) {//si existe una venta, la cierra.
//            avisoDemo.dispose();
//            }
//
//            avisoDemo = new AvisoDemo();
//            avisoDemo.setTitle("Modo de evaluación");
//            avisoDemo.setVisible(true);
//            avisoDemo.setAlwaysOnTop(true);
        }
    }
    
    public static void comprobarPermisos() {
        if (tipo.toLowerCase().equals("limitado")) {
            PanelPrincipal.setEnabledAt(2, false);
            PanelPrincipal.setEnabledAt(4, false);
            PanelPrincipal.setEnabledAt(5, false);
            PanelPrincipal.setEnabledAt(6, false);
            PanelPrincipal.setEnabledAt(7, false);
            menuAdmin.setEnabled(false);
            menuBase.setEnabled(false);
            panelOpt1.setVisible(false);
            panelOpt2.setVisible(false);
            panelAvisoOPC.setVisible(true);
        } else {
            panelOpt1.setVisible(true);
            panelOpt2.setVisible(true);
            panelAvisoOPC.setVisible(false);
        }
    }
    
    public static void cargarTableModels() {
        modeloUsuarios = new TMUsuario(new ArrayList<>());
        listaUsuarios.setModel(modeloUsuarios);    
        
        modeloClientes = new TMCliente(new ArrayList<>());
        tablaClientes.setModel(modeloClientes);
        
        modeloCobradores = new TMCobrador(new ArrayList<>());
        tablaCobradores.setModel(modeloCobradores);
        
        modeloVencimientosSemanales = new TMVencimiento(new ArrayList<>());
        tablaVencSemanales.setModel(modeloVencimientosSemanales);
        
        modeloVencimientosDiarios = new TMVencimiento(new ArrayList<>());
        tablaVencDiarios.setModel(modeloVencimientosDiarios);
    }
    
    
    public static void cargarUsuarios() {
        Conector c = new Conector();
        c.conectar();
        usuarios = c.getUsuarios();
        c.cerrar();
        
        modeloUsuarios = new TMUsuario(usuarios);
        listaUsuarios.setModel(modeloUsuarios);

        Action editar = new AbstractAction() {
                 public void actionPerformed(ActionEvent e) {
                    int codABuscar = Integer.parseInt((String)listaUsuarios.getValueAt(listaUsuarios.getSelectedRow(), 0));
                    EditarUsuario editarUsuario = new EditarUsuario(codABuscar);
                    editarUsuario.setTitle("Editar usuario: " + listaUsuarios.getValueAt(listaUsuarios.getSelectedRow(), 1));
                    editarUsuario.setVisible(true);
      
        }
        };
        ButtonColumn listaUsuariosCol = new ButtonColumn(listaUsuarios, editar, 3);
    }
    
    public static void cargarClientesPrestamos() {
        comboClientes.removeAllItems();
        ArrayList<String> nombreClientes = new ArrayList<String>();;
        
        for (Cliente cli: Principal.clientes) {
            nombreClientes.add(cli.getNombre().toLowerCase());
        }
        
        Collections.sort(nombreClientes);
        
        for (String nom: nombreClientes) {
            comboClientes.addItem(nom);
        }
        
        //Texto predictivo comboChoferes
        JTextField campoCli = (JTextField) comboClientes.getEditor().getEditorComponent();
        //campoDestinos.setText("");
        campoCli.addKeyListener(new ComboKeyHandler(comboClientes));
    }
    
    public static void cargarClientes() {
        clientes = new ArrayList<>();
        prestamosCliente = new ArrayList<>();
        
        Conector c = new Conector();
        c.conectar();
        clientes = c.getClientes();
        c.cerrar();
        
        modeloClientes = new TMCliente(clientes);
        tablaClientes.setModel(modeloClientes);
        
        //DEFINE ANCHOS
        TableColumnModel columnModelClientes = tablaClientes.getColumnModel();
        columnModelClientes.getColumn(0).setPreferredWidth(50);
        columnModelClientes.getColumn(1).setPreferredWidth(130);
        columnModelClientes.getColumn(2).setPreferredWidth(150);
        columnModelClientes.getColumn(3).setPreferredWidth(100);
        columnModelClientes.getColumn(4).setPreferredWidth(100);
        columnModelClientes.getColumn(5).setPreferredWidth(150);
        
        
        //ORDENAR CUOTAS PRESTAMOS
        TableRowSorter<TableModel> elQueOrdenaClientes = new TableRowSorter<TableModel>(modeloClientes);
        tablaClientes.setRowSorter(elQueOrdenaClientes);
        
        tablaClientes.setAutoCreateRowSorter(true);
        DefaultRowSorter sorterClientes = ((DefaultRowSorter)tablaClientes.getRowSorter()); 
        ArrayList listaClientes = new ArrayList();
        listaClientes.add( new RowSorter.SortKey(1, SortOrder.ASCENDING) );
        sorterClientes.setSortKeys(listaClientes);
        sorterClientes.sort();
        
        cargarLocalidades();
        
        tablaClientes.setComponentPopupMenu(popupMenuClientes);
        tablaClientes.addMouseListener(new TableMouseListenerClientes(tablaClientes));
        
        lbInfoTablaClientes.setText(clientes.size()+ " Clientes cargados.");
    }
    
    public static void cargarLocalidades() {
        localidades = new ArrayList<Localidad>();

        Conector c = new Conector();
        c.conectar();
        localidades = c.getLocalidades();
        c.cerrar();
        
        //CARGA COMBO LOC
        comboLoc.removeAllItems();
        ArrayList<String> locs = new ArrayList<String>();;
        locs.add("XXX - Sin especificar");
        for (Localidad lX: Principal.localidades) {
            locs.add(lX.getCodInt()+" - "+Funciones.capitalize(lX.getNombre()));
        }
        
        Collections.sort(locs);
        
        for (String loc: locs) {
            comboLoc.addItem(loc);
        }
        comboLoc.setSelectedItem("XXX - Sin especificar");
    }
    
    public static void cargarCobradores() {
        cobradores = new ArrayList<Cobrador>();
               
        Conector c = new Conector();
        c.conectar();
        cobradores = c.getCobradores();
        c.cerrar();
        
        modeloCobradores = new TMCobrador(cobradores);
        tablaCobradores.setModel(modeloCobradores);
        
//        //DEFINE ANCHOS
//        TableColumnModel columnModelClientes = tablaCobradores.getColumnModel();
//        columnModelClientes.getColumn(0).setPreferredWidth(50);
//        columnModelClientes.getColumn(1).setPreferredWidth(130);
//        columnModelClientes.getColumn(2).setPreferredWidth(150);
//        columnModelClientes.getColumn(3).setPreferredWidth(100);
//        columnModelClientes.getColumn(4).setPreferredWidth(100);
//        columnModelClientes.getColumn(5).setPreferredWidth(150);
//        
//        
        //ORDENAR CUOTAS PRESTAMOS
        TableRowSorter<TableModel> elQueOrdenaClientes = new TableRowSorter<TableModel>(modeloCobradores);
        tablaCobradores.setRowSorter(elQueOrdenaClientes);
        
        tablaCobradores.setAutoCreateRowSorter(true);
        DefaultRowSorter sorterClientes = ((DefaultRowSorter)tablaCobradores.getRowSorter()); 
        ArrayList listaClientes = new ArrayList();
        listaClientes.add( new RowSorter.SortKey(1, SortOrder.ASCENDING) );
        sorterClientes.setSortKeys(listaClientes);
        sorterClientes.sort();
     
        //Carga combo cobradores en cliente
        comboRutas.removeAllItems();
        
        ArrayList<String> nombreRuta = new ArrayList<String>();;
        for (Cobrador cob: cobradores) {
            nombreRuta.add(cob.getCodInt()+" - "+Funciones.capitalize(cob.getNombre()));
        }
  
        nombreRuta.add("XXX - Sin especificar");
        
        Collections.sort(nombreRuta);
        
        for (String nom: nombreRuta) {
            comboRutas.addItem(nom);
        }
        comboRutas.setSelectedItem("XXX - Sin especificar");
        
        //tablaCobradores.setComponentPopupMenu(popupMenuUsuariosAppCob);
        //tablaCobradores.addMouseListener(new TableMouseListenerCobradoresApp(tablaCobradores));
    }
    
    public static void cargarClientesBuscados(ArrayList<Cliente> clientesBuscados) {
        
        modeloClientes = new TMCliente(clientesBuscados);
        tablaClientes.setModel(modeloClientes);
        
        //DEFINE ANCHOS
        TableColumnModel columnModelClientes = tablaClientes.getColumnModel();
        columnModelClientes.getColumn(0).setPreferredWidth(50);
        columnModelClientes.getColumn(1).setPreferredWidth(130);
        columnModelClientes.getColumn(2).setPreferredWidth(150);
        columnModelClientes.getColumn(3).setPreferredWidth(100);
        columnModelClientes.getColumn(4).setPreferredWidth(100);
        columnModelClientes.getColumn(5).setPreferredWidth(150);
        
        
        //ORDENAR CUOTAS PRESTAMOS
        TableRowSorter<TableModel> elQueOrdenaClientes = new TableRowSorter<TableModel>(modeloClientes);
        tablaClientes.setRowSorter(elQueOrdenaClientes);
        
        tablaClientes.setAutoCreateRowSorter(true);
        DefaultRowSorter sorterClientes = ((DefaultRowSorter)tablaClientes.getRowSorter()); 
        ArrayList listaClientes = new ArrayList();
        listaClientes.add( new RowSorter.SortKey(1, SortOrder.ASCENDING) );
        sorterClientes.setSortKeys(listaClientes);
        sorterClientes.sort();
    }
    
    public static void cargarPrestamosYCuotas() {
        Conector c = new Conector();
        c.conectar();
        todosLosPrestamos = c.getPrestamos();
        todasLasCuotas = c.getCuotas();
        c.cerrar();
        cuotasDelDia.clear();
        cuotasDeLaSemana.clear();
        //Carga cuotas del dia
        Date fechaActDate = Funciones.stringADate(Funciones.devolverFechaActualStr());
        String fechaAct = Funciones.devolverFechaActualStr();
        String mesAct = fechaAct.substring(3);
        double totalPagadoHoy = 0.0;
        
        //VALORES ESTADISTICAS
        double totalPrestadoAux = 0.0;
        double totalDevolverAux = 0.0;
        double totalDevueltoAux = 0.0;
        double totalDevueltoMesAux = 0.0;
    
        for (Cuota cuota: todasLasCuotas) {
            
            //CARGA ESTADISTICAS
            totalPrestadoAux += Funciones.getImportePrestadoCuota(cuota);
            totalDevolverAux += Double.parseDouble(cuota.getImporteTotal());
            totalDevueltoAux += Double.parseDouble(cuota.getImportePagado());
            if (mesAct.equals(cuota.getFechaPagado().substring(3))) {
                totalDevueltoMesAux += Double.parseDouble(cuota.getImportePagado()); 
            }
            
            
            
            
            if (cuota.getFechaPagado().equals(fechaAct)) {
                totalPagadoHoy += Double.parseDouble(cuota.getImportePagado());
            }
            if (morosos.isSelected()) {
                //CARGA CUOTAS DEL DIA Y ANTIGUAS SIN PAGAR
                if (cuota.getFechaPagar().equals(fechaAct) || (Funciones.stringADate(cuota.getFechaPagar()).before(Funciones.stringADate(fechaAct)))) {
                    //CARGA SOLO CUOTAS PAGAS
                    if (mostrarPagas.isSelected()) {
                        cuotasDelDia.add(cuota);
                    } else {
                     //CARGA CUOTAS SIN PAGAR
//                        if ((Double.parseDouble(cuota.getImporteTotal()) -  Double.parseDouble(cuota.getImportePagado()))>1) {
                            
                        if (!Funciones.getEstadoCuota(cuota).equals("PAGADA")) {
                            if (mAnt.isSelected()) {
                                if (Funciones.calcularDiasDeDiferencia(Funciones.devolverFechaActualStr(), cuota.getFechaPagar())>Integer.parseInt(diasVFinal.getText()) && Funciones.calcularDiasDeDiferencia(Funciones.devolverFechaActualStr(), cuota.getFechaPagar())<Integer.parseInt(diasVInicial.getText())) {
                                    cuotasDelDia.add(cuota);;
                                }
                            } else {
                                cuotasDelDia.add(cuota);
                            }   
                            
                            
                        }
                    }

                }
            } else {
                
            
                //CARGA SOLO CUOTAS DEL DIA
                if (cuota.getFechaPagar().equals(fechaAct)) {
                    //CARGA SOLO CUOTAS PAGAS
                    if (mostrarPagas.isSelected()) {
                        cuotasDelDia.add(cuota);
                    } else {
                     //CARGA CUOTAS SIN PAGAR
                        if ((Double.parseDouble(cuota.getImporteTotal()) -  Double.parseDouble(cuota.getImportePagado()))>1) {
                            cuotasDelDia.add(cuota);
                        }
                    }

                }
            }
        }
        
        //Carga estadisticas
        prestamosTotLB.setText(Integer.toString(todosLosPrestamos.size()));
        cuotasTotLB.setText(Integer.toString(todosLosPrestamos.size()));
        totalPrestadoLB.setText("$"+Double.toString(Funciones.formatearDecimales(totalPrestadoAux,2)));
        totalDevueltoLB.setText("$"+Double.toString(Funciones.formatearDecimales(totalDevueltoAux,2)));
        totalCobrarLB.setText("$"+Double.toString(Funciones.formatearDecimales(totalDevolverAux-totalDevueltoAux,2)));
        totalDevueltoMesLB.setText("$"+Double.toString(Funciones.formatearDecimales(totalDevueltoAux,2)));
        
        clientesRegLB.setText(Integer.toString(clientes.size()));
        
        //Carga cuotas de la semana
        Date fechaActDateMas7 = Funciones.sumarDias(fechaActDate, 7);
        for (Cuota cuota: todasLasCuotas) {
            Date fechaPagar = Funciones.stringADate(cuota.getFechaPagar());
            
            if ((fechaPagar.after(fechaActDate) || fechaPagar.equals(fechaActDate)) && (fechaPagar.before(fechaActDateMas7) || fechaPagar.equals(fechaActDateMas7))) {
                cuotasDeLaSemana.add(cuota);
            }
        }
        
        modeloVencimientosSemanales = new TMVencimiento(cuotasDeLaSemana);
        tablaVencSemanales.setModel(modeloVencimientosSemanales);
        
        modeloVencimientosDiarios = new TMVencimiento(cuotasDelDia);
        tablaVencDiarios.setModel(modeloVencimientosDiarios);
        
        //DEFINE ANCHOS
        TableColumnModel columnModel = tablaVencSemanales.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(50);
        columnModel.getColumn(1).setPreferredWidth(90);
        columnModel.getColumn(2).setPreferredWidth(170);
        columnModel.getColumn(3).setPreferredWidth(180);
        columnModel.getColumn(4).setPreferredWidth(50);
        columnModel.getColumn(5).setPreferredWidth(60);
        columnModel.getColumn(6).setPreferredWidth(60);
        
        TableColumnModel columnModelDiarios = tablaVencDiarios.getColumnModel();
        columnModelDiarios.getColumn(0).setPreferredWidth(50);
        columnModelDiarios.getColumn(1).setPreferredWidth(90);
        columnModelDiarios.getColumn(2).setPreferredWidth(170);
        columnModelDiarios.getColumn(3).setPreferredWidth(180);
        columnModelDiarios.getColumn(4).setPreferredWidth(50);
        columnModelDiarios.getColumn(5).setPreferredWidth(60);
        columnModelDiarios.getColumn(6).setPreferredWidth(60);
        
        
        //ORDENAR SEMANALES
        TableRowSorter<TableModel> elQueOrdenaSemana = new TableRowSorter<TableModel>(modeloVencimientosSemanales);
        tablaVencSemanales.setRowSorter(elQueOrdenaSemana);
        
        tablaVencSemanales.setAutoCreateRowSorter(true);
        DefaultRowSorter sorter = ((DefaultRowSorter)tablaVencSemanales.getRowSorter()); 
        ArrayList list = new ArrayList();
        list.add( new RowSorter.SortKey(1, SortOrder.ASCENDING) );
        sorter.setSortKeys(list);
        sorter.sort();
        
        //ORDENAR SEMANALES
        TableRowSorter<TableModel> elQueOrdenaDia = new TableRowSorter<TableModel>(modeloVencimientosDiarios);
        tablaVencDiarios.setRowSorter(elQueOrdenaDia);
        
        tablaVencDiarios.setAutoCreateRowSorter(true);
        DefaultRowSorter sorterDia = ((DefaultRowSorter)tablaVencDiarios.getRowSorter()); 
        ArrayList listDia = new ArrayList();
        listDia.add( new RowSorter.SortKey(1, SortOrder.ASCENDING) );
        sorterDia.setSortKeys(listDia);
        sorterDia.sort();
        
        //COLOREA CELDAS
        tablaVencSemanales.setDefaultRenderer(Object.class, colorearVenc);
        tablaVencDiarios.setDefaultRenderer(Object.class, colorearVenc);
        
        
        //CARGA DATOS DIARIOS
        int totalVencHoy = 0;
        double totalCobrarHoy =0.0;
        double totalCobradoHoy =0.0;
        String fechAux = Funciones.devolverFechaActualStr();
        for (Cuota cuotaAux2: todasLasCuotas) {
            if (cuotaAux2.getFechaPagar().equals(fechAux)){
                totalCobrarHoy += Double.parseDouble(cuotaAux2.getImporteTotal())-Double.parseDouble(cuotaAux2.getImportePagado());
                totalVencHoy +=1;
            }
            if (cuotaAux2.getFechaPagado().equals(fechAux)){
                totalCobradoHoy += Double.parseDouble(cuotaAux2.getImportePagado());
            }
            
        }
        
        
        
        
        lbVencsHoy.setText(Integer.toString(totalVencHoy));
        lbCobrarHoy.setText("$"+Double.toString(Funciones.formatearDecimales(totalCobrarHoy,2)));
        lbCobradoHoy.setText("$"+Double.toString(Funciones.formatearDecimales(totalPagadoHoy,2)));
        
        
        //AGREGA BOTONES
        Action cerrar = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int codCuota = (int)tablaVencDiarios.getValueAt(tablaVencDiarios.getSelectedRow(), 0);
                c.conectar();
                Cuota cuotaAux = c.getCuotaPorCod(codCuota);
                c.cerrar();
                
                FuncionesPagos.cobrarCuota(cuotaAux);
            }

        };
        Action cerrarSemanal = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int codCuota = (int)tablaVencSemanales.getValueAt(tablaVencSemanales.getSelectedRow(), 0);
                c.conectar();
                Cuota cuotaAux = c.getCuotaPorCod(codCuota);
                c.cerrar();
                
                FuncionesPagos.cobrarCuota(cuotaAux);
            }

        };
        columnaPago = new ButtonColumn(tablaVencDiarios, cerrar, 8);
        columnaPagoSemanal = new ButtonColumn(tablaVencSemanales, cerrarSemanal, 8);
        
    }
    public static void cargarPlanes() {
        planes = new ArrayList<>();
        
        Conector c = new Conector();
        c.conectar();
        planes = c.getPlanes();
        c.cerrar();
        
       
        Collections.sort(planes);
        
         
        listaPlanes.removeAll();
        
        //Recorrer el contenido del ArrayList
        int cantPlanesNormales = 0;
        int cantPlanesPersonalizados = 0;
        for(Plan planX: planes) {
            if (!planX.getNombre().toLowerCase().contains("-app")) {
                if (!planX.getA4().equals("personalizado")) {
                    listaPlanes.add(planX.getNombre().toUpperCase());    
                    cantPlanesNormales++;
                } else {
                    cantPlanesPersonalizados++;
                }
            } else {
                    cantPlanesPersonalizados++;
            }
        }
        
        cantPlanes.setText(Integer.toString(cantPlanesNormales));
        cantPlanesPers.setText(Integer.toString(cantPlanesPersonalizados));
        
        
    }
    

    public static void blanquearCamposCliente() {
        cod.setText("");
        nombre.setText("");
        fechaNac.setDate(Funciones.stringADate("01/01/2099"));
        dni.setText("");
        direccion.setText("");
        comboLoc.setSelectedItem("XXX - Sin especificar");
        telfijo.setText("");
        telcel.setText("");
        email.setText("");
        comboPrestamos.removeAllItems();
        obs.setText("");
        comboRutas.setSelectedItem("XXX - Sin especificar");
        lbUltPago.setText("");
        botonEliminar.setEnabled(false);
        botonActualizar.setEnabled(false);
        botonReferencias.setEnabled(false);
        botonAgregarPrestamo.setEnabled(false);
        btnVertodos.setEnabled(false);
        btnCartel.setEnabled(false);
        botonGuardar.setEnabled(true);
        setFotoInicial();
        clienteNuevo.setSelected(true);
        clienteNuevo.setBackground(new Color(255,255,51));
    }
    public static void blanquearCamposCobrador() {
        codCob.setText("");
        nombreCob.setText("");
        telCob.setText("");
        obsCob.setText("");
        codIntCob.setText("");
        nombreApp.setText("");
        
        botonEliminarCob.setEnabled(false);
        botonActualizarCob.setEnabled(false);
        btnVertodosCob.setEnabled(false);
        botonGuardarCob.setEnabled(true);
    }
    public static void blanquearCamposPlan() {
        codPlan.setText("");
        nomPlan.setText("");
        importePrestar.setText("0.0");
        importeDevolver.setText("0.0");
        interesDinero.setText("0.0");
        interesPorc.setText("0");
        dineroCobrador.setText("0.0");
        porcCobrador.setText("0");
        pInicial.setText("0.0");
        impTotal.setText("0.0");
        
        esPrestamo.setSelected(true);
        
        diario.setSelected(true);
        diasPers.setVisible(false);
        lbPers.setVisible(false);
     
        porc.setSelected(true);
        imp.setSelected(false);
        
        comboCuotas.setSelectedItem("1");
        valorCuota.setText("0.0");
        
        botonGuardarPlan.setEnabled(true);
        botonEliminarPlan.setEnabled(false);
        botonActualizarPlan.setEnabled(false);
        mostrarInputProd(false);
        nomPlan.setEditable(true);
    }
    public static void blanquearCamposPrestamos() {
        botonEliminarPrestamo.setEnabled(false);
        btnCobrarPrestamo.setEnabled(false);
        btnRefinanciar.setEnabled(false);
        btnCarton.setEnabled(false);
        botonImprimirPrestamo.setEnabled(false);
        botonImprimirComprobante.setEnabled(false);
        botonAnular.setEnabled(false);
        botonReciboA4.setEnabled(false);
        botonCambiarVenc.setEnabled(false);
        
        prestamosCliente2 = new ArrayList<Prestamo>();
        cuotasPrestamo = new ArrayList<Cuota>();
        prestamoEnPrestamos = new Prestamo();
        
        listaPrestamos.removeAll();
        //Recorrer el contenido del ArrayList
        for(Prestamo prestaX: prestamosCliente2) {
            listaPrestamos.add(prestaX.getPlan().toUpperCase());    
        }
        
        modeloCuotasPrestamo = new TMVencimiento(cuotasPrestamo);
        tablaCuotasPrestamo.setModel(modeloCuotasPrestamo);
        
        //DEFINE ANCHOS
        TableColumnModel columnModelCuotas = tablaCuotasPrestamo.getColumnModel();
        columnModelCuotas.getColumn(0).setPreferredWidth(50);
        columnModelCuotas.getColumn(1).setPreferredWidth(90);
        columnModelCuotas.getColumn(2).setPreferredWidth(170);
        columnModelCuotas.getColumn(3).setPreferredWidth(180);
        columnModelCuotas.getColumn(4).setPreferredWidth(50);
        columnModelCuotas.getColumn(5).setPreferredWidth(60);
        columnModelCuotas.getColumn(6).setPreferredWidth(60);
        
        //ORDENAR CUOTAS PRESTAMOS
        TableRowSorter<TableModel> elQueOrdenaCuotas = new TableRowSorter<TableModel>(modeloCuotasPrestamo);
        tablaCuotasPrestamo.setRowSorter(elQueOrdenaCuotas);
        
        tablaCuotasPrestamo.setAutoCreateRowSorter(true);
        DefaultRowSorter sorterCuotas = ((DefaultRowSorter)tablaCuotasPrestamo.getRowSorter()); 
        ArrayList listCuotas = new ArrayList();
        listCuotas.add( new RowSorter.SortKey(0, SortOrder.ASCENDING) );
        sorterCuotas.setSortKeys(listCuotas);
        sorterCuotas.sort();
        
        //COLOREA CELDAS
        tablaCuotasPrestamo.setDefaultRenderer(Object.class, colorearVenc);
        
         Action cerrarCuota = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int codCuota = (int)tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 0);
                con.conectar();
                Cuota cuotaAux = con.getCuotaPorCod(codCuota);
                con.cerrar();
                FuncionesPagos.cobrarCuota(cuotaAux);
            }

        };
        columnaPagoCuota = new ButtonColumn(tablaCuotasPrestamo, cerrarCuota, 7);
        
    }
    public static void cargarValoresIniciales() {
        Date hoy = Funciones.stringADate(Funciones.devolverFechaActualStr());
        Date hoyMas7 = Funciones.sumarDias(hoy, 7);
        lbVencHoy.setText(Funciones.dateAString(hoy));
        //lbVenc7Dias.setText("("+Funciones.dateAString(hoy) +" - " + Funciones.dateAString(hoyMas7)+")");
        blanquearCamposCliente();
        blanquearCamposCobrador();
        blanquearCamposPlan();
        blanquearCamposPrestamos();
    }
    
    //RECALCULA VALORES DE PLAN
    public static void recalcularValorPlan() {
        String cantCuotasAux = (String) comboCuotas.getSelectedItem();
        int cantCuotasInt = Integer.parseInt(cantCuotasAux);
        
        //SETEA IMPORTE TOTAL POR IMPORTE MENSUAL
        double interesTotalD = 0.0;
        double interesMensualD = 0.0;
        try {
            interesMensualD = Double.parseDouble(interesMensual.getText());
        } catch (Exception e) {
            
        }

        if (diario.isSelected()) {
            interesTotalD = (cantCuotasInt * interesMensualD)/30;
        }
        if (semanal.isSelected()) {
            interesTotalD = (cantCuotasInt * interesMensualD)/4;
        }
        if (mensual.isSelected()) {
            interesTotalD = cantCuotasInt * interesMensualD;
        }
        if (pers.isSelected()) {
            interesTotalD = (cantCuotasInt * interesMensualD)/(Integer.parseInt(diasPers.getText())/7);
        }
        
        interesTotalD = Funciones.formatearDecimales(interesTotalD, 2);
        
        interesPorc.setText(Double.toString(interesTotalD));
        
        
        double impPrestar = 0.0;
        double impDevolver = 0.0;
        double valAux1 = 0.0;
        double intPorc = 0.0;
        double valorCuotaAux = 0.0;
        
        //SI ES PRODUCTO
        double impTotalD = 0.0;
        double impInicial = 0.0;
        if (esProducto.isSelected()) {
            if (impTotal.getText().equals("")) {
                impTotalD = 0.0;
            } else {
                impTotalD = Double.parseDouble(impTotal.getText());
            }
            if (pInicial.getText().equals("")) {
                impInicial = 0.0;
            } else {
                impInicial = Double.parseDouble(pInicial.getText());
            }
        }
        
        if (!porc.isSelected()) {
            if (importePrestar.getText().equals("")) {
                impPrestar = 0.0;
            } else {
                impPrestar = Double.parseDouble(importePrestar.getText());
            }
            //SI ES PRODUCTO
            if (esProducto.isSelected()) {
                impPrestar = impTotalD - impInicial;
                importePrestar.setText(Double.toString(Funciones.formatearDecimales(impPrestar, 1)));
            }
            
            
            if (importeDevolver.getText().equals("")) {
                impDevolver = 0.0;
            } else {
                impDevolver = Double.parseDouble(importeDevolver.getText());
            }
            valAux1 = ((impDevolver * 100) / impPrestar)-100;
            intPorc = Funciones.formatearDecimales(valAux1,2);

            interesPorc.setText(Double.toString(intPorc));
            
            double impInteres = impDevolver - impPrestar;
        interesDinero.setText(Double.toString(impInteres));
        } else {
            impPrestar = Double.parseDouble(importePrestar.getText());
            //SI ES PRODUCTO
            if (esProducto.isSelected()) {
                impPrestar = impTotalD - impInicial;
                importePrestar.setText(Double.toString(Funciones.formatearDecimales(impPrestar, 1)));
            }
            
            impDevolver = 0.0;
            intPorc = 0.0;
            if (interesPorc.getText().equals("")) {
                intPorc = 0.0;
            } else {
                intPorc = Double.parseDouble(interesPorc.getText());
                
            }
            
            
            intPorc += 100;
            valAux1 = (impPrestar * intPorc) / 100;      
            
            impDevolver = valAux1;
            
            importeDevolver.setText(Double.toString(valAux1));
            
       
            double impInteres = valAux1 - impPrestar;
            interesDinero.setText(Double.toString(impInteres));
        }

        valorCuotaAux = impDevolver / Double.parseDouble(cantCuotasAux);
        valorCuota.setText(Double.toString(Funciones.formatearDecimales(valorCuotaAux, 2)));
        
        //CALCULA COBRADOR
        double porcCob;
        if (porcCobrador.getText().equals("")) {
                porcCob = 0.0;
            } else {
                porcCob = Double.parseDouble(porcCobrador.getText());
        }
        double dineroCob = (porcCob * impPrestar) / 100;
        dineroCob = Funciones.formatearDecimales(dineroCob,2);
        dineroCobrador.setText(Double.toString(dineroCob));
        
    }
    
    
    
    private void botonBlanquearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBlanquearActionPerformed
        blanquearCamposCliente();
    }//GEN-LAST:event_botonBlanquearActionPerformed

    private void botonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarActionPerformed
        int dialogResult = JOptionPane.showConfirmDialog(null, "Desea eliminar a "+Funciones.capitalize(nombre.getText())+"?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            WorkersApp.eliminarUsuario(dni.getText());
            con.conectar();
            for (Prestamo p: Funciones.getPrestamosPorCli(todosLosPrestamos, cod.getText())) {
                con.eliminarPrestamoPorCod(p.getCod());
                con.eliminarCuotaPorCodPrestamo(Integer.toString(p.getCod()));
            }
            
            con.eliminarClientePorCod(Integer.parseInt(cod.getText()));
            con.cerrar();
            
        }
        blanquearCamposCliente();
        Workers.workerRefresh();
    }//GEN-LAST:event_botonEliminarActionPerformed

    private void botonActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonActualizarActionPerformed
        if (nombre.getText().toLowerCase().equals("")) {
                JOptionPane.showMessageDialog(null,"Debe ingresar un nombre.", "Ingrese un nombre", 1);
            } else {
        
        Cliente cN = new Cliente();
        cN.setCod(Integer.parseInt(cod.getText()));
        cN.setNombre(nombre.getText());
        cN.setFechaNac(Funciones.dateAString(fechaNac.getDate()));
        cN.setDni(dni.getText());
        cN.setDireccion(direccion.getText());

        if (clienteNuevo.isSelected()) {
            cN.setEsClienteNuevo("si");
        } else {
            cN.setEsClienteNuevo("no");
        }

        String locSel = (String) comboLoc.getSelectedItem();
        String[] arr = locSel.split("-");
        arr[0] = arr[0].replaceAll(" ", "");
        
        cN.setLocalidad(arr[0]);
        if(locSel.toLowerCase().contains("xxx")) {
            cN.setLocalidad("xxx");
        }
        
        //CHEQUEO RUTA
        String rutaSel = (String) comboRutas.getSelectedItem();
        String[] arr1 = rutaSel.split("-");
        arr1[0] = arr1[0].replaceAll(" ", "");
        
        if (!arr1[0].equals(Funciones.getClientePorCod(clientes, cN.getCod()).getA1())) {


            Object[] options = {"TODOS", "NUEVOS"};
            int dialogResult = JOptionPane.showOptionDialog(null,"Desea modificar ruta para TODOS los préstamos\n\n de este cliente o solo los NUEVOS que se crearán?","ALERTA!",
            JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,null,options,options[0]);
            if(dialogResult == JOptionPane.YES_OPTION){
                //TODOS
                ArrayList<Prestamo> psAux1 = Funciones.getPrestamosPorCli(todosLosPrestamos, cod.getText());
                
                workerActRutaPrestamos(psAux1, arr1[0]);
            }
        }
        cN.setA1(arr1[0]);
         if(rutaSel.toLowerCase().contains("xxx")) {
            cN.setA1("xxx");
        }
                
        cN.setTelefono(telfijo.getText());
        cN.setCelular(telcel.getText());
        cN.setEmail(email.getText());
        cN.setObservaciones(obs.getText());
        
        con.conectar();
        con.actualizarCliente(cN);
        con.cerrar();
        
        cargarClientes();
        blanquearCamposCliente();
        WorkersApp.actualizarUsuario(cN);
        
        } 
    }//GEN-LAST:event_botonActualizarActionPerformed

    private void botonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarActionPerformed

            boolean guardadoX = false;
            for (Cliente cAux11: Principal.clientes) {
                if (cAux11.getNombre().toLowerCase().equals(nombre.getText().toLowerCase())) {
                    guardadoX = true;
                }
            }    
            if (guardadoX) {
                JOptionPane.showMessageDialog(null,"Ya existe un cliente con ese nombre, modifique el nombre.", "Nombre existente", 1);
            } else if (nombre.getText().toLowerCase().equals("")) {
                JOptionPane.showMessageDialog(null,"Debe ingresar un nombre.", "Ingrese un nombre", 1);
            } else {
                
                Cliente cN = new Cliente();
                cN.setNombre(nombre.getText());
                if (fechaNac.getDate() != null) {
                    cN.setFechaNac(Funciones.dateAString(fechaNac.getDate()));
                }
                cN.setDni(dni.getText());
                cN.setDireccion(direccion.getText());
                
                String locSel = (String) comboLoc.getSelectedItem();
                String[] arr = locSel.split("-");
                arr[0] = arr[0].replaceAll(" ", "");

                cN.setLocalidad(arr[0]);
                
                //CHEQUEO RUTA
                String rutaSel = (String) comboRutas.getSelectedItem();
                
                String[] arr1 = rutaSel.split("-");
                arr1[0] = arr1[0].replaceAll(" ", "");

                cN.setA1(arr1[0]);
                
                cN.setTelefono(telfijo.getText());
                cN.setCelular(telcel.getText());
                cN.setEmail(email.getText());
                cN.setObservaciones(obs.getText());

                if (clienteNuevo.isSelected()) {
                    cN.setEsClienteNuevo("si");
                } else {
                    cN.setEsClienteNuevo("no");
                }
                
                con.conectar();
                con.guardarCliente(cN);
                con.cerrar();
                
                Workers.workerRefresh();
            }
            
            int dialogResult = JOptionPane.showConfirmDialog(null, "Desea generar un usuario en la aplicación para: \n\n"+Funciones.capitalize(nombre.getText())+"?", "Crear usuario", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if(dialogResult == JOptionPane.YES_OPTION){
                con.conectar();
                Cliente cliAux1 = con.getClientePorNombre(nombre.getText());
                con.cerrar();
                WorkersApp.crearUsuario(cliAux1);

            }
            blanquearCamposCliente();

    }//GEN-LAST:event_botonGuardarActionPerformed

    private void comboPrestamosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboPrestamosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboPrestamosActionPerformed

    private void botonReferenciasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonReferenciasActionPerformed
         if (referencias != null) {//si existe una venta, la cierra.
            referencias.dispose();
        }
        
        con.conectar();
        Cliente cli = con.getClientePorCod(Integer.parseInt(cod.getText()));
        con.cerrar();
        
        referencias = new Referencias(cli);
        referencias.setTitle("Referencias");
        referencias.setVisible(true);
    }//GEN-LAST:event_botonReferenciasActionPerformed

    private void diarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diarioActionPerformed
        recalcularValorPlan();
    }//GEN-LAST:event_diarioActionPerformed

    private void semanalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_semanalActionPerformed
        recalcularValorPlan();
    }//GEN-LAST:event_semanalActionPerformed

    private void mensualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mensualActionPerformed
        recalcularValorPlan();           
    }//GEN-LAST:event_mensualActionPerformed

    private void diarioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_diarioMouseClicked
        lbPers.setVisible(false);
        diasPers.setVisible(false); 
    }//GEN-LAST:event_diarioMouseClicked

    private void semanalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_semanalMouseClicked
        lbPers.setVisible(false);
        diasPers.setVisible(false); 
    }//GEN-LAST:event_semanalMouseClicked

    private void mensualMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mensualMouseClicked
        lbPers.setVisible(false);
        diasPers.setVisible(false); 
    }//GEN-LAST:event_mensualMouseClicked

    private void interesPorcKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_interesPorcKeyReleased
        recalcularValorPlan();
    }//GEN-LAST:event_interesPorcKeyReleased

    private void importePrestarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_importePrestarKeyReleased
        recalcularValorPlan();
    }//GEN-LAST:event_importePrestarKeyReleased

    private void importeDevolverKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_importeDevolverKeyReleased
        recalcularValorPlan();
    }//GEN-LAST:event_importeDevolverKeyReleased

    private void botonGuardarPlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarPlanActionPerformed
        if (activado || (!activado && planes.size()<=4)) {
            boolean valido = true;
            boolean existe = false;
            //VALIDA DATOS
            int diasPerson;
            double porcCob = 0.0;
            double impPrestar = 0.0;
            double impDevolver = 0.0;
            double impInteres = 0.0;
            double porcInteres = 0.0;
            
            
            //VALIDA EXISTENCIA
            for (Plan p: planes) {
                if (p.getNombre().toLowerCase().equals(nomPlan.getText().toLowerCase())) {
                    existe = true;
                }
            }
            
            try {
                diasPerson = Integer.parseInt(diasPers.getText());
            } catch (Exception e) {
                diasPerson = 14;
            }
            try {
                porcCob = Double.parseDouble(porcCobrador.getText().replaceAll(",","."));
                impPrestar = Double.parseDouble(importePrestar.getText().replaceAll(",","."));
                impDevolver = Double.parseDouble(importeDevolver.getText().replaceAll(",","."));
                impInteres = Double.parseDouble(interesDinero.getText().replaceAll(",","."));
                porcInteres = Double.parseDouble(interesPorc.getText().replaceAll(",","."));
            } catch (Exception e) {
                porcCob = 0.0;
                valido = false;
            }
            
            if (!existe) {
                
            
            if (valido) {
                Plan pNew = new Plan();

                pNew.setNombre(nomPlan.getText());
                pNew.setImportePrestar(Double.toString(impPrestar));
                pNew.setImporteDevolver(Double.toString(impDevolver));
                pNew.setImporteInteres(Double.toString(impInteres));
                pNew.setPorcentajeInteres(Double.toString(porcInteres));
                pNew.setPorcentajeCobrador(Double.toString(porcCob));

                String cantCuotasAux = (String) comboCuotas.getSelectedItem();
                pNew.setCantCuotas(cantCuotasAux);

                if (porc.isSelected()) {
                    pNew.setCalculaPor("porc");
                } else {
                    pNew.setCalculaPor("imp");
                }

                if (diario.isSelected()) {
                    pNew.setTipo("diario");
                } else if (semanal.isSelected()) {
                    pNew.setTipo("semanal");
                } else if (mensual.isSelected()) {
                    pNew.setTipo("mensual");
                } else {
                    pNew.setTipo("p-"+Integer.toString(diasPerson));
                }

                //TIPO DE PLAN
                if (esProducto.isSelected()) {
                    pNew.setA1("producto");
                    double pagoI = 0;
                    try {
                        pagoI = Double.parseDouble(pInicial.getText().replaceAll(",","."));
                    } catch (Exception ex) {

                    }
                    pNew.setA3(impTotal.getText().replaceAll(",","."));
                    pNew.setA2(Double.toString(pagoI));
                }

                if (nomPlan.getText().equals("")) {
                    JOptionPane.showMessageDialog(null,"El nombre del plan no puede estar vacío.", "Error", 0);
                } else {
                    con.conectar();
                    con.guardarPlan(pNew);
                    con.cerrar();

                    blanquearCamposPlan();
                    cargarPlanes();
                }
            } else {
                JOptionPane.showMessageDialog(null,"Revise los valores:\n\nLOS CAMPOS NO PUEDEN ESTAR VACÍOS.\nLOS CAMPOS NO PUEDEN CONTENER LETRAS NI ESPACIOS.\nUSAR PUNTO COMO SEPARADOR DE DECIMALES.", "Revise los valores", 1);
                
            }
            } else {
                JOptionPane.showMessageDialog(null,"Ya existe un plan con ese nombre.", "Revise el nombre", 1);
                
            }  
        
        } else {
            JOptionPane.showMessageDialog(null,"No puede agregar más planes en la versión de prueba.", "Límite alcanzado", 1);
        }
    }//GEN-LAST:event_botonGuardarPlanActionPerformed

    private void botonBlanquearPlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBlanquearPlanActionPerformed
        blanquearCamposPlan();
    }//GEN-LAST:event_botonBlanquearPlanActionPerformed

    private void botonEliminarPlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarPlanActionPerformed
        int dialogResult = JOptionPane.showConfirmDialog(null, "Esta seguro de querer eliminar el plan" + nomPlan.getText() +"?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
             boolean usado = false;
            
            for (Prestamo p: todosLosPrestamos) {
                if (p.getPlan().equalsIgnoreCase(nomPlan.getText())) {
                    usado = true;
                }
            }
            
            if (usado) {
                JOptionPane.showMessageDialog(this, "No puede eliminar este plan.\n\nHay préstamos creados con este plan.\nElimine primero los préstamos.", "INFO", 1);
            } else {
                con.conectar();
                con.eliminarPlanPorCod(Integer.parseInt(codPlan.getText()));
                con.cerrar();
                cargarPlanes();
                blanquearCamposPlan();
            }
            
        }
        
    }//GEN-LAST:event_botonEliminarPlanActionPerformed

    private void listaPlanesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listaPlanesMouseClicked
        blanquearCamposPlan();
        String planSelec = listaPlanes.getSelectedItem().toLowerCase();
        con.conectar();
        Plan planMostrar = con.getPlanPorNombre(planSelec);
        con.cerrar();
        
        botonGuardarPlan.setEnabled(false);
        botonEliminarPlan.setEnabled(true);
        botonActualizarPlan.setEnabled(true);
        btnSubirPlan.setEnabled(true);
        btnBajarPlan.setEnabled(true);
        btnGuardarOrden.setEnabled(true);
        
        
        if (planMostrar.getA1().equals("producto")) {
            esProducto.setSelected(true);
            
            mostrarInputProd(true);
        } else {
            esPrestamo.setSelected(true);
        }
        
        if (planMostrar.getCalculaPor().equals("porc")) {
            porc.setSelected(true);
            imp.setSelected(false);
        } else {
            imp.setSelected(true);
            porc.setSelected(false);
        }
        
        if (planMostrar.getTipo().equals("diario")) {
            diario.setSelected(true);

        } else if (planMostrar.getTipo().equals("semanal")) {
            semanal.setSelected(true);

        } else if (planMostrar.getTipo().equals("mensual")) {
            mensual.setSelected(true);
        } else {
            String[] arr = planMostrar.getTipo().split("-");
            diasPers.setVisible(true);
            lbPers.setVisible(true);
            pers.setSelected(true);
            diasPers.setText(arr[1]);
        }
        
        codPlan.setText(Integer.toString(planMostrar.getCod()));
        nomPlan.setText(planMostrar.getNombre().toUpperCase());
        importePrestar.setText(planMostrar.getImportePrestar());
        importeDevolver.setText(planMostrar.getImporteDevolver());
        interesDinero.setText(planMostrar.getImporteInteres());
        interesMensual.setText(planMostrar.getPorcentajeInteres());
        pInicial.setText(planMostrar.getA2());
        impTotal.setText(planMostrar.getA3());
        comboCuotas.setSelectedItem(planMostrar.getCantCuotas());
        valorCuota.setText(Double.toString(Funciones.formatearDecimales(Double.parseDouble(importeDevolver.getText())/Double.parseDouble(planMostrar.getCantCuotas()), 2)));
        
        nomPlan.setEditable(false);
    }//GEN-LAST:event_listaPlanesMouseClicked

    private void comboCuotasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCuotasActionPerformed
        recalcularValorPlan();
    }//GEN-LAST:event_comboCuotasActionPerformed

    private void botonActualizarPlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonActualizarPlanActionPerformed
        Plan pAnt = Funciones.getPlanPorCod(Integer.parseInt(codPlan.getText()));
        boolean hayP = false;
        for (Prestamo p: todosLosPrestamos) {
            if (pAnt.getNombre().equals(p.getPlan())) {
                hayP = true;
            }
        }
        if (!pAnt.getNombre().toLowerCase().equals(nomPlan.getText().toLowerCase()) && hayP) {
           JOptionPane.showMessageDialog(null, "Existen uno o más préstamos con el plan seleccionado.\n\nNO PUEDE MODIFICARSE EL NOMBRE DEL PLAN\n\nCree un nuevo plan con el nombre deseado.", "** INFO **",0);
        } else {
             //VALIDA DATOS
             boolean valido = true;
            int diasPerson;
            
            double porcCob = 0.0;
            double impPrestar = 0.0;
            double impDevolver = 0.0;
            double impInteres = 0.0;
            double porcInteres = 0.0;
            
            
            
            
            try {
                diasPerson = Integer.parseInt(diasPers.getText());
            } catch (Exception e) {
                diasPerson = 14;
            }
            try {
                porcCob = Double.parseDouble(porcCobrador.getText().replaceAll(",","."));
                impPrestar = Double.parseDouble(importePrestar.getText().replaceAll(",","."));
                impDevolver = Double.parseDouble(importeDevolver.getText().replaceAll(",","."));
                impInteres = Double.parseDouble(interesDinero.getText().replaceAll(",","."));
                porcInteres = Double.parseDouble(interesPorc.getText().replaceAll(",","."));
            } catch (Exception e) {
                porcCob = 0.0;
                valido = false;
            }
            
            if (valido) {
                Plan pNew = new Plan();

            pNew.setCod(Integer.parseInt(codPlan.getText()));
            pNew.setNombre(nomPlan.getText());
            pNew.setNombre(nomPlan.getText());
            pNew.setImportePrestar(Double.toString(impPrestar));
            pNew.setImporteDevolver(Double.toString(impDevolver));
            pNew.setImporteInteres(Double.toString(impInteres));
            pNew.setPorcentajeInteres(Double.toString(porcInteres));
            pNew.setPorcentajeCobrador(Double.toString(porcCob));

            String cantCuotasAux = (String) comboCuotas.getSelectedItem();
            pNew.setCantCuotas(cantCuotasAux);

            if (porc.isSelected()) {
                pNew.setCalculaPor("porc");
            } else {
                pNew.setCalculaPor("imp");
            }

            if (diario.isSelected()) {
                pNew.setTipo("diario");
            } else if (semanal.isSelected()) {
                pNew.setTipo("semanal");
             } else if (mensual.isSelected()) {
                pNew.setTipo("mensual");
            } else {
                pNew.setTipo("p-"+Integer.toString(diasPerson));
            }

            //TIPO DE PLAN
            if (esProducto.isSelected()) {
                pNew.setA1("producto");
                double pagoI = 0;
                try {
                    pagoI = Double.parseDouble(pInicial.getText());
                } catch (Exception ex) {

                }
                pNew.setA3(impTotal.getText());
                pNew.setA2(Double.toString(pagoI));
            } else {
                pNew.setA1("");
                pNew.setA3("");
                pNew.setA2("");
            }

            con.conectar();
            con.actualizarPlan(pNew);
            con.cerrar();

            blanquearCamposPlan();
            cargarPlanes();
            } else {
                JOptionPane.showMessageDialog(null,"Revise los valores:\n\nLOS CAMPOS NO PUEDEN ESTAR VACÍOS.\nLOS CAMPOS NO PUEDEN CONTENER LETRAS NI ESPACIOS.\nUSAR PUNTO COMO SEPARADOR DE DECIMALES.", "Revise los valores", 1);
                
            }
            
            
            
        }
    }//GEN-LAST:event_botonActualizarPlanActionPerformed

    private void botonAgregarPrestamoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarPrestamoActionPerformed
        if (activado || (!activado && todosLosPrestamos.size()<=4)) {
            
        
            if (agregarPrestamo != null) {//si existe una venta, la cierra.
                agregarPrestamo.dispose();
            }

            con.conectar();
            Cliente cli = con.getClientePorCod(Integer.parseInt(cod.getText()));
            con.cerrar();
            
            
            ArrayList<Cuota> cuotasAdeudadas = cli.getCuotasAdeudadas();
            String cuotasAdeudadasStr = "";
            for (Cuota c: cuotasAdeudadas) {
                cuotasAdeudadasStr += "NÚM: "+c.getNumCuota()+"   "+ "FECHA: "+c.getFechaPagar()+"   "+" IMPORTE: $"+c.getImporteTotal()+"\n";
            }
            int cantAdeudadas = cuotasAdeudadas.size();
            
            if (cantAdeudadas>1) {
                JOptionPane.showMessageDialog(null,"El cliente adeuda MÁS de una cuota.\n\n"+"CUOTAS ADEUDADAS: "+cantAdeudadas+"\n\n"+cuotasAdeudadasStr, "Crédito rechazado", 0);
            } else if (cantAdeudadas == 1 || cli.tieneCuotasPagasYDebeInteres()) {
                if (checkClienteWindow != null) {//si existe una venta, la cierra.
                    checkClienteWindow.dispose();
                }

                checkClienteWindow = new CheckAgregarPrestamo(cli);
                checkClienteWindow.setTitle("Listado de préstamos otorgados");
                checkClienteWindow.setVisible(true);
            } else {
                agregarPrestamo = new AgregarPrestamo(cli);
                agregarPrestamo.setTitle("Nuevo Préstamo");
                agregarPrestamo.setVisible(true);
            }
            
        } else {
            JOptionPane.showMessageDialog(null,"No puede agregar más préstamos en la versión de prueba.", "Límite alcanzado", 1);
        }
    }//GEN-LAST:event_botonAgregarPrestamoActionPerformed

    private void listaPrestamosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listaPrestamosMouseClicked
        actListaCuotasPP();
        
    }//GEN-LAST:event_listaPrestamosMouseClicked

    public static void actListaCuotasPP() {
        String prestamoSel = listaPrestamos.getSelectedItem();
        prestamoEnPrestamos = Funciones.getPrestamoPorCod(todosLosPrestamos, Funciones.getCodPorPrestamoStr(prestamoSel));


        if (ocultarPagasPrestamo.isSelected()) {
            ArrayList<Cuota> cuotasAuxiliar = new ArrayList<>();
            cuotasAuxiliar = Funciones.getCuotasPorCodPrestamo(todasLasCuotas, Integer.toString(prestamoEnPrestamos.getCod()));
            cuotasPrestamo = new ArrayList<>();
            for (Cuota ca1: cuotasAuxiliar) {
//                System.out.println(ca1.getImportePagado() +" - "+ca1.getImporteTotal());
                if (Double.parseDouble(ca1.getImportePagado()) < (Double.parseDouble(ca1.getImporteTotal()))){
                    cuotasPrestamo.add(ca1);
                }
            }
        } else {
            cuotasPrestamo = Funciones.getCuotasPorCodPrestamo(todasLasCuotas, Integer.toString(prestamoEnPrestamos.getCod()));
        }
        
        modeloCuotasPrestamo = new TMVencimiento(cuotasPrestamo);
        tablaCuotasPrestamo.setModel(modeloCuotasPrestamo);
        
        //DEFINE ANCHOS
        TableColumnModel columnModelCuotas = tablaCuotasPrestamo.getColumnModel();
        columnModelCuotas.getColumn(0).setPreferredWidth(50);
        columnModelCuotas.getColumn(1).setPreferredWidth(90);
        columnModelCuotas.getColumn(2).setPreferredWidth(153);
        columnModelCuotas.getColumn(3).setPreferredWidth(180);
        columnModelCuotas.getColumn(4).setPreferredWidth(50);
        columnModelCuotas.getColumn(5).setPreferredWidth(60);
        columnModelCuotas.getColumn(6).setPreferredWidth(60);
        columnModelCuotas.getColumn(7).setPreferredWidth(80);
        columnModelCuotas.getColumn(8).setPreferredWidth(103);
        
        //ORDENAR CUOTAS PRESTAMOS
        TableRowSorter<TableModel> elQueOrdenaCuotas = new TableRowSorter<TableModel>(modeloCuotasPrestamo);
        tablaCuotasPrestamo.setRowSorter(elQueOrdenaCuotas);
        
        tablaCuotasPrestamo.setAutoCreateRowSorter(true);
        DefaultRowSorter sorterCuotas = ((DefaultRowSorter)tablaCuotasPrestamo.getRowSorter()); 
        ArrayList listCuotas = new ArrayList();
        listCuotas.add( new RowSorter.SortKey(0, SortOrder.ASCENDING) );
        sorterCuotas.setSortKeys(listCuotas);
        sorterCuotas.sort();
        
        //COLOREA CELDAS
        tablaCuotasPrestamo.setDefaultRenderer(Object.class, colorearVenc);
        
         Action cerrarCuota = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int codCuota = (int)tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 0);
                con.conectar();
                Cuota cuotaAux = con.getCuotaPorCod(codCuota);
                con.cerrar();
                FuncionesPagos.cobrarCuota(cuotaAux);
            }

        };
         
        columnaPagoCuota = new ButtonColumn(tablaCuotasPrestamo, cerrarCuota, 8);
        
        planPrest.setText(prestamoEnPrestamos.getPlan().toUpperCase());
        if (prestamoEnPrestamos.getCodCobrador().equals("XXX")) {
            cobAsign.setText("Sin especificar");
        } else {
            Cobrador cA1 = Funciones.getCobradorPorRuta(cobradores, prestamoEnPrestamos.getCodCobrador());
            cobAsign.setText(cA1.getCodInt()+" - "+Funciones.capitalize(cA1.getNombre()));
        }
        
        codPrest.setText(Integer.toString(prestamoEnPrestamos.getCod()));
        fechaPrest.setText(prestamoEnPrestamos.getFechaGenerado());
        valorImportePrest.setText("$"+Funciones.getPlanPorNombre(prestamoEnPrestamos.getPlan()).getImportePrestar());
        
        //SI ES PRODUCTO Y TIENE ANTICIPO
        Plan px = Funciones.getPlanPorNombre(prestamoEnPrestamos.getPlan());
        double impIni = 0.0;
        try {
            impIni = Double.parseDouble(px.getA2());
        } catch (Exception e) {
            
        }
        
        if (px.getA1().equals("producto") && impIni>0) {
            cuotasPrest.setText(Integer.toString(prestamoEnPrestamos.getPlanObj().getCantCuotasInt()-1)+" + ANTICIPO");
        } else {
            int cantAdic = 0;
            int cuotasNorm = prestamoEnPrestamos.getPlanObj().getCantCuotasInt();
            for(Cuota c: prestamoEnPrestamos.getCuotas()) {
                int num = Integer.parseInt(c.getNumCuota());
                if (num != 0 && num != 999 && num > cuotasNorm) {
                    cantAdic++;
                }
            }
            if (cantAdic>0) {
                cuotasPrest.setText(Integer.toString(prestamoEnPrestamos.getPlanObj().getCantCuotasInt())+" + "+cantAdic+" ADIC.");
            } else {
                cuotasPrest.setText(Integer.toString(prestamoEnPrestamos.getPlanObj().getCantCuotasInt()));
            }
                
            
        }
        
        valorCuotaPrest.setText("$" + prestamoEnPrestamos.getCuotas().get(0).getImporteTotal());
        
        double totalPresAux = 0;
        for (Cuota cuotaAux: prestamoEnPrestamos.getCuotas()) {
            totalPresAux += Double.parseDouble(cuotaAux.getImporteTotal());
        }
        
        double totalPagAux = 0;
        for (Cuota cuotaAux: prestamoEnPrestamos.getCuotas()) {
            totalPagAux += Double.parseDouble(cuotaAux.getImportePagado());
        }
        
        double impAdeudado = totalPresAux - totalPagAux;
        if (impAdeudado<0) {
            impAdeudado = 0.0;
        }
        
        totalPrest.setText("$" + Double.toString(Funciones.formatearDecimales(totalPresAux,2)));
        pagadoPrest.setText("$" + Double.toString(Funciones.formatearDecimales(totalPagAux,2)));
        adeudadoPrest.setText("$" + Double.toString(Funciones.formatearDecimales(impAdeudado,2)));
        if (prestamoEnPrestamos.getImporteDescuento().equals("")) {
            descontadoPrest.setText("$0.0");
            sobrePrest.setText("$" + Double.toString(Funciones.formatearDecimales(totalPresAux,2)));
        } else {
            descontadoPrest.setText("$" + prestamoEnPrestamos.getImporteDescuento());
            double importeSobre = Double.parseDouble(prestamoEnPrestamos.getPlanObj().getImportePrestar()) - Double.parseDouble(prestamoEnPrestamos.getImporteDescuento()); 
            sobrePrest.setText("$" + Double.toString(Funciones.formatearDecimales(importeSobre,2)));
        }
            
        obsPrestamo.setText(prestamoEnPrestamos.getObservacion());
        modifCob.setEnabled(true);
        botonEliminarPrestamo.setEnabled(true);
        btnCobrarPrestamo.setEnabled(true);
        btnRefinanciar.setEnabled(true);
        btnCarton.setEnabled(true);
        botonImprimirPrestamo.setEnabled(true);
        
        Plan pAux = Funciones.getPlanPorNombre(prestamoEnPrestamos.getPlan());
        if (pAux.getA1().equals("producto")) {
            botonImprimirComprobante.setEnabled(true);
        } else {
            botonImprimirComprobante.setEnabled(false);
        }
        
        //botonAnular.setEnabled(true);
    }
    
    private void comboClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboClientesActionPerformed
        actListaPrestamos();
    }//GEN-LAST:event_comboClientesActionPerformed

    public static void actListaPrestamos() {
        String clienteSel = (String) comboClientes.getSelectedItem();
        if (clienteSel != null) {
            
        
        if (clienteSel.equals("Sin especificar")) {
            prestamosCliente2 = new ArrayList<Prestamo>();
        } else {
            cliAux = Funciones.getClientePorNombre(clientes, clienteSel.toLowerCase());
            prestamosCliente2 = Funciones.getPrestamosPorCli(todosLosPrestamos,Integer.toString(cliAux.getCod()));
        }
        
        listaPrestamos.removeAll();
        //Recorrer el contenido del ArrayList
//        double totalPrestado = 0.0;
        double totalAdeudado = 0.0;
        
        ArrayList<String> planesCli = new ArrayList<>();
        for(Prestamo prestaX: prestamosCliente2) {
            for (Cuota cuotaX: prestaX.getCuotas()) {
//                totalPrestado += Double.parseDouble(cuotaX.getImporteTotal());
                totalAdeudado += Double.parseDouble(cuotaX.getImporteTotal())-Double.parseDouble(cuotaX.getImportePagado());
                
            }
//            lbTotalPrestado.setText(Double.toString(Funciones.formatearDecimales(totalPrestado, 2)));
            planesCli.add(prestaX.getPlan());
            
            
                    
            listaPrestamos.add("["+Integer.toString(prestaX.getCod())+ "]" +" "+ prestaX.getPlan().toUpperCase());    
        }
        
        lbCantPrestamos.setText(Integer.toString(prestamosCliente2.size()));
        
        double totalPrestado = 0.0;
        for (String plan: planesCli) {
            totalPrestado += Double.parseDouble(Funciones.getPlanPorNombre(plan).getImportePrestar());
        }
        
        lbTotalPrestado.setText(Double.toString(Funciones.formatearDecimales(totalPrestado, 2)));
        lbTotalAdeudado.setText(Double.toString(Funciones.formatearDecimales(totalAdeudado, 2)));
        
        }
        
        
            
        //BLANQUEA DATOS PRESTAMO
        blanqueaDatosPrestamo();
        
        modeloCuotasPrestamo = new TMVencimiento(new ArrayList<Cuota>());
        tablaCuotasPrestamo.setModel(modeloCuotasPrestamo);
    }
    
    private void tablaVencDiariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaVencDiariosMouseClicked

    }//GEN-LAST:event_tablaVencDiariosMouseClicked

    private void tablaVencSemanalesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaVencSemanalesMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tablaVencSemanalesMouseClicked

    private void morososActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_morososActionPerformed
        cargarPrestamosYCuotas();
        if (morosos.isSelected()) {
            mAnt.setEnabled(true);
            diasVInicial.setEnabled(true);
            dAnt.setEnabled(true);
            if (diasVInicial.getText().equals("")) {
                diasVInicial.setText("60");
            }
        } else {
            mAnt.setEnabled(false);
            diasVInicial.setEnabled(false);
            dAnt.setEnabled(false);
        }
    }//GEN-LAST:event_morososActionPerformed

    private void mostrarPagasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mostrarPagasActionPerformed
        cargarPrestamosYCuotas();
    }//GEN-LAST:event_mostrarPagasActionPerformed

    private void botonEliminarPrestamoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarPrestamoActionPerformed
        String cliS = (String) comboClientes.getSelectedItem();
        int dialogResult = JOptionPane.showConfirmDialog(null, "¿Desea eliminar el préstamo "+planPrest.getText()+"?\n\nCliente: "+Funciones.capitalize(cliS)+"\nGenerado el: "+fechaPrest.getText()+"\n\nEL MISMO NO PODRÁ SER RECUPERADO.", "¡¡¡ATENCIÓN!!!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            int codigoBorrar = Funciones.getCodPorPrestamoStr(listaPrestamos.getSelectedItem());
            Prestamo prest = Funciones.getPrestamoPorCod(todosLosPrestamos, codigoBorrar);
            con.conectar();
            con.eliminarPrestamoPorCod(codigoBorrar);
            con.eliminarCuotaPorCodPrestamo(Integer.toString(codigoBorrar));
            //BORRA PLAN PERS
            Plan p = Funciones.getPrestamoPorCod(todosLosPrestamos, codigoBorrar).getPlanObj();
            if (p.getA4().equalsIgnoreCase("personalizado")) {
                con.eliminarPlanPorNombre(p.getNombre());
            }
            con.cerrar();
            
            WorkersApp.eliminarPrestYCuotas(prest);
            Workers.workerRefresh();
            blanquearCamposPrestamos();
        }    
        
    }//GEN-LAST:event_botonEliminarPrestamoActionPerformed

    private void botonImprimirPrestamoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonImprimirPrestamoActionPerformed
        InputStream inputStream = null;
        JasperPrint jasperPrint= null;
        PrestamoDataSource datasource = new PrestamoDataSource();
        String clienteSel = (String) comboClientes.getSelectedItem();
        cliAux = Funciones.getClientePorNombre(clientes, clienteSel.toLowerCase());
        datasource.cargarCliente(cliAux);
        String[] referenciaAux1 = cliAux.getRef1().split("-i-");
        String[] referenciaAux2 = cliAux.getRef2().split("-i-");
        String[] referenciaAux3 = cliAux.getRef3().split("-i-");
        String[] referenciaAux4 = cliAux.getRef4().split("-i-");
        
        datasource.cargarRef1(referenciaAux1);
        datasource.cargarRef2(referenciaAux2);
        datasource.cargarRef3(referenciaAux3);
        datasource.cargarRef4(referenciaAux4);
        
        datasource.agregarPrestamo(prestamoEnPrestamos);
        datasource.cargarCuotas(cuotasPrestamo);
        
        Conector con = new Conector();
        con.conectar();
        ArrayList<PagoParcial> pagos = con.getPagosParcialesCond(null, null, 0, prestamoEnPrestamos.getCod(), 0);
        con.cerrar();
        String pagosP = "Pagos parciales: ";
        for (PagoParcial p: pagos) {
            pagosP += "$"+Double.toString(Funciones.formatearDecimales(p.getImporte(),2))+" ("+Funciones.dateAString(p.getFecha())+")  -";
        }
        datasource.setPagosParciales(pagosP);
        
         
       try {
            inputStream = new FileInputStream ("reportePrestamo.jrxml");
        } catch (FileNotFoundException ex) {
           javax.swing.JOptionPane.showMessageDialog(null,"Error al leer el fichero de carga reportePrestamo\n "+ex.getMessage());
        }
         
        try{
            JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            jasperPrint = JasperFillManager.fillReport(jasperReport, null, datasource);

            //JasperExportManager.exportReportToPdfFile(jasperPrint, "src/agencia/reporte01.pdf");
            //JasperViewer.viewReport(jasperPrint, false);
            JasperViewer jv = new JasperViewer(jasperPrint, false);
            jv.setTitle("Informe de préstamo");
            jv.setVisible(true);
           
        }catch (JRException e){
            javax.swing.JOptionPane.showMessageDialog(null,"Error al cargar fichero jrml jasper report "+e.getMessage());
        }
        
    }//GEN-LAST:event_botonImprimirPrestamoActionPerformed

    private void listaPrestamosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listaPrestamosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_listaPrestamosActionPerformed

    private void tablaCuotasPrestamoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaCuotasPrestamoMouseClicked
        botonAnular.setEnabled(true);
        botonReciboA4.setEnabled(true);
        botonCambiarVenc.setEnabled(true);
        
        int cods = (int) tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 0);
        Cuota cX = Funciones.getCuotaPorCod(cods);
        
        //DIAS ATRASO
        String estado = (String) tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 7);
        if (estado.equals("PAGADA") || estado.equals("PARCIAL")) {
            String textoPagada = "Cuota Nº"+cX.getNumCuota()+" - Pagada el día "+cX.getFechaPagado()+" ("+Funciones.getDiaSemana(Funciones.stringADate(cX.getFechaPagado()))+")";
            if (cX.getDiasAtrasoPago()>0) {
                if (cX.getImporteInteres().equals("no") || cX.getImporteInteres().equals("0.0")) {
                    textoPagada = textoPagada + " [Atraso: "+cX.getDiasAtrasoPago() +" días / Recargo: NO COBRADO]";
                } else {
                    textoPagada = textoPagada + " [Atraso: "+cX.getDiasAtrasoPago() +" días / Recargo: $"+cX.getInteresCalculado()+"]";
                }
                
            }
            lbPagado.setText(textoPagada);
            
            //DIAS ATRASO
            
        } else {
            String textoPagada = "La cuota no fue pagada.";
            String impInteres = Double.toString(Funciones.formatearDecimales(Funciones.getInteresCalculadoPorDiasAtrasoPorcentaje(cX),2));
            if (cX.getDiasAtrasoHastaHoy()>0) {
                textoPagada = textoPagada + " [Atraso: "+cX.getDiasAtrasoHastaHoy() +" días / Recargo: $"+impInteres+" (Al " +Funciones.devolverFechaActualStr()+ " "+Double.toString(Principal.opciones.getInteresdia())+"% por día)]";
            }
            
            lbPagado.setText(textoPagada);
        }
        
    }//GEN-LAST:event_tablaCuotasPrestamoMouseClicked

    private void botonAgregarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarUsuarioActionPerformed
        Usuario usuario = new Usuario();

        usuario.setNombre(nombreUsrNew.getText());
        usuario.setPassword(String.valueOf(password.getPassword()));
        usuario.setTipo((String)comboTipo.getSelectedItem());

        con.conectar();
        con.guardarUsuario(usuario);
        con.cerrar();
        
        Principal.cargarUsuarios();

    }//GEN-LAST:event_botonAgregarUsuarioActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Acceso acceso;
        try {
            acceso = new Acceso();
            acceso.setTitle("Ingreso al sistema");
        acceso.setVisible   (true);
        this.dispose();
        } catch (InterruptedException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void itemSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemSalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_itemSalirActionPerformed

    private void botonActivarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonActivarActionPerformed
        if (serial != null) {//si existe una venta, la cierra.
            serial.dispose();
        }
        
        serial = new Serial();
        serial.setTitle("Ingresar código");
        serial.setVisible(true);
    }//GEN-LAST:event_botonActivarActionPerformed

    private void btnGuardarOpt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarOpt1ActionPerformed
        guardarOpciones();
    }//GEN-LAST:event_btnGuardarOpt1ActionPerformed

    private void mostrarPagasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mostrarPagasMouseClicked
        guardarOpciones();
    }//GEN-LAST:event_mostrarPagasMouseClicked

    private void morososMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_morososMouseClicked
        guardarOpciones();
    }//GEN-LAST:event_morososMouseClicked

    private void nombreBusquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nombreBusquedaKeyReleased
        if (!nombreBusqueda.getText().equals("")) {
            dniBusqueda.setText("");
            dniBusqueda.setEnabled(false);
            cargarClientesBuscados(Funciones.getClientesPorNombre(nombreBusqueda.getText()));
        } else {
            cargarClientes();
            dniBusqueda.setEnabled(true);
        }
    }//GEN-LAST:event_nombreBusquedaKeyReleased

    private void btnGuardarDatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarDatosActionPerformed
        guardarOpciones();
    }//GEN-LAST:event_btnGuardarDatosActionPerformed

    private void tablaClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaClientesMouseClicked
        try {
            int cod = (int) tablaClientes.getValueAt(tablaClientes.getSelectedRow(), 0);
        mostrarCliente(Funciones.getClientePorCod(clientes, cod));
        } catch (Exception e) {
            
        }
        
    }//GEN-LAST:event_tablaClientesMouseClicked

    private void botonImprimirPrestamo1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonImprimirPrestamo1ActionPerformed
        InputStream inputStream = null;
        JasperPrint jasperPrint= null;
        CuotasDiaDataSource datasource = new CuotasDiaDataSource();
        String fechaAct = Funciones.devolverFechaActualStr();
        ArrayList<Cuota> cuotasDiaPrn = new ArrayList<Cuota>();
        double totalCob = 0;
        for (Cuota cuotaAuxPrn: todasLasCuotas) {
            
            //CARGA LAS CUOTAS CON ANTERIOR
            if (parcialesCheck.isSelected()) {
                if (Funciones.stringADate(cuotaAuxPrn.getFechaPagar()).before(Funciones.stringADate(fechaAct)) && (Double.parseDouble(cuotaAuxPrn.getImporteTotal()) - Double.parseDouble(cuotaAuxPrn.getImportePagado()) > 1)) {
                    cuotasDiaPrn.add(cuotaAuxPrn);
                    totalCob += Double.parseDouble(cuotaAuxPrn.getImporteTotal()) - Double.parseDouble(cuotaAuxPrn.getImportePagado()); 
                }   
            }
            
            //CARGA LAS CUOTAS CON FECHA DEL DIA
            if (cuotaAuxPrn.getFechaPagar().equals(fechaAct)) {
                cuotasDiaPrn.add(cuotaAuxPrn);
                totalCob += Double.parseDouble(cuotaAuxPrn.getImporteTotal()) - Double.parseDouble(cuotaAuxPrn.getImportePagado()); 
            }
        }
            
       
        
        datasource.cargarTotal(Funciones.formatearDecimales(totalCob, 2));
        datasource.cargarCuotas(cuotasDiaPrn);
         
       try {
            inputStream = new FileInputStream ("reporteCuotasDia.jrxml");
        } catch (FileNotFoundException ex) {
           javax.swing.JOptionPane.showMessageDialog(null,"Error al leer el fichero de carga reportePrestamo\n "+ex.getMessage());
        }
         
        try{
            JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            jasperPrint = JasperFillManager.fillReport(jasperReport, null, datasource);

            //JasperExportManager.exportReportToPdfFile(jasperPrint, "src/agencia/reporte01.pdf");
            //JasperViewer.viewReport(jasperPrint, false);
            JasperViewer jv = new JasperViewer(jasperPrint, false);
            jv.setTitle("Informe de cuotas del día");
            jv.setVisible(true);
           
        }catch (JRException e){
            javax.swing.JOptionPane.showMessageDialog(null,"Error al cargar fichero jrml jasper report "+e.getMessage());
        }
    }//GEN-LAST:event_botonImprimirPrestamo1ActionPerformed

    private void botonImprimirPrestamo2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonImprimirPrestamo2ActionPerformed
        InputStream inputStream = null;
        JasperPrint jasperPrint= null;
        CuotasDiaDataSource datasource = new CuotasDiaDataSource();
        Date fechaActDate = Funciones.stringADate(Funciones.devolverFechaActualStr());
        ArrayList<Cuota> cuotasSemanalesPrn = new ArrayList<Cuota>();
        double totalCob = 0;
        
         //Carga cuotas de la semana
        Date fechaActDateMas7 = Funciones.sumarDias(fechaActDate, 7);
        for (Cuota cuota: todasLasCuotas) {
            Date fechaPagar = Funciones.stringADate(cuota.getFechaPagar());
            
            if ((fechaPagar.after(fechaActDate) || fechaPagar.equals(fechaActDate)) && (fechaPagar.before(fechaActDateMas7) || fechaPagar.equals(fechaActDateMas7))) {
                cuotasSemanalesPrn.add(cuota);
                totalCob += Double.parseDouble(cuota.getImporteTotal()) - Double.parseDouble(cuota.getImportePagado()); 
            }
        }
        
        datasource.cargarTotal(Funciones.formatearDecimales(totalCob, 2));
        datasource.cargarCuotas(cuotasSemanalesPrn);
         
       try {
            inputStream = new FileInputStream ("reporteCuotas7Dias.jrxml");
        } catch (FileNotFoundException ex) {
           javax.swing.JOptionPane.showMessageDialog(null,"Error al leer el fichero de carga reportePrestamo\n "+ex.getMessage());
        }
         
        try{
            JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            jasperPrint = JasperFillManager.fillReport(jasperReport, null, datasource);

            //JasperExportManager.exportReportToPdfFile(jasperPrint, "src/agencia/reporte01.pdf");
            //JasperViewer.viewReport(jasperPrint, false);
            JasperViewer jv = new JasperViewer(jasperPrint, false);
            jv.setTitle("Informe de cuotas a 7 dìas");
            jv.setVisible(true);
           
        }catch (JRException e){
            javax.swing.JOptionPane.showMessageDialog(null,"Error al cargar fichero jrml jasper report "+e.getMessage());
        }
    }//GEN-LAST:event_botonImprimirPrestamo2ActionPerformed

    private void comboTipoIntActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboTipoIntActionPerformed
        String sel = (String) comboTipoInt.getSelectedItem();
        if (sel.equals("Efectivo")) {
            lbTipoInt.setText("$");
        } else {
            lbTipoInt.setText("%");
        }
        
    }//GEN-LAST:event_comboTipoIntActionPerformed

    private void botonAnularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAnularActionPerformed
        int csAux = (int) tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 0);
        String fechaAux = (String) tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 1);
        String clAux = (String) tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 2);
        String planAux = (String) tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 3);
//        String numCuotaAux = (String) tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 4);
        String impTotalAux = (String) tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 5);
        String impPagadoAux = (String) tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 6);
        
        int dialogResult = JOptionPane.showConfirmDialog(null, "¿Desea anular el pago de la cuota?\n\nCliente: "+clAux+"\nPlan: "+planAux+"\ncód: "+Integer.toString(csAux)+"\nVencimiento: "+fechaAux+"\nImporte: $"+impTotalAux+"\nPagado: $"+impPagadoAux, "¡¡¡ATENCIÓN!!!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
          Principal.con.conectar();
          Principal.con.anularPagoCuota(csAux);
          Principal.con.setCobPorCod(csAux, "");
          Principal.con.cerrar();
          
          Principal.cargarPrestamosYCuotas();
          
          //ACTUALIZA EN LA APP
          WorkersApp.guardarPrestamo(Funciones.getPrestamoPorCod(todosLosPrestamos, Funciones.getCuotaPorCod(csAux).getCodPrestamoInt()));
        }
        
        String clienteSel = (String) comboClientes.getSelectedItem();
        if (clienteSel != null) {
            
        
        if (clienteSel.equals("Sin especificar")) {
            prestamosCliente2 = new ArrayList<Prestamo>();
        } else {
            cliAux = Funciones.getClientePorNombre(clientes, clienteSel.toLowerCase());
            prestamosCliente2 = Funciones.getPrestamosPorCli(todosLosPrestamos,Integer.toString(cliAux.getCod()));
        }
        
        listaPrestamos.removeAll();
        //Recorrer el contenido del ArrayList
        double totalPrestado = 0.0;
        double totalAdeudado = 0.0;
        
        for(Prestamo prestaX: prestamosCliente2) {
            for (Cuota cuotaX: prestaX.getCuotas()) {
                totalPrestado += Double.parseDouble(cuotaX.getImporteTotal());
                totalAdeudado += Double.parseDouble(cuotaX.getImporteTotal())-Double.parseDouble(cuotaX.getImportePagado());
                
            }
            lbTotalPrestado.setText(Double.toString(Funciones.formatearDecimales(totalPrestado, 2)));
            lbTotalAdeudado.setText(Double.toString(Funciones.formatearDecimales(totalAdeudado, 2)));
            
                    
            listaPrestamos.add("["+Integer.toString(prestaX.getCod())+ "]" +" "+ prestaX.getPlan().toUpperCase());    
        }
        
        lbCantPrestamos.setText(Integer.toString(prestamosCliente2.size()));
        }
        
        //BLANQUEA DATOS PRESTAMO
       blanqueaDatosPrestamo();
       
       
       
    }//GEN-LAST:event_botonAnularActionPerformed

    private static void blanqueaDatosPrestamo() {
        planPrest.setText("NINGÚN PRESTAMO SELECCIONADO");
        codPrest.setText("0");
        cuotasPrest.setText("0");
        valorCuotaPrest.setText("$0");
        totalPrest.setText("$0");
        pagadoPrest.setText("$0");
        adeudadoPrest.setText("$0");
        obsPrestamo.setText("");
        
        botonEliminarPrestamo.setEnabled(false);
        btnCobrarPrestamo.setEnabled(false);
        btnRefinanciar.setEnabled(false);
        btnCarton.setEnabled(false);
        botonImprimirPrestamo.setEnabled(false);
        botonAnular.setEnabled(false);
        botonReciboA4.setEnabled(false);
    }
    private void guardarObsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardarObsBtnActionPerformed
        prestamoEnPrestamos.setObservacion(obsPrestamo.getText());
        Principal.con.conectar();
        Principal.con.actualizarPrestamo(prestamoEnPrestamos);
        Principal.con.cerrar();
    }//GEN-LAST:event_guardarObsBtnActionPerformed
    
    private void actListaPlanes() {
        listaPlanes.removeAll();
        //Recorrer el contenido del ArrayList
        for(Plan planX: planes) {
            if (!planX.getNombre().contains("-app") && planX.getNombre().toUpperCase().contains(buscaPlan.getText().toUpperCase())) {
                if (pTodos.isSelected()) {
                    listaPlanes.add(planX.getNombre().toUpperCase());    
                } else if (pPrestamos.isSelected()) {
                    if (planX.getA1().equals("")) {
                        listaPlanes.add(planX.getNombre().toUpperCase());    
                    }
                } else {
                    if (planX.getA1().equals("producto")) {
                        listaPlanes.add(planX.getNombre().toUpperCase());    
                    }
                }
                
            }
            
        }
        cantPlanes.setText(Integer.toString(listaPlanes.getItemCount()));
    }
    private void buscaPlanKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_buscaPlanKeyReleased
        actListaPlanes();
    }//GEN-LAST:event_buscaPlanKeyReleased

    private void btnVertodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVertodosActionPerformed
       PanelPrincipal.setSelectedIndex(3);
       comboClientes.setSelectedItem(nombre.getText());
    }//GEN-LAST:event_btnVertodosActionPerformed

    private void nombreBusquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nombreBusquedaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nombreBusquedaActionPerformed

    private void dniBusquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dniBusquedaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dniBusquedaActionPerformed

    private void dniBusquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dniBusquedaKeyReleased
        if (!dniBusqueda.getText().equals("")) {
            nombreBusqueda.setText("");
            nombreBusqueda.setEnabled(false);
            cargarClientesBuscados(Funciones.getClientesPorDni(dniBusqueda.getText()));
        } else {
            cargarClientes();
            nombreBusqueda.setEnabled(true);
        }
    }//GEN-LAST:event_dniBusquedaKeyReleased

    private void ocultarPagasPrestamoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ocultarPagasPrestamoActionPerformed
        if (ocultarPagasPrestamo.isSelected()) {
            opciones.setExtra2("si");
        } else {
            opciones.setExtra2("no");
        }
        con.conectar();
        con.guardarOpciones(opciones);
        con.cerrar();
        
    }//GEN-LAST:event_ocultarPagasPrestamoActionPerformed

    private void botonReciboA4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonReciboA4ActionPerformed
        String estado = (String) tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 7);
        int cods = (int) tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 0);
        Cuota cX = Funciones.getCuotaPorCod(cods);
        lbPagado.setText("Cuota Nº"+cX.getNumCuota()+" - Pagada el día "+cX.getFechaPagado()+" ("+Funciones.getDiaSemana(Funciones.stringADate(cX.getFechaPagado()))+")");

        String fecha = Funciones.getFechaTexto(Funciones.stringADate(cX.getFechaPagado()));
        Cliente cx = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, cX.getCodPrestamoInt()).getCodCliente()));
        String cliente = Funciones.capitalize(cx.getNombre());
        String cantidad = Funciones.importeDoubleALetras(cX.getImportePagadoD())+" pesos.";
        Prestamo p = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, cX.getCodPrestamoInt());
        Plan pn = Funciones.getPlanPorNombre(p.getPlan());

        String concepto = "";
        if (cX.getNumCuota().equals("0")) {
            concepto = "ANTICIPO - "+pn.getNombre().toUpperCase();
        } else {
            concepto = "CUOTA N°"+cX.getNumCuota()+" de "+pn.getCantCuotas()+"  -  "+pn.getNombre().toUpperCase();
        }

        String impefectivo = "$"+cX.getImportePagadoD();
        String impTotal1 = "$"+cX.getImportePagadoD();
        String obs1 = "Observaciones: "+Funciones.leerObsA4();
        
        if (estado.equals("PAGADA")) {
            if (Principal.opcionesExtra.getX2().equals("SI")) {
                //XLS
                DatosComprobante d = new DatosComprobante();
                
                d.setCant1("1");
                d.setDetalle1(concepto);
                d.setMonto1(impTotal1);
                
                d.setDireccion(Funciones.capitalize(cx.getDireccion()));
                d.setFecha(cX.getFechaPagado());
                d.setNombre(cliente);
                d.setNumero("0001-"+Funciones.formatearNumRecibo(Integer.toString(cX.getCod())));
                d.setObs(obs1);
                d.setTelefono(cx.getTelefono()+ "   "+cx.getCelular());
                d.setTipo("RECIBO");
                d.setTotal(impTotal1);
                
                TestExcel.rellenarXls(d);
                
            } else {
                //PDF
                InputStream inputStream = null;
                JasperPrint jasperPrint= null;
                ReciboA4DataSource datasource = new ReciboA4DataSource();

                datasource.setCantidad(cantidad);
                datasource.setCliente(cliente);
                datasource.setConcepto(concepto);
                datasource.setFecha(fecha);
                datasource.setImpefectivo(impefectivo);
                datasource.setImptotal(impTotal1);
                datasource.setObs(obs1);


               try {
                    inputStream = new FileInputStream ("reporteRecibo1.jrxml");
                } catch (FileNotFoundException ex) {
                   javax.swing.JOptionPane.showMessageDialog(null,"Error al leer el fichero de carga reporteRecibo1\n "+ex.getMessage());
                }

                try{
                    JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
                    JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                    jasperPrint = JasperFillManager.fillReport(jasperReport, null, datasource);

                    //JasperExportManager.exportReportToPdfFile(jasperPrint, "src/agencia/reporte01.pdf");
                    //JasperViewer.viewReport(jasperPrint, false);
                    JasperViewer jv = new JasperViewer(jasperPrint, false);
                    jv.setTitle("Recibo A4");
                    jv.setVisible(true);

                }catch (JRException e){
                    javax.swing.JOptionPane.showMessageDialog(null,"Error al cargar fichero jrml jasper report "+e.getMessage());
                }
            }
            
            
        } else {
            JOptionPane.showMessageDialog(null,"La cuota aún no fue pagada.","Información",0);
            
        }
        
    }//GEN-LAST:event_botonReciboA4ActionPerformed

    private void btnRefinanciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefinanciarActionPerformed
        int filas = tablaCuotasPrestamo.getRowCount();
        double importeNuevo = 0.0;
        for (int fila = 0; fila < filas; fila++) {
//            System.out.println(fila);
            String estado = (String) tablaCuotasPrestamo.getValueAt(fila, 7);            
            if (estado.equals("PENDIENTE") || estado.equals("PARCIAL") || estado.equals("ATRASADA")) {
                String importeCuota = (String) tablaCuotasPrestamo.getValueAt(fila, 5);            
                double importeCuotaD = Double.parseDouble(importeCuota);
                String importePagado = (String) tablaCuotasPrestamo.getValueAt(fila, 6);            
                double importePagadoD = Double.parseDouble(importePagado);
                
                if (importePagadoD<importeCuotaD) {
                    importeCuotaD = importeCuotaD - importePagadoD;
                }
                
                importeNuevo += importeCuotaD;
            }
        }
        importeNuevo = Funciones.formatearDecimales(importeNuevo, 0);
//        String csAux = (String) tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 0);
        
        String cliS = (String) comboClientes.getSelectedItem();
        int dialogResult = JOptionPane.showConfirmDialog(null, "¿Desea refinanciar el préstamo "+planPrest.getText()+"?\n\nCliente: "+Funciones.capitalize(cliS)+"\nGenerado el: "+fechaPrest.getText()+"\nImporte a refinanciar: $"+Double.toString(importeNuevo)+"\n\nEl préstamo se marcará como pagado y se generará\nun nuevo préstamo con el saldo del anterior.", "¡¡¡ATENCIÓN!!!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            Refinanciar rf = new Refinanciar(planPrest.getText().toLowerCase(), importeNuevo,fechaPrest.getText(),codPrest.getText(), "refinancia");
            rf.setTitle("Refinanciar préstamo");
            rf.setVisible(true);
        }
    }//GEN-LAST:event_btnRefinanciarActionPerformed

    private void inicioTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inicioTotalActionPerformed
    }//GEN-LAST:event_inicioTotalActionPerformed

    private void finTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finTotalActionPerformed
    }//GEN-LAST:event_finTotalActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        if (totales != null) {//si existe una venta, la cierra.
            totales.dispose();
        }
        
        totales = new Totales();
        totales.setTitle("Totales");
        totales.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void mAntActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mAntActionPerformed
        cargarPrestamosYCuotas();
    }//GEN-LAST:event_mAntActionPerformed

    private void ocultarFinalizadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ocultarFinalizadosActionPerformed
        JOptionPane.showMessageDialog(null,"Debe reiniciar el programa para ver reflejado este cambio en los listados.", "Reinicie el programa",1);
        if (ocultarFinalizados.isSelected()) {
            Funciones.guardarOcultaFinalizados(true);
        } else {
            Funciones.guardarOcultaFinalizados(false);
        }
    }//GEN-LAST:event_ocultarFinalizadosActionPerformed

    private void PanelPrincipalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PanelPrincipalMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_PanelPrincipalMouseClicked

    private void actListado1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actListado1ActionPerformed
        workerRefresh();
    }//GEN-LAST:event_actListado1ActionPerformed

    private void actListado2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actListado2ActionPerformed
        workerRefresh();
    }//GEN-LAST:event_actListado2ActionPerformed

    private void actListado3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actListado3ActionPerformed
        workerRefresh();
    }//GEN-LAST:event_actListado3ActionPerformed

    private void botonBlanquearCobActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBlanquearCobActionPerformed
        blanquearCamposCobrador();
    }//GEN-LAST:event_botonBlanquearCobActionPerformed

    private void botonActualizarCobActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonActualizarCobActionPerformed
        boolean guardadoX = false;
            for (Cobrador cAux11: Principal.cobradores) {
                if (cAux11.getNombre().toLowerCase().equals(nombreCob.getText().toLowerCase())) {
                    guardadoX = true;
                }
            }    
                    boolean rutaX = false;
            for (Cobrador cAux11: Principal.cobradores) {
                if (cAux11.getCodInt().toLowerCase().equals(codIntCob.getText().toLowerCase())) {
                    rutaX = true;
                }
            } 
        
        if (nombreCob.getText().toLowerCase().equals("")) {
                JOptionPane.showMessageDialog(null,"Debe ingresar un nombre.", "Ingrese un nombre", 1);
        } else if (!Funciones.validarFormatoRuta(codIntCob.getText())) {
                JOptionPane.showMessageDialog(null,"Formato de ruta inválido.\n\nEl formato debe ser de tipo numérico y de 3 dígitos.\nEJEMPLOS: 014 / 328 / 140", "Ingrese un nombre", 1);
        } else if (guardadoX) {
                //JOptionPane.showMessageDialog(null,"Ya existe un cobrador con ese nombre, modifique el nombre.", "Nombre existente", 1);
                Cobrador cN = new Cobrador();
                cN.setCod(Integer.parseInt(codCob.getText()));
                cN.setNombre(nombreCob.getText());
                cN.setTelefono(telCob.getText());
                cN.setCodInt(codIntCob.getText());
                cN.setNombreApp(nombreApp.getText());
                con.conectar();
                con.actualizarCobrador(cN);
                con.cerrar();

                cargarCobradores();
                blanquearCamposCobrador();
        } else {
            Cobrador cN = new Cobrador();
            cN.setCod(Integer.parseInt(codCob.getText()));
            cN.setNombre(nombreCob.getText());
            cN.setTelefono(telCob.getText());
            cN.setCodInt(codIntCob.getText());
            cN.setNombreApp(nombreApp.getText());
            con.conectar();
            con.actualizarCobrador(cN);
            con.cerrar();

            cargarCobradores();
            blanquearCamposCobrador();
        }
            
            
        
    }//GEN-LAST:event_botonActualizarCobActionPerformed

    private void botonGuardarCobActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarCobActionPerformed
         boolean guardadoX = false;
            for (Cobrador cAux11: Principal.cobradores) {
                if (cAux11.getNombre().toLowerCase().equals(nombreCob.getText().toLowerCase())) {
                    guardadoX = true;
                }
                if (cAux11.getCodInt().toLowerCase().equals(codIntCob.getText().toLowerCase())) {
                    guardadoX = true;
                }
            }    
            if (guardadoX) {
                JOptionPane.showMessageDialog(null,"Ya existe un cobrador con ese nombre o ruta, modifique el nombre.", "Nombre existente", 1);
            } else if (nombreCob.getText().toLowerCase().equals("")) {
                JOptionPane.showMessageDialog(null,"Debe ingresar un nombre.", "Ingrese un nombre", 1);
            } else if (!Funciones.validarFormatoRuta(codIntCob.getText())) {
                JOptionPane.showMessageDialog(null,"Formato de ruta inválido.\n\nEl formato debe ser de tipo numérico y de 3 dígitos.\nEJEMPLOS: 014 / 328 / 140", "Ingrese un nombre", 1);
            } else {
                System.out.println("guardando");
                Cobrador cN = new Cobrador();
                cN.setNombre(nombreCob.getText());
                cN.setCodInt(codIntCob.getText());
                cN.setObs(obsCob.getText());
                cN.setTelefono(telCob.getText());
                cN.setNombreApp(nombreApp.getText());

                con.conectar();
                con.guardarCobrador(cN);
                con.cerrar();
                blanquearCamposCobrador();
                cargarCobradores();
                
            }
    }//GEN-LAST:event_botonGuardarCobActionPerformed

    private void botonEliminarCobActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarCobActionPerformed
        int dialogResult = JOptionPane.showConfirmDialog(null, "Desea eliminar a "+Funciones.capitalize(nombreCob.getText())+"?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            con.conectar();
            con.eliminarCobradorPorCod(Integer.parseInt(codCob.getText()));
            con.cerrar();
            
        }
        blanquearCamposCobrador();
        cargarCobradores();
    }//GEN-LAST:event_botonEliminarCobActionPerformed

    private void btnVertodosCobActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVertodosCobActionPerformed
        ArrayList<Prestamo> ps = new ArrayList<Prestamo>();
        for (Prestamo pX2: todosLosPrestamos) {
                if (globales.isSelected()) {
                     if (muestraFin.isSelected()) {
                        ps.add(pX2);
                    } else {
                        
                        ArrayList<Cuota> cs = pX2.getCuotas();
                        boolean termino = true;
                        for (Cuota cx: cs) {
                            termino = termino && cx.yaSePago();
                        }
                        if (!termino) {
                            ps.add(pX2);
                        }
                    
                    
                }    
                } else {
                    
                
//SI MUESTRA TODOS DEL COB
                if (pX2.getCodCobrador().equals(codIntCob.getText())) {
                    if (muestraFin.isSelected()) {
                        ps.add(pX2);
                    } else {
                        
                        ArrayList<Cuota> cs = pX2.getCuotas();
                        boolean termino = true;
                        for (Cuota cx: cs) {
                            termino = termino && cx.yaSePago();
                        }
                        if (!termino) {
                            ps.add(pX2);
                        }
                    
                    
                }    
           
             }
            }
            
        }
        if (lPrestamos != null) {//si existe una venta, la cierra.
            lPrestamos.dispose();
        }
        
        lPrestamos = new ListadoPrestamos(ps);
        lPrestamos.setTitle("Préstamos");
        lPrestamos.setVisible(true);
    }//GEN-LAST:event_btnVertodosCobActionPerformed

    private void nombreBusqueda1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nombreBusqueda1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nombreBusqueda1ActionPerformed

    private void nombreBusqueda1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nombreBusqueda1KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_nombreBusqueda1KeyReleased

    private void actListado4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actListado4ActionPerformed
        WorkersApp.cargarUsuariosApp();
    }//GEN-LAST:event_actListado4ActionPerformed

    private void tablaCobradoresMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaCobradoresMouseClicked
        Integer codStr = (Integer) tablaCobradores.getValueAt(tablaCobradores.getSelectedRow(), 0);
        mostrarCobrador(Funciones.getCobradorPorCod(cobradores, codStr));
    }//GEN-LAST:event_tablaCobradoresMouseClicked

    private void tablaClientesMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaClientesMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_tablaClientesMouseEntered

    private void modifCobActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifCobActionPerformed
        if (mCobrador != null) {//si existe una venta, la cierra.
            mCobrador.dispose();
        }
        
        mCobrador = new ModifCobrador(prestamoEnPrestamos);
        mCobrador.setTitle("Seleccionar Cobrador");
        mCobrador.setVisible(true);
    }//GEN-LAST:event_modifCobActionPerformed

    private void btnListaCobradorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListaCobradorActionPerformed
        if (lCobrador != null) {//si existe una venta, la cierra.
            lCobrador.dispose();
        }
        
        lCobrador = new ListadoPorCob();
        lCobrador.setTitle("Listado por Cobrador");
        lCobrador.setVisible(true);
    }//GEN-LAST:event_btnListaCobradorActionPerformed

    private void marcarFinalizadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_marcarFinalizadosActionPerformed
        guardarOpcionesExtra();
    }//GEN-LAST:event_marcarFinalizadosActionPerformed

    private void habImpresionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_habImpresionActionPerformed
        guardarOpcionesExtra();
    }//GEN-LAST:event_habImpresionActionPerformed

    private void comboRutasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboRutasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboRutasActionPerformed

    private void comboLocActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboLocActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboLocActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        if (locWindow != null) {//si existe una venta, la cierra.
            locWindow.dispose();
        }
        
        locWindow = new Localidades();
        locWindow.setTitle("Localidades");
        locWindow.setVisible(true);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void btnListaZonaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListaZonaActionPerformed
                if (lLocalidades != null) {//si existe una venta, la cierra.
            lLocalidades.dispose();
        }
        
        lLocalidades = new ListadoPorLocalidad();
        lLocalidades.setTitle("Listado por Localidades");
        lLocalidades.setVisible(true);
    }//GEN-LAST:event_btnListaZonaActionPerformed

    private void modifCob2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifCob2ActionPerformed
        RutaXLS rutaXLS = new RutaXLS();
        rutaXLS.setVisible(true);
        rutaXLS.setTitle("Seleccione archivo XLS a importar");
    }//GEN-LAST:event_modifCob2ActionPerformed

    private void modifCob3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifCob3ActionPerformed
        RutaXLSVarios rutaXLSV = new RutaXLSVarios();
        rutaXLSV.setVisible(true);
        rutaXLSV.setTitle("Seleccione archivo XLS a importar");
    }//GEN-LAST:event_modifCob3ActionPerformed

    private void porcCobradorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_porcCobradorKeyReleased
       recalcularValorPlan();
    }//GEN-LAST:event_porcCobradorKeyReleased

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        RutaLogo rutaLogo = new RutaLogo();
        rutaLogo.setTitle("Cambiar ruta Logo");
        rutaLogo.setVisible(true);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
                       
        
        try {
            Path origenPath = FileSystems.getDefault().getPath("C:\\Prestav2\\nologo.jpg");
            Path destinoPath = FileSystems.getDefault().getPath("C:\\Prestav2\\logo.jpg");
            //sobreescribir el fichero de destino si existe y lo copia
            Files.copy(origenPath, destinoPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null,"Error al intentar cambiar el logo. \n" + ex.getMessage(),"Error",1);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,"Error al intentar cambiar el logo. \n" + ex.getMessage(),"Error",1);
        }
        
    }//GEN-LAST:event_jButton6ActionPerformed

    private void btnLista30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLista30ActionPerformed
        if (listaDias != null) {//si existe una venta, la cierra.
            listaDias.dispose();
        }
        
        listaDias = new ListadoPorXDias(30);
        listaDias.setTitle("Listado 30 días");
        listaDias.setVisible(true);
    }//GEN-LAST:event_btnLista30ActionPerformed

    private void btnLista60ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLista60ActionPerformed
        if (listaDias != null) {//si existe una venta, la cierra.
            listaDias.dispose();
        }
        
        listaDias = new ListadoPorXDias(60);
        listaDias.setTitle("Listado 60 días");
        listaDias.setVisible(true);
    }//GEN-LAST:event_btnLista60ActionPerformed

    private void ocultaDirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ocultaDirActionPerformed
         guardarOpcionesExtra();
    }//GEN-LAST:event_ocultaDirActionPerformed

    private void porcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_porcActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_porcActionPerformed

    private void impActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_impActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_impActionPerformed

    private void esProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_esProductoActionPerformed
        if (esProducto.isSelected()) {
            pInicial.setText("0");
            pInicial.setEditable(true);
            
            
            mostrarInputProd(true);
        }
    }//GEN-LAST:event_esProductoActionPerformed

    private static void mostrarInputProd(boolean m) {
            lbImpTotalPesos.setVisible(m);
            lbImpTotal.setVisible(m);
            impTotal.setVisible(m);
            lbPInicial.setVisible(m);
            pInicial.setVisible(m);
            lbPPesos.setVisible(m);
            lbSoloProd.setVisible(m);
            pInicial.setEditable(m);
            
            if (m) {
                lbFinan.setText("Importe a financiar:");
            } else {
                lbFinan.setText("Importe a prestar:");
            }
            
            impTotal.setText("0.0");
    }
    
    private void esPrestamoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_esPrestamoActionPerformed
        if (esPrestamo.isSelected()) {
            pInicial.setText("0");
            pInicial.setEditable(false);


             mostrarInputProd(false);

        }
    }//GEN-LAST:event_esPrestamoActionPerformed

    private void impTotalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_impTotalKeyReleased
        recalcularValorPlan();
    }//GEN-LAST:event_impTotalKeyReleased

    private void pInicialKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pInicialKeyReleased
        recalcularValorPlan();
    }//GEN-LAST:event_pInicialKeyReleased

    private void pTodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pTodosActionPerformed
        actListaPlanes();
    }//GEN-LAST:event_pTodosActionPerformed

    private void pPrestamosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pPrestamosActionPerformed
        actListaPlanes();
    }//GEN-LAST:event_pPrestamosActionPerformed

    private void pProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pProductosActionPerformed
        actListaPlanes();
    }//GEN-LAST:event_pProductosActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        if (obsA4Window != null) {//si existe una venta, la cierra.
            obsA4Window.dispose();
        }
        
        obsA4Window = new ObsA4();
        obsA4Window.setTitle("Observaciones Recibo A4");
        obsA4Window.setVisible(true);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void botonCambiarVencActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCambiarVencActionPerformed
        int csAux = (int) tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 0);
        String estado = (String) tablaCuotasPrestamo.getValueAt(tablaCuotasPrestamo.getSelectedRow(), 7);
        
        if (estado.equals("PAGADA")) {
            JOptionPane.showMessageDialog(null,"La cuota seleccionada ya fue pagada.","Información",0);
        } else {
            CambiarVencimiento cV = new CambiarVencimiento(csAux);
            cV.setTitle("Cambiar vencimiento de cuota");
            cV.setVisible(true);
        }
    }//GEN-LAST:event_botonCambiarVencActionPerformed

    private void recibosA4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_recibosA4ActionPerformed
        guardarOpcionesExtra();
    }//GEN-LAST:event_recibosA4ActionPerformed

    private void botonImprimirComprobanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonImprimirComprobanteActionPerformed
        String fecha = prestamoEnPrestamos.getFechaGenerado();
        Cliente cx = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(prestamoEnPrestamos.getCodCliente()));
        String cliente = Funciones.capitalize(cx.getNombre());
        Plan pn = Funciones.getPlanPorNombre(prestamoEnPrestamos.getPlan());

        String concepto = pn.getNombre().toUpperCase();
        

        String impefectivo = "$"+pn.getA3();
        String impTotal1 = "$"+pn.getA3();
        String obs1 = "Observaciones: "+Funciones.leerObsA4();
        
                //XLS
                DatosComprobante d = new DatosComprobante();
                
                d.setCant1("1");
                d.setDetalle1(concepto);
                d.setMonto1(impTotal1);
                
                d.setDireccion(Funciones.capitalize(cx.getDireccion()));
                d.setFecha(fecha);
                d.setNombre(cliente);
                d.setNumero("0001-"+Funciones.formatearNumRecibo(Integer.toString(prestamoEnPrestamos.getCod())));
                d.setObs(obs1);
                d.setTelefono(cx.getTelefono()+ "   "+cx.getCelular());
                d.setTipo(opciones.getExtra3().toUpperCase());
                d.setTotal(impTotal1);
                
                TestExcel.rellenarXls(d);
    }//GEN-LAST:event_botonImprimirComprobanteActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        opciones.setExtra3(leyendaComprobante.getText());
        guardarOpciones();
        
    }//GEN-LAST:event_jButton8ActionPerformed

    private void lbFotoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbFotoMouseClicked
        int codX = 0;
        try {
            codX = Integer.parseInt(cod.getText());
            
            if (fotoCliWindow != null) {//si existe una venta, la cierra.
                fotoCliWindow.dispose();
            }

            fotoCliWindow = new VerFotoCli(Funciones.getClientePorCod(clientes, codX));
            fotoCliWindow.setTitle("Foto del cliente");
            fotoCliWindow.setVisible(true);
        } catch (Exception e) {
            
        }
        
    }//GEN-LAST:event_lbFotoMouseClicked

    private void itemGuardarCopiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemGuardarCopiaActionPerformed
        JOptionPane.showMessageDialog(null,"Este proceso puede demorar dependiendo del tamaño de su base de datos.","** Información **",1);
        FuncionesMySQL.backup();
        JOptionPane.showMessageDialog(null,"Copia de seguridad guardada en la carpeta backup.","** Información **",1);
    }//GEN-LAST:event_itemGuardarCopiaActionPerformed

    private void itemRestaurarCopiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemRestaurarCopiaActionPerformed
        JOptionPane.showMessageDialog(null,"Este proceso puede demorar dependiendo del tamaño de su base de datos.","** Información **",1);
         if (rutaMySQL != null) {//si existe una venta, la cierra.
                    rutaMySQL.dispose();
                }

                rutaMySQL = new RutaMySQL();
                rutaMySQL.setTitle("Ruta de copia de seguridad");
                rutaMySQL.setVisible(true);
        
    }//GEN-LAST:event_itemRestaurarCopiaActionPerformed

    private void itemAbrirCarpetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemAbrirCarpetaActionPerformed
        File file = new File ("c:\\prestav2\\backup"); 
        Desktop desktop = Desktop.getDesktop(); 
        try { 
            desktop.open(file);
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_itemAbrirCarpetaActionPerformed

    private void itemBorrarPrestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemBorrarPrestActionPerformed
                int dialogResult = JOptionPane.showConfirmDialog(null, "Esta seguro de querer eliminar todos los préstamos?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            con.conectar();
            ArrayList<Usuario> usuariosBD = con.getUsuarios();
            con.cerrar();
            JPasswordField pf = new JPasswordField();
            int okCxl = JOptionPane.showConfirmDialog(null, pf, "Contraseña para: " + Funciones.capitalize(usuariosBD.get(0).getNombre()), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (okCxl == JOptionPane.OK_OPTION) {
                String passwordAux = new String(pf.getPassword());
                if (Funciones.checkPassword(usuariosBD, usuariosBD.get(0).getNombre(), passwordAux)) {
                    con.conectar();
                    con.borrarPrestamosYCuotas();
                    con.cerrar();
                    
                    cargarValoresIniciales();
                    cargarUsuarios();
                    recalcularValorPlan();
                    cargarClientes();
                    cargarPrestamosYCuotas();
                    cargarClientesPrestamos();
                    cargarPlanes();
        
                }
                else {
                   JOptionPane.showMessageDialog(null,"Contraseña incorrecta.", "Error", 0);
                }
            }
            
            
        }
    }//GEN-LAST:event_itemBorrarPrestActionPerformed

    private void itemBorrarClieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemBorrarClieActionPerformed
                int dialogResult = JOptionPane.showConfirmDialog(null, "Esta seguro de querer eliminar todos los clientes?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            con.conectar();
            ArrayList<Usuario> usuariosBD = con.getUsuarios();
            con.cerrar();
            JPasswordField pf = new JPasswordField();
            int okCxl = JOptionPane.showConfirmDialog(null, pf, "Contraseña para: " + Funciones.capitalize(usuariosBD.get(0).getNombre()), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (okCxl == JOptionPane.OK_OPTION) {
                String passwordAux = new String(pf.getPassword());
                if (Funciones.checkPassword(usuariosBD, usuariosBD.get(0).getNombre(), passwordAux)) {
                    con.conectar();
                    con.borrarClientes();
                    con.cerrar();
                    
                    cargarValoresIniciales();
                    cargarUsuarios();
                    recalcularValorPlan();
                    cargarClientes();
                    cargarPrestamosYCuotas();
                    cargarClientesPrestamos();
                    cargarPlanes();                    
                }
                else {
                   JOptionPane.showMessageDialog(null,"Contraseña incorrecta.", "Error", 0);
                }
            }
            
            
        }
    }//GEN-LAST:event_itemBorrarClieActionPerformed

    private void itemBorrarPlanesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemBorrarPlanesActionPerformed
                int dialogResult = JOptionPane.showConfirmDialog(null, "Esta seguro de querer eliminar todos los planes?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            con.conectar();
            ArrayList<Usuario> usuariosBD = con.getUsuarios();
            con.cerrar();
            JPasswordField pf = new JPasswordField();
            int okCxl = JOptionPane.showConfirmDialog(null, pf, "Contraseña para: " + Funciones.capitalize(usuariosBD.get(0).getNombre()), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (okCxl == JOptionPane.OK_OPTION) {
                String passwordAux = new String(pf.getPassword());
                if (Funciones.checkPassword(usuariosBD, usuariosBD.get(0).getNombre(), passwordAux)) {
                    con.conectar();
                    con.borrarPlanes();
                    con.cerrar();
                    
                    cargarValoresIniciales();
                    cargarUsuarios();
                    recalcularValorPlan();
                    cargarClientes();
                    cargarPrestamosYCuotas();
                    cargarClientesPrestamos();
                    cargarPlanes();
                    
                }
                else {
                   JOptionPane.showMessageDialog(null,"Contraseña incorrecta.", "Error", 0);
                }
            }
            
            
        }
    }//GEN-LAST:event_itemBorrarPlanesActionPerformed

    private void itemBorrarCObrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemBorrarCObrActionPerformed
                       int dialogResult = JOptionPane.showConfirmDialog(null, "Esta seguro de querer eliminar todos los cobradores?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            con.conectar();
            ArrayList<Usuario> usuariosBD = con.getUsuarios();
            con.cerrar();
            JPasswordField pf = new JPasswordField();
            int okCxl = JOptionPane.showConfirmDialog(null, pf, "Contraseña para: " + Funciones.capitalize(usuariosBD.get(0).getNombre()), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (okCxl == JOptionPane.OK_OPTION) {
                String passwordAux = new String(pf.getPassword());
                if (Funciones.checkPassword(usuariosBD, usuariosBD.get(0).getNombre(), passwordAux)) {
                    con.conectar();
                    con.borrarCobradores();
                    con.cerrar();
                    
                    cargarCobradores();
        
                }
                else {
                   JOptionPane.showMessageDialog(null,"Contraseña incorrecta.", "Error", 0);
                }
            }
            
            
        }
    }//GEN-LAST:event_itemBorrarCObrActionPerformed

    private void itemBorrarLocActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemBorrarLocActionPerformed
                       int dialogResult = JOptionPane.showConfirmDialog(null, "Esta seguro de querer eliminar todas las localidades?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            con.conectar();
            ArrayList<Usuario> usuariosBD = con.getUsuarios();
            con.cerrar();
            JPasswordField pf = new JPasswordField();
            int okCxl = JOptionPane.showConfirmDialog(null, pf, "Contraseña para: " + Funciones.capitalize(usuariosBD.get(0).getNombre()), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (okCxl == JOptionPane.OK_OPTION) {
                String passwordAux = new String(pf.getPassword());
                if (Funciones.checkPassword(usuariosBD, usuariosBD.get(0).getNombre(), passwordAux)) {
                    con.conectar();
                    con.borrarLocalidades();
                    con.cerrar();
                    
                    cargarLocalidades();
        
                }
                else {
                   JOptionPane.showMessageDialog(null,"Contraseña incorrecta.", "Error", 0);
                }
            }
            
            
        }
    }//GEN-LAST:event_itemBorrarLocActionPerformed

    private void itemHistActividadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemHistActividadActionPerformed
        if (histWindow != null) {//si existe una venta, la cierra.
                    histWindow.dispose();
                }

                histWindow = new Historial();
                histWindow.setTitle("Historial de actividad");
                histWindow.setVisible(true);
    }//GEN-LAST:event_itemHistActividadActionPerformed

    private void persActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_persActionPerformed
        lbPers.setVisible(true);
        diasPers.setVisible(true);
        diasPers.setText("14");
        recalcularValorPlan();
        
    }//GEN-LAST:event_persActionPerformed

    private void ocultaTelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ocultaTelActionPerformed
        guardarOpcionesExtra();
    }//GEN-LAST:event_ocultaTelActionPerformed

    private void itemVaciarHistActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemVaciarHistActionPerformed
        int dialogResult = JOptionPane.showConfirmDialog(null, "Esta seguro de querer vaciar el historial de actividades?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            con.conectar();
            ArrayList<Usuario> usuariosBD = con.getUsuarios();
            con.cerrar();
            JPasswordField pf = new JPasswordField();
            int okCxl = JOptionPane.showConfirmDialog(null, pf, "Contraseña para: " + Funciones.capitalize(usuariosBD.get(0).getNombre()), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (okCxl == JOptionPane.OK_OPTION) {
                String passwordAux = new String(pf.getPassword());
                if (Funciones.checkPassword(usuariosBD, usuariosBD.get(0).getNombre(), passwordAux)) {
                    con.conectar();
                    con.borrarLogs();
                    con.cerrar();
                   
                }
                else {
                   JOptionPane.showMessageDialog(null,"Contraseña incorrecta.", "Error", 0);
                }
            }
            
            
        }
    }//GEN-LAST:event_itemVaciarHistActionPerformed

    private void hojaa4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hojaa4ActionPerformed
        guardarOpcionesExtra();
    }//GEN-LAST:event_hojaa4ActionPerformed

    private void ticket80mmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ticket80mmActionPerformed
        guardarOpcionesExtra();
    }//GEN-LAST:event_ticket80mmActionPerformed

    private void itemSeleccionarNubeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemSeleccionarNubeActionPerformed
        if (rutaDrive != null) {//si existe una venta, la cierra.
            rutaDrive.dispose();
        }

        rutaDrive = new RutaDrive();
        rutaDrive.setTitle("Ruta a carpeta de Google Drive");
        rutaDrive.setVisible(true);

    }//GEN-LAST:event_itemSeleccionarNubeActionPerformed

    private void itemSubirADriveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemSubirADriveActionPerformed
        copiarCarpeta(new File("C:\\prestav2\\backup\\"), new File(opcionesExtra.getX6()));
        copiarCarpeta(new File("C:\\prestav2\\res\\fotos"), new File(opcionesExtra.getX6()+"\\fotos"));
        JOptionPane.showMessageDialog(null, "Se ha copiado la carpeta de copias de seguridad y fotos\nhacia la carpeta de Drive.", "** INFO **", 1);
    }//GEN-LAST:event_itemSubirADriveActionPerformed

    private void itemBajarDeDriveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemBajarDeDriveActionPerformed
        int dialogResult = JOptionPane.showConfirmDialog(null, "Esta seguro de querer reemplazar los backups locales\npor los almacenados en la nube?", "Confirmar reemplazo", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            copiarCarpeta(new File(opcionesExtra.getX6()),new File("C:\\prestav2\\backup\\"));
            copiarCarpeta(new File(opcionesExtra.getX6()+"\\fotos"),new File("C:\\prestav2\\res\\fotos"));
            JOptionPane.showMessageDialog(null, "Se ha restaurado la carpeta de copias de seguridad y fotos\ndesde la carpeta de Drive.", "** INFO **", 1);
        }
        
    }//GEN-LAST:event_itemBajarDeDriveActionPerformed

    private void itemAbrirCarpetaDriveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemAbrirCarpetaDriveActionPerformed
       File file = new File (opcionesExtra.getX6()); 
        Desktop desktop = Desktop.getDesktop(); 
        try { 
            desktop.open(file);
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_itemAbrirCarpetaDriveActionPerformed

    private void itemHistCobranzasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemHistCobranzasActionPerformed
        if (histCobWindow != null) {//si existe una venta, la cierra.
            histCobWindow.dispose();
        }

        histCobWindow = new HistorialCobranzas();
        histCobWindow.setTitle("Historial de cobranzas");
        histCobWindow.setVisible(true);
    }//GEN-LAST:event_itemHistCobranzasActionPerformed

    private void itemBackupAutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemBackupAutoActionPerformed
        
        if (bkpAutoWindow != null) {//si existe una venta, la cierra.
            bkpAutoWindow.dispose();
        }

        bkpAutoWindow = new BKPAuto();
        bkpAutoWindow.setTitle("Configurar backup automático diario");
        bkpAutoWindow.setVisible(true);
    }//GEN-LAST:event_itemBackupAutoActionPerformed

    
    private void btnCartelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCartelActionPerformed
         //PDF
        Cliente auxiliar = Funciones.getClientePorCod(clientes, Integer.parseInt(cod.getText()));
        
        InputStream inputStream = null;
        JasperPrint jasperPrint= null;
        DeudorDataSource datasource = new DeudorDataSource();

        datasource.setNombre(auxiliar.getNombre());
        datasource.setDireccion(auxiliar.getDireccion());
        datasource.setDeuda("$"+Double.toString(Funciones.getDeudaTotalPorCliente(auxiliar, true)));

        Image img = fotoCli.getImage();

        BufferedImage bi = new BufferedImage(img.getWidth(null),img.getHeight(null),BufferedImage.TYPE_4BYTE_ABGR);

        Graphics2D g2 = bi.createGraphics();
        g2.drawImage(img, 0, 0, null);
        g2.dispose();
        try {
            ImageIO.write(bi, "jpg", new File("fotocartel.jpg"));
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
        

       try {
            inputStream = new FileInputStream ("reporteDeudor.jrxml");
        } catch (FileNotFoundException ex) {
           javax.swing.JOptionPane.showMessageDialog(null,"Error al leer el fichero de carga reporte\n "+ex.getMessage());
        }

        try{
            JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            jasperPrint = JasperFillManager.fillReport(jasperReport, null, datasource);

            //JasperExportManager.exportReportToPdfFile(jasperPrint, "src/agencia/reporte01.pdf");
            //JasperViewer.viewReport(jasperPrint, false);
            JasperViewer jv = new JasperViewer(jasperPrint, false);
            jv.setTitle("Deudor");
            jv.setVisible(true);

        }catch (JRException e){
            javax.swing.JOptionPane.showMessageDialog(null,"Error al cargar fichero jrml jasper report "+e.getMessage());
        }
    }//GEN-LAST:event_btnCartelActionPerformed

    private void btnCartonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCartonActionPerformed
        if (genCartonWindow != null) {//si existe una venta, la cierra.
            genCartonWindow.dispose();
        }

        genCartonWindow = new GenerarCarton(prestamoEnPrestamos);
        genCartonWindow.setTitle("Generar Cartón");
        genCartonWindow.setVisible(true);
    }//GEN-LAST:event_btnCartonActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        javax.swing.JOptionPane.showMessageDialog(null,"La carga de estadísticas puede demorar.\n\nEl tiempo dependerá del tamaño de su base de datos.","INFO",1);
        if (statsWindow != null) {//si existe una venta, la cierra.
            statsWindow.dispose();
        }

        statsWindow = new VentanaEstadisticas();
        statsWindow.setTitle("Ventana de estadísticas");
        statsWindow.setVisible(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try        
        {
            Runtime rt = Runtime.getRuntime();           
            Process p = rt.exec("calc");            
            p.waitFor();        
        }        
        catch ( IOException ioe )       
        {            
            ioe.printStackTrace();
        }         
        catch ( InterruptedException ie )
        {            
            ie.printStackTrace();     
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void interesMensualKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_interesMensualKeyReleased
        recalcularValorPlan();
    }//GEN-LAST:event_interesMensualKeyReleased

    private void itemPlanillaSMSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemPlanillaSMSActionPerformed
        try {
            TestExcel.rellenarXlsSms();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null,"Se produjo un error al generar planilla para envío de sms.", "INFO",0);
        }
    }//GEN-LAST:event_itemPlanillaSMSActionPerformed

    private void btnCobrarPrestamoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCobrarPrestamoActionPerformed
        try {
            FuncionesPagos.cobrarCuota(prestamoEnPrestamos.getUltimaCuotaPendienteOParcial());
        } catch (Exception e) {
            
        }
        
    }//GEN-LAST:event_btnCobrarPrestamoActionPerformed

    private void itemResta1CuotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemResta1CuotaActionPerformed
        if (lista1CuotaWindow != null) {//si existe una venta, la cierra.
            lista1CuotaWindow.dispose();
        }

        lista1CuotaWindow = new Listado1CuotaPendiente();
        lista1CuotaWindow.setTitle("Listado de clientes con 1 cuota pendiente");
        lista1CuotaWindow.setVisible(true);
    }//GEN-LAST:event_itemResta1CuotaActionPerformed

    private void itemFinalizaronYNoRenovaronActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemFinalizaronYNoRenovaronActionPerformed
        if (noRenovaronWindow != null) {//si existe una venta, la cierra.
            noRenovaronWindow.dispose();
        }

        noRenovaronWindow = new ListadoTodoPagoNoRenovaron();
        noRenovaronWindow.setTitle("Clientes con todo pago y no renovaron");
        noRenovaronWindow.setVisible(true);
    }//GEN-LAST:event_itemFinalizaronYNoRenovaronActionPerformed

    private void itemPrestamosPorPeriodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemPrestamosPorPeriodoActionPerformed
        if (otorgadosWindow != null) {//si existe una venta, la cierra.
            otorgadosWindow.dispose();
        }

        otorgadosWindow = new ListadoPrestamosOtorgados();
        otorgadosWindow.setTitle("Listado de préstamos otorgados");
        otorgadosWindow.setVisible(true);
    }//GEN-LAST:event_itemPrestamosPorPeriodoActionPerformed

    private void btnSubirPlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubirPlanActionPerformed
        int minOrden = 0;
        Plan p = Funciones.getPlanPorNombre(listaPlanes.getSelectedItem());
        Plan planAnteriorPorOrden = new Plan();
        
        ArrayList<Plan> planesConOrdenMenor = new ArrayList<>();
        
        for (Plan px: planes) {
            if (!px.getA4().equalsIgnoreCase("personalizado")) {
                
                if (px.getOrden()<p.getOrden()) {
                    planesConOrdenMenor.add(px);
                }
            }
            
        }
        
        
        if (p.getOrden()>1) {
            int maxConsecutivo = 0;
            for (Plan px1: planesConOrdenMenor) {
                if (px1.getOrden()>maxConsecutivo) {
                    maxConsecutivo = px1.getOrden();
                    planAnteriorPorOrden = px1;
                }
            }
            
            p.setOrden(p.getOrden()-1);
            planAnteriorPorOrden.setOrden(planAnteriorPorOrden.getOrden()+1);
            
//            con.conectar();
//            con.actualizarOrdenPlan(p);
//            con.actualizarOrdenPlan(planAnteriorPorOrden);
//            con.cerrar();
//
//            cargarPlanes();
            
             Collections.sort(planes);
        
         
            listaPlanes.removeAll();

            //Recorrer el contenido del ArrayList
            for(Plan planX: planes) {
                if (!planX.getA4().equalsIgnoreCase("personalizado") && !planX.getNombre().toLowerCase().contains("-app")) {
                    listaPlanes.add(planX.getNombre().toUpperCase());   
                }

            }
            
            
            int cursor = 0;
            for (int i = 0; i<listaPlanes.getItemCount();i++) {
                if (listaPlanes.getItem(i).equalsIgnoreCase(p.getNombre())) {
                    cursor = i;
                }
            }
            listaPlanes.select(cursor);
        }    
        
    }//GEN-LAST:event_btnSubirPlanActionPerformed

    private void btnBajarPlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBajarPlanActionPerformed
       int maxOrden = 0;
        Plan p = Funciones.getPlanPorNombre(listaPlanes.getSelectedItem());
        Plan planConMayorOrdenConsecutivo = new Plan();
        
        ArrayList<Plan> planesConOrdenMayor = new ArrayList<>();

        for (Plan px: planes) {
            if (!px.getA4().equalsIgnoreCase("personalizado")) {
                maxOrden ++;
                if (px.getOrden()>p.getOrden()) {
                    planesConOrdenMayor.add(px);
                }
            }
            
        }
        
        if (p.getOrden()<maxOrden) {
            int maxConsecutivo = Integer.MAX_VALUE;
            for (Plan px1: planesConOrdenMayor) {
                if (px1.getOrden()<maxConsecutivo) {
                    maxConsecutivo = px1.getOrden();
                    planConMayorOrdenConsecutivo = px1;
                }
            }

            p.setOrden(p.getOrden()+1);
            planConMayorOrdenConsecutivo.setOrden(planConMayorOrdenConsecutivo.getOrden()-1);

//            con.conectar();
//            con.actualizarOrdenPlan(p);
//            con.actualizarOrdenPlan(planConMayorOrdenConsecutivo);
//            con.cerrar();
//
//            cargarPlanes();
            

  Collections.sort(planes);
        
         
            listaPlanes.removeAll();

           //Recorrer el contenido del ArrayList
//             for(Plan planX: planes) {
//                if (!planX.getA4().equalsIgnoreCase("personalizado") && !planX.getNombre().toLowerCase().contains("-app")) {
//                    listaPlanes.add(planX.getNombre().toUpperCase());   
//                }
//
//            }
             planes.stream().filter((planX) -> (!planX.getA4().equalsIgnoreCase("personalizado") && !planX.getNombre().toLowerCase().contains("-app"))).forEachOrdered((planX) -> {
            listaPlanes.add(planX.getNombre().toUpperCase());
            });
            
            int cursor = 0;
            for (int i = 0; i<listaPlanes.getItemCount();i++) {
                if (listaPlanes.getItem(i).equalsIgnoreCase(p.getNombre())) {
                    cursor = i;
                }
            }
            listaPlanes.select(cursor);
        }
    }//GEN-LAST:event_btnBajarPlanActionPerformed

    private void itemPlanillaCuotasFechaAdeudadasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemPlanillaCuotasFechaAdeudadasActionPerformed
//        try {
//            TestExcel.rellenarXlsCuotasALaFecha();
//        } catch (Exception ex) {
//            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
//            String add = "";
//            if (ex.getMessage().contains("siendo utilizado")) {
//                add += "\n\nEl archivo se encuentra abierto, cierrelo y vuelva a intentarlo.";
//            }
//            JOptionPane.showMessageDialog(this, ex.getMessage()+add, "ERROR", 0);
//        }

            WorkerPlanillaXLSCuotasFecha worker = new WorkerPlanillaXLSCuotasFecha();
            loadWindow = new Ventana();
            loadWindow.setVisible(true);
            worker.setProgreso(Ventana.barra);
            worker.setTodasLasCuotas(false);
             worker.setTodasLasFechas(false);
            worker.execute();
    }//GEN-LAST:event_itemPlanillaCuotasFechaAdeudadasActionPerformed

    private void clienteNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clienteNuevoActionPerformed
        if (clienteNuevo.isSelected()) {
            clienteNuevo.setBackground(new Color(255,255,51));
        } else {
            clienteNuevo.setBackground(null);
        }
    }//GEN-LAST:event_clienteNuevoActionPerformed

    private void btnGuardarOrdenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarOrdenActionPerformed
        con.conectar();
        for (Plan p: planes) {
            if (!p.getA4().equalsIgnoreCase("personalizado") || !p.getNombre().toLowerCase().contains("-app")) {
                con.actualizarOrdenPlan(p);
            }
        }
        con.cerrar();

        cargarPlanes();
    }//GEN-LAST:event_btnGuardarOrdenActionPerformed

    private void itemMantBDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemMantBDActionPerformed
         int dialogResult = JOptionPane.showConfirmDialog(null, "Esta función eliminará los planes personalizados que\n"
                                                            + "NO se encuentran asociados a ningún crédito.\n\nEstos planes no pueden reutilizarse y solo ocupan espacio en la Base.\n\nSin embargo, es recomendable realizar una copia de seguridad\nantes de continuar.\n\nESTÁ SEGURO DE QUE DESEA CONTINUAR??", "Confirmar mantenimiento", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
         
        if(dialogResult == JOptionPane.YES_OPTION){
            con.conectar();
            for (Plan p: planes) {
                if (p.getA4().equalsIgnoreCase("personalizado") || p.getNombre().toLowerCase().contains("-app")) {
                    boolean planPersTienePrestamo = false;
                    for (Prestamo prest: todosLosPrestamos) {
                        if (prest.getPlan().equalsIgnoreCase(p.getNombre())) {
                            con.eliminarPlanPorNombre(p.getNombre().toLowerCase());
                        }
                    }
                }
            }
            con.cerrar();
        }
    }//GEN-LAST:event_itemMantBDActionPerformed

    private void itemPlanillaCuotasFechaTodasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemPlanillaCuotasFechaTodasActionPerformed
        WorkerPlanillaXLSCuotasFecha worker = new WorkerPlanillaXLSCuotasFecha();
            loadWindow = new Ventana();
            loadWindow.setVisible(true);
            worker.setProgreso(Ventana.barra);
            worker.setTodasLasCuotas(true);
            worker.setTodasLasFechas(false);
            worker.execute();
    }//GEN-LAST:event_itemPlanillaCuotasFechaTodasActionPerformed

    private void itemTodasLasCuotasAdeudadasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemTodasLasCuotasAdeudadasActionPerformed
        WorkerPlanillaXLSCuotasFecha worker = new WorkerPlanillaXLSCuotasFecha();
            loadWindow = new Ventana();
            loadWindow.setVisible(true);
            worker.setProgreso(Ventana.barra);
            worker.setTodasLasCuotas(false);
             worker.setTodasLasFechas(true);
            worker.execute();
    }//GEN-LAST:event_itemTodasLasCuotasAdeudadasActionPerformed

    private void itemActivarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemActivarActionPerformed
        if (serial != null) {//si existe una venta, la cierra.
            serial.dispose();
        }

        serial = new Serial();
        serial.setTitle("Ingresar código");
        serial.setVisible(true);
    }//GEN-LAST:event_itemActivarActionPerformed

    private void nombreBusquedaUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nombreBusquedaUserActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nombreBusquedaUserActionPerformed

    private void nombreBusquedaUserKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nombreBusquedaUserKeyReleased
        if (!nombreBusquedaUser.getText().equals("")) {
            dniBusquedaUser.setText("");
            dniBusquedaUser.setEnabled(false);
            busquedaDeUsuarios();
        } else {
            dniBusquedaUser.setEnabled(true);
            busquedaDeUsuarios();
        }
    }//GEN-LAST:event_nombreBusquedaUserKeyReleased

    private void dniBusquedaUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dniBusquedaUserActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dniBusquedaUserActionPerformed

    private void dniBusquedaUserKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dniBusquedaUserKeyReleased
        if (!dniBusquedaUser.getText().equals("")) {
            nombreBusquedaUser.setText("");
            nombreBusquedaUser.setEnabled(false);
            busquedaDeUsuarios();
        } else {
            nombreBusquedaUser.setEnabled(true);
            busquedaDeUsuarios();
        }
    }//GEN-LAST:event_dniBusquedaUserKeyReleased

    private void actListado5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actListado5ActionPerformed
       cargarUsuariosApp();
    }//GEN-LAST:event_actListado5ActionPerformed

    private void tablaClientesAppMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaClientesAppMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tablaClientesAppMouseClicked

    private void tablaClientesAppMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaClientesAppMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_tablaClientesAppMouseEntered

    private void btnActualizarAppActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarAppActionPerformed
        WorkersApp.actualizarPendientes();
    }//GEN-LAST:event_btnActualizarAppActionPerformed

    private void btnGenerarUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarUsuariosActionPerformed
        int dialogResult = JOptionPane.showConfirmDialog(null, "Se generarán usuarios para la aplicación para TODOS los clientes registrados.\n\nDesea continuar?", "Confirmar creación de usuarios", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            WorkersApp.workerGenerarUsers();
        }
    }//GEN-LAST:event_btnGenerarUsuariosActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        if (cfgAppWindow != null) {//si existe una venta, la cierra.
            cfgAppWindow.dispose();
        }
        
        cfgAppWindow = new ConfigApp();
        cfgAppWindow.setTitle("Ajustes de la aplicación");
        cfgAppWindow.setVisible(true);
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        int dialogResult = JOptionPane.showConfirmDialog(null, "Esta seguro de querer eliminar todos los USUARIOS, PRÉSTAMOS, PLANES Y CUOTAS de la app?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(dialogResult == JOptionPane.YES_OPTION){
            con.conectar();
            ArrayList<Usuario> usuariosBD = con.getUsuarios();
            con.cerrar();
            JPasswordField pf = new JPasswordField();
            int okCxl = JOptionPane.showConfirmDialog(null, pf, "Contraseña para: " + Funciones.capitalize(usuariosBD.get(0).getNombre()), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (okCxl == JOptionPane.OK_OPTION) {
                String passwordAux = new String(pf.getPassword());
                if (Funciones.checkPassword(usuariosBD, usuariosBD.get(0).getNombre(), passwordAux)) {
                    WorkersApp.eliminarTodosLosUsuarios();
                   
                }
                else {
                   JOptionPane.showMessageDialog(null,"Contraseña incorrecta.", "Error", 0);
                }
            }
            
            
        }
    }//GEN-LAST:event_jButton10ActionPerformed

    private void itemPagosParcialesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemPagosParcialesActionPerformed
        if (pagosWindow != null) {//si existe una venta, la cierra.
            pagosWindow.dispose();
        }

        pagosWindow = new PagosParciales();
        pagosWindow.setTitle("PagosParciales");
        pagosWindow.setVisible(true);
    }//GEN-LAST:event_itemPagosParcialesActionPerformed

    private void itemSincroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemSincroActionPerformed
        JOptionPane.showMessageDialog(null, "Se sincronizarán las cuotas locales con las cuotas del servidor online.\n\nEste proceso puede demorar dependiendo de la cantidad de cuotas.","INFO",1);
        WorkerSincronizarCuotas worker = new WorkerSincronizarCuotas();
        loadWindow = new Ventana();
        loadWindow.setVisible(true);
        worker.setProgreso(Ventana.barra);
        worker.execute();
    }//GEN-LAST:event_itemSincroActionPerformed

    private void itemAdelantarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemAdelantarActionPerformed
        if(Funciones.leerOptDB().getTipo().equalsIgnoreCase("servidor")) {
            SelectorAdelantoCuotas sel = new SelectorAdelantoCuotas();
            sel.setVisible(true);
            sel.setTitle("Adelantar cuotas");
        } else {
            JOptionPane.showMessageDialog(null, "La función debe utilizarse desde el equipo SERVIDOR", "INFO", 1);
        }
                
    }//GEN-LAST:event_itemAdelantarActionPerformed

  
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel PanelAplicacion;
    private javax.swing.JPanel PanelClientes;
    private javax.swing.JPanel PanelCobradores;
    private javax.swing.JPanel PanelEstadisticas;
    private javax.swing.JPanel PanelImportar;
    private javax.swing.JPanel PanelInicio;
    private javax.swing.JPanel PanelOpciones;
    private javax.swing.JPanel PanelPlanes;
    private javax.swing.JPanel PanelPrestamos;
    public static javax.swing.JTabbedPane PanelPrincipal;
    private javax.swing.JButton actListado1;
    private javax.swing.JButton actListado2;
    private javax.swing.JButton actListado3;
    private javax.swing.JButton actListado4;
    private javax.swing.JButton actListado5;
    public static javax.swing.JLabel adeudadoPrest;
    public static javax.swing.JButton botonActivar;
    public static javax.swing.JButton botonActualizar;
    public static javax.swing.JButton botonActualizarCob;
    public static javax.swing.JButton botonActualizarPlan;
    public static javax.swing.JButton botonAgregarPrestamo;
    private javax.swing.JButton botonAgregarUsuario;
    public static javax.swing.JButton botonAnular;
    public static javax.swing.JButton botonBlanquear;
    public static javax.swing.JButton botonBlanquearCob;
    public static javax.swing.JButton botonBlanquearPlan;
    public static javax.swing.JButton botonCambiarVenc;
    public static javax.swing.JButton botonEliminar;
    public static javax.swing.JButton botonEliminarCob;
    public static javax.swing.JButton botonEliminarPlan;
    public static javax.swing.JButton botonEliminarPrestamo;
    public static javax.swing.JButton botonGuardar;
    public static javax.swing.JButton botonGuardarCob;
    public static javax.swing.JButton botonGuardarPlan;
    public static javax.swing.JButton botonImprimirComprobante;
    public static javax.swing.JButton botonImprimirPrestamo;
    public static javax.swing.JButton botonImprimirPrestamo1;
    public static javax.swing.JButton botonImprimirPrestamo2;
    public static javax.swing.JButton botonReciboA4;
    public static javax.swing.JButton botonReferencias;
    public static javax.swing.JButton btnActualizarApp;
    private javax.swing.JButton btnBajarPlan;
    private static javax.swing.JButton btnCartel;
    private static javax.swing.JButton btnCarton;
    private static javax.swing.JButton btnCobrarPrestamo;
    private javax.swing.JButton btnGenerarUsuarios;
    private javax.swing.JButton btnGuardarDatos;
    private javax.swing.JButton btnGuardarOpt1;
    private javax.swing.JButton btnGuardarOrden;
    private javax.swing.JButton btnLista30;
    private javax.swing.JButton btnLista60;
    private javax.swing.JButton btnListaCobrador;
    private javax.swing.JButton btnListaZona;
    public static javax.swing.JButton btnRefinanciar;
    private javax.swing.JButton btnSubirPlan;
    public static javax.swing.JButton btnVertodos;
    public static javax.swing.JButton btnVertodosCob;
    public javax.swing.JTextField buscaPlan;
    public static javax.swing.JTextField cantPlanes;
    public static javax.swing.JTextField cantPlanesPers;
    private static javax.swing.JCheckBox clienteNuevo;
    public static javax.swing.JLabel clientesRegLB;
    public static javax.swing.JLabel cobAsign;
    public static javax.swing.JTextField cod;
    public static javax.swing.JTextField codCob;
    public static javax.swing.JTextField codIntCob;
    public static javax.swing.JTextField codPlan;
    public static javax.swing.JLabel codPrest;
    private javax.swing.JLabel codPrest1;
    private javax.swing.JLabel codPrest10;
    private javax.swing.JLabel codPrest11;
    private javax.swing.JLabel codPrest12;
    private javax.swing.JLabel codPrest2;
    private javax.swing.JLabel codPrest3;
    private javax.swing.JLabel codPrest4;
    private javax.swing.JLabel codPrest5;
    private javax.swing.JLabel codPrest6;
    private javax.swing.JLabel codPrest8;
    public static javax.swing.JComboBox<String> comboClientes;
    public static javax.swing.JComboBox<String> comboCuotas;
    public static javax.swing.JComboBox<String> comboLoc;
    public static javax.swing.JComboBox<String> comboPrestamos;
    public static javax.swing.JComboBox<String> comboRutas;
    private javax.swing.JComboBox<String> comboTipo;
    public static javax.swing.JComboBox<String> comboTipoInt;
    public static javax.swing.JLabel cuotasPrest;
    public static javax.swing.JLabel cuotasTotLB;
    public static javax.swing.JLabel dAnt;
    public static javax.swing.JLabel descontadoPrest;
    public static javax.swing.JCheckBox diario;
    private static javax.swing.JTextField diasPers;
    public static javax.swing.JTextField diasVFinal;
    public static javax.swing.JTextField diasVInicial;
    public static javax.swing.JTextField dineroCobrador;
    public static javax.swing.JTextField dirEmpresa;
    public static javax.swing.JTextField direccion;
    public static javax.swing.JTextField dni;
    public static javax.swing.JTextField dniBusqueda;
    public static javax.swing.JTextField dniBusquedaUser;
    public static javax.swing.JTextField email;
    public static javax.swing.JRadioButton esPrestamo;
    public static javax.swing.JRadioButton esProducto;
    public static org.jdesktop.swingx.JXDatePicker fechaNac;
    public static javax.swing.JLabel fechaPrest;
    public static org.jdesktop.swingx.JXDatePicker finTotal;
    private javax.swing.JCheckBox globales;
    private javax.swing.JButton guardarObsBtn;
    public static javax.swing.JCheckBox habImpresion;
    private static javax.swing.JRadioButton hojaa4;
    public static javax.swing.JCheckBox imp;
    private static javax.swing.JTextField impTotal;
    public static javax.swing.JTextField importeDevolver;
    public static javax.swing.JTextField importePrestar;
    public static org.jdesktop.swingx.JXDatePicker inicioTotal;
    public static javax.swing.JTextField interesDinero;
    public static javax.swing.JTextField interesMensual;
    public static javax.swing.JTextField interesPorc;
    private javax.swing.JMenuItem itemAbrirCarpeta;
    private javax.swing.JMenuItem itemAbrirCarpetaDrive;
    private static javax.swing.JMenuItem itemActivar;
    private javax.swing.JMenuItem itemAdelantar;
    private javax.swing.JMenuItem itemBackupAuto;
    private javax.swing.JMenuItem itemBajarDeDrive;
    private javax.swing.JMenuItem itemBorrarCObr;
    private javax.swing.JMenuItem itemBorrarClie;
    private javax.swing.JMenuItem itemBorrarLoc;
    private javax.swing.JMenuItem itemBorrarPlanes;
    private javax.swing.JMenuItem itemBorrarPrest;
    private javax.swing.JMenuItem itemFinalizaronYNoRenovaron;
    private javax.swing.JMenuItem itemGuardarCopia;
    private javax.swing.JMenuItem itemHistActividad;
    private javax.swing.JMenuItem itemHistCobranzas;
    private javax.swing.JMenuItem itemMantBD;
    private javax.swing.JMenuItem itemPagosParciales;
    private javax.swing.JMenuItem itemPlanillaCuotasFechaAdeudadas;
    private javax.swing.JMenuItem itemPlanillaCuotasFechaTodas;
    private javax.swing.JMenuItem itemPlanillaSMS;
    private javax.swing.JMenuItem itemPrestamosPorPeriodo;
    private javax.swing.JMenuItem itemResta1Cuota;
    private javax.swing.JMenuItem itemRestaurarCopia;
    private javax.swing.JMenuItem itemSalir;
    private javax.swing.JMenuItem itemSeleccionarNube;
    private javax.swing.JMenuItem itemSincro;
    private javax.swing.JMenuItem itemSubirADrive;
    private javax.swing.JMenuItem itemTodasLasCuotasAdeudadas;
    private javax.swing.JMenuItem itemVaciarHist;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel100;
    private javax.swing.JLabel jLabel101;
    private javax.swing.JLabel jLabel102;
    private javax.swing.JLabel jLabel103;
    private javax.swing.JLabel jLabel104;
    private javax.swing.JLabel jLabel105;
    private javax.swing.JLabel jLabel106;
    private javax.swing.JLabel jLabel107;
    private javax.swing.JLabel jLabel108;
    private javax.swing.JLabel jLabel109;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel110;
    private javax.swing.JLabel jLabel111;
    private javax.swing.JLabel jLabel112;
    private javax.swing.JLabel jLabel113;
    private javax.swing.JLabel jLabel114;
    private javax.swing.JLabel jLabel115;
    private javax.swing.JLabel jLabel116;
    private javax.swing.JLabel jLabel117;
    private javax.swing.JLabel jLabel118;
    private javax.swing.JLabel jLabel119;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel120;
    private javax.swing.JLabel jLabel121;
    private javax.swing.JLabel jLabel122;
    private javax.swing.JLabel jLabel123;
    private javax.swing.JLabel jLabel124;
    private javax.swing.JLabel jLabel125;
    private javax.swing.JLabel jLabel126;
    private javax.swing.JLabel jLabel127;
    private javax.swing.JLabel jLabel128;
    private javax.swing.JLabel jLabel129;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel130;
    private javax.swing.JLabel jLabel131;
    private javax.swing.JLabel jLabel132;
    private javax.swing.JLabel jLabel133;
    private javax.swing.JLabel jLabel134;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JLabel jLabel84;
    private javax.swing.JLabel jLabel85;
    private javax.swing.JLabel jLabel86;
    private javax.swing.JLabel jLabel87;
    private javax.swing.JLabel jLabel88;
    private javax.swing.JLabel jLabel89;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel90;
    private javax.swing.JLabel jLabel91;
    private javax.swing.JLabel jLabel92;
    private javax.swing.JLabel jLabel93;
    private javax.swing.JLabel jLabel94;
    private javax.swing.JLabel jLabel95;
    private javax.swing.JLabel jLabel96;
    private javax.swing.JLabel jLabel97;
    private javax.swing.JLabel jLabel98;
    private javax.swing.JLabel jLabel99;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel29;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel30;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel35;
    private javax.swing.JPanel jPanel36;
    private javax.swing.JPanel jPanel37;
    private javax.swing.JPanel jPanel38;
    private javax.swing.JPanel jPanel39;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel40;
    private javax.swing.JPanel jPanel41;
    private javax.swing.JPanel jPanel42;
    private javax.swing.JPanel jPanel43;
    private javax.swing.JPanel jPanel44;
    private javax.swing.JPanel jPanel45;
    private javax.swing.JPanel jPanel46;
    private javax.swing.JPanel jPanel47;
    private javax.swing.JPanel jPanel48;
    private javax.swing.JPanel jPanel49;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JPopupMenu.Separator jSeparator13;
    private javax.swing.JPopupMenu.Separator jSeparator14;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JSeparator jSeparator16;
    private javax.swing.JSeparator jSeparator17;
    private javax.swing.JSeparator jSeparator18;
    private javax.swing.JSeparator jSeparator19;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator20;
    private javax.swing.JSeparator jSeparator21;
    private javax.swing.JSeparator jSeparator22;
    private javax.swing.JSeparator jSeparator23;
    private javax.swing.JSeparator jSeparator24;
    private javax.swing.JSeparator jSeparator25;
    private javax.swing.JSeparator jSeparator26;
    private javax.swing.JSeparator jSeparator27;
    private javax.swing.JSeparator jSeparator28;
    private javax.swing.JSeparator jSeparator29;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator30;
    private javax.swing.JSeparator jSeparator31;
    private javax.swing.JSeparator jSeparator32;
    private javax.swing.JSeparator jSeparator33;
    private javax.swing.JSeparator jSeparator34;
    private javax.swing.JPopupMenu.Separator jSeparator35;
    private javax.swing.JPopupMenu.Separator jSeparator36;
    private javax.swing.JSeparator jSeparator37;
    private javax.swing.JPopupMenu.Separator jSeparator38;
    private javax.swing.JPopupMenu.Separator jSeparator39;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator40;
    private javax.swing.JPopupMenu.Separator jSeparator41;
    private javax.swing.JPopupMenu.Separator jSeparator42;
    private javax.swing.JSeparator jSeparator43;
    private javax.swing.JPopupMenu.Separator jSeparator44;
    private javax.swing.JPopupMenu.Separator jSeparator45;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTabbedPane jTabbedPane1;
    public static javax.swing.JLabel lbCantPrestamos;
    public static javax.swing.JLabel lbCargando1;
    public static javax.swing.JLabel lbCargando2;
    public static javax.swing.JLabel lbCargando3;
    public static javax.swing.JLabel lbCobradoHoy;
    public static javax.swing.JLabel lbCobrarHoy;
    public static javax.swing.JLabel lbEstadoApp;
    public static javax.swing.JLabel lbEstadoAppAbajo;
    public static javax.swing.JLabel lbFecha;
    private static javax.swing.JLabel lbFinan;
    private static javax.swing.JLabel lbFoto;
    public static javax.swing.JLabel lbHora;
    private static javax.swing.JLabel lbImpTotal;
    private static javax.swing.JLabel lbImpTotalPesos;
    public static javax.swing.JTextArea lbInfo;
    public static javax.swing.JLabel lbInfoLinea1;
    public static javax.swing.JLabel lbInfoLinea2;
    public static javax.swing.JLabel lbInfoLinea3;
    private static javax.swing.JLabel lbInfoTablaClientes;
    private static javax.swing.JLabel lbPInicial;
    private static javax.swing.JLabel lbPPesos;
    private javax.swing.JLabel lbPagado;
    private static javax.swing.JLabel lbPers;
    private static javax.swing.JLabel lbSoloProd;
    public static javax.swing.JLabel lbTipoInt;
    public static javax.swing.JLabel lbTotalAdeudado;
    public static javax.swing.JLabel lbTotalPrestado;
    private static javax.swing.JLabel lbUltPago;
    public static javax.swing.JLabel lbVencHoy;
    public static javax.swing.JLabel lbVencsHoy;
    private static javax.swing.JTextField leyendaComprobante;
    public static java.awt.List listaPlanes;
    public static java.awt.List listaPrestamos;
    public static javax.swing.JTable listaUsuarios;
    public static javax.swing.JCheckBox mAnt;
    public static javax.swing.JCheckBox marcarFinalizados;
    public static javax.swing.JCheckBox mensual;
    private static javax.swing.JMenu menuAdmin;
    private javax.swing.JMenu menuArchivo;
    private static javax.swing.JMenu menuBase;
    private javax.swing.JMenu menuCli;
    private javax.swing.JMenu menuGenerarPlanillaCuotas;
    private javax.swing.JMenu menuPrestamos;
    private javax.swing.JMenu menuReportes;
    public static javax.swing.JButton modifCob;
    public static javax.swing.JButton modifCob2;
    public static javax.swing.JButton modifCob3;
    public static javax.swing.JCheckBox morosos;
    public static javax.swing.JCheckBox mostrarPagas;
    private javax.swing.JCheckBox muestraFin;
    public static javax.swing.JTextField nomPlan;
    public static javax.swing.JTextField nombre;
    public static javax.swing.JTextField nombreApp;
    public static javax.swing.JTextField nombreBusqueda;
    public static javax.swing.JTextField nombreBusqueda1;
    public static javax.swing.JTextField nombreBusquedaUser;
    public static javax.swing.JTextField nombreCob;
    public static javax.swing.JTextField nombreEmpresa;
    private javax.swing.JTextField nombreUsrNew;
    public static javax.swing.JTextArea obs;
    public static javax.swing.JTextArea obsCob;
    public static javax.swing.JTextArea obsPrestamo;
    public static javax.swing.JCheckBox ocultaDir;
    public static javax.swing.JCheckBox ocultaTel;
    public static javax.swing.JCheckBox ocultarFinalizados;
    private static javax.swing.JCheckBox ocultarPagasPrestamo;
    private static javax.swing.JTextField pInicial;
    private static javax.swing.JRadioButton pPrestamos;
    private static javax.swing.JRadioButton pProductos;
    private static javax.swing.JRadioButton pTodos;
    public static javax.swing.JLabel pagadoPrest;
    public static javax.swing.JPanel panelAvisoOPC;
    public static javax.swing.JPanel panelCarga;
    private javax.swing.JPanel panelDB1;
    private javax.swing.JPanel panelDB2;
    private javax.swing.JPanel panelDB3;
    private javax.swing.JPanel panelDB4;
    private javax.swing.JPanel panelDB5;
    private javax.swing.JPanel panelDB6;
    private javax.swing.JPanel panelDatos;
    public static javax.swing.JPanel panelEstadoApp;
    private javax.swing.JPanel panelIzqClientes;
    private javax.swing.JPanel panelIzqClientes1;
    public static javax.swing.JPanel panelOpt1;
    public static javax.swing.JPanel panelOpt2;
    public static javax.swing.JPanel panelOpt3;
    public static javax.swing.JPanel panelOpt4;
    public static javax.swing.JPanel panelStats1;
    public static javax.swing.JPanel panelStats2;
    public static javax.swing.JPanel panelStats3;
    public static javax.swing.JPanel panelStats4;
    private javax.swing.JCheckBox parcialesCheck;
    private javax.swing.JPasswordField password;
    private static javax.swing.JCheckBox pers;
    public static javax.swing.JLabel planPrest;
    public static javax.swing.JLabel planPrest1;
    public static javax.swing.JLabel planPrest2;
    public static javax.swing.JLabel planPrest3;
    public static javax.swing.JLabel planPrest4;
    public static javax.swing.JCheckBox porc;
    public static javax.swing.JTextField porcCobrador;
    public static javax.swing.JLabel prestamosTotLB;
    private static javax.swing.JCheckBox recibosA4;
    public static javax.swing.JCheckBox semanal;
    public static javax.swing.JLabel sobrePrest;
    public static javax.swing.JTable tablaClientes;
    public static javax.swing.JTable tablaClientesApp;
    public static javax.swing.JTable tablaCobradores;
    public static javax.swing.JTable tablaCuotasPrestamo;
    public static javax.swing.JTable tablaVencDiarios;
    public static javax.swing.JTable tablaVencSemanales;
    public static javax.swing.JTextField telCob;
    public static javax.swing.JTextField telEmpresa;
    public static javax.swing.JTextField telcel;
    public static javax.swing.JTextField telfijo;
    private static javax.swing.JRadioButton ticket80mm;
    public static javax.swing.JLabel totalCobrarLB;
    public static javax.swing.JLabel totalDevueltoLB;
    public static javax.swing.JLabel totalDevueltoMesLB;
    public static javax.swing.JLabel totalPrest;
    public static javax.swing.JLabel totalPrestadoLB;
    public static javax.swing.JTextField valorCuota;
    public static javax.swing.JLabel valorCuotaPrest;
    public static javax.swing.JLabel valorImportePrest;
    public static javax.swing.JTextField valorIntAtraso;
    // End of variables declaration//GEN-END:variables
}

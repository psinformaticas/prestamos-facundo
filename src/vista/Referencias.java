/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.Funciones;
import modelo.*;

/**
 *
 * @author Will
 */
public class Referencias extends javax.swing.JFrame {

    public static Cliente cliente;
    public static String r1;
    public static String r2;
    public static String r3;
    public static String r4;
    
    
    public Referencias() {
        initComponents();
    }
    
    public Referencias(Cliente cli) {
        initComponents();
        setLocationRelativeTo(null);
        
        cliente = cli;
        
        //REMUEVE PRIMER CARACTER SI ES ESPACIO
        r1 = cli.getRef1();
        r2 = cli.getRef2();
        r3 = cli.getRef3();
        r4 = cli.getRef4();
        
        lbNombre.setText(Funciones.capitalize(cli.getNombre()));
        
        cargarDatos();
    }

    public static void cargarDatos() {
        String[] referencia1 = r1.split("-i-");
        String[] referencia2 = r2.split("-i-");
        String[] referencia3 = r3.split("-i-");
        String[] referencia4 = r4.split("-i-");

        nombre_ref1.setText(Funciones.capitalize(referencia1[0]));
        tel_ref1.setText(referencia1[1]);
        det_ref1.setText(referencia1[2]);
        
        nombre_ref2.setText(Funciones.capitalize(referencia2[0]));
        tel_ref2.setText(referencia2[1]);
        det_ref2.setText(referencia2[2]);
        
        nombre_ref3.setText(Funciones.capitalize(referencia3[0]));
        tel_ref3.setText(referencia3[1]);
        det_ref3.setText(referencia3[2]);
        
        nombre_ref4.setText(Funciones.capitalize(referencia4[0]));
        tel_ref4.setText(referencia4[1]);
        det_ref4.setText(referencia4[2]);
        
        if (nombre_ref1.getText().equals(" ")) {
            nombre_ref1.setText("");
        }
        if (tel_ref1.getText().equals(" ")) {
            tel_ref1.setText("");
        }
        if (det_ref1.getText().equals(" ")) {
            det_ref1.setText("");
        }
        
        if (nombre_ref2.getText().equals(" ")) {
            nombre_ref2.setText("");
        }
        if (tel_ref2.getText().equals(" ")) {
            tel_ref2.setText("");
        }
        if (det_ref2.getText().equals(" ")) {
            det_ref2.setText("");
        }
        
        if (nombre_ref3.getText().equals(" ")) {
            nombre_ref3.setText("");
        }
        if (tel_ref3.getText().equals(" ")) {
            tel_ref3.setText("");
        }
        if (det_ref3.getText().equals(" ")) {
            det_ref3.setText("");
        }
        
        if (nombre_ref4.getText().equals(" ")) {
            nombre_ref4.setText("");
        }
        if (tel_ref4.getText().equals(" ")) {
            tel_ref4.setText("");
        }
        if (det_ref4.getText().equals(" ")) {
            det_ref4.setText("");
        }
    }
    
    public static void guardarDatos() {
        String nomAux1;
        String nomAux2;
        String nomAux3;
        String nomAux4;
        if (nombre_ref1.getText().equals("")) {
            nomAux1 = " ";
        } else {
            nomAux1 = nombre_ref1.getText();
        }
        if (nombre_ref2.getText().equals("")) {
            nomAux2 = " ";
        } else {
            nomAux2 = nombre_ref2.getText();
        }
        if (nombre_ref3.getText().equals("")) {
            nomAux3 = " ";
        } else {
            nomAux3 = nombre_ref3.getText();
        }
        if (nombre_ref4.getText().equals("")) {
            nomAux4 = " ";
        } else {
            nomAux4 = nombre_ref4.getText();
        }
        
        
        String telAux1;
        String telAux2;
        String telAux3;
        String telAux4;
        if (tel_ref1.getText().equals("")) {
            telAux1 = " ";
        } else {
            telAux1 = tel_ref1.getText();
        }
        if (tel_ref2.getText().equals("")) {
            telAux2 = " ";
        } else {
            telAux2 = tel_ref2.getText();
        }
        if (tel_ref3.getText().equals("")) {
            telAux3 = " ";
        } else {
            telAux3 = tel_ref3.getText();
        }
        if (tel_ref4.getText().equals("")) {
            telAux4 = " ";
        } else {
            telAux4 = tel_ref4.getText();
        }
        
        
        String detAux1;
        String detAux2;
        String detAux3;
        String detAux4;
        if (det_ref1.getText().equals("")) {
            detAux1 = " ";
        } else {
            detAux1 = det_ref1.getText();
        }
        if (det_ref2.getText().equals("")) {
            detAux2 = " ";
        } else {
            detAux2 = det_ref2.getText();
        }
        if (det_ref3.getText().equals("")) {
            detAux3 = " ";
        } else {
            detAux3 = det_ref3.getText();
        }
        if (det_ref4.getText().equals("")) {
            detAux4 = " ";
        } else {
            detAux4 = det_ref4.getText();
        }
        
        
        String refer1 = nomAux1 + "-I-" + telAux1 + "-I-" + detAux1;
        String refer2 = nomAux2 + "-I-" + telAux2 + "-I-" + detAux2;
        String refer3 = nomAux3 + "-I-" + telAux3 + "-I-" + detAux3;
        String refer4 = nomAux4 + "-I-" + telAux4 + "-I-" + detAux4;
        
        
        cliente.setRef1(refer1);
        cliente.setRef2(refer2);
        cliente.setRef3(refer3);
        cliente.setRef4(refer4);
        
        Principal.con.conectar();
        Principal.con.actualizarRefCliente(cliente);
        Principal.con.cerrar();
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel11 = new javax.swing.JLabel();
        lbNombre = new javax.swing.JLabel();
        refe1 = new javax.swing.JPanel();
        nombre_ref1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        tel_ref1 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        det_ref1 = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        refe2 = new javax.swing.JPanel();
        nombre_ref2 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        tel_ref2 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        det_ref2 = new javax.swing.JTextArea();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        refe3 = new javax.swing.JPanel();
        nombre_ref3 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        tel_ref3 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        det_ref3 = new javax.swing.JTextArea();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        refe5 = new javax.swing.JPanel();
        nombre_ref4 = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        tel_ref4 = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        det_ref4 = new javax.swing.JTextArea();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        botonGuardar = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_ref.png"))); // NOI18N
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel11.setText("Referencias: ");

        lbNombre.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbNombre.setText("Nombre");

        refe1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        nombre_ref1.setToolTipText("");

        jLabel2.setText("Nombre:");

        jLabel3.setText("Teléfono:");

        det_ref1.setColumns(20);
        det_ref1.setRows(5);
        jScrollPane1.setViewportView(det_ref1);

        jLabel4.setText("Detalles");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Referencia 1");

        javax.swing.GroupLayout refe1Layout = new javax.swing.GroupLayout(refe1);
        refe1.setLayout(refe1Layout);
        refe1Layout.setHorizontalGroup(
            refe1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(refe1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(refe1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                    .addGroup(refe1Layout.createSequentialGroup()
                        .addGroup(refe1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(refe1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tel_ref1)
                            .addComponent(nombre_ref1)))
                    .addGroup(refe1Layout.createSequentialGroup()
                        .addGroup(refe1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        refe1Layout.setVerticalGroup(
            refe1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(refe1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(refe1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombre_ref1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(refe1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tel_ref1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(9, 9, 9)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        refe2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel6.setText("Nombre:");

        jLabel7.setText("Teléfono:");

        det_ref2.setColumns(20);
        det_ref2.setRows(5);
        jScrollPane2.setViewportView(det_ref2);

        jLabel8.setText("Detalles");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Referencia 2");

        javax.swing.GroupLayout refe2Layout = new javax.swing.GroupLayout(refe2);
        refe2.setLayout(refe2Layout);
        refe2Layout.setHorizontalGroup(
            refe2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(refe2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(refe2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                    .addGroup(refe2Layout.createSequentialGroup()
                        .addGroup(refe2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(refe2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tel_ref2)
                            .addComponent(nombre_ref2)))
                    .addGroup(refe2Layout.createSequentialGroup()
                        .addGroup(refe2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        refe2Layout.setVerticalGroup(
            refe2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(refe2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(refe2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombre_ref2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(refe2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tel_ref2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(9, 9, 9)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        refe3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel10.setText("Nombre:");

        jLabel12.setText("Teléfono:");

        det_ref3.setColumns(20);
        det_ref3.setRows(5);
        jScrollPane3.setViewportView(det_ref3);

        jLabel13.setText("Detalles");

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel14.setText("Referencia 3");

        javax.swing.GroupLayout refe3Layout = new javax.swing.GroupLayout(refe3);
        refe3.setLayout(refe3Layout);
        refe3Layout.setHorizontalGroup(
            refe3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(refe3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(refe3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                    .addGroup(refe3Layout.createSequentialGroup()
                        .addGroup(refe3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(refe3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tel_ref3)
                            .addComponent(nombre_ref3)))
                    .addGroup(refe3Layout.createSequentialGroup()
                        .addGroup(refe3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13)
                            .addComponent(jLabel14))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        refe3Layout.setVerticalGroup(
            refe3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(refe3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(refe3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombre_ref3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(refe3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tel_ref3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addGap(9, 9, 9)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        refe5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel19.setText("Nombre:");

        jLabel20.setText("Teléfono:");

        det_ref4.setColumns(20);
        det_ref4.setRows(5);
        jScrollPane5.setViewportView(det_ref4);

        jLabel21.setText("Detalles");

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel22.setText("Referencia 4");

        javax.swing.GroupLayout refe5Layout = new javax.swing.GroupLayout(refe5);
        refe5.setLayout(refe5Layout);
        refe5Layout.setHorizontalGroup(
            refe5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(refe5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(refe5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                    .addGroup(refe5Layout.createSequentialGroup()
                        .addGroup(refe5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel19, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(refe5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tel_ref4)
                            .addComponent(nombre_ref4)))
                    .addGroup(refe5Layout.createSequentialGroup()
                        .addGroup(refe5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel21)
                            .addComponent(jLabel22))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        refe5Layout.setVerticalGroup(
            refe5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(refe5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(refe5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombre_ref4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(refe5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tel_ref4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20))
                .addGap(9, 9, 9)
                .addComponent(jLabel21)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        botonGuardar.setText("Guardar");
        botonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(botonGuardar)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(refe3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(refe5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(refe1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(refe2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 15, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lbNombre)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jSeparator1))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(refe1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(refe2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(refe3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(refe5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botonGuardar)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarActionPerformed
        guardarDatos();
        Principal.cargarClientes();
        this.dispose();
    }//GEN-LAST:event_botonGuardarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Referencias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Referencias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Referencias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Referencias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Referencias().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonGuardar;
    public static javax.swing.JTextArea det_ref1;
    public static javax.swing.JTextArea det_ref2;
    public static javax.swing.JTextArea det_ref3;
    public static javax.swing.JTextArea det_ref4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lbNombre;
    public static javax.swing.JTextField nombre_ref1;
    public static javax.swing.JTextField nombre_ref2;
    public static javax.swing.JTextField nombre_ref3;
    public static javax.swing.JTextField nombre_ref4;
    private javax.swing.JPanel refe1;
    private javax.swing.JPanel refe2;
    private javax.swing.JPanel refe3;
    private javax.swing.JPanel refe5;
    public static javax.swing.JTextField tel_ref1;
    public static javax.swing.JTextField tel_ref2;
    public static javax.swing.JTextField tel_ref3;
    public static javax.swing.JTextField tel_ref4;
    // End of variables declaration//GEN-END:variables
}

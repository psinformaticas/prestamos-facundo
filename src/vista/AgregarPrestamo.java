/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import aplicacion.AplicacionControlador;
import aplicacion.WorkersApp;
import controlador.Conector;
import controlador.Funciones;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import javax.swing.JOptionPane;
import modelo.*;

import static vista.Principal.activado;
import workers.Workers;

/**
 *
 * @author Will
 */
public class AgregarPrestamo extends javax.swing.JFrame {

    public static Cliente cliente;
    public static ArrayList<Plan> planes;
    public static ArrayList<Cuota> cuotas;
    public static ArrayList<Cuota> cuotasAux;
    public static TMCuota modeloCuotas;
    public static Plan planN;
    public static EditFecha eFecha;
    
    private static double importeDescuento; 
    private static double importeCuota; 
    private static int cantCuotas; 
    private static String status; 
    static Conector con;
    
    public AgregarPrestamo() {
        initComponents();
    }
    
    public AgregarPrestamo(Cliente cli) {
        initComponents();
        setLocationRelativeTo(null);
        con = new Conector();
        cliente = cli;
        cantCuotas = 0;
        importeDescuento = 0.0;
        lbNombre.setText(Funciones.capitalize(cli.getNombre()));
        status = "nodebia";
        //CARGA PLANES
        
        listaPlanes.removeAll();
        //Recorrer el contenido del ArrayList
        int contPlanes =0;
        for(Plan planX: Principal.planes) {
            System.out.println("PLAN: "+planX.getNombre() + " CONTIENE APP: "+planX.getNombre().toLowerCase().contains("-app"));
            if (!planX.getNombre().toLowerCase().contains("-app")) {
                if (!planX.getA4().equals("personalizado")) {
                    listaPlanes.add(planX.getNombre().toUpperCase());    
                    contPlanes++;
                }
            }
        }
        lbPlanesExis.setText("Planes existentes ["+Integer.toString(contPlanes)+"]: ");
        //INICIALIZA TABLA CUOTAS
        cuotas = new ArrayList<>();
        cuotasAux = new ArrayList<>();
        
        modeloCuotas = new TMCuota(cuotas);
        tablaClientes.setModel(modeloCuotas);
        
        //INICIALIZA VALORES
        lbFechaAct.setText(Funciones.devolverFechaActualStr());
        fechaPrimerPago.setDate(Funciones.stringADate(Funciones.devolverFechaActualStr()));
        
        comboCobradores.removeAllItems();
        ArrayList<String> nombreCob = new ArrayList<String>();;
        
        nombreCob.add("XXX - Sin especificar");
        
        for (Cobrador c: Principal.cobradores) {
            nombreCob.add(c.getCodInt()+" - "+Funciones.capitalize(c.getNombre()));
        }
        
        Collections.sort(nombreCob);
        
        for (String nom: nombreCob) {
            comboCobradores.addItem(nom);
        }
        
        
        if (cliente.getA1().equals("xxx")) {
            comboCobradores.setSelectedItem("XXX - Sin especificar");
        }
        Cobrador c1 = Funciones.getCobradorPorRuta(Principal.cobradores, cliente.getA1());
        comboCobradores.setSelectedItem(c1.getCodInt()+" - "+Funciones.capitalize(c1.getNombre()));
    }
    
    public AgregarPrestamo(Cliente cli, int cCuotas, double iCuota, double iDescuento, Date fechaRenovacion) {
        initComponents();
        setLocationRelativeTo(null);
        
        status = "debia";
        importeDescuento = iDescuento; 
        importeCuota = iCuota; 
        cantCuotas = cCuotas;
    
        for (int i = 0; i<190; i++) {
            comboCuotasDesc.addItem(Integer.toString(i));
        }
         
        descInicial.setText(Double.toString(iDescuento));
        impCuotaDesc.setText(Double.toString(iCuota));
        comboCuotasDesc.setSelectedItem(Integer.toString(cCuotas));
        
       
        
        cliente = cli;
        
        lbNombre.setText(Funciones.capitalize(cli.getNombre()));
        
        listaPlanes.removeAll();
        
        //Recorrer el contenido del ArrayList
        for(Plan planX: Principal.planes) {
            if (!planX.getA4().equalsIgnoreCase("personalizado") || !planX.getNombre().toLowerCase().contains("-app")) {
                listaPlanes.add(planX.getNombre().toUpperCase());    
            }
        }
        
        //INICIALIZA TABLA CUOTAS
        cuotas = new ArrayList<>();
        cuotasAux = new ArrayList<>();
        
        modeloCuotas = new TMCuota(cuotas);
        tablaClientes.setModel(modeloCuotas);
        
        //INICIALIZA VALORES
        lbFechaAct.setText(Funciones.dateAString(fechaRenovacion));
        fechaPrimerPago.setDate(fechaRenovacion);
        
        comboCobradores.removeAllItems();
        ArrayList<String> nombreCob = new ArrayList<String>();;
        
        nombreCob.add("XXX - Sin especificar");
        
        for (Cobrador c: Principal.cobradores) {
            nombreCob.add(c.getCodInt()+" - "+Funciones.capitalize(c.getNombre()));
        }
        
        Collections.sort(nombreCob);
        
        for (String nom: nombreCob) {
            comboCobradores.addItem(nom);
        }
        
        
        if (cliente.getA1().equals("xxx")) {
            comboCobradores.setSelectedItem("XXX - Sin especificar");
        }
        Cobrador c1 = Funciones.getCobradorPorRuta(Principal.cobradores, cliente.getA1());
        comboCobradores.setSelectedItem(c1.getCodInt()+" - "+Funciones.capitalize(c1.getNombre()));
    }

   
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel11 = new javax.swing.JLabel();
        lbNombre = new javax.swing.JLabel();
        botonGuardar = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        listaPlanes = new java.awt.List();
        lbPlanesExis = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        lbImpPrestar = new javax.swing.JLabel();
        lbImpDevolver = new javax.swing.JLabel();
        lbImpInteres = new javax.swing.JLabel();
        lbPlan = new javax.swing.JLabel();
        lbPorcInteres = new javax.swing.JLabel();
        lbTipo = new javax.swing.JLabel();
        lbImpFin = new javax.swing.JLabel();
        lbCuotas = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lbFin = new javax.swing.JLabel();
        lbp1 = new javax.swing.JLabel();
        lbp2 = new javax.swing.JLabel();
        lbPagoInicial = new javax.swing.JLabel();
        lbImpTotal = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lbFechaAct = new javax.swing.JTextField();
        sabados = new javax.swing.JCheckBox();
        jLabel12 = new javax.swing.JLabel();
        domingos = new javax.swing.JCheckBox();
        jLabel10 = new javax.swing.JLabel();
        fechaPrimerPago = new org.jdesktop.swingx.JXDatePicker();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaClientes = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        obsPrestamo = new javax.swing.JTextArea();
        jLabel9 = new javax.swing.JLabel();
        btnPers = new javax.swing.JButton();
        buscarPlan = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        comboCobradores = new javax.swing.JComboBox<>();
        jPanel5 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        descInicial = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        comboCuotasDesc = new javax.swing.JComboBox<>();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        impCuotaDesc = new javax.swing.JTextField();
        dineroEnSobre = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_prestamo.png"))); // NOI18N
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel11.setText("Nuevo préstamo / financiación:");

        lbNombre.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbNombre.setText("Nombre");

        botonGuardar.setText("Generar préstamo");
        botonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarActionPerformed(evt);
            }
        });

        listaPlanes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listaPlanesMouseClicked(evt);
            }
        });

        lbPlanesExis.setText("Planes existentes:");

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbImpPrestar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbImpPrestar.setText("$0.0");

        lbImpDevolver.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbImpDevolver.setText("$0.0");

        lbImpInteres.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbImpInteres.setText("$0.0");

        lbPlan.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbPlan.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbPlan.setText("NINGÚN PLAN SELECCIONADO");

        lbPorcInteres.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbPorcInteres.setText("0%");

        lbTipo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbTipo.setText("Diario");

        lbImpFin.setText("Imp. Prestar:");

        lbCuotas.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbCuotas.setText("0");

        jLabel3.setText("Imp. Devolver:");

        jLabel4.setText("Porc. Interés:");

        jLabel5.setText("Imp. Interés:");

        jLabel6.setText("Tipo:");

        jLabel7.setText("Cuotas:");

        lbFin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbFin.setText(" ");

        lbp1.setText("Imp. Total:");

        lbp2.setText("Pago Inicial:");

        lbPagoInicial.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbPagoInicial.setText("$0.0");

        lbImpTotal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbImpTotal.setText("$0.0");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbImpFin, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lbp1, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lbp2, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(lbImpPrestar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lbImpInteres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lbImpDevolver, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lbPorcInteres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lbImpTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lbPagoInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(53, 53, 53)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lbCuotas, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 36, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbFin, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbPlan, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbPlan)
                .addGap(2, 2, 2)
                .addComponent(lbFin)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbp1)
                    .addComponent(lbImpTotal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbp2)
                    .addComponent(lbPagoInicial))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbImpFin)
                    .addComponent(lbImpPrestar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lbImpDevolver))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(lbImpInteres))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(lbPorcInteres))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(lbTipo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(lbCuotas))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbFechaAct.setEditable(false);
        lbFechaAct.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbFechaActMouseClicked(evt);
            }
        });

        sabados.setBackground(new java.awt.Color(255, 255, 255));
        sabados.setSelected(true);
        sabados.setText("Permitir pagos los Sábados");
        sabados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sabadosActionPerformed(evt);
            }
        });

        jLabel12.setText("Generado el:");

        domingos.setBackground(new java.awt.Color(255, 255, 255));
        domingos.setText("Permitir pagos los Domingos");
        domingos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                domingosActionPerformed(evt);
            }
        });

        jLabel10.setText("Primer pago:");

        fechaPrimerPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fechaPrimerPagoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbFechaAct, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fechaPrimerPago, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(38, 38, 38)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(domingos)
                    .addComponent(sabados))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbFechaAct, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel12))
                    .addComponent(sabados))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(fechaPrimerPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10))
                    .addComponent(domingos))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tablaClientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaClientesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaClientes);

        jLabel8.setText("Simulador de cuotas");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        obsPrestamo.setColumns(20);
        obsPrestamo.setRows(5);
        jScrollPane2.setViewportView(obsPrestamo);

        jLabel9.setText("Observaciones (360 caract.)");

        btnPers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/img/ico_custom.png"))); // NOI18N
        btnPers.setText("Utilizar plan personalizado");
        btnPers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPersActionPerformed(evt);
            }
        });

        buscarPlan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                buscarPlanKeyReleased(evt);
            }
        });

        jLabel13.setText("Buscar plan por nombre:");

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel14.setText("Cobrador asignado:");

        comboCobradores.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Sin especificar" }));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(comboCobradores, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboCobradores, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(43, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel15.setText("Descuentos:");

        descInicial.setEditable(false);
        descInicial.setBackground(new java.awt.Color(255, 255, 255));
        descInicial.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        descInicial.setForeground(new java.awt.Color(204, 0, 0));

        jLabel2.setText("Descuento incial:");

        comboCuotasDesc.setEnabled(false);

        jLabel16.setText("Cuotas:");

        jLabel17.setText("Importe cuota desc:");

        impCuotaDesc.setEditable(false);
        impCuotaDesc.setBackground(new java.awt.Color(255, 255, 255));
        impCuotaDesc.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        impCuotaDesc.setForeground(new java.awt.Color(204, 0, 0));

        dineroEnSobre.setEditable(false);
        dineroEnSobre.setBackground(new java.awt.Color(255, 255, 255));
        dineroEnSobre.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        dineroEnSobre.setForeground(new java.awt.Color(51, 153, 0));

        jLabel18.setText("En sobre:");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15)
                    .addComponent(jLabel2)
                    .addComponent(descInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(comboCuotasDesc, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(impCuotaDesc)))
                    .addComponent(jLabel18)
                    .addComponent(dineroEnSobre, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(descInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jLabel17))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboCuotasDesc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(impCuotaDesc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dineroEnSobre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(botonGuardar)
                        .addGap(10, 10, 10))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lbNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 291, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(listaPlanes, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
                                    .addComponent(buscarPlan, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel13)
                                    .addComponent(lbPlanesExis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(btnPers, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane2)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel9)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(lbPlanesExis)
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(listaPlanes, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buscarPlan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPers, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void setFecha(Date fecha) {
        lbFechaAct.setText(Funciones.dateAString(fecha));
    }
    
    private void botonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarActionPerformed
            if (tablaClientes.getRowCount()<1) {
                JOptionPane.showMessageDialog(null,"Seleccione un plan para el nuevo préstamo","Error", 0);
            } else {
                if (lbPlan.getText().equals("NINGÚN PLAN SELECCIONADO")) {
                    JOptionPane.showMessageDialog(null,"Seleccione un plan para el nuevo préstamo","Error", 0);
                } else {
                    
                
            Prestamo prestamo = new Prestamo();

            String cobradorSel = (String) comboCobradores.getSelectedItem();

            if (cobradorSel.equals("XXX - Sin especificar")) {
                prestamo.setCodCobrador("XXX");
            } else {

                    String[] arr1 = cobradorSel.split("-");
                    arr1[0] = arr1[0].replaceAll(" ", "");

                    prestamo.setCodCobrador(arr1[0]);
            }


            prestamo.setCodCliente(Integer.toString(cliente.getCod()));
            prestamo.setFechaGenerado(lbFechaAct.getText());
            prestamo.setPlan(lbPlan.getText());
            prestamo.setObservacion(obsPrestamo.getText());
            prestamo.setImporteDescuento(Double.toString(importeDescuento));

            con.conectar();
            
            if (status.equals("debia")) {
                //PAGA CUOTA PENDIENTE 
                if (cliente.getCuotasAdeudadas().size()>0) {
                    Cuota cuotaSinPagar = cliente.getCuotasAdeudadas().get(0);
                    cuotaSinPagar.setImportePagadoD(cuotaSinPagar.getImporteTotalD());
                    cuotaSinPagar.setFechaPagado(Funciones.devolverFechaActualStr());

                    con.actualizarCuota(cuotaSinPagar);
                }
//                PAGAR CUOTA INTERES PENDIENTE SI HAY
                if (cliente.getCuotasInteresAdeudadas().size()>0) {
                    Cuota cuotaIntSinPagar = cliente.getCuotasInteresAdeudadas().get(0);
                    cuotaIntSinPagar.setFechaPagado(Funciones.dateAString(new Date()));
                    con.actualizarCuota(cuotaIntSinPagar);
                }
            }
            
            
            con.guardarPrestamo(prestamo);
            prestamo = con.getUltimoPrestamo();
            for (Cuota cuotaAux: cuotas) {
                cuotaAux.setCodPrestamo(Integer.toString(prestamo.getCod()));
                con.guardarCuota(cuotaAux);
                cuotasAux.add(con.getUltimaCuota());
            }
            prestamo.setCuotas(cuotasAux);
            
            con.actualizarPrestamo(prestamo);
            con.cerrar();

            //COMPRUEBA QUE SE HAYA GENERADO CORRECTAMENTE Y ASIGNADO EL COD DE PRESTAMO A LAS CUOTAS
            con.conectar();
            ArrayList<Cuota> cuotasFallidas = con.getCuotasPorCodPrestamo("999999");
            con.cerrar();

            if (cuotasFallidas.size()>0) {
                JOptionPane.showMessageDialog(null,"El produjo un error al asignar cuotas al préstamo,revise los datos \ndel préstamo y en caso de no ser correctos, eliminelo y vuelva a cargarlo.","Préstamo generado", 0);
                con.conectar();
                con.eliminarCuotaPorCodPrestamo("999999");
                con.cerrar();
            } else {
                JOptionPane.showMessageDialog(null,"El préstamo se ha generado correctamente","Préstamo generado", 1);
            }
            
            Principal.blanquearCamposCliente();
            Workers.workerRefresh();
            //GUARDA EN LA APP
            WorkersApp.guardarUltimoPrestamoPorCodCli(cliente.getCod());
            this.dispose();
            }
                }
        
    }//GEN-LAST:event_botonGuardarActionPerformed

    public static void cargarPlanPersonalizado(String nPlan) {
        listaPlanes.removeAll();
        listaPlanes.add(nPlan);
        listaPlanes.select(0);
        lbPlan.setText(nPlan.toUpperCase());
        con.conectar();
        planN = con.getPlanPorNombre(nPlan.toLowerCase());
        con.cerrar();
        
        lbImpPrestar.setText("$" + planN.getImportePrestar());
        lbImpDevolver.setText("$" + planN.getImporteDevolver());
        lbImpInteres.setText("$" + planN.getImporteInteres());
        lbPorcInteres.setText(planN.getPorcentajeInteres() + "%");
        
        if (planN.getTipo().contains("p-")) {
            String[] arr = planN.getTipo().split("-");
            lbTipo.setText("Cada "+arr[1]+" días.");
        } else {
            lbTipo.setText(Funciones.capitalize(planN.getTipo()));
        }
        
        lbCuotas.setText(planN.getCantCuotas());
        
        generarCuotas();
    }
    
    private void listaPlanesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listaPlanesMouseClicked
        String selec = listaPlanes.getSelectedItem();
        lbPlan.setText(selec.toUpperCase());
        con.conectar();
        planN = con.getPlanPorNombre(selec.toLowerCase());
        con.cerrar();
        
        lbImpPrestar.setText("$" + planN.getImportePrestar());
        lbImpDevolver.setText("$" + planN.getImporteDevolver());
        lbImpInteres.setText("$" + planN.getImporteInteres());
        lbPorcInteres.setText(planN.getPorcentajeInteres() + "%");
        
        if (planN.getA1().equals("producto")) {
            lbFin.setText("FINANCIACIÓN");
            lbp1.setText("Imp. total:");
            lbp2.setText("Pago inicial:");
            lbImpTotal.setText("$" + planN.getA3());
            lbPagoInicial.setText("$" + planN.getA2());
            lbImpFin.setText("Imp. Financiar:");
        } else {
            lbFin.setText("PRÉSTAMO");
            lbp1.setText("");
            lbp2.setText("");
            lbImpTotal.setText("");
            lbPagoInicial.setText("");
            lbImpFin.setText("Imp. Prestar:");
        }
        
        
        if (planN.getTipo().contains("p-")) {
            String[] arr = planN.getTipo().split("-");
            lbTipo.setText("Cada "+arr[1]+" días.");
        } else {
            lbTipo.setText(Funciones.capitalize(planN.getTipo()));
        }
        
        lbCuotas.setText(planN.getCantCuotas());
        
        generarCuotas();
    }//GEN-LAST:event_listaPlanesMouseClicked

    private static void generarCuotas() {
        cuotas.clear();
        Date fechaInicial = fechaPrimerPago.getDate();
        
        //SI ES PRODUCTO Y TIENE PAGO INICIAL
        if (planN.getA1().equals("producto")) {
            double impIni = 0.0;
            try {
                impIni = Double.parseDouble(planN.getA2());
            } catch (Exception e) {
                
            }
            if (impIni>0) {
                Cuota newCuota = new Cuota();
            
                newCuota.setCodPrestamo("");
                newCuota.setFechaPagar(Funciones.devolverFechaActualStr());
                newCuota.setFechaPagado(Funciones.devolverFechaActualStr());
                
                
                newCuota.setImportePagado(planN.getA2());
                newCuota.setImporteTotal(planN.getA2());
                newCuota.setNumCuota("0");
                cuotas.add(newCuota);
            }
            
        }
        
        
        double valorCuotaAux = Double.parseDouble(planN.getImporteDevolver()) / Double.parseDouble(planN.getCantCuotas());
        Cuota newCuota = new Cuota();
        
        for (int i = 0; i<Integer.parseInt(planN.getCantCuotas())+cantCuotas; i++){
            newCuota = new Cuota();
            newCuota.setNumCuota(Integer.toString(i+1));
            int numCuota = i+1;
            newCuota.setCodPrestamo("");
            
                
            if (i == 0) {
                newCuota.setFechaPagar(Funciones.dateAString(fechaInicial));
            } else {
                if (planN.getTipo().equals("semanal")) {
                    fechaInicial = Funciones.sumarDias(fechaInicial, 7);
                    if (!sabados.isSelected()) {
                        if (Funciones.caeSabado(fechaInicial)) {
                           fechaInicial = Funciones.sumarDias(fechaInicial, 2);
                        }
                    }   
                    if (!domingos.isSelected()) {
                        if (Funciones.caeDomingo(fechaInicial)) {
                           fechaInicial = Funciones.sumarDias(fechaInicial, 1);
                        }   
                    }
                }
                if (planN.getTipo().equals("diario")) {
                    fechaInicial = Funciones.sumarDias(fechaInicial, 1);
                    if (!sabados.isSelected()) {
                        if (Funciones.caeSabado(fechaInicial)) {
                           fechaInicial = Funciones.sumarDias(fechaInicial, 2);
                        }
                    }   
                    if (!domingos.isSelected()) {
                        if (Funciones.caeDomingo(fechaInicial)) {
                           fechaInicial = Funciones.sumarDias(fechaInicial, 1);
                        }   
                    }
                }    
                if (planN.getTipo().equals("mensual")) {
                    fechaInicial = Funciones.sumarMeses(fechaInicial, 1);
                    if (!sabados.isSelected()) {
                        if (Funciones.caeSabado(fechaInicial)) {
                           fechaInicial = Funciones.sumarDias(fechaInicial, 2);
                        }
                    }   
                    if (!domingos.isSelected()) {
                        if (Funciones.caeDomingo(fechaInicial)) {
                           fechaInicial = Funciones.sumarDias(fechaInicial, 1);
                        }   
                    }
                }
                if (planN.getTipo().contains("p-")) {
                    String[] arr = planN.getTipo().split("-");
                    fechaInicial = Funciones.sumarDias(fechaInicial, Integer.parseInt(arr[1]));
                }
                newCuota.setFechaPagar(Funciones.dateAString(fechaInicial));
            }
            
            newCuota.setFechaPagado("01/01/2099");
            newCuota.setImportePagado("0.0");
            
            if (numCuota>Integer.parseInt(planN.getCantCuotas())) {
                newCuota.setImporteTotal(Double.toString(Funciones.formatearDecimales(importeCuota,2)));
            } else {
                newCuota.setImporteTotal(Double.toString(Funciones.formatearDecimales(valorCuotaAux,2)));
            }
            
            
            cuotas.add(newCuota);
        }
        
        //SI HAY CUOTAS ADICIONALES DE INTERES QUE AGREGAR SE AGREGAN
        if (cantCuotas>0) {
            for (int i = 0; i<cantCuotas; i++) {
                newCuota = new Cuota();
                newCuota.setCodPrestamo("");
                newCuota.setFechaPagar("01/01/2099");
                newCuota.setFechaPagado("01/01/2099");
                newCuota.setImportePagado("0.0");
                newCuota.setImporteTotal("0.0");
                newCuota.setNumCuota("999");
            }
        }
        
        //AGREGA CUOTA NUM 999 QUE ES INTERES ACUMULADO
        newCuota = new Cuota();
        newCuota.setCodPrestamo("");
        newCuota.setFechaPagar("01/01/2099");
        newCuota.setFechaPagado("01/01/2099");
        newCuota.setImportePagado("0.0");
        newCuota.setImporteTotal("0.0");
        newCuota.setNumCuota("999");
        cuotas.add(newCuota);
        
        //COMPRUEBA SI CAE DOMINGO O SABADO
//        int acumuladorDias = 0;
//        
//        for (Cuota cuota: cuotas) {
//            Date fechaPago = Funciones.stringADate(fecha);
//            if (!sabados.isSelected()) {
//                if (Funciones.caeSabado(fechaPago)) {
//                   acumuladorDias +=2;
//                }
//            }   
//            if (!domingos.isSelected()) {
//                if (Funciones.caeDomingo(fechaPago)) {
//                    acumuladorDias +=1;
//                }
//            }   
//            fechaPago = Funciones.sumarDias(fechaPago,acumuladorDias);
//            
//            cuota.setFechaPagar(Funciones.dateAString(fechaPago));
//
//        }
        
        
        modeloCuotas = new TMCuota(cuotas);
        tablaClientes.setModel(modeloCuotas);
        
        //ACTUALIZA DINERO EN SOBRE
        dineroEnSobre.setText(Double.toString(Double.parseDouble(planN.getImportePrestar()) - importeDescuento));
    }
    
    private void tablaClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaClientesMouseClicked
        

    }//GEN-LAST:event_tablaClientesMouseClicked

    private void fechaPrimerPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fechaPrimerPagoActionPerformed
        generarCuotas();
    }//GEN-LAST:event_fechaPrimerPagoActionPerformed

    private void domingosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_domingosActionPerformed
        generarCuotas();
    }//GEN-LAST:event_domingosActionPerformed

    private void btnPersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPersActionPerformed
        PlanPers pp = new PlanPers();
        pp.setTitle("Utilizar plan personalizado");
        pp.setVisible(true);
    }//GEN-LAST:event_btnPersActionPerformed

    private void sabadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sabadosActionPerformed
        generarCuotas();
    }//GEN-LAST:event_sabadosActionPerformed

    private void buscarPlanKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_buscarPlanKeyReleased
        listaPlanes.removeAll();

        //Recorrer el contenido del ArrayList
        for(Plan planX: Principal.planes) {
            if (!planX.getA4().equals("personalizado") && !planX.getNombre().contains("-app") && planX.getNombre().toUpperCase().contains(buscarPlan.getText().toUpperCase())) {
                        listaPlanes.add(planX.getNombre().toUpperCase());    
            }
        }
    }//GEN-LAST:event_buscarPlanKeyReleased

    private void lbFechaActMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbFechaActMouseClicked
        if (eFecha != null) {//si existe una venta, la cierra.
            eFecha.dispose();
        }
        
        eFecha = new EditFecha(lbFechaAct.getText(), "agregarprestamo");
        eFecha.setTitle("Editar fecha");
        eFecha.setVisible(true);
    }//GEN-LAST:event_lbFechaActMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AgregarPrestamo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AgregarPrestamo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AgregarPrestamo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AgregarPrestamo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AgregarPrestamo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonGuardar;
    private javax.swing.JButton btnPers;
    private javax.swing.JTextField buscarPlan;
    private javax.swing.JComboBox<String> comboCobradores;
    private static javax.swing.JComboBox<String> comboCuotasDesc;
    private static javax.swing.JTextField descInicial;
    private static javax.swing.JTextField dineroEnSobre;
    public static javax.swing.JCheckBox domingos;
    public static org.jdesktop.swingx.JXDatePicker fechaPrimerPago;
    private static javax.swing.JTextField impCuotaDesc;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    public static javax.swing.JLabel jLabel3;
    public static javax.swing.JLabel jLabel4;
    public static javax.swing.JLabel jLabel5;
    public static javax.swing.JLabel jLabel6;
    public static javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    public static javax.swing.JLabel lbCuotas;
    public static javax.swing.JTextField lbFechaAct;
    private javax.swing.JLabel lbFin;
    public static javax.swing.JLabel lbImpDevolver;
    public static javax.swing.JLabel lbImpFin;
    public static javax.swing.JLabel lbImpInteres;
    public static javax.swing.JLabel lbImpPrestar;
    public static javax.swing.JLabel lbImpTotal;
    private javax.swing.JLabel lbNombre;
    public static javax.swing.JLabel lbPagoInicial;
    public static javax.swing.JLabel lbPlan;
    private javax.swing.JLabel lbPlanesExis;
    public static javax.swing.JLabel lbPorcInteres;
    public static javax.swing.JLabel lbTipo;
    public static javax.swing.JLabel lbp1;
    public static javax.swing.JLabel lbp2;
    public static java.awt.List listaPlanes;
    private javax.swing.JTextArea obsPrestamo;
    public static javax.swing.JCheckBox sabados;
    public static javax.swing.JTable tablaClientes;
    // End of variables declaration//GEN-END:variables
}

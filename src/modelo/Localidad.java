/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.Funciones;
import java.util.Date;

/**
 *
 * @author Will
 */
public class Localidad {
    private int cod;
    private String codInt, nombre, a1, a2;

    public Localidad() {
        this.cod = 0;
        this.codInt = "XXX";
        this.nombre = "";
        this.a1 = "";
        this.a2 = "";
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getCodInt() {
        return codInt;
    }

    public void setCodInt(String codInt) {
        this.codInt = codInt;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getA1() {
        return a1;
    }

    public void setA1(String a1) {
        this.a1 = a1;
    }

    public String getA2() {
        return a2;
    }

    public void setA2(String a2) {
        this.a2 = a2;
    }

    
}

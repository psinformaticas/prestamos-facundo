/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;


public class TMStatsCob implements TableModel {

    private List<String> statsCob;
    
    public TMStatsCob(List<String> lista) {
        statsCob = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return statsCob.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Cobrador";
                break;
            }
            case 1: {
                titulo = "N° Clientes";
                break;                
            }
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
            return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String linea = statsCob.get(rowIndex);
        String[] aux = linea.split("-i-");
        String valor = null;
        switch(columnIndex) {
            case 0: {
                valor  = aux[0];
                break;
            }
            case 1: {
                valor  = aux[1];
                break;                
            }
        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

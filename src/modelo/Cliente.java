/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.Funciones;
import java.util.ArrayList;
import java.util.Collections;
import vista.Principal;

/**
 *
 * @author Will
 */
public class Cliente {
    
    private int cod;
    private String nombre, dni, fechaNac, fechaAlta, observaciones, direccion, localidad, codPrestamos, telefono, celular, email, ref1, ref2, ref3, ref4, a1, a2, a3, ultimoPago, esClienteNuevo;

    public Cliente() {
        this.cod = 99999999;
        this.nombre = "";
        this.dni = "";
        this.fechaNac = "01/01/1900";
        this.fechaNac = "01/01/1900";
        this.observaciones = "";
        this.direccion = "";
        this.localidad = "";
        this.codPrestamos = "";
        this.telefono = "";
        this.celular = "";
        this.email = "";
        this.ref1 = " -i- -i- ";
        this.ref2 = " -i- -i- ";
        this.ref3 = " -i- -i- ";
        this.ref4 = " -i- -i- ";
        this.a1 = "";
        this.a2 = "";
        this.a3 = "";
        this.ultimoPago = "01/01/2099-00:00-1000";
        this.esClienteNuevo = "";
    }

    public boolean tieneTodasLasCuotasPagasSinContarInt() {
        if (getCuotasAdeudadas().size() == 0) {
            return true;
        }
        return false;
    }
    
    public boolean tieneCuotasPagasYDebeInteres() {
        if (getCuotasAdeudadas().size() == 0) {
            ArrayList<Cuota> cuotasRet = new ArrayList<>();
            for (Prestamo prestamo: getPrestamos()) {
                if (prestamo.getInteresTotalAlDiaDeHoy()>0) {
                    for (Cuota cuota: prestamo.getCuotas()) {
                        int numCuota = Integer.parseInt(cuota.getNumCuota());
                        if (numCuota==999 && cuota.getImportePagadoD() == 0) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
            
    public ArrayList<Cuota> getCuotasAdeudadas() {
        ArrayList<Cuota> cuotasRet = new ArrayList<>();
        for (Prestamo prestamo: getPrestamos()) {
            for (Cuota cuota: prestamo.getCuotas()) {
                int numCuota = Integer.parseInt(cuota.getNumCuota());
                if (numCuota>0 && numCuota<999 && !cuota.yaSePago()) {
                    cuotasRet.add(cuota);
                }
            }
        }
        return cuotasRet;
    }
    
    public ArrayList<Cuota> getCuotasInteresAdeudadas() {
        ArrayList<Cuota> cuotasRet = new ArrayList<>();
        for (Prestamo prestamo: getPrestamos()) {
            for (Cuota cuota: prestamo.getCuotas()) {
                int numCuota = Integer.parseInt(cuota.getNumCuota());
                if (numCuota==999 && cuota.getFechaPagado().equals("01/01/2099")) {
                    cuotasRet.add(cuota);
                }
            }
        }
        return cuotasRet;
    }
    
    public ArrayList<Prestamo> getPrestamos() {
        return Funciones.getPrestamosPorCli(Principal.todosLosPrestamos, Integer.toString(cod));
    }
    
    public Prestamo getUltimoPrestamo() {
        ArrayList<Prestamo> prestamos = getPrestamos();
        System.out.println("SIZE: " + prestamos.size());
        if (prestamos.size()>0) {
            Collections.sort(prestamos);
            return prestamos.get(prestamos.size()-1);   
        }
        return null;
        
    }
    
    //GETTERS Y SETTERS
    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getCodPrestamos() {
        return codPrestamos;
    }

    public void setCodPrestamos(String codPrestamos) {
        this.codPrestamos = codPrestamos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRef1() {
        return ref1;
    }

    public void setRef1(String ref1) {
        this.ref1 = ref1;
    }

    public String getRef2() {
        return ref2;
    }

    public void setRef2(String ref2) {
        this.ref2 = ref2;
    }

    public String getRef3() {
        return ref3;
    }

    public void setRef3(String ref3) {
        this.ref3 = ref3;
    }

    public String getRef4() {
        return ref4;
    }

    public void setRef4(String ref4) {
        this.ref4 = ref4;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getA1() {
        return a1;
    }

    public void setA1(String a1) {
        this.a1 = a1;
    }

    public String getA2() {
        return a2;
    }

    public void setA2(String a2) {
        this.a2 = a2;
    }

    public String getA3() {
        return a3;
    }

    public void setA3(String a3) {
        this.a3 = a3;
    }
    
    public String getUltimoPagoFecha() {
        return ultimoPago.split("-")[0];
    }
    
    public String getUltimoPagoHora() {
        return ultimoPago.split("-")[1];
    }
    
    public String getUltimoPagoImporte() {
        return ultimoPago.split("-")[2];
    }
    
    public void setUltimoPago(String str) {
        this.ultimoPago = str;
    }
    
    public String getUltimoPago() {
        return this.ultimoPago;
    }

    public String getEsClienteNuevo() {
        return esClienteNuevo;
    }

    public void setEsClienteNuevo(String esClienteNuevo) {
        this.esClienteNuevo = esClienteNuevo;
    }
    
    
}

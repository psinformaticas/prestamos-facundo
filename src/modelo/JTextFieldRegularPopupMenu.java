/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.*;

import java.awt.event.ActionEvent;
import javax.swing.JPopupMenu;
import javax.swing.undo.*;
import javax.swing.JOptionPane;

public class JTextFieldRegularPopupMenu {
    public static void addTo(JTextField txtField) 
    {
        JPopupMenu popup = new JPopupMenu();
        
        Action pasteAction = new AbstractAction("Pegar") {
            @Override
            public void actionPerformed(ActionEvent ae) {
                txtField.paste();
            }
        };



        popup.add (pasteAction);

       txtField.setComponentPopupMenu(popup);
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.Date;

/**
 *
 * @author Will
 */
public class UsuarioApp {
    
    private int userid, groupid, lastActive, showAvt, banned, regTime, codPC, codCobPC;
    private String username, displayName, password, email, key, validated, fbProfile, localidad, token, location;

    public UsuarioApp() {
        this.userid = 0;
        this.codPC = 0;
        this.codCobPC = 0;
        this.groupid = 1;
        this.lastActive = 1;
        this.showAvt = 1;
        this.banned = 1;
        this.regTime = 1;
        this.username = "";
        this.displayName = "";
        this.password = "";
        this.email = "";
        this.key = "";
        this.validated = "";
        this.fbProfile = "";
        this.localidad = "";
        this.location = "";
        this.token = "";
    }

    @Override
    public String toString() {
        return "ID: "+this.userid+"\nUSERNAME: "+this.username+"\nPASSWORD: "+this.getPassword()+"\nFB PROFILE: "+this.fbProfile;
    }
    
    
    public int getBanned() {
        return banned;
    }

    public void setBanned(int banned) {
        this.banned = banned;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getGroupid() {
        return groupid;
    }

    public void setGroupid(int groupid) {
        this.groupid = groupid;
    }

    public int getLastActive() {
        return lastActive;
    }

    public void setLastActive(int lastActive) {
        this.lastActive = lastActive;
    }

    public int getShowAvt() {
        return showAvt;
    }

    public void setShowAvt(int showAvt) {
        this.showAvt = showAvt;
    }

    public int getRegTime() {
        return regTime;
    }

    public void setRegTime(int regTime) {
        this.regTime = regTime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValidated() {
        return validated;
    }

    public void setValidated(String validated) {
        this.validated = validated;
    }

    public String getFbProfile() {
        return fbProfile;
    }

    public void setFbProfile(String fbProfile) {
        this.fbProfile = fbProfile;
    }

    public int getCodPC() {
        return codPC;
    }

    public void setCodPC(int codPC) {
        this.codPC = codPC;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getCodCobPC() {
        return codCobPC;
    }

    public void setCodCobPC(int codCobPC) {
        this.codCobPC = codCobPC;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
        
    
    
    
    
    
}

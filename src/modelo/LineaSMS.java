/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Usuario
 */
public class LineaSMS {
    private String telefono, nombre, deudaCuotas, deudaRecargos, fechaHoy;

    public LineaSMS() {
        this.telefono = "";
        this.nombre = "";
        this.deudaCuotas = "";
        this.deudaRecargos = "";
        this.fechaHoy = "";
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDeudaCuotas() {
        return deudaCuotas;
    }

    public void setDeudaCuotas(String deudaCuotas) {
        this.deudaCuotas = deudaCuotas;
    }

    public String getDeudaRecargos() {
        return deudaRecargos;
    }

    public void setDeudaRecargos(String deudaRecargos) {
        this.deudaRecargos = deudaRecargos;
    }

    public String getFechaHoy() {
        return fechaHoy;
    }

    public void setFechaHoy(String fechaHoy) {
        this.fechaHoy = fechaHoy;
    }
    
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.Funciones;
import java.util.Date;

/**
 *
 * @author Will
 */
public class DatosComprobante {

    private String tipo, numero, fecha, nombre, direccion, telefono;
    private String cant1, detalle1, monto1, total;
    private String obs;

    public DatosComprobante() {
        this.tipo = "";
        this.numero = "";
        this.fecha = "";
        this.nombre = "";
        this.direccion = "";
        this.telefono = "";
        this.cant1 = "";
        this.detalle1 = "";
        this.monto1 = "";
        this.total = "";
        this.obs = "";
    }

    
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCant1() {
        return cant1;
    }

    public void setCant1(String cant1) {
        this.cant1 = cant1;
    }

    public String getDetalle1() {
        return detalle1;
    }

    public void setDetalle1(String detalle1) {
        this.detalle1 = detalle1;
    }

    public String getMonto1() {
        return monto1;
    }

    public void setMonto1(String monto1) {
        this.monto1 = monto1;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }
    
    

            
}

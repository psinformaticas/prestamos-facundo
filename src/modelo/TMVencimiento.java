/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;
import java.util.Date;
import vista.*;


public class TMVencimiento implements TableModel {

    private final List<Cuota> cuotas;
    private Prestamo prestamoAux;
    
    public TMVencimiento(List<Cuota> lista) {
        cuotas = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return cuotas.size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Cód.";
                break;
            }
            case 1: {
                titulo = "Vencimiento";
                break;
            }
            case 2: {
                titulo = "Cliente";
                break;                
            }

            case 3: {
                titulo = "Plan";
                break;                
            }
            case 4: {
                titulo = "Cuota";
                break;                
            }
            case 5: {
                titulo = "Importe";
                break;                
            }
            case 6: {
                titulo = "Pagado";
                break;                
            }
            case 7: {
                titulo = "Estado";
                break;                
            }
            case 8: {
                titulo = "";
                break;                
            }
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 0) {
            return Integer.class;
        }
        if (columnIndex == 1) {
            return Date.class;
        }
        if (columnIndex == 4) {
            return Integer.class;
        }
        else {
            return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 8) {
            return true;
        }
        
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Cuota cuota = cuotas.get(rowIndex);
        prestamoAux = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, Integer.parseInt(cuota.getCodPrestamo()));
        String codCli = prestamoAux.getCodCliente();
        Cliente cli = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(codCli));
        String valor = null;
        int valorInt = 0;
        
        switch(columnIndex) {
            case 0: {
                //valor  = Integer.toString(cuota.getCod());
                valorInt = cuota.getCod();
                
                return valorInt;
                
                
            }
            case 1: {
                return cuota.getFechaPagarD();
            }
            case 2: {
                
                valor = Funciones.capitalize(cli.getNombre());
                break;                
            }
            case 3: {
                valor = prestamoAux.getPlan().toUpperCase() + " ["+Integer.toString(prestamoAux.getCod())+"]";
                
                break;                
            }
            case 4: {
                if (cuota.getNumCuota().equals("0")) {
                    return "ANT.";
                } else if (cuota.getNumCuota().equals("999")) {
                    return "INT.";
                } else {
                    return Integer.parseInt(cuota.getNumCuota());
                }
            }
            case 5: {
                if (cuota.getNumCuota().equals("999")) {
                    
                    valor = Double.toString(Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, cuota.getCodPrestamoInt()).getInteresTotalAlDiaDeHoy());
                } else {
                    valor = cuota.getImporteTotal();
                }
                

                break;                
            }
            case 6: {
                valor = cuota.getImportePagado();
                break;                
            }
            case 7: {
                valor = Funciones.getEstadoCuota(cuota);
                break;                
            }
            case 8: {
                valor = "Cobrar";
                break;                
            }
        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

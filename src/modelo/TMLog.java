/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;


public class TMLog implements TableModel {

    private List<Log> logs;
    
    public TMLog(List<Log> lista) {
        logs = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return logs.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Usuario";
                break;
            }
            case 1: {
                titulo = "Fecha";
                break;                
            }
            case 2: {
                titulo = "Hora";
                break;                
            }
            case 3: {
                titulo = "Acción";
                break;                
            }            
            case 4: {
                titulo = "Detalles";
                break;                
            }
         
                        
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
 
            return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Log l = logs.get(rowIndex);
        String valor = null;
        switch(columnIndex) {
            case 0: {
                valor = Funciones.capitalize(l.getUsuario());
                break;
            }
            case 1: {
                valor = l.getFecha();
                break;                
            }
            case 2: {
                valor = l.getHora();
                break;                
            }
            case 3: {
                valor = l.getAccion();
                break;                
            }
            case 4: {
                valor = l.getDetalles();
                break;                
            }

        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;
import java.util.Date;
import vista.*;


public class TMVencimientoCob implements TableModel {

    private final List<Cuota> cuotas;
    private Prestamo prestamoAux;
    
    public TMVencimientoCob(List<Cuota> lista) {
        cuotas = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return cuotas.size();
    }

    @Override
    public int getColumnCount() {
        return 12;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Cód.";
                break;
            }
            case 1: {
                titulo = "F. Venc.";
                break;                
            }
            case 2: {
                titulo = "Cliente";
                break;                
            }
            case 3: {
                titulo = "Dirección";
                break;                
            }
            case 4: {
                titulo = "Plan";
                break;                
            }
            case 5: {
                titulo = "Cuota";
                break;                
            }
            case 6: {
                titulo = "Importe";
                break;                
            }
            case 7: {
                titulo = "Pagado";
                break;                
            }
            case 8: {
                titulo = "Atraso";
                break;                
            }
            case 9: {
                titulo = "Interés (est.)";
                break;                
            }
            case 10: {
                titulo = "Estado";
                break;                
            }
            case 11: {
                titulo = "";
                break;                
            }
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 0) {
            return Integer.class;
        }
        if (columnIndex == 1) {
            return Date.class;
        }
        if (columnIndex == 4) {
            return Integer.class;
        }
        else {
            return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 11) {
            return true;
        }
        
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Cuota cuota = cuotas.get(rowIndex);
        String valor = null;
        int valorInt = 0;
        
        switch(columnIndex) {
            case 0: {
                //valor  = Integer.toString(cuota.getCod());
                valorInt = cuota.getCod();
                return valorInt;
            }
            case 1: {
                return cuota.getFechaPagarD();
            }
            case 2: {
                prestamoAux = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, Integer.parseInt(cuota.getCodPrestamo()));
                String codCli = prestamoAux.getCodCliente();
                Cliente cli = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(codCli));
                valor = Funciones.capitalize(cli.getNombre());
                break;   
            }
            case 3: {
                prestamoAux = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, Integer.parseInt(cuota.getCodPrestamo()));
                String codCli = prestamoAux.getCodCliente();
                Cliente cli = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(codCli));
                valor = Funciones.capitalize(cli.getDireccion());
                break;                
            }
            case 4: {
                valor = prestamoAux.getPlan().toUpperCase() + " ["+Integer.toString(prestamoAux.getCod())+"]";
                
                break;                
            }
            case 5: {
                if (cuota.getNumCuota().equals("0")) {
                    return "ANT.";
                } else if (cuota.getNumCuota().equals("999")) {
                    return "INT.";
                } else {
                    return Integer.parseInt(cuota.getNumCuota());
                }
                
            }
            case 6: {
                if (Double.parseDouble(cuota.getImporteTotal()) < Double.parseDouble(cuota.getImportePagado())) {
                    valor = cuota.getImportePagado();
                } else {
                    valor = cuota.getImporteTotal();
                }
                break;                
            }
            case 7: {
                valor = cuota.getImportePagado();
                break;                
            }
            case 8: { 
                String estado = Funciones.getEstadoCuota(cuota);
                if (estado.equals("PAGADA") || estado.equals("PARCIAL")) {
                    if (cuota.getDiasAtrasoPago()>0) {
                         valor = cuota.getDiasAtrasoPago() + " días";
                    } else {
                        valor = "0 días";
                    }
                } else {
                    if (cuota.getDiasAtrasoHastaHoy()>0) {
                        valor = cuota.getDiasAtrasoHastaHoy() + " días";
                    } else {
                        valor = "0 días";
                    }
                }
                
                break;                
            }
            case 9: {
                String estado = Funciones.getEstadoCuota(cuota);
                 if (estado.equals("PAGADA") || estado.equals("PARCIAL")) {
                    if (cuota.getDiasAtrasoPago()>0) {
                         valor = cuota.getImporteInteres();
                    } else {
                        valor = "0.0";
                    }
                } else {
                    if (cuota.getDiasAtrasoHastaHoy()>0) {
                        String impInteres = Double.toString(Funciones.formatearDecimales(Funciones.getInteresCalculadoPorDiasAtrasoPorcentaje(cuota),2));
                        valor = impInteres;
                    } else {
                        valor = "0.0";
                    }
                }
                 
                              break;                
            }
            case 10: {
                valor = Funciones.getEstadoCuota(cuota);
                break;                
            }
            case 11: {
                valor = "Cobrar";
                break;                
            }
        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

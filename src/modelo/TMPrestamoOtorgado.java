/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;


public class TMPrestamoOtorgado implements TableModel {

    private List<Prestamo> prestamos;
    
    public TMPrestamoOtorgado(List<Prestamo> lista) {
        prestamos = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return prestamos.size();
    }

    @Override
    public int getColumnCount() {
        return 12;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Cód.";
                break;
            }
            case 1: {
                titulo = "Otorgado";
                break;                
            }
            case 2: {
                titulo = "Cliente";
                break;                
            }
            case 3: {
                titulo = "Dirección";
                break;                
            }
            case 4: {
                titulo = "Teléfono";
                break;                
            }
            case 5: {
                titulo = "Plan";
                break;                
            }
            case 6: {
                titulo = "Imp. Prestado";
                break;                
            }
            case 7: {
                titulo = "Cuotas";
                break;                
            }

            case 8: {
                titulo = "Imp. Cuota";
                break;                
            }
            case 9: {
                titulo = "Devolver";
                break;                
            }
            case 10: {
                titulo = "% Int.";
                break;                
            }
            case 11: {
                titulo = "Ganancia";
                break;                
            }
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 0) {
            return Integer.class;
            
        }
        else {
            return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Prestamo prestamo = prestamos.get(rowIndex);
        Cliente cli = prestamo.getCliente();
        Plan plan = Funciones.getPlanPorNombre(prestamo.getPlan());
        String valor = null;
        switch(columnIndex) {
            case 0: {
                return prestamo.getCod();
            }
            case 1: {
                valor = prestamo.getFechaGenerado();
                break;                
            }
            case 2: {
                valor = Funciones.capitalize(prestamo.getCliente().getNombre());
                break;                
            }
            case 3: {
                valor = Funciones.capitalize(cli.getDireccion()+" - "+ Funciones.getLocalidadPorCodInt(cli.getLocalidad()).getNombre());
                break;                
            }
            case 4: {
                valor = cli.getCelular()+" - "+ cli.getTelefono();
                break;                
            }
            case 5: {
                valor = prestamo.getPlan().toUpperCase();
                break;                
            }
            case 6: {
                valor = "$"+plan.getImportePrestar();
                break;                
            }
            case 7: {
                valor = plan.getCantCuotas();
                break;                
            }
            case 8: {
                valor = "$"+prestamo.getCuotas().get(1).getImporteTotal();
                break;                
            }
            case 9: {
                valor = "$"+plan.getImporteDevolver();
                break;                
            }
            case 10: {
                valor = plan.getPorcentajeInteres()+"%";
                break;                
            }
            case 11: {
                valor = "$"+plan.getImporteInteres();
                break;                
            }
        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

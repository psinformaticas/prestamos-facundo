/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.Funciones;
import java.util.ArrayList;

/**
 *
 * @author Will
 */
public class Plan implements Comparable<Plan>{
    private int cod, orden;
    private String nombre, importePrestar, importeDevolver, importeInteres, porcentajeInteres, calculaPor, tipo, cantCuotas, porcentajeCobrador, a1, a2, a3, a4;

    public Plan() {
        this.cod = 9999999;
        this.orden = 0;
        this.nombre = "";
        this.importePrestar = "0.0";
        this.importeDevolver = "0.0";
        this.importeInteres = "0.0";
        this.porcentajeInteres = "0.0";
        this.calculaPor = "porc";
        this.tipo = "semanal";
        this.cantCuotas = "0";
        this.porcentajeCobrador = "0.0";
        this.a1 = "";
        this.a2 = "";
        this.a3 = "";
        
        //SI ES PERSONALIZADO LLEVA "personalizado"
        this.a4 = "";
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImportePrestar() {
        return importePrestar;
    }

    public void setImportePrestar(String importePrestar) {
        this.importePrestar = importePrestar;
    }

    public String getImporteDevolver() {
        return importeDevolver;
    }

    public void setImporteDevolver(String importeDevolver) {
        this.importeDevolver = importeDevolver;
    }

    public String getImporteInteres() {
        return importeInteres;
    }
    
    public void calcularImporteInt() {
        this.importeInteres = Double.toString(Funciones.formatearDecimales(Double.parseDouble(importeDevolver) - Double.parseDouble(importePrestar),2));
    }
    
    public void calcularPorcInt() {
        
        this.porcentajeInteres = Double.toString(Funciones.formatearDecimales((Double.parseDouble(this.importeInteres)*100)/Double.parseDouble(this.importePrestar),2));
        
        
    }


    public void setImporteInteres(String importeInteres) {
        this.importeInteres = importeInteres;
    }

    public String getPorcentajeInteres() {
        return porcentajeInteres;
    }

    public void setPorcentajeInteres(String porcentajeInteres) {
        this.porcentajeInteres = porcentajeInteres;
    }

    public String getCalculaPor() {
        return calculaPor;
    }

    public void setCalculaPor(String calculaPor) {
        this.calculaPor = calculaPor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCantCuotas() {
        return cantCuotas;
    }
    
    public int getCantCuotasInt() {
        return Integer.parseInt(cantCuotas);
    }

    public void setCantCuotas(String cantCuotas) {
        this.cantCuotas = cantCuotas;
    }

    public String getPorcentajeCobrador() {
        return porcentajeCobrador;
    }
    
    public Double getPorcentajeCobradorD() {
        return Funciones.formatearDecimales(Double.parseDouble(porcentajeCobrador),3);
    }

    public void setPorcentajeCobrador(String porcentajeCobrador) {
        this.porcentajeCobrador = porcentajeCobrador;
    }

    public String getA1() {
        return a1;
    }

    public void setA1(String a1) {
        this.a1 = a1;
    }

    public String getA2() {
        return a2;
    }

    public void setA2(String a2) {
        this.a2 = a2;
    }

    public String getA3() {
        return a3;
    }

    public void setA3(String a3) {
        this.a3 = a3;
    }

    public String getA4() {
        return a4;
    }

    public void setA4(String a4) {
        this.a4 = a4;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    @Override
    public int compareTo(Plan t) {
        int estado=-1;
        if(this.orden==t.getOrden()){
            //Los objetos son iguales
            estado=0;
        }else if(this.orden>t.getOrden()){
            //El objeto 1 es mayor que la pasada por parametro
            estado=1;
        }
        return estado;
    }
    
    
}

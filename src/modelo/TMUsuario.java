/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;

public class TMUsuario implements TableModel {

    private List<Usuario> usuarios;
    
    public TMUsuario(List<Usuario> lista) {
        usuarios = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return usuarios.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Código";
                break;
            }
            case 1: {
                titulo = "Nombre";
                break;                
            }
            case 2: {
                titulo = "Tipo";
                break;                
            }
            case 3: {
                titulo = "";
                break;                
            }            
                                    
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 0) {
            return Integer.class;
            
        }
        else {
            return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex > 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Usuario usuario = usuarios.get(rowIndex);
        String valor = null;
        switch(columnIndex) {
            case 0: {
                valor  = Integer.toString(usuario.getCod());
                break;
            }
            case 1: {
                valor = Funciones.capitalize(usuario.getNombre());
                break;                
            }
            case 2: {
                valor = Funciones.capitalize(usuario.getTipo());
                break;                
            }
            case 3: {
                valor = "Editar";
                break;                
            }
        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

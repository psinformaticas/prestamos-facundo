/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.Funciones;
import java.util.Date;
import vista.Principal;

/**
 *
 * @author Usuario
 */
public class PagoParcial {
    
    private int cod, codCliente, codCuota, codPrestamo, codCobrador;
    private double importe;
    private String usuario;
    private Date fecha;
    private Prestamo prestamo;

    public PagoParcial() {
        this.cod = 0;
        this.codCliente = 0;
        this.codCobrador = 0;
        this.codCuota = 0;
        this.codPrestamo = 0;
        this.importe = 0.0;
        this.usuario = "";
        this.fecha = new Date();
        this.prestamo = new Prestamo();
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public int getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(int codCliente) {
        this.codCliente = codCliente;
    }

    public int getCodCuota() {
        return codCuota;
    }

    public void setCodCuota(int codCuota) {
        this.codCuota = codCuota;
    }

    public int getCodPrestamo() {
        return codPrestamo;
    }

    public void setCodPrestamo(int codPrestamo) {
        this.codPrestamo = codPrestamo;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Prestamo getPrestamo() {
        return prestamo;
    }

    public void setPrestamo(Prestamo prestamo) {
        this.prestamo = prestamo;
    }

    public int getCodCobrador() {
        return codCobrador;
    }

    public void setCodCobrador(int codCobrador) {
        this.codCobrador = codCobrador;
    }

    
    
    
}

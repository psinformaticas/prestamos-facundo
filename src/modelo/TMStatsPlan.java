/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;


public class TMStatsPlan implements TableModel {

    private List<String> statsPlan;
    
    public TMStatsPlan(List<String> lista) {
        statsPlan = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return statsPlan.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Plan";
                break;
            }
            case 1: {
                titulo = "N° Préstamos";
                break;                
            }
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
            return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String linea = statsPlan.get(rowIndex);
        String[] aux = linea.split("-i-");
        String valor = null;
        switch(columnIndex) {
            case 0: {
                valor  = aux[0];
                break;
            }
            case 1: {
                valor  = aux[1];
                break;                
            }
        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

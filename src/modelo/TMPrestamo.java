/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;
import vista.Principal;


public class TMPrestamo implements TableModel {

    private List<Prestamo> prestamos;
    
    public TMPrestamo(List<Prestamo> lista) {
        prestamos = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return prestamos.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Cód";
                break;
            }
            case 1: {
                titulo = "Plan";
                break;                
            }
            case 2: {
                titulo = "Cobrador";
                break;                
            }
            case 3: {
                titulo = "Importe Prestado";
                break;                
            }
            case 4: {
                titulo = "Importe Devolver";
                break;                
            }
            case 5: {
                titulo = "Importe Interés";
                break;                
            }
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 0) {
            return Integer.class;
            
        }
        else {
            return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Prestamo p = prestamos.get(rowIndex);
        String valor = null;
        switch(columnIndex) {
            case 0: {
                valor  = Integer.toString(p.getCod());
                break;
            }
            case 1: {
                valor = p.getPlan().toUpperCase();
                break;                
            }
            case 2: {
                Cobrador c = Funciones.getCobradorPorRuta(Principal.cobradores, p.getCodCobrador());
                valor =  c.getCodInt()+" - "+c.getNombre();
                break;                
            }
            case 3: {
                Plan pX = Funciones.getPlanPorNombre(p.getPlan());
                valor = pX.getImportePrestar();
                break;                
            }
            case 4: {
                Plan pX = Funciones.getPlanPorNombre(p.getPlan());
                valor = pX.getImporteDevolver();
                break;                
            }
            case 5: {
                Plan pX = Funciones.getPlanPorNombre(p.getPlan());
                valor = pX.getImporteInteres();
                break;                
            }
        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

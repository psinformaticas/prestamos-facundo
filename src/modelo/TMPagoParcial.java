/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;
import vista.Principal;


public class TMPagoParcial implements TableModel {

    private List<PagoParcial> pagos;
    
    public TMPagoParcial(List<PagoParcial> lista) {
        pagos = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return pagos.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Cód";
                break;
            }
            case 1: {
                titulo = "Cliente";
                break;                
            }
            case 2: {
                titulo = "Cobrador";
                break;                
            }
            case 3: {
                titulo = "Importe";
                break;                
            }
            case 4: {
                titulo = "Usuario";
                break;                
            }
            case 5: {
                titulo = "Fecha";
                break;                
            }
         
                        
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
 
            return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        PagoParcial p = pagos.get(rowIndex);
        String valor = null;
        switch(columnIndex) {
            case 0: {
                valor = Integer.toString(p.getCod());
                break;
            }
            case 1: {
                valor = Funciones.capitalize(Funciones.getClientePorCod(Principal.clientes, p.getCodCliente()).getNombre());
                break;                
            }
            case 2: {
                valor = Funciones.capitalize(Funciones.getCobradorPorRuta(Principal.cobradores, Integer.toString(p.getCodCobrador())).getNombre());
                break;                
            }
            case 3: {
                valor = "$"+Double.toString(Funciones.formatearDecimales(p.getImporte(),2));
                break;                
            }
            case 4: {
                valor = Funciones.capitalize(p.getUsuario());
                break;                
            }
            case 5: {
                valor = Funciones.dateAString(p.getFecha());
                break;                
            }

        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;


public class TM1CuotaPorVencer implements TableModel {

    private List<Cuota> cuotas;
    
    public TM1CuotaPorVencer(List<Cuota> lista) {
        cuotas = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return cuotas.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Cód";
                break;
            }
            case 1: {
                titulo = "F. de Pago";
                break;
            }
            case 2: {
                titulo = "Cliente";
                break;
            }
            case 3: {
                titulo = "Dirección";
                break;
            }
            case 4: {
                titulo = "Núm. Cuota";
                break;                
            }
            case 5: {
                titulo = "Préstamo";
                break;                
            }
            case 6: {
                titulo = "Importe";
                break;
            }
            case 7: {
                titulo = "Pagado";
                break;
            }
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 0) {
            return Integer.class;
            
        }
        else {
            return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Cuota cuota = cuotas.get(rowIndex);
        String valor = null;
        switch(columnIndex) {
            case 0: {
                return cuota.getCod();
            }
            case 1: {
                valor  = cuota.getFechaPagar();
                break;
            }
            case 2: {
                valor = Funciones.capitalize(cuota.getCliente().getNombre());
                break;                
            }
            case 3: {
                Cliente cli = cuota.getCliente();
                valor = Funciones.capitalize(cli.getDireccion()+" - "+Funciones.getLocalidadPorCodInt(cli.getLocalidad()).getNombre());
                break;                
            }
            case 4: {
                valor  = cuota.getNumCuota();
                if (valor.equals("0")) {
                    valor = "ANT.";
                }
                if (valor.equals("999")) {
                    valor = "INT.";
                }
                break;               
            }
            case 5: {
                valor  = cuota.getNombrePlan().toUpperCase();
                break;               
            }    
            case 6: {
                valor = "$" + cuota.getImporteTotal();
                break;                
            }
            case 7: {
                valor = "$" + cuota.getImportePagado();
                break;                
            }
                 
        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

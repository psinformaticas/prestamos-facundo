/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;


public class TMCliente implements TableModel {

    private List<Cliente> clientes;
    
    public TMCliente(List<Cliente> lista) {
        clientes = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return clientes.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Código";
                break;
            }
            case 1: {
                titulo = "Nombre";
                break;                
            }
            case 2: {
                titulo = "Dirección";
                break;                
            }
            case 3: {
                titulo = "Tel. Fijo";
                break;                
            }            
            case 4: {
                titulo = "Tel. Celular";
                break;                
            }
            case 5: {
                titulo = "Localidad";
                break;                
            }
         
                        
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 0) {
            return Integer.class;
            
        }
        else {
            return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Cliente cliente = clientes.get(rowIndex);
        String valor = null;
        switch(columnIndex) {
            case 0: {
                return cliente.getCod();
            }
            case 1: {
                valor = Funciones.capitalize(cliente.getNombre());
                break;                
            }
            case 2: {
                valor = Funciones.capitalize(cliente.getDireccion());
                break;                
            }
            case 3: {
                valor = cliente.getTelefono();
                break;                
            }
            case 4: {
                valor = cliente.getCelular();
                break;                
            }
            case 5: {
                valor = Funciones.capitalize(Funciones.getLocalidadPorCodInt(cliente.getLocalidad()).getNombre());
                break;                
            }

        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

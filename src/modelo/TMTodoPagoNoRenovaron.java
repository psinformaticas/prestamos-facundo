/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;


public class TMTodoPagoNoRenovaron implements TableModel {

    private List<Cliente> clientes;
    
    public TMTodoPagoNoRenovaron(List<Cliente> lista) {
        clientes = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return clientes.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Cliente";
                break;
            }
            case 1: {
                titulo = "Dirección";
                break;
            }
            case 2: {
                titulo = "Teléfono";
                break;
            }
            case 3: {
                titulo = "Últ. Préstamo";
                break;
            }
            case 4: {
                titulo = "Fecha Últ. Préstamo";
                break;                
            }
            case 5: {
                titulo = "Debe interés";
                break;                
            }
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
            return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Cliente cliente = clientes.get(rowIndex);
        String valor = null;
        switch(columnIndex) {
            case 0: {
                valor  = Funciones.capitalize(cliente.getNombre());
                break;
            }
            case 1: {
                valor = Funciones.capitalize(cliente.getDireccion()+" - "+Funciones.getLocalidadPorCodInt(cliente.getLocalidad()).getNombre());
                break;
            }
            case 2: {
                valor = cliente.getCelular() + " - " + cliente.getTelefono();
                break;                
            }
            case 3: {
                valor = cliente.getUltimoPrestamo().getPlan().toUpperCase();
                break;                
            }
            case 4: {
                valor  = cliente.getUltimoPrestamo().getFechaGenerado();
                break;               
            }
            case 5: {
                if (cliente.tieneCuotasPagasYDebeInteres()) {
                    valor = "SI";
                } else {
                    valor = "NO";
                }
                break;               
            }    

        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

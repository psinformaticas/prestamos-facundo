/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.Funciones;
import java.io.Serializable;
import java.util.Date;
import vista.Principal;

/**
 *
 * @author Will
 */
public class Cuota implements Comparable<Cuota>, Serializable {
    private int cod, finalizada;
    private String codPrestamo, numCuota, fechaPagar, fechaPagado, importeTotal, importePagado, importeInteres, observacion, a1, a2, a3;

    public Cuota() {
        this.cod = 99999999;
        this.codPrestamo = "";
        this.numCuota = "";
        this.fechaPagar = "";
        this.fechaPagado = "";
        this.importeTotal = "0.0";
        this.importePagado = "0.0";
        this.importeInteres = "0.0";
        this.observacion = "";
        this.finalizada = 0;
        //COD COBRADOR
        this.a1 = "";
        this.a2 = "";
        this.a3 = "";
    }

    @Override
    public int compareTo(Cuota c) {
        return this.getFechaPagarD().compareTo(c.getFechaPagarD());
    }
     
    @Override
    public String toString() {
        return getFechaPagar();
    }
    
    //FUNCIONES PARA RELLENAR XLS CUOTAS HASTA LA FECHA
    public Cobrador getCobrador() {
        return Funciones.getCobradorPorRuta(Principal.cobradores, Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, Integer.parseInt(codPrestamo)).getCodCobrador());
    }
    
    //FIN FUNCIONES RELLENAR XLS CUOTAS HASTA LA FECHA
    public String getNombrePlan() {
        return Funciones.getPlanPorNombre(Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, Integer.parseInt(codPrestamo)).getPlan()).getNombre();
    }
    
    public Cliente getCliente() {
        Prestamo p = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, Integer.parseInt(codPrestamo));
        return Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(p.getCodCliente()));
    }
    
    //FUNCION PAGAR
    public double realizaPagoCuotaYDevuelveResto(String cobrador, String importePagar, boolean seCobraInteres, String fechaPago) {
        
        double impPagarTotalD = Double.parseDouble(importePagar.replace(",","."));
        double importeDePagoEstaCuota = getImporteTotalD() - getImportePagadoD(); 
        double importeYaPagado = getImportePagadoD();
    
        
        if (impPagarTotalD <importeDePagoEstaCuota) {
            importeDePagoEstaCuota = Double.valueOf(impPagarTotalD);
        }
        
        if (impPagarTotalD < 0) {
            return 0;
        } else {
            impPagarTotalD = impPagarTotalD - importeDePagoEstaCuota;
        }
        
                
        //FECHA DE PAGO
        setFechaPagado(fechaPago);
        
        //GUARDA IMPORTE PAGADO
        setImportePagadoD(getImportePagadoD()+importeDePagoEstaCuota);
        
        //GUARDA COB
        setA1(cobrador);
        
        //SET INTERES
        if (!seCobraInteres) {
            setImporteInteres("0.0");
        } else {
                Date fechaPagoAux = Funciones.stringADate(getFechaPagar());
                Date fechaHoyAux = Funciones.stringADate(fechaPago);
                int diasDif = Funciones.calcularDiasDeDiferencia(Funciones.dateAString(fechaHoyAux), Funciones.dateAString(fechaPagoAux));
                if (diasDif<0) {
                    diasDif = 0;
                }
                double cuotaTotalAux = Double.parseDouble(getImporteTotal());
                double porcIntDia = Principal.opciones.getInteresdia();
                double valorIntDia = (importeDePagoEstaCuota * porcIntDia) / 100;

                double totalIntAux = valorIntDia * diasDif;
                
                double interesAnterior = 0.0;
                try {
                    interesAnterior = Double.parseDouble(this.importeInteres);
                } catch (Exception e) {
                    
                }
                interesAnterior += totalIntAux;
                
                setImporteInteres(Double.toString(Funciones.formatearDecimales(interesAnterior, 2)));
        }
        
        return impPagarTotalD;
        
    }
      
    public double getInteresCalculado() {
        if (this.yaSePago() && this.getImporteInteresD()>0) {
            return Funciones.formatearDecimales(this.getImporteInteresD(),2);
        } else if ((this.yaSePago() && this.getImporteInteresD()==0)){
            return Funciones.formatearDecimales(this.getImporteInteresD(),2);
        } else if (this.getImportePagadoD()<this.getImporteTotalD() && this.getImporteInteresD()>0) {
            double importeIntDia = 0;
            importeIntDia = (getImporteAdeudadoD()  * Principal.opciones.getInteresdia()) /100;
            importeIntDia = importeIntDia * getDiasAtrasoPago();
            importeIntDia += this.getImporteInteresD();
            return Funciones.formatearDecimales((importeIntDia),2);
        } else {
            double importeIntDia = 0;
            importeIntDia = (getImporteTotalD()  * Principal.opciones.getInteresdia()) /100;
            return Funciones.formatearDecimales((importeIntDia*getDiasAtrasoPago()),2);
        }
    }
    
    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getCodPrestamo() {
        return codPrestamo;
    }
    
    public int getCodPrestamoInt() {
        return Integer.parseInt(codPrestamo);
    }

    public void setCodPrestamo(String codPrestamo) {
        this.codPrestamo = codPrestamo;
    }

    public String getNumCuota() {
        return numCuota;
    }

    public void setNumCuota(String numCuota) {
        this.numCuota = numCuota;
    }

    public String getFechaPagar() {
        return fechaPagar;
    }
    public Date getFechaPagarD() {
        return Funciones.stringADate(fechaPagar);
    }

    public Date getFechaPagarDate() {
        return Funciones.stringADate(fechaPagar);
    }
    
    public Date getFPago() {
        return Funciones.stringADate(fechaPagado);
    }
    
    public void setFechaPagar(String fechaPagar) {
        this.fechaPagar = fechaPagar;
    }

    public String getFechaPagado() {
        return fechaPagado;
    }
    public Date getFecha() {
        return Funciones.stringADate(fechaPagado);
    }

    public void setFechaPagado(String fechaPagado) {
        this.fechaPagado = fechaPagado;
    }

    public String getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(String importeTotal) {
        this.importeTotal = importeTotal;
    }

    public String getImportePagado() {
        return importePagado;
    }
    
    public Double getImportePagadoD() {
        return Double.parseDouble(importePagado);
    }
    
    public Double getImporteTotalD() {
        return Double.parseDouble(importeTotal);
    }
    
    public void setImporteTotalD(Double d) {
        importeTotal = Double.toString(Funciones.formatearDecimales(d, 2));
    }
    
    public boolean yaSePago() {
        System.out.println("IMP PAGADO: "+ getImportePagado());
        return getImportePagadoD()>=(getImporteTotalD()-1) && getImportePagadoD()>0;
    }

    public void setImportePagado(String importePagado) {
        this.importePagado = importePagado;
    }
    
    public void setImportePagadoD(Double importePagado) {
        this.importePagado = Double.toString(importePagado);
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public int getFinalizada() {
        return finalizada;
    }

    public void setFinalizada(int finalizada) {
        this.finalizada = finalizada;
    }

    public String getA1() {
        return a1;
    }

    public void setA1(String a1) {
        this.a1 = a1;
    }

    public String getA2() {
        return a2;
    }

    public void setA2(String a2) {
        this.a2 = a2;
    }

    public String getA3() {
        return a3;
    }

    public void setA3(String a3) {
        this.a3 = a3;
    }

    public String getImporteInteres() {
        return importeInteres;
    }

     public Double getImporteInteresD() {
         double ret = 0.0;
         try {
             ret = Double.parseDouble(importeInteres);
         } catch (Exception e) {
             ret = 0.0;
         }
        return ret;
    }
     
    public void setImporteInteres(String importeInteres) {
        this.importeInteres = importeInteres;
    }
    
    public void setImporteInteresD(Double importeInteres) {
        this.importeInteres = Double.toString(Funciones.formatearDecimales(importeInteres, 2));
    }
    
    public int getDiasAtrasoHastaHoy() {
        return Funciones.calcularDiasDeDiferencia(Funciones.devolverFechaActualStr(), fechaPagar);
    }
    
    public int getDiasAtrasoPago() {
        return Funciones.calcularDiasDeDiferencia(fechaPagado, fechaPagar);
    }
    
    public Double getImporteAdeudadoD() {
        return getImporteTotalD()-getImportePagadoD();
    }                
    
    
            
}

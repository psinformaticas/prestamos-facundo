/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.Conector;
import controlador.Funciones;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import vista.Principal;

/**
 *
 * @author Will
 */
public class Prestamo implements Comparable<Prestamo>, Serializable {
    private int cod, finalizado;
    private ArrayList<Cuota> cuotas;
    private String codCobrador, plan, observacion, fechaGenerado, codCliente, a1, a2, a3, a4, a5, a6;

    public Prestamo() {
        this.cod = 999999;
        this.cuotas = new ArrayList<>();
        this.plan = "";
        this.observacion = "";
        this.fechaGenerado = "";
        this.codCliente = "";
        this.a1 = "";
        this.a2 = "";
        this.a3 = "";
        this.a4 = "";
        this.a5 = "";
        this.a6 = "";
        this.finalizado = 0;
        this.codCobrador = "";
    }

    @Override
    public int compareTo(Prestamo p) {
        return this.getDateGenerado().compareTo(p.getDateGenerado());
    }
     
    @Override
    public String toString() {
        return fechaGenerado;
    }
    
    public Cuota getCuotaNumero(int num) {
        for (Cuota c: cuotas) {
            if (Integer.parseInt(c.getNumCuota())==num) {
                return c;
            }
        }
        return null;
    }
    
    public Date getDateGenerado() {
        return Funciones.stringADate(fechaGenerado);
    }
    
    public Cliente getCliente() {
        return Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(codCliente));
    }
    
    //GETTERS Y SETTERS
    
    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public ArrayList<Cuota> getCuotas() {
        return cuotas;
    }

    public void setCuotas(ArrayList<Cuota> cuotas) {
        this.cuotas = cuotas;
    }

    public String getPlan() {
        return plan;
    }
    
    public Plan getPlanObj() {
        return Funciones.getPlanPorNombre(plan);
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    
    public String getCodCuotasStr() {
        String cods = "";
        for (Cuota cuota: cuotas) {
            cods += Integer.toString(cuota.getCod()) + "-";
        }
        return cods;
    }
    
    public void cargarCuotasPorStr(String cods) {
        cuotas.clear();
        String[] codCuotas = cods.split("-");
        for (String cod: codCuotas) {
            if (!cod.equals("")) {
                Conector con = new Conector();
                con.conectar();
                cuotas.add(con.getCuotaPorCod(Integer.parseInt(cod)));
                con.cerrar();
            }
            
        }
    }

    public String getFechaGenerado() {
        return fechaGenerado;
    }

    public Date getFecha() {
        return Funciones.stringADate(fechaGenerado);
    }
    
    public void setFechaGenerado(String fechaGenerado) {
        this.fechaGenerado = fechaGenerado;
    }

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public int getFinalizado() {
        return finalizado;
    }

    public void setFinalizado(int finalizado) {
        this.finalizado = finalizado;
    }

    public String getImporteDescuento() {
        return a1;
    }

    public void setImporteDescuento(String a1) {
        this.a1 = a1;
    }

    public String getA2() {
        return a2;
    }

    public void setA2(String a2) {
        this.a2 = a2;
    }

    public String getA3() {
        return a3;
    }

    public void setA3(String a3) {
        this.a3 = a3;
    }

    public String getCodCobrador() {
        return codCobrador;
    }

    public void setCodCobrador(String codCobrador) {
        this.codCobrador = codCobrador;
    }

    public String getA4() {
        return a4;
    }

    public void setA4(String a4) {
        this.a4 = a4;
    }

    public String getA5() {
        return a5;
    }

    public void setA5(String a5) {
        this.a5 = a5;
    }

    public String getA6() {
        return a6;
    }

    public void setA6(String a6) {
        this.a6 = a6;
    }
    
    //TODAS LAS CUOTAS CALCULADAS EN EL MOMENTO
//    public double getInteresTotalAlDiaDeHoy() {
//        double interes = 0.0;
//        
//        for (Cuota cuota: getCuotas()) {
//            if (!cuota.getNumCuota().equals("999") && !cuota.getImporteInteres().equals("no")) {
//                if (cuota.yaSePago()) {
//                    double intCuota = Funciones.getInteresCalculadoPorDiasAtrasoHastaFechaPagada(cuota);
//                    interes += intCuota;
//                } else {
//                    double intCuota = Funciones.getInteresCalculadoPorDiasAtrasoHastaHoy(cuota);
//                    interes += intCuota;
//                }
//            }    
//        }
//                return Funciones.formatearDecimales(interes, 2);
//    }
    
    public double getInteresTotalAlDiaDeHoy() {
        double interes = 0.0;
        
        for (Cuota cuota: getCuotas()) {
            if (!cuota.getNumCuota().equals("999") && !cuota.getImporteInteres().equals("no")) {
                if (cuota.yaSePago()) {
                    double intCuota = cuota.getImporteInteresD();
                    interes += intCuota;
                } else if (cuota.getImportePagadoD()<cuota.getImporteAdeudadoD()) {
                    double intCuota = Funciones.getInteresCalculadoPorDiasAtrasoHastaHoy(cuota);
                    interes += intCuota;
                    interes += cuota.getImporteInteresD();
                } else {
                    double intCuota = Funciones.getInteresCalculadoPorDiasAtrasoHastaHoy(cuota);
                    interes += intCuota;
                }
            }    
        }
         for (Cuota cuotax: getCuotas()) {
             //SI SE PASA A OTRO CREDITO EL INTERES
            if (cuotax.getNumCuota().equals("999") && !cuotax.getFechaPagado().equals("01/01/2099")) {
                interes =0;
            }
         }
        
                return Funciones.formatearDecimales(interes, 2);
    }
    
    public double getInteresTotalALaFecha(Date fecha) {
        double interes = 0.0;
        
        for (Cuota cuota: getCuotas()) {
            if (!cuota.getNumCuota().equals("999") && !cuota.getImporteInteres().equals("no")) {
                if (cuota.yaSePago()) {
                    double intCuota = cuota.getImporteInteresD();
                    interes += intCuota;
                } else if (cuota.getImportePagadoD()<cuota.getImporteAdeudadoD()) {
                    double intCuota = Funciones.getInteresCalculadoPorDiasAtrasoHastaHoy(cuota, fecha);
                    interes += intCuota;
                    interes += cuota.getImporteInteresD();
                } else {
                    double intCuota = Funciones.getInteresCalculadoPorDiasAtrasoHastaHoy(cuota, fecha);
                    interes += intCuota;
                }
            }    
            //SI SE PASA A OTRO CREDITO EL INTERES
            for (Cuota cuotax: getCuotas()) {
             //SI SE PASA A OTRO CREDITO EL INTERES
            if (cuotax.getNumCuota().equals("999") && !cuotax.getFechaPagado().equals("01/01/2099")) {
                interes =0;
            }
         }
        }
                return Funciones.formatearDecimales(interes, 2);
    }
    
    public boolean tieneCuotasAnterioresAEstaSinPagar(Cuota cuotaAct) {
        int numCuotaAct = Integer.parseInt(cuotaAct.getNumCuota());
        
        for (Cuota cuota: getCuotas()) {
            int numCuota = Integer.parseInt(cuota.getNumCuota());
            if (numCuota<numCuotaAct && !cuota.yaSePago()) {
                return true;
            }
        }
        
        return false;
    }
    
    public Cuota getUltimaCuotaPendienteOParcial() {
        for (Cuota cuota: getCuotas()) {
            if (!cuota.yaSePago()) {
                return cuota;
            }
        }
        return null;
    }
    
}

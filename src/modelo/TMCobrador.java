/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import aplicacion.AplicacionControlador;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;


public class TMCobrador implements TableModel {

    private List<Cobrador> cobradores;
    
    public TMCobrador(List<Cobrador> lista) {
        cobradores = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return cobradores.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Código";
                break;
            }
            case 1: {
                titulo = "Nombre";
                break;                
            }
            case 2: {
                titulo = "Ruta";
                break;                
            }
            case 3: {
                titulo = "Teléfono";
                break;                
            }            
            case 4: {
                titulo = "Nombre App";
                break;                
            }
            //case 5: {
            //    titulo = "Usuario APP";
            //    break;                
            //}            
            //case 6: {
            //    titulo = "Contraseña";
            //    break;                
            //}    
         
                        
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 0) {
            return Integer.class;
            
        }
        else {
            return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Cobrador c = cobradores.get(rowIndex);
        String valor = null;
        //UsuarioApp user = AplicacionControlador.cobradorTieneUsuarioApp(c.getCod());
        Integer valorInt = null;
        switch(columnIndex) {
            case 0: {
                return c.getCod();
                
            }
            case 1: {
                valor = Funciones.capitalize(c.getNombre());
                break;                
            }
            case 2: {
                valor = c.getCodInt().toUpperCase();
                break;                
            }
            case 3: {
                valor = c.getTelefono();
                break;                
            }
            case 4: {
                valor = c.getNombreApp();
                break;                
            }
            //case 5: {
            //    if (user != null) {
//                    valor = user.getUsername();
//                } else {
//                    valor = "SIN USUARIO";
//                }
//                
//                break;                
//            }
//            case 6: {
//                if (user != null) {
//                    valor = user.getPassword();
//                } else {
//                    valor = "SIN USUARIO";
//                }
//                break;                
//            }
        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

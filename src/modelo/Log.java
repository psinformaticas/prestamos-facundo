/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.Funciones;
import java.util.Date;
import vista.Principal;

/**
 *
 * @author Usuario
 */
public class Log {
    
    private int cod;
    private String fecha, hora, usuario, accion, detalles, a1, a2, a3;

    public Log() {
        this.cod = 0;
        this.fecha = Funciones.devolverFechaActualStr();
        this.hora = Funciones.devolverHoraActualStr();
        this.usuario = Principal.usuario;
        this.accion = "";
        this.detalles = "";
        this.a1 = "";
        this.a2 = "";
        this.a3 = "";
    }

    public Date getFechaDate() {
        return Funciones.stringADate(fecha);
    }
    
    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public String getA1() {
        return a1;
    }

    public void setA1(String a1) {
        this.a1 = a1;
    }

    public String getA2() {
        return a2;
    }

    public void setA2(String a2) {
        this.a2 = a2;
    }

    public String getA3() {
        return a3;
    }

    public void setA3(String a3) {
        this.a3 = a3;
    }
    
    
    
}

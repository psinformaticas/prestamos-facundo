/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;


public class TMCuota implements TableModel {

    private List<Cuota> cuotas;
    
    public TMCuota(List<Cuota> lista) {
        cuotas = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return cuotas.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Fecha de pago";
                break;
            }
            case 1: {
                titulo = "Importe";
                break;                
            }
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 0) {
            return Integer.class;
            
        }
        else {
            return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Cuota cuota = cuotas.get(rowIndex);
        String valor = null;
        switch(columnIndex) {
            case 0: {
                valor  = cuota.getFechaPagar();
                if (cuota.getFechaPagar().equals("01/01/2099")) {
                    valor  = "Int. Acumulado";
                }
                break;
            }
            case 1: {
                valor = cuota.getImporteTotal();
                break;                
            }
        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

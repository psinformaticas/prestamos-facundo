/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.*;
/**
 *
 * @author Will
 */
public class OpcionesDB {
    
    private String tipo, usr, pass, srv, bd, port;

    public OpcionesDB() {
        this.tipo = "";
        this.usr = "";
        this.pass = "";
        this.srv = "";
        this.bd = "";
        this.port = "";
    }

    
    
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String activo) {
        this.tipo = activo;
    }

    public String getUsr() {
        return usr;
    }

    public void setUsr(String usr) {
        this.usr = usr;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getSrv() {
        return srv;
    }

    public void setSrv(String srv) {
        this.srv = srv;
    }

    public String getBd() {
        return bd;
    }

    public void setBd(String bd) {
        this.bd = bd;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
    
    
    


}

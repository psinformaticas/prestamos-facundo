/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion;

import modelo.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;


public class TMUserApp implements TableModel {

    private List<Cliente> clientes;
    
    public TMUserApp(List<Cliente> lista) {
        clientes = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return clientes.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Código";
                break;
            }
            case 1: {
                titulo = "Nombre";
                break;                
            }
            case 2: {
                titulo = "DNI";
                break;                
            }
            case 3: {
                titulo = "Email";
                break;                
            }            
            case 4: {
                titulo = "Contraseña";
                break;                
            }
            case 5: {
                titulo = "Usuario APP";
                break;                
            }
         
                        
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 0) {
            return Integer.class;
            
        }
        else {
            return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Cliente cliente = clientes.get(rowIndex);
        UsuarioApp user = AplicacionControlador.tieneUsuarioApp(cliente.getDni());
        String valor = null;
        switch(columnIndex) {
            case 0: {
                return cliente.getCod();
            }
            case 1: {
                valor = Funciones.capitalize(cliente.getNombre());
                break;                
            }
            case 2: {
                valor = cliente.getDni();
                break;                
            }
            case 3: {
                if (user == null) {
                    valor = "No disponible";
                } else {
                    valor = user.getEmail();
                }
                
                break;                
            }
            case 4: {
                if (user == null) {
                    valor = "No disponible";
                } else {
                    valor = user.getPassword();
                }

                break;                
            }
            case 5: {
                String usuarioStr = "";
                if (user == null) {
                    usuarioStr = "SIN USUARIO";
                } else {
                    usuarioStr = user.getUsername();
                }
                valor = usuarioStr;
                
                break;                
            }

        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

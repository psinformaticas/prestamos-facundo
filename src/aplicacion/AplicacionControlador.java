/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion;

import controlador.Conector;
import controlador.ConectorApp;
import controlador.Funciones;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import modelo.Cliente;
import modelo.Cobrador;
import modelo.Cuota;
import modelo.Plan;
import modelo.Prestamo;
import modelo.UsuarioApp;
import vista.Principal;

/**
 *
 * @author Usuario
 */
public class AplicacionControlador {

    //************
    //* USUARIOS *
    //************
    
    public static UsuarioApp getUsrAppPorUsername(String username) {
        ConectorApp c = new ConectorApp();
        c.conectar();
        UsuarioApp u = c.getUsuarioAppPorUsername(username);
        c.cerrar();
        
        return u;
    }
    
    public static ArrayList<UsuarioApp> getUsuariosApp() {
        ArrayList<UsuarioApp> usersApp = new ArrayList<>();
        
        ConectorApp c = new ConectorApp();
        c.conectar();
        usersApp = c.getUsuariosApp();
        c.cerrar();
        
        return usersApp;
    }
    
    public static UsuarioApp tieneUsuarioApp(String dni) {
        for (UsuarioApp u: Principal.usuariosApp) {
            if (u.getUsername().equalsIgnoreCase(dni)) {
                return u;
            }
        }
        return null;
    }
    
    public static void eliminarUsuarioApp(String dni) {
        try {
            if (getUsrAppPorUsername(dni) != null) {
                ConectorApp c = new ConectorApp();
                Cliente cli = Funciones.getClientePorDni(dni);
                c.conectar();
                UsuarioApp user = c.getUsuarioAppPorUsername(dni);
                c.eliminarUsuarioAppPorUsername(dni);
                c.eliminarPrivacidadUser(user.getUserid());
                for (Prestamo p: Funciones.getPrestamosPorCli(Principal.todosLosPrestamos, Integer.toString(cli.getCod()))) {
                    eliminarPrestamoYCuotas(p);
                }
                c.cerrar();
            }
        } catch (Exception e) {
            Conector con = new Conector();
            con.conectar();
            con.guardarActFallidaUsuario(dni,"eliminar");
            con.cerrar();
        }
    }
    
    public static void blanquearPassUsuario(String dni) {
        ConectorApp c = new ConectorApp();
        c.conectar();
        c.blanquearPassUsuarioPorUsername(dni);
        c.cerrar();
    }        
    
    public static void generarUsuarioPorCliente(Cliente cli, boolean aviso) {
        if (cli.getDni().equalsIgnoreCase("")) {
            if (aviso) {
                JOptionPane.showMessageDialog(null,"Debe registrar DNI del cliente para generar usuario en la aplicación.","Info",1);
            }
        } else {
            try {
                UsuarioApp usr = new UsuarioApp();

                usr.setDisplayName(Funciones.capitalize(cli.getNombre()));
                usr.setUsername(cli.getDni().replace(".", ""));
                usr.setPassword("");
                usr.setEmail(cli.getEmail());
                usr.setKey("");
                usr.setValidated("1");
                usr.setGroupid(2);
                usr.setLastActive(Funciones.returnEpochActual());
                usr.setShowAvt(1);
                usr.setBanned(0);
                usr.setRegTime(Funciones.returnEpochActual());
                usr.setFbProfile("");
                usr.setCodPC(cli.getCod());
                usr.setLocalidad(Funciones.capitalize(Funciones.getLocalidadPorCodInt(cli.getLocalidad()).getNombre()));
                ConectorApp c = new ConectorApp();
                c.conectar();
                if (c.getUsuarioAppPorUsername(cli.getDni().replace(".","")) == null) {
                    c.guardarUsuarioApp(usr);
                    UsuarioApp ultimoUsr = c.getUsuarioAppPorUsername(usr.getUsername());
                    c.guardarPrivacidadUser(ultimoUsr.getUserid());
                }

                c.cerrar();



                //CARGA PRESTAMOS
                if (true) {
                    ArrayList<Prestamo> prestamos = Funciones.getPrestamosPorCli(Principal.todosLosPrestamos, Integer.toString(cli.getCod()));
                    for (Prestamo p: prestamos) {
                        guardarPrestamo(p);
                    }
                }

                if (aviso) {
                    JOptionPane.showMessageDialog(null,"Se ha generado el usuario","Info",1);
                }

            } catch (Exception e) {
                Conector con = new Conector();
                con.conectar();
                con.guardarActFallidaUsuario(cli.getDni(),"generar");
                con.cerrar();
            }
        }
        
    }
    
    public static void generarUsuarioPorClienteRequiereConector(Cliente cli, boolean aviso, ConectorApp con) {
        if (cli.getDni().equalsIgnoreCase("")) {
            if (aviso) {
                JOptionPane.showMessageDialog(null,"Debe registrar DNI del cliente para generar usuario en la aplicación.","Info",1);
            }
        } else {
            try {
                UsuarioApp usr = new UsuarioApp();

                usr.setDisplayName(Funciones.capitalize(cli.getNombre()));
                usr.setUsername(cli.getDni().replace(".", ""));
                usr.setPassword("");
                usr.setEmail(cli.getEmail());
                usr.setKey("");
                usr.setValidated("1");
                usr.setGroupid(2);
                usr.setLastActive(Funciones.returnEpochActual());
                usr.setShowAvt(1);
                usr.setBanned(0);
                usr.setRegTime(Funciones.returnEpochActual());
                usr.setFbProfile("");
                usr.setCodPC(cli.getCod());
                usr.setLocalidad(Funciones.capitalize(Funciones.getLocalidadPorCodInt(cli.getLocalidad()).getNombre()));
                if (con.getUsuarioAppPorUsername(cli.getDni().replace(".","")) == null) {
                    con.guardarUsuarioApp(usr);
                    UsuarioApp ultimoUsr = con.getUsuarioAppPorUsername(usr.getUsername());
                    con.guardarPrivacidadUser(ultimoUsr.getUserid());
                }




                //CARGA PRESTAMOS
                if (true) {
                    ArrayList<Prestamo> prestamos = Funciones.getPrestamosPorCli(Principal.todosLosPrestamos, Integer.toString(cli.getCod()));
                    for (Prestamo p: prestamos) {
                        guardarPrestamo(p);
                    }
                }

                if (aviso) {
                    JOptionPane.showMessageDialog(null,"Se ha generado el usuario","Info",1);
                }

            } catch (Exception e) {
                Conector conX = new Conector();
                conX.conectar();
                conX.guardarActFallidaUsuario(cli.getDni(),"generar");
                conX.cerrar();
            }
        }
        
    }
    
    public static void eliminarUsuarioPorDNIRequiereConector(String dni, boolean aviso, ConectorApp c) {
           try {
            if (getUsrAppPorUsername(dni) != null) {
                Cliente cli = Funciones.getClientePorDni(dni);
                UsuarioApp user = c.getUsuarioAppPorUsername(dni);
                c.eliminarUsuarioAppPorUsername(dni);
                c.eliminarPrivacidadUser(user.getUserid());
                for (Prestamo p: Funciones.getPrestamosPorCli(Principal.todosLosPrestamos, Integer.toString(cli.getCod()))) {
                    eliminarPrestamoYCuotas(p);
                }
            }
        } catch (Exception e) {
            Conector con = new Conector();
            con.conectar();
            con.guardarActFallidaUsuario(dni,"eliminar");
            con.cerrar();
        }
    }
    
    public static void actualizarUsuarioPorCliente(Cliente cli, boolean aviso) {
        if (cli.getDni().equalsIgnoreCase("")) {
            if (aviso) {
                JOptionPane.showMessageDialog(null,"Debe registrar DNI del cliente para generar usuario en la aplicación.","Info",1);
            }
        } else {
            try {
                UsuarioApp usr = new UsuarioApp();
                usr.setUsername(cli.getDni());
                usr.setDisplayName(Funciones.capitalize(cli.getNombre()));
                usr.setLocalidad(Funciones.capitalize(Funciones.getLocalidadPorCodInt(cli.getLocalidad()).getNombre()));
                ConectorApp c = new ConectorApp();
                c.conectar();
                if (c.getUsuarioAppPorUsername(cli.getDni().replace(".","")) != null) {
                    c.actualizarUsuario(usr);
                }
                c.cerrar();

                if (aviso) {
                    JOptionPane.showMessageDialog(null,"Se ha actualizado el usuario","Info",1);
                }

            } catch (Exception e) {
                Conector con = new Conector();
                con.conectar();
                con.guardarActFallidaUsuario(cli.getDni(),"actualizar");
                con.cerrar();
            }
        }
        
    }
    
    
    
    //******************************
    //* PLANES, PRÉSTAMOS Y CUOTAS *
    //******************************
    
    //------------
    // PRÉSTAMOS -
    //------------
    
    public static void guardarUltimoPrestamoClienteEnApp(int codCli) {
        Conector con = new Conector();
        con.conectar();
        Prestamo p = con.getUltimoPrestamoPorCli(Integer.toString(codCli));
        con.cerrar();
        
        guardarPrestamo(p);
    }
    
    public static void guardarPrestamo(Prestamo prest) {
        try {
            Conector conX = new Conector();
            conX.conectar();
            Prestamo p = conX.getPrestamoPorCod(prest.getCod());
            conX.cerrar();
        
            ConectorApp c = new ConectorApp();
            c.conectar();
            if (c.getPrestamoPorCod(p.getCod()) == null) {
                c.guardarPrestamo(p);    
            } else {
                c.actualizarPrestamo(p);
            }
            c.cerrar();

            guardarPlanPrestamo(p);
            guardarCuotasPrestamo(p);
        } catch (Exception e) {
            Conector con = new Conector();
            con.conectar();
            con.guardarActFallidaPrestamo(prest.getCod());
            con.cerrar();
        }
        
    }
    
    public static void eliminarPrestamoYCuotas(Prestamo prest) {
        ConectorApp c = new ConectorApp();
        c.conectar();
        c.eliminarPrestamoPorCod(prest.getCod());    
        c.eliminarCuotasPorCodPrestamo(prest.getCod());
        ArrayList<Prestamo> prestamos = c.getPrestamosPorPlan(prest.getPlan());
        if (prestamos.size()<1) {
            c.eliminarPlanPorNombre(prest.getPlan());
        }
        c.cerrar();
    }
    
    //---------
    // PLANES -
    //---------
    
    public static void guardarPlanPrestamo(Prestamo prest) {
        Conector c = new Conector();
        c.conectar();
        Plan plan = c.getPlanPorNombre(prest.getPlan());
        c.cerrar();
        
        ConectorApp cApp = new ConectorApp();
        cApp.conectar();
        if (cApp.getPlanPorNombre(plan.getNombre()) == null) {
            cApp.guardarPlan(plan);    
        }
        cApp.cerrar();
    }
    
    //---------
    // CUOTAS -
    //---------
    public static void guardarCuotasPrestamo(Prestamo prest) {
        Conector c = new Conector();
        c.conectar();
        ArrayList<Cuota> cuotas = c.getCuotasPorCodPrestamo(Integer.toString(prest.getCod()));
        c.cerrar();
        
        ConectorApp cApp = new ConectorApp();
        cApp.conectar();
        
        for (Cuota cuota: cuotas) {
            if (cApp.getCuotaPorCod(cuota.getCod()) == null) {
                cApp.guardarCuota(cuota);    
            } else {
                cApp.actualizarCuota(cuota);
            }
        }
        
        cApp.cerrar();
    }
    
    
    
    //**************
    //* COBRADORES *
    //**************
    
    public static ArrayList<UsuarioApp> getUsuariosCobradoresApp() {
        ArrayList<UsuarioApp> usersApp = new ArrayList<>();
        try {
            ConectorApp c = new ConectorApp();
            c.conectar();
            usersApp = c.getUsuariosCobradoresApp();
            c.cerrar();
        } catch (Exception e) {
            return new ArrayList<UsuarioApp>();
        }
        
        
        return usersApp;
    }
    
    public static UsuarioApp cobradorTieneUsuarioApp(int codPc) {
        for (UsuarioApp u: Principal.cobradoresApp) {
            if (u.getCodCobPC() == codPc) {
                return u;
            }
        }
        return null;
    }
    
    public static void generarUsuarioPorCobrador(Cobrador cob, boolean aviso) {
        if (cob.getCodInt().equalsIgnoreCase("")) {
            if (aviso) {
                JOptionPane.showMessageDialog(null,"Debe registrar DNI del cliente para generar usuario en la aplicación.","Info",1);
            }
        } else {
            try {
                UsuarioApp usr = new UsuarioApp();

                usr.setDisplayName(Funciones.capitalize(cob.getNombreApp()));
                usr.setUsername(cob.getCodInt());
                usr.setPassword("");
                usr.setEmail("Sin especificar");
                usr.setKey("");
                usr.setValidated("1");
                usr.setGroupid(3);
                usr.setLastActive(Funciones.returnEpochActual());
                usr.setShowAvt(1);
                usr.setBanned(0);
                usr.setRegTime(Funciones.returnEpochActual());
                usr.setFbProfile("");
                usr.setCodCobPC(cob.getCod());
                usr.setLocalidad("");
                ConectorApp c = new ConectorApp();
                c.conectar();
                if (c.getUsuarioAppPorUsername(cob.getCodInt()) == null) {
                    c.guardarUsuarioCobradorApp(usr);
                    UsuarioApp ultimoUsr = c.getUsuarioAppPorUsername(usr.getUsername());
                    c.guardarPrivacidadUser(ultimoUsr.getUserid());
                }

                c.cerrar();



                if (aviso) {
                    JOptionPane.showMessageDialog(null,"Se ha generado el usuario","Info",1);
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null,"Error al generar usuario para cobrador","Info",0);
            }
        }
        
    }
    
    
}

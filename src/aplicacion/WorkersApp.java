/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion;

import java.util.ArrayList;
import javax.swing.DefaultRowSorter;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import modelo.Cliente;
import modelo.Cobrador;
import modelo.Prestamo;
import static vista.Principal.cargarActFallidas;
import static vista.Principal.cargarMenuUsuariosApp;
import static vista.Principal.clientes;
import static vista.Principal.colorearUsers;
import workers.*;
import static vista.Principal.loadWindow;
import static vista.Principal.modeloUsers;
import static vista.Principal.popupMenuUsuariosApp;
import static vista.Principal.tablaClientesApp;
import static vista.Principal.usuariosApp;
import static vista.Principal.webEnabled;

/**
 *
 * @author Usuario
 */
public class WorkersApp {
    
    public static void workerGenerarUsers() {
        WorkerCrearUsuarios actu = new WorkerCrearUsuarios();
        actu.execute();
    }
    
    public static void cargarUsuariosApp() {
        WorkerCargarUsuarios actu = new WorkerCargarUsuarios();
        actu.execute();
    }
    
    public static void actualizarPendientes() {
        WorkerActualizarPendientes actu = new WorkerActualizarPendientes();
        actu.execute();
    }
    
    public static void guardarPrestamo(Prestamo p) {
        WorkerGuardarPrestamo actu = new WorkerGuardarPrestamo();
        actu.setPrestamo(p);
        actu.execute();
    }
    
    public static void guardarUltimoPrestamoPorCodCli(int cod) {
        WorkerGuardarUltimoPrestamoCliente actu = new WorkerGuardarUltimoPrestamoCliente();
        actu.setCodCliente(cod);
        actu.execute();
    }
    
    public static void actualizarUsuario(Cliente c) {
        WorkerActUsuario actu = new WorkerActUsuario();
        actu.setCliente(c);
        actu.execute();
    }
    
    public static void crearUsuario(Cliente c) {
        WorkerCrearUsuario actu = new WorkerCrearUsuario();
        actu.setCliente(c);
        actu.execute();
    }
    
    public static void eliminarUsuario(String username) {
        WorkerEliminarUsuario actu = new WorkerEliminarUsuario();
        actu.setUser(username);
        actu.execute();
    }
    
    public static void eliminarTodosLosUsuarios() {
        WorkerEliminarTodosLosUsuarios actu = new WorkerEliminarTodosLosUsuarios();
        actu.execute();
    }
    
    public static void blanquearContraseña(String username) {
        WorkerEliminarUsuario actu = new WorkerEliminarUsuario();
        actu.setUser(username);
        actu.execute();
    }
    
    public static void eliminarPrestYCuotas(Prestamo p) {
        WorkerEliminarPrestYCuotas actu = new WorkerEliminarPrestYCuotas();
        actu.setPrestamo(p);
        actu.execute();
    }
    
    //WORKERS COBRADORES
    
    public static void crearUsuarioCobrador(Cobrador c) {
        WorkerCrearUsuarioCob actu = new WorkerCrearUsuarioCob();
        actu.setCobrador(c);
        actu.execute();
    }
}

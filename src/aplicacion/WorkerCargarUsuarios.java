/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion;


import workers.*;
import controlador.Conector;
import controlador.Funciones;
import controlador.Main;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultRowSorter;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.SwingWorker;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import modelo.Cliente;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;
import static vista.Principal.cargarActFallidas;
import static vista.Principal.cargarMenuUsuariosApp;
import static vista.Principal.clientes;
import static vista.Principal.colorearUsers;
import static vista.Principal.modeloUsers;
import static vista.Principal.popupMenuUsuariosApp;
import static vista.Principal.tablaClientesApp;
import static vista.Principal.usuariosApp;
import static vista.Principal.cobradoresApp;
import static vista.Principal.webEnabled;


/**
 *
 * @author Will
 */
public class WorkerCargarUsuarios extends SwingWorker<Double, Integer> {
   

    Color rojo = new Color(204, 51, 0);
    Color amarillo = new Color(254, 255, 184);
    Color verde = new Color(0,153,0);
    Color naranja = new Color(255, 133, 12);
    
    
    @Override
    protected Double doInBackground() throws Exception {
      
        Principal.lbEstadoApp.setText("Cargando usuarios de la aplicación...");
        Principal.panelEstadoApp.setBackground(naranja);
        
        colorearUsers = new ColorearTablaUsuariosApp();        

         //CARGA USUARIOS APP DESDE MYSQL
         try {
            usuariosApp = AplicacionControlador.getUsuariosApp();
            //cobradoresApp = AplicacionControlador.getUsuariosCobradoresApp();
            cobradoresApp = new ArrayList<>();
            Principal.lbInfoLinea1.setText(Principal.clientes.size()+" clientes registrados.");
            Principal.lbInfoLinea2.setText(usuariosApp.size()+" usuarios creados.");
            Principal.lbInfoLinea3.setText(Principal.clientes.size()-usuariosApp.size()+" clientes sin usuario.");
            webEnabled = true;
            cargarActFallidas();
             
            Principal.lbEstadoApp.setText("Usuarios cargados...");
            Principal.panelEstadoApp.setBackground(verde);
        
         } catch (Exception e) {
             Principal.lbInfoLinea1.setText(" ");
             Principal.lbInfoLinea2.setText(" ");
             Principal.lbInfoLinea3.setText(" ");
             usuariosApp = new ArrayList<>();
             webEnabled = false;
             cargarActFallidas();
             Color rojo = new Color(204, 51, 0);
             Principal.lbEstadoApp.setText("Se produjo un error al cargar los usuarios...");
             Principal.panelEstadoApp.setBackground(rojo);
         }


         modeloUsers = new TMUserApp(clientes);
         tablaClientesApp.setModel(modeloUsers);

         //DEFINE ANCHOS
         TableColumnModel columnModelClientes = tablaClientesApp.getColumnModel();
         columnModelClientes.getColumn(0).setPreferredWidth(50);
         columnModelClientes.getColumn(1).setPreferredWidth(130);
         columnModelClientes.getColumn(2).setPreferredWidth(120);
         columnModelClientes.getColumn(3).setPreferredWidth(150);
         columnModelClientes.getColumn(4).setPreferredWidth(100);
         columnModelClientes.getColumn(5).setPreferredWidth(120);


         //ORDENAR CUOTAS PRESTAMOS
         TableRowSorter<TableModel> elQueOrdenaClientes = new TableRowSorter<TableModel>(modeloUsers);
         tablaClientesApp.setRowSorter(elQueOrdenaClientes);

         tablaClientesApp.setAutoCreateRowSorter(true);
         DefaultRowSorter sorterClientes = ((DefaultRowSorter)tablaClientesApp.getRowSorter()); 
         ArrayList listaClientes = new ArrayList();
         listaClientes.add( new RowSorter.SortKey(1, SortOrder.ASCENDING) );
         sorterClientes.setSortKeys(listaClientes);
         sorterClientes.sort();

         cargarMenuUsuariosApp();


         tablaClientesApp.setComponentPopupMenu(popupMenuUsuariosApp);
         tablaClientesApp.addMouseListener(new TableMouseListenerUsersApp(tablaClientesApp));

         tablaClientesApp.setDefaultRenderer(Object.class, colorearUsers);
        
            //javax.swing.JOptionPane.showMessageDialog(null,"Liquidación guardada correctamente.","Guardada",1);
            
            
        System.out.println("COBRADORES: "+Principal.cobradoresApp.size());    

            // Se pasa valor para la barra de progreso. ESto llamara al metodo
            // process() en el hilo de despacho de eventos.
            
         Principal.cargarCobradores();
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()

    @Override
    protected void process(List<Integer> chunks) {

    }
    
    
    public void setProgreso(JProgressBar prog) {
    }
    
    
}

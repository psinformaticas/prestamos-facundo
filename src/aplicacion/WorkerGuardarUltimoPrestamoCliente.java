/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion;


import workers.*;
import controlador.Conector;
import controlador.Funciones;
import controlador.Main;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cliente;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerGuardarUltimoPrestamoCliente extends SwingWorker<Double, Integer> {
   

    Color rojo = new Color(204, 51, 0);
    Color amarillo = new Color(254, 255, 184);
    Color verde = new Color(0,153,0);
    Color naranja = new Color(255, 133, 12);
    int codCliente;
    @Override
    protected Double doInBackground() throws Exception {
      
        Principal.lbEstadoApp.setText("Guardando último préstamo del cliente...");
        Principal.panelEstadoApp.setBackground(naranja);
        try {
            AplicacionControlador.guardarUltimoPrestamoClienteEnApp(codCliente);
            Principal.lbEstadoApp.setText("Último préstamo del cliente guardado...");
            Principal.panelEstadoApp.setBackground(verde);
        } catch (Exception e) {
            Principal.lbEstadoApp.setText("Error al guardar último préstamo del cliente...");
            Principal.panelEstadoApp.setBackground(rojo);  
        }
        
            
        
        
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()

    @Override
    protected void process(List<Integer> chunks) {

    }
    
    
    public void setProgreso(JProgressBar prog) {
    }
    
    public void setCodCliente(int c) {
        codCliente = c;
    }
    
}

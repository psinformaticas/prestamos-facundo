/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion;


import workers.*;
import controlador.Conector;
import controlador.ConectorApp;
import controlador.Funciones;
import controlador.Main;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cliente;
import modelo.Cuota;
import modelo.Prestamo;
import modelo.UsuarioApp;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerEliminarTodosLosUsuarios extends SwingWorker<Double, Integer> {
   

    
    Color rojo = new Color(204, 51, 0);
    Color amarillo = new Color(254, 255, 184);
    Color verde = new Color(0,153,0);
    Color naranja = new Color(255, 133, 12);
    private Cliente cli;
    ConectorApp con;
    @Override
    protected Double doInBackground() throws Exception {
      
        Principal.lbEstadoApp.setText("Eliminando usuarios de la aplicación...");
        Principal.panelEstadoApp.setBackground(naranja);
        con = new ConectorApp();
        con.conectar();
        try{
        
            for (int i=0; i < Principal.usuariosApp.size(); i++) {

                double porc = ((i+1)*100)/(Principal.clientes.size());    
                int porcAux = (int) porc;

                UsuarioApp usr = Principal.usuariosApp.get(i);

                Principal.lbEstadoApp.setText("Eliminando usuario "+(i+1)+" de "+Principal.clientes.size()+"...");
                AplicacionControlador.eliminarUsuarioPorDNIRequiereConector(usr.getUsername(), false, con);
            }
        
        } catch (Exception e) {
            Principal.lbEstadoApp.setText("Se produjo un error al eliminar usuarios...");
            Principal.panelEstadoApp.setBackground(rojo);
        }
        con.cerrar();

            // Se pasa valor para la barra de progreso. ESto llamara al metodo
            // process() en el hilo de despacho de eventos.
            
        
        
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()

    @Override
    protected void process(List<Integer> chunks) {

    }
    
    
    public void setCliente(Cliente c) {
        this.cli= c;
    }
    
    
}

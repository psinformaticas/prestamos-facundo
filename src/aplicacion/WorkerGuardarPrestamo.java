/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion;


import workers.*;
import controlador.Conector;
import controlador.Funciones;
import controlador.Main;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cliente;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerGuardarPrestamo extends SwingWorker<Double, Integer> {
   

    private Prestamo prest;
    Color rojo = new Color(204, 51, 0);
    Color amarillo = new Color(254, 255, 184);
    Color verde = new Color(0,153,0);
    Color naranja = new Color(255, 133, 12);
    
    @Override
    protected Double doInBackground() throws Exception {
      
        Principal.lbEstadoApp.setText("Guardando/Actualizando préstamo...");
        Principal.panelEstadoApp.setBackground(naranja);
        
        AplicacionControlador.guardarPrestamo(prest);
            
        Principal.lbEstadoApp.setText("Se han actualizado los datos...");
        Principal.panelEstadoApp.setBackground(verde);
        
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()

    @Override
    protected void process(List<Integer> chunks) {

    }
    
    
    public void setProgreso(JProgressBar prog) {
    }
    
    public void setPrestamo(Prestamo p) {
        prest = p;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion;


import workers.*;
import controlador.Conector;
import controlador.Funciones;
import controlador.Main;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cliente;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerActualizarPendientes extends SwingWorker<Double, Integer> {
   

    
    Color rojo = new Color(204, 51, 0);
    Color amarillo = new Color(254, 255, 184);
    Color verde = new Color(0,153,0);
    Color naranja = new Color(255, 133, 12);
    
    @Override
    protected Double doInBackground() throws Exception {
      
        Principal.lbEstadoApp.setText("Actualizando datos pendientes en la aplicación...");
        Principal.panelEstadoApp.setBackground(naranja);
        
        try {
            Principal.actualizarPendientes();
            Principal.lbEstadoApp.setText("Se han actualizado los datos en la aplicación...");
            Principal.panelEstadoApp.setBackground(verde);
        } catch (Exception e) {
            Principal.lbEstadoApp.setText("Se produjo un error al actualizar los datos...");
            Principal.panelEstadoApp.setBackground(rojo);

        }
        
        
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()

    @Override
    protected void process(List<Integer> chunks) {

    }
    
    
    public void setProgreso(JProgressBar prog) {
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion;


import workers.*;
import controlador.Conector;
import controlador.ConectorApp;
import controlador.Funciones;
import controlador.Main;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cliente;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerCrearUsuarios extends SwingWorker<Double, Integer> {
   

    
    Color rojo = new Color(204, 51, 0);
    Color amarillo = new Color(254, 255, 184);
    Color verde = new Color(0,153,0);
    Color naranja = new Color(255, 133, 12);
    ConectorApp con;
    @Override
    protected Double doInBackground() throws Exception {
      
        Principal.lbEstadoApp.setText("Creando usuarios de la aplicación...");
        Principal.panelEstadoApp.setBackground(naranja);
        con = new ConectorApp();
        con.conectar();
        try{
        
            for (int i=0; i < Principal.clientes.size(); i++) {

                double porc = ((i+1)*100)/(Principal.clientes.size());    
                int porcAux = (int) porc;

                Cliente cli = Principal.clientes.get(i);

                Principal.lbEstadoApp.setText("Creando usuario "+(i+1)+" de "+Principal.clientes.size()+"...");
                AplicacionControlador.generarUsuarioPorClienteRequiereConector(cli, false, con);
            }
        
        } catch (Exception e) {
            Principal.lbEstadoApp.setText("Se produjo un error al crear usuarios...");
            Principal.panelEstadoApp.setBackground(rojo);
        }
        con.cerrar();
            //javax.swing.JOptionPane.showMessageDialog(null,"Liquidación guardada correctamente.","Guardada",1);
            
            
            

            // Se pasa valor para la barra de progreso. ESto llamara al metodo
            // process() en el hilo de despacho de eventos.
            
        Principal.lbEstadoApp.setText("Se han creado los usuarios de la aplicación...");
        Principal.panelEstadoApp.setBackground(verde);
        
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()

    @Override
    protected void process(List<Integer> chunks) {

    }
    
    
    public void setProgreso(JProgressBar prog) {
    }
    
    
}

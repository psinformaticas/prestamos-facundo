/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion;


import workers.*;
import controlador.Conector;
import controlador.ConectorApp;
import controlador.Funciones;
import controlador.Main;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cliente;
import modelo.Cobrador;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerCrearUsuarioCob extends SwingWorker<Double, Integer> {
   

    
    Color rojo = new Color(204, 51, 0);
    Color amarillo = new Color(254, 255, 184);
    Color verde = new Color(0,153,0);
    Color naranja = new Color(255, 133, 12);
    private Cobrador cob;
    
    @Override
    protected Double doInBackground() throws Exception {
      
        Principal.lbEstadoApp.setText("Creando usuario de la aplicación para "+cob.getNombreApp()+" ...");
        Principal.panelEstadoApp.setBackground(naranja);
        try{
        
            AplicacionControlador.generarUsuarioPorCobrador(cob, false);
            Principal.lbEstadoApp.setText("Se ha creado correctamente el usuario...");
            Principal.panelEstadoApp.setBackground(verde);
        } catch (Exception e) {
            Principal.lbEstadoApp.setText("Se produjo un error al crear usuarios...");
            Principal.panelEstadoApp.setBackground(rojo);
        }
        
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()

    @Override
    protected void process(List<Integer> chunks) {

    }
    
    
    public void setCobrador(Cobrador c) {
        this.cob = c;
    }
    
    
}

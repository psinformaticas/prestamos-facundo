/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion;


import workers.*;
import controlador.Conector;
import controlador.ConectorApp;
import controlador.Funciones;
import controlador.Main;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cliente;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerEliminarUsuario extends SwingWorker<Double, Integer> {
   

    
    Color rojo = new Color(204, 51, 0);
    Color amarillo = new Color(254, 255, 184);
    Color verde = new Color(0,153,0);
    Color naranja = new Color(255, 133, 12);
    private String user;
    
    @Override
    protected Double doInBackground() throws Exception {
      
        Principal.lbEstadoApp.setText("Eliminando usuario de la aplicación para "+user+" ...");
        Principal.panelEstadoApp.setBackground(naranja);
        try{
        
            AplicacionControlador.eliminarUsuarioApp(user);
            Principal.lbEstadoApp.setText("Se han eliminado correctamente el usuario...");
            Principal.panelEstadoApp.setBackground(verde);
            WorkersApp.cargarUsuariosApp();
        } catch (Exception e) {
            Principal.lbEstadoApp.setText("Se produjo un error al eliminar usuario...");
            Principal.panelEstadoApp.setBackground(rojo);
        }
            //javax.swing.JOptionPane.showMessageDialog(null,"Liquidación guardada correctamente.","Guardada",1);
            
            
            

            // Se pasa valor para la barra de progreso. ESto llamara al metodo
            // process() en el hilo de despacho de eventos.
            
        
        
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()

    @Override
    protected void process(List<Integer> chunks) {

    }
    
    
    public void setUser(String c) {
        this.user= c;
    }
    
    
}

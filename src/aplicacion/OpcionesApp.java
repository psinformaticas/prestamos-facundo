/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion;

/**
 *
 * @author Usuario
 */
public class OpcionesApp {
    private int permitirLogin;
    private String numWhatsapp, numComunicarse, novedades;

    public OpcionesApp() {
        this.numWhatsapp = "";
        this.numComunicarse = "";
        this.novedades = "";
        this.permitirLogin = 1;
    }

    public String getNumWhatsapp() {
        return numWhatsapp;
    }

    public void setNumWhatsapp(String numWhatsapp) {
        this.numWhatsapp = numWhatsapp;
    }

    public String getNumComunicarse() {
        return numComunicarse;
    }

    public void setNumComunicarse(String numComunicarse) {
        this.numComunicarse = numComunicarse;
    }

    public String getNovedades() {
        return novedades;
    }

    public void setNovedades(String novedades) {
        this.novedades = novedades;
    }

    public int getPermitirLogin() {
        return permitirLogin;
    }

    public void setPermitirLogin(int permitirLogin) {
        this.permitirLogin = permitirLogin;
    }
    
    
    
    
    
}

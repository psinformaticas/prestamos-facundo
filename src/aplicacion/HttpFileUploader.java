/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author Usuario
 */
public class HttpFileUploader implements Runnable{
 
    URL connectURL;
    String params;
    String responseString;
    String fileName;
    byte[] dataToServer;
 
    public HttpFileUploader(String urlString, String params, String fileName ){
        try{
            connectURL = new URL(urlString);
        }catch(Exception ex){
        }
        this.params = params+"=";
        this.fileName = fileName;
    }
 
    public void doStart(FileInputStream stream){
        fileInputStream = stream;
        thirdTry();
    }
 
    FileInputStream fileInputStream = null;
    void thirdTry() {
        String exsistingFileName = fileName;
 
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        String Tag="3rd";
        try
        {
            //------------------ CLIENT REQUEST
 
            // Abrimos una conexión http con la URL
 
            HttpURLConnection conn = (HttpURLConnection) connectURL.openConnection();
 
            // Permitimos Inputs
            conn.setDoInput(true);
 
            // Permitimos Outputs
            conn.setDoOutput(true);
 
            // Deshabilitamos el uso de la copia cacheada.
            conn.setUseCaches(false);
 
            // Usamos el método post esto podemos cambiarlo.
            conn.setRequestMethod("POST");
 
            conn.setRequestProperty("Connection", "Keep-Alive");
 
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);
 
            DataOutputStream dos = new DataOutputStream( conn.getOutputStream() );
 
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + exsistingFileName +"\"" + lineEnd);
            dos.writeBytes(lineEnd);
 
 
            // creamos un buffer con el tamaño maximo de archivo, lo pondremos en 1MB
 
            int bytesAvailable = fileInputStream.available();
            int maxBufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);
            byte[] buffer = new byte[bufferSize];
 
            int bytesRead = fileInputStream.read(buffer, 0, bufferSize);
 
            while (bytesRead > 0)
            {
            dos.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }
 
            // enviar multipart form data
 
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
 
            // cerramos
            fileInputStream.close();
            dos.flush();
 
            InputStream is = conn.getInputStream();
            // retrieve the response from server
            int ch;
 
            StringBuffer b =new StringBuffer();
            while( ( ch = is.read() ) != -1 ){
            b.append( (char)ch );
            }
            String s=b.toString();
            dos.close();
 
        }
        catch (MalformedURLException ex)
        {
        }
 
        catch (IOException ioe)
        {
        }
    }
 
@Override
public void run() {
 
}
 



}
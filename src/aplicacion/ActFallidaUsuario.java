/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion;

import modelo.*;
import controlador.Funciones;
import java.util.Date;
import vista.Principal;

/**
 *
 * @author Usuario
 */
public class ActFallidaUsuario {
    
    private int cod;
    private String accion, dni;

    public ActFallidaUsuario() {
        this.cod = 0;
        this.accion = "";
        this.dni = "";
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    
    
}

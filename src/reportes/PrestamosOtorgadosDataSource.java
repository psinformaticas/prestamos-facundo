package reportes;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import javax.swing.JOptionPane;
import modelo.*;
import controlador.*;
import vista.*;
/**
 *
 * @author Sandra
 */
public class PrestamosOtorgadosDataSource implements JRDataSource{

    private ArrayList<Prestamo> prestamos = new ArrayList<>();
    private int indicePrestamos = -1;
    private Prestamo prestamoAux;
    private String periodo;
    private String totalPrestado;
    private String totalGanancia;
    
    
    @Override
    public boolean next() throws JRException {
       return ++indicePrestamos < prestamos.size();
    }
            
   

    public void cargarPrestamos(ArrayList<Prestamo> ps){
        this.prestamos = ps;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public void setTotalPrestado(String totalPrestado) {
        this.totalPrestado = totalPrestado;
    }

    public void setTotalGanancia(String totalGanancia) {
        this.totalGanancia = totalGanancia;
    }
    
    
    
    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
       Object valor = null;
       Cliente cli = prestamos.get(indicePrestamos).getCliente();
       Plan plan = Funciones.getPlanPorNombre(prestamos.get(indicePrestamos).getPlan());
        if ("nombreempresa".equals(jrf.getName())){
            valor = Funciones.capitalize(Principal.opciones.getNombre());
        }
        else if ("dirempresa".equals(jrf.getName())){
            valor = Funciones.capitalize(Principal.opciones.getDireccion());
        }
        else if ("telempresa".equals(jrf.getName())){
            valor = Principal.opciones.getTelefono().toUpperCase();
        }
      
        
        else if ("periodo".equals(jrf.getName())){
            valor = periodo;
        }
        else if ("cantprestamos".equals(jrf.getName())){
            valor = Integer.toString(prestamos.size());
        }
        else if ("totalprestado".equals(jrf.getName())){
            valor = totalPrestado;
        }
        else if ("totalganancia".equals(jrf.getName())){
            valor = totalGanancia;
        }
        
        else if ("cliente".equals(jrf.getName())){
            valor = Funciones.capitalize(cli.getNombre());
        }
        else if ("direccion".equals(jrf.getName())){
            valor = Funciones.capitalize(cli.getDireccion()+" - "+ Funciones.getLocalidadPorCodInt(cli.getLocalidad()).getNombre());
        }
        else if ("telefono".equals(jrf.getName())){
            valor = cli.getCelular()+" - "+ cli.getTelefono();
        }
        
        
        else if ("otorgado".equals(jrf.getName())){
            valor = prestamos.get(indicePrestamos).getFechaGenerado();
        }
        else if ("plan".equals(jrf.getName())){
            valor = prestamos.get(indicePrestamos).getPlan().toUpperCase();
        }
        else if ("cantcuotas".equals(jrf.getName())){
            valor = plan.getCantCuotas();
        }
        else if ("cuota".equals(jrf.getName())){
            valor = "$"+prestamos.get(indicePrestamos).getCuotas().get(1).getImporteTotal();
        }
        else if ("prestado".equals(jrf.getName())){
            valor = "$"+plan.getImportePrestar();
        }
        else if ("devolver".equals(jrf.getName())){
            valor = "$"+plan.getImporteDevolver();
        }
        else if ("ganancia".equals(jrf.getName())){
            valor = "$"+plan.getImporteInteres();
        }
        else if ("int".equals(jrf.getName())){
            valor = plan.getPorcentajeInteres();
        }
        
        
       else if ("usuario".equals(jrf.getName())){
            valor = Funciones.capitalize(Principal.usuario);
       } 
       else if ("fechaactual".equals(jrf.getName())){
            valor = Funciones.devolverFechaActualStr() + " " + Funciones.devolverHoraActualStr() ;
       } 

        return valor;
    }
    
}

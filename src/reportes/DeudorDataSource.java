
package reportes;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import javax.swing.JOptionPane;
import modelo.*;
import controlador.*;
import vista.*;
/**
 *
 * @author Sandra
 */
public class DeudorDataSource implements JRDataSource{

    
    private String nombre, direccion, deuda;
    

    private int indice = -1;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDeuda() {
        return deuda;
    }

    public void setDeuda(String deuda) {
        this.deuda = deuda;
    }
    
    
    
    @Override
    public boolean next() throws JRException {
       return ++indice < 1;
    }

  
    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }
            
    
    
    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
       Object valor = null;

        if ("nombre".equals(jrf.getName())){
            valor = Funciones.capitalize(nombre);
        }
        else if ("direccion".equals(jrf.getName())){
            valor = Funciones.capitalize(direccion);
        }
        else if ("deuda".equals(jrf.getName())){
            valor = deuda;
        }
        return valor;
    }
    
}

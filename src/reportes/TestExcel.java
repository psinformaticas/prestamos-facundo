/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportes;

import controlador.Conector;
import controlador.Funciones;
import controlador.FuncionesReportes;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Carton;
import modelo.Cliente;
import modelo.Cuota;
import modelo.DatosComprobante;
import modelo.LineaSMS;
import modelo.Plan;
import modelo.Prestamo;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.PatternFormatting;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import vista.Principal;

/**
 *
 * @author Will
 */
public class TestExcel {
    
    public static void rellenarXls(DatosComprobante d){
        InputStream inp = null;
        try {
            inp = new FileInputStream("modr.xls");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestExcel.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", 0);
        }
        Workbook wb = null;
        try {
            wb = WorkbookFactory.create(inp);
            
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", 0);
        } catch (EncryptedDocumentException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", 0);
        }
        Sheet sheet = wb.getSheetAt(0);
        
        //EMPRESA
        Row row= sheet.getRow(1);
        Cell cell = row.getCell(1);
        cell.setCellValue(Principal.opciones.getNombre().toUpperCase());
        //DIRE
        row= sheet.getRow(5);
        cell = row.getCell(1);
        cell.setCellValue(Funciones.capitalize(Principal.opciones.getDireccion()));
        //TEL
        row= sheet.getRow(8);
        cell = row.getCell(1);
        cell.setCellValue(Funciones.capitalize(Principal.opciones.getTelefono()));
        
        //TIPO
        row= sheet.getRow(2);
        cell = row.getCell(23);
        cell.setCellValue(d.getTipo().toUpperCase());
        //NUM
        row= sheet.getRow(5);
        cell = row.getCell(23);
        cell.setCellValue(d.getNumero());
        //FECHA
        row= sheet.getRow(8);
        cell = row.getCell(23);
        cell.setCellValue("Fecha: "+d.getFecha());
        
        //NOM
        row= sheet.getRow(12);
        cell = row.getCell(16);
        cell.setCellValue(Funciones.capitalize(d.getNombre()));
        //DIR
        row= sheet.getRow(13);
        cell = row.getCell(7);
        cell.setCellValue(Funciones.capitalize(d.getDireccion()));
        System.out.println(Funciones.capitalize(d.getDireccion()));
        //TEL
        row= sheet.getRow(14);
        cell = row.getCell(7);
        cell.setCellValue(Funciones.capitalize(d.getTelefono()));
        
        //CANT1
        row= sheet.getRow(18);
        cell = row.getCell(1);
        cell.setCellValue(d.getCant1());
        //DET1
        row= sheet.getRow(18);
        cell = row.getCell(4);
        cell.setCellValue(Funciones.capitalize(d.getDetalle1()));
        //MONTO1
        row= sheet.getRow(18);
        cell = row.getCell(33);
        cell.setCellValue(Funciones.capitalize(d.getMonto1()));
        
        //TOTAL
        row= sheet.getRow(41);
        cell = row.getCell(33);
        cell.setCellValue(Funciones.capitalize(d.getTotal()));
        
        //OBS
        row= sheet.getRow(43);
        cell = row.getCell(1);
        cell.setCellValue(Funciones.pMayus(d.getObs()));
        
        
        try (FileOutputStream fileOut = new FileOutputStream("comprobante.xls")) {
            wb.write(fileOut);
            File archivoXLS = new File("comprobante.xls");
            Desktop.getDesktop().open(archivoXLS);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error", 0);
            System.out.println("ERROR");
        }
    }
        
    public static void rellenarXls(Carton c) throws FileNotFoundException, IOException{
        InputStream inp = new FileInputStream("carton.xls");
        Workbook wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(0);
        
        //CANT CUOTAS
        Row row= sheet.getRow(1);
        Cell cell = row.getCell(3);
        cell.setCellValue(c.getCuotas());
        //TOTAL PREST
        row= sheet.getRow(2);
        cell = row.getCell(2);
        cell.setCellValue(c.getPrestamo());
        //IMP CUOTA
        row= sheet.getRow(2);
        cell = row.getCell(3);
        cell.setCellValue(c.getCuota());
        //F ENTREGA
        row= sheet.getRow(2);
        cell = row.getCell(5);
        cell.setCellValue(c.getfEntrega());
        //EN SOBRE
        row= sheet.getRow(3);
        cell = row.getCell(5);
        cell.setCellValue(c.getEnSobre());
        //DESC
        row= sheet.getRow(4);
        cell = row.getCell(5);
        cell.setCellValue(c.getDescontado());
        //NOMBRE
        row= sheet.getRow(18);
        cell = row.getCell(7);
        cell.setCellValue(c.getNombre());
        //LOC
        row= sheet.getRow(20);
        cell = row.getCell(8);
        cell.setCellValue(c.getLocalidad());
        //DIR
        row= sheet.getRow(21);
        cell = row.getCell(7);
        cell.setCellValue(c.getLocalidad());
        //TEL
        row= sheet.getRow(22);
        cell = row.getCell(8);
        cell.setCellValue(c.getTelefono());
        //INT DIA
        row= sheet.getRow(24);
        cell = row.getCell(5);
        cell.setCellValue(c.getIntDiario());
        
        //CUOTA1
        row= sheet.getRow(11);
        cell = row.getCell(3);
        cell.setCellValue(c.getC1());
        
        //CUOTA2
        row= sheet.getRow(12);
        cell = row.getCell(3);
        cell.setCellValue(c.getC2());
        
        //CUOTA3
        row= sheet.getRow(13);
        cell = row.getCell(3);
        cell.setCellValue(c.getC3());
        
        //CUOTA4
        row= sheet.getRow(14);
        cell = row.getCell(3);
        cell.setCellValue(c.getC4());
        
        //CUOTA5
        row= sheet.getRow(15);
        cell = row.getCell(3);
        cell.setCellValue(c.getC5());
        
        //CUOTA6
        row= sheet.getRow(16);
        cell = row.getCell(3);
        cell.setCellValue(c.getC6());
        
        //CUOTA7
        row= sheet.getRow(17);
        cell = row.getCell(3);
        cell.setCellValue(c.getC7());
        
        //CUOTA8
        row= sheet.getRow(18);
        cell = row.getCell(3);
        cell.setCellValue(c.getC8());
        
        //CUOTA9
        row= sheet.getRow(19);
        cell = row.getCell(3);
        cell.setCellValue(c.getC9());
        
        //CUOTA10
        row= sheet.getRow(20);
        cell = row.getCell(3);
        cell.setCellValue(c.getC10());
        
        try (FileOutputStream fileOut = new FileOutputStream("imprimir.xls")) {
            System.out.println("GUARDANDO");
            wb.write(fileOut);
            File archivoXLS = new File("imprimir.xls");
            Desktop.getDesktop().open(archivoXLS);
        } catch (Exception e) {
            System.out.println("ERROR");
        }
    }
    
    public static void rellenarXlsSobre(Cliente c) throws FileNotFoundException, IOException{
        InputStream inp = new FileInputStream("sobre.xls");
        Workbook wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(0);
        
        //NOMBRE
        Row row= sheet.getRow(6);
        Cell cell = row.getCell(2);
        cell.setCellValue(Funciones.capitalize(c.getNombre()));
        //DIR
        row= sheet.getRow(9);
        cell = row.getCell(2);
        cell.setCellValue(Funciones.capitalize(c.getDireccion()));
        //LOC
        row= sheet.getRow(12);
        cell = row.getCell(2);
        cell.setCellValue(Funciones.capitalize(Funciones.getLocalidadPorCodInt(c.getLocalidad()).getNombre()));
        //DNI
        row= sheet.getRow(15);
        cell = row.getCell(2);
        cell.setCellValue(c.getDni());
        //COD
        row= sheet.getRow(5);
        cell = row.getCell(8);
        cell.setCellValue(Integer.toString(c.getCod()));
        
        
        try (FileOutputStream fileOut = new FileOutputStream("gensobre.xls")) {
            wb.write(fileOut);
            File archivoXLS = new File("gensobre.xls");
            Desktop.getDesktop().open(archivoXLS);
        } catch (Exception e) {
            System.out.println("ERROR");
        }
    }
    
    public static void rellenarXlsSms() throws Exception{
        
        ArrayList<LineaSMS> lineas = FuncionesReportes.getDatosParaEnvioSMS();
        
        //Datos para crear hoja
        int cantLineas = lineas.size();
        
        
        /*La ruta donde se creará el archivo*/
        String rutaArchivo = System.getProperty("user.home")+"/enviosms.xls";
        /*Se crea el objeto de tipo File con la ruta del archivo*/
        File archivoXLS = new File(rutaArchivo);
        /*Si el archivo existe se elimina*/
        if(archivoXLS.exists()) archivoXLS.delete();
        /*Se crea el archivo*/
        archivoXLS.createNewFile();
        
        /*Se crea el libro de excel usando el objeto de tipo Workbook*/
        Workbook libro = new HSSFWorkbook();
        /*Se inicializa el flujo de datos con el archivo xls*/
        FileOutputStream archivo = new FileOutputStream(archivoXLS);
        
        /*Utilizamos la clase Sheet para crear una nueva hoja de trabajo dentro del libro que creamos anteriormente*/
        Sheet hoja = libro.createSheet("Planilla SMS");
        hoja.getPrintSetup().setLandscape(true);
        hoja.getPrintSetup().setPaperSize(HSSFPrintSetup.A4_PAPERSIZE); 
        
        
        hoja.setColumnWidth(0, 3500);
        hoja.setColumnWidth(1, 4500);
        hoja.setColumnWidth(2, 3500);
        hoja.setColumnWidth(3, 3500);
        hoja.setColumnWidth(3, 3500);
        
        
        /*Hacemos un ciclo para inicializar los valores de 10 filas de celdas*/
        for(int f=0;f<cantLineas+1;f++){
            
            
            /*La clase Row nos permitirá crear las filas*/
            Row fila = hoja.createRow(f);
            
            /*Cada fila tendrá 5 celdas de datos*/
            for(int c=0;c<11;c++){
                /*Creamos la celda a partir de la fila actual*/
                Cell celda = fila.createCell(c);
                
                /*Si la fila es la número 0, estableceremos los encabezados*/
                if(f==0){
                    if (c==0) {
                        celda.setCellValue("Telefono");
                    }
                    if (c==1) {
                        celda.setCellValue("Nombre Cliente");
                    }
                    if (c==2) {
                        celda.setCellValue("Deuda Cuotas");
                    }
                    if (c==3) {
                        celda.setCellValue("Deuda Recargos");
                    }
                    if (c==4) {
                        celda.setCellValue("Fecha de Hoy");
                    }
                   
                    
                }else{
                    //Carga vehiculo en el ciclo
                    LineaSMS linea = lineas.get(f-1);
                    if (c==0) {
                        celda.setCellValue(linea.getTelefono());
                    }
                    if (c==1) {
                        celda.setCellValue(Funciones.capitalize(linea.getNombre()));
                    }
                    if (c==2) {
                        celda.setCellValue(linea.getDeudaCuotas());
                    }
                    if (c==3) {
                        celda.setCellValue(linea.getDeudaRecargos());
                    }
                    if (c==4) {
                        celda.setCellValue(linea.getFechaHoy());
                    }
                    /*Si no es la primera fila establecemos un valor*/
//                    hoja.autoSizeColumn(c);
                }
            }
        }
        
        /*Escribimos en el libro*/
        libro.write(archivo);
        /*Cerramos el flujo de datos*/
        archivo.close();
        /*Y abrimos el archivo con la clase Desktop*/
        Desktop.getDesktop().open(archivoXLS);
    }
    

}

package reportes;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import javax.swing.JOptionPane;
import modelo.*;
import controlador.*;
import vista.*;
/**
 *
 * @author Sandra
 */
public class ReciboA4DataSource implements JRDataSource{

    public static Conector con = new Conector();
    
    private String fecha, cliente, cantidad, concepto, impefectivo, imptotal, obs;
    

    private int indice = -1;
    
    
    
    @Override
    public boolean next() throws JRException {
       return ++indice < 1;
    }

    public static Conector getCon() {
        return con;
    }

    public static void setCon(Conector con) {
        ReciboA4DataSource.con = con;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getImpefectivo() {
        return impefectivo;
    }

    public void setImpefectivo(String impefectivo) {
        this.impefectivo = impefectivo;
    }

    public String getImptotal() {
        return imptotal;
    }

    public void setImptotal(String imptotal) {
        this.imptotal = imptotal;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }
            
    
    
    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
       Object valor = null;

        if ("cliente".equals(jrf.getName())){
            valor = Funciones.capitalize(cliente);
        }
        else if ("linea1".equals(jrf.getName())){
            valor = Funciones.capitalize(Principal.opciones.getNombre());
        }
        else if ("linea2".equals(jrf.getName())){
            valor = Funciones.capitalize(Principal.opciones.getDireccion());
        }
        else if ("linea3".equals(jrf.getName())){
            valor = Principal.opciones.getTelefono().toUpperCase();
        }
        else if ("fecha".equals(jrf.getName())){
            valor = fecha;
        }
        else if ("concepto".equals(jrf.getName())){
            valor = concepto;
        }
        else if ("cantidad".equals(jrf.getName())){
            valor = cantidad;
        }
        else if ("impefectivo".equals(jrf.getName())){
            valor = impefectivo;
        }
        else if ("imptotal".equals(jrf.getName())){
            valor = imptotal;
        }
        else if ("obs".equals(jrf.getName())){
            valor = obs;
        }
        
        return valor;
    }
    
}

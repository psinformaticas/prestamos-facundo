package reportes;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import javax.swing.JOptionPane;
import modelo.*;
import controlador.*;
import vista.*;
/**
 *
 * @author Sandra
 */
public class PrestamoDataSource implements JRDataSource{

    public static Conector con = new Conector();
    
    private Cliente cliente = new Cliente();
    private ArrayList<Cuota> cuotas = new ArrayList<>();
    private ArrayList<Prestamo> prestamos = new ArrayList<>();
    private int indiceCuotas = -1;
    private String[] referencia1;
    private String[] referencia2;
    private String[] referencia3;
    private String[] referencia4;
    private String pagosParciales;

        
    
    
    
    @Override
    public boolean next() throws JRException {
       return ++indiceCuotas < cuotas.size();
    }
            
    public void cargarRef1(String[] ref1){
        this.referencia1 = ref1;
    }
    
    public void cargarRef2(String[] ref2){
        this.referencia2 = ref2;
    }
    
    public void cargarRef3(String[] ref3){
        this.referencia3 = ref3;
    }
    
    public void cargarRef4(String[] ref4){
        this.referencia4 = ref4;
    }
    public void agregarPrestamo(Prestamo presta){
        this.prestamos.add(presta);
    }

    public void cargarCuotas(ArrayList<Cuota> cuots){
        this.cuotas = cuots;
    }
    
    public void cargarCliente(Cliente cli){
        this.cliente = cli;
    }

    public void setPagosParciales(String pagosParciales) {
        this.pagosParciales = pagosParciales;
    }
    
    
    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
       Object valor = null;

        if ("nombre".equals(jrf.getName())){
            valor = Funciones.capitalize(cliente.getNombre());
        }
        else if ("nombreempresa".equals(jrf.getName())){
            valor = Funciones.capitalize(Principal.opciones.getNombre());
        }
        else if ("dirempresa".equals(jrf.getName())){
            valor = Funciones.capitalize(Principal.opciones.getDireccion());
        }
        else if ("telempresa".equals(jrf.getName())){
            valor = Principal.opciones.getTelefono().toUpperCase();
        }
        else if ("plan".equals(jrf.getName())){
            valor = prestamos.get(0).getPlan().toUpperCase();
        }
        else if ("fechanac".equals(jrf.getName())){
            valor = cliente.getFechaNac();
        }
        
        else if ("dni".equals(jrf.getName())){
            valor = cliente.getDni();
        }
        else if ("direccion".equals(jrf.getName())){
            if (Principal.opcionesExtra.getX1().equals("SI")) {
                valor = "No disponible";
            } else {
                valor = Funciones.capitalize(cliente.getDireccion());
            }
            
        }
        else if ("localidad".equals(jrf.getName())){
            valor = Funciones.capitalize(cliente.getLocalidad()+" - "+Funciones.capitalize(Funciones.getLocalidadPorCodInt(cliente.getLocalidad()).getNombre()));
        }
        else if ("tipo".equals(jrf.getName())){
            Prestamo p = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, Integer.parseInt(cuotas.get(0).getCodPrestamo()));
            Plan pn = Funciones.getPlanPorNombre(p.getPlan());
            if (pn.getA1().equals("producto")) {
                valor = "FINANCIACIÓN";
            } else {
                valor = "PRÉSTAMO";
            }
        }
        else if ("telfijo".equals(jrf.getName())){
            valor = cliente.getTelefono();
        }
        else if ("telcel".equals(jrf.getName())){
            valor = cliente.getCelular();
        }
        else if ("nombreref1".equals(jrf.getName())){
            valor = Funciones.capitalize(referencia1[0]);
        }
        else if ("telref1".equals(jrf.getName())){
            valor = referencia1[1];
        }
        else if ("nombreref2".equals(jrf.getName())){
            valor = Funciones.capitalize(referencia2[0]);
        }
        else if ("telref2".equals(jrf.getName())){
            valor = referencia2[1];
        }
        else if ("nombreref3".equals(jrf.getName())){
            valor = Funciones.capitalize(referencia3[0]);
        }
        else if ("telref3".equals(jrf.getName())){
            valor = referencia3[1];
        }
        else if ("nombreref4".equals(jrf.getName())){
            valor = Funciones.capitalize(referencia4[0]);
        }
        else if ("telref4".equals(jrf.getName())){
            valor = referencia4[1];
        }
        else if ("numcuota".equals(jrf.getName())){
            valor = cuotas.get(indiceCuotas).getNumCuota();
            if (valor.equals("0")) {
                valor = "ANT.";
            }
            if (valor.equals("999")) {
                valor = "INT.";
            }
        }
        else if ("fechadepago".equals(jrf.getName())){
            valor = cuotas.get(indiceCuotas).getFechaPagar();
            if (cuotas.get(indiceCuotas).getFechaPagar().equals("01/01/2099")) {
                valor = "AL FINALIZAR";
            }
            
        }
        else if ("fechapagado".equals(jrf.getName())){
            if (cuotas.get(indiceCuotas).getFechaPagado().equals("01/01/2099")) {
                valor = "";
            } else {
                valor = cuotas.get(indiceCuotas).getFechaPagado();
            }
            
        }
        else if ("importe".equals(jrf.getName())){
            String numCuota = cuotas.get(indiceCuotas).getNumCuota();
            
            if (numCuota.equals("999")) {
                valor = Double.toString(Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, cuotas.get(indiceCuotas).getCodPrestamoInt()).getInteresTotalAlDiaDeHoy());
            } else {
                valor = "$ " + cuotas.get(indiceCuotas).getImporteTotal();
            }
            
            
        }
        else if ("importepagado".equals(jrf.getName())){
            valor = "$ " + cuotas.get(indiceCuotas).getImportePagado();
        }
        else if ("pagosparciales".equals(jrf.getName())){
            valor = pagosParciales;
        }
        else if ("interes".equals(jrf.getName())){
            String numCuota = cuotas.get(indiceCuotas).getNumCuota();
            
            if (numCuota.equals("999")) {
                valor = "-";
            } else {
                valor = "$ " + Double.toString(Funciones.formatearDecimales((cuotas.get(indiceCuotas).getInteresCalculado()),2));
            }
            
        }
        else if ("retraso".equals(jrf.getName())){
            valor = "-";
            if (Funciones.getEstadoCuota(cuotas.get(indiceCuotas)).equals("PENDIENTE") || Funciones.getEstadoCuota(cuotas.get(indiceCuotas)).equals("PARCIAL")|| Funciones.getEstadoCuota(cuotas.get(indiceCuotas)).equals("ATRASADA")) {
                valor = (cuotas.get(indiceCuotas).getDiasAtrasoHastaHoy())+" días";
            }
            if (Funciones.getEstadoCuota(cuotas.get(indiceCuotas)).equals("PAGADA")) {
                valor = (cuotas.get(indiceCuotas).getDiasAtrasoPago())+" días";
            }
                
        }
        else if ("estado".equals(jrf.getName())){
            valor = Funciones.getEstadoCuota(cuotas.get(indiceCuotas));
        }
       
       
       else if ("usuario".equals(jrf.getName())){
            valor = "Usuario";
       } 
       else if ("fechaactual".equals(jrf.getName())){
            valor = Funciones.devolverFechaActualStr() + " " + Funciones.devolverHoraActualStr() ;
       } 
        return valor;
    }
    
}

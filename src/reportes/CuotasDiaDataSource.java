package reportes;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import javax.swing.JOptionPane;
import modelo.*;
import controlador.*;
import vista.*;
/**
 *
 * @author Sandra
 */
public class CuotasDiaDataSource implements JRDataSource{

    public static Conector con = new Conector();
    
    private ArrayList<Cuota> cuotas = new ArrayList<Cuota>();
    private int indiceCuotas = -1;
    private Prestamo prestamoAux;
    private Cliente cliAux;
    private double totalCobrar;
    

        
    
    
    
    @Override
    public boolean next() throws JRException {
       return ++indiceCuotas < cuotas.size();
    }
            
   

    public void cargarCuotas(ArrayList<Cuota> cuots){
        this.cuotas = cuots;
    }
    
    public void cargarTotal(double cob){
        this.totalCobrar = cob;
    }
    

    
    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
       Object valor = null;

        if ("nombreempresa".equals(jrf.getName())){
            valor = Funciones.capitalize(Principal.opciones.getNombre());
        }
        else if ("dirempresa".equals(jrf.getName())){
            valor = Funciones.capitalize(Principal.opciones.getDireccion());
        }
        else if ("telempresa".equals(jrf.getName())){
            valor = Principal.opciones.getTelefono().toUpperCase();
        }
        else if ("cuotasdeldia".equals(jrf.getName())){
            valor = Integer.toString(cuotas.size());
        }
        else if ("totalcobrar".equals(jrf.getName())){
            valor = Double.toString(totalCobrar);
        }
        else if ("cliente".equals(jrf.getName())){
            prestamoAux = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, Integer.parseInt(cuotas.get(indiceCuotas).getCodPrestamo()));
            cliAux = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(prestamoAux.getCodCliente()));
            valor = Funciones.capitalize(cliAux.getNombre());
        }
        else if ("direccion".equals(jrf.getName())){
            if (Principal.opcionesExtra.getX1().equals("SI")) {
                valor = "No disponible";
            } else {
                prestamoAux = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, Integer.parseInt(cuotas.get(indiceCuotas).getCodPrestamo()));
                cliAux = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(prestamoAux.getCodCliente()));
                valor = Funciones.capitalize(cliAux.getDireccion());
            }
            
        }
        else if ("numcuota".equals(jrf.getName())){
            valor = cuotas.get(indiceCuotas).getNumCuota();
        }
        else if ("fechadepago".equals(jrf.getName())){
            valor = cuotas.get(indiceCuotas).getFechaPagar();
        }
        else if ("fechapagado".equals(jrf.getName())){
            if (cuotas.get(indiceCuotas).getFechaPagado().equals("01/01/2099")) {
                valor = "";
            } else {
                valor = cuotas.get(indiceCuotas).getFechaPagado();
            }
            
        }
        else if ("importe".equals(jrf.getName())){
            valor = "$ " + cuotas.get(indiceCuotas).getImporteTotal();
        }
        else if ("importepagado".equals(jrf.getName())){
            valor = "$ " + cuotas.get(indiceCuotas).getImportePagado();
        }
        else if ("estado".equals(jrf.getName())){
            valor = Funciones.getEstadoCuota(cuotas.get(indiceCuotas));
        }
       
       
       else if ("usuario".equals(jrf.getName())){
            valor = "Usuario";
       } 
       else if ("fechaactual".equals(jrf.getName())){
            valor = Funciones.devolverFechaActualStr() + " " + Funciones.devolverHoraActualStr() ;
       } 
        return valor;
    }
    
}

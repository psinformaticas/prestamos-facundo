package reportes;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import javax.swing.JOptionPane;
import modelo.*;
import controlador.*;
import vista.*;
/**
 *
 * @author Sandra
 */
public class SinRenovarDataSource implements JRDataSource{

    
    private ArrayList<Cliente> clientes = new ArrayList<>();
    private int indiceCli = -1;
    private Cliente cliAux;
    
    

        
    
    
    
    @Override
    public boolean next() throws JRException {
       return ++indiceCli < clientes.size();
    }
            
   

    public void cargarClientes(ArrayList<Cliente> clis){
        this.clientes = clis;
    }
  

    
    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
       Object valor = null;

        if ("nombreempresa".equals(jrf.getName())){
            valor = Funciones.capitalize(Principal.opciones.getNombre());
        }
        else if ("dirempresa".equals(jrf.getName())){
            valor = Funciones.capitalize(Principal.opciones.getDireccion());
        }
        else if ("telempresa".equals(jrf.getName())){
            valor = Principal.opciones.getTelefono().toUpperCase();
        }
       else if ("cliente".equals(jrf.getName())){
            valor = Funciones.capitalize(clientes.get(indiceCli).getNombre());
        }
        else if ("direccion".equals(jrf.getName())){
            valor  =  Funciones.capitalize(clientes.get(indiceCli).getDireccion()+" - "+ Funciones.getLocalidadPorCodInt(clientes.get(indiceCli).getLocalidad()).getNombre());
        }
        else if ("telefono".equals(jrf.getName())){
            valor  =  clientes.get(indiceCli).getCelular()+" - "+ clientes.get(indiceCli).getTelefono();
        }
        else if ("ultimoprestamo".equals(jrf.getName())){
            valor  =  clientes.get(indiceCli).getUltimoPrestamo().getPlan().toUpperCase();
        }
        else if ("fechaultimo".equals(jrf.getName())){
            valor  =  clientes.get(indiceCli).getUltimoPrestamo().getFechaGenerado();
        }
        else if ("debeint".equals(jrf.getName())){
            if (clientes.get(indiceCli).tieneCuotasPagasYDebeInteres()) {
                valor = "SI";
            } else {
                valor = "NO";
            }
        }
       
       
       else if ("usuario".equals(jrf.getName())){
            valor = "Usuario";
       } 
       else if ("fechaactual".equals(jrf.getName())){
            valor = Funciones.devolverFechaActualStr() + " " + Funciones.devolverHoraActualStr() ;
       } 
        return valor;
    }
    
}

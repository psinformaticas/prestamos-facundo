package reportes;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import javax.swing.JOptionPane;
import modelo.*;
import controlador.*;
import vista.*;
/**
 *
 * @author Sandra
 */
public class CuotasCobHistDataSource implements JRDataSource{

    public static Conector con = new Conector();
    
    private ArrayList<Cuota> cuotas = new ArrayList<Cuota>();
    private int indiceCuotas = -1;
    private Prestamo prestamoAux;
    private Cliente cliAux;
    private String totalCobrar;
    private String cobrador;
    private String periodo;
    

        
    
    
    
    @Override
    public boolean next() throws JRException {
       return ++indiceCuotas < cuotas.size();
    }
            
   

    public void cargarCuotas(ArrayList<Cuota> cuots){
        this.cuotas = cuots;
    }
    
    public void cargarTotal(String cob){
        this.totalCobrar = cob;
    }
    
    public void setCobrador(String cob){
        this.cobrador = cob;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }
    
    
    
    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
       Object valor = null;

        if ("nombreempresa".equals(jrf.getName())){
            valor = Funciones.capitalize(Principal.opciones.getNombre());
        }
        else if ("dirempresa".equals(jrf.getName())){
            valor = Funciones.capitalize(Principal.opciones.getDireccion());
        }
        else if ("telempresa".equals(jrf.getName())){
            valor = Principal.opciones.getTelefono().toUpperCase();
        }
        else if ("cuotasdeldia".equals(jrf.getName())){
            valor = Integer.toString(cuotas.size());
        }
        else if ("totalcobrar".equals(jrf.getName())){
            valor = totalCobrar;
        }
        else if ("periodo".equals(jrf.getName())){
            valor = periodo;
        }
        else if ("cliente".equals(jrf.getName())){
            prestamoAux = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, Integer.parseInt(cuotas.get(indiceCuotas).getCodPrestamo()));
            cliAux = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(prestamoAux.getCodCliente()));
            if (Funciones.tieneUnSoloPrestamo(cuotas.get(indiceCuotas))) {
                valor = "* "+Funciones.capitalize(cliAux.getNombre());
            } else {
                valor = Funciones.capitalize(cliAux.getNombre());
            }
            
        }
        else if ("primero".equals(jrf.getName())){
            if (Funciones.tieneUnSoloPrestamo(cuotas.get(indiceCuotas)) && cuotas.get(indiceCuotas).getCliente().getEsClienteNuevo().equalsIgnoreCase("si")) {
                valor = "1";
            } else {
                valor = "0";
            }
        }
        else if ("ultimopago".equals(jrf.getName())){
            prestamoAux = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, Integer.parseInt(cuotas.get(indiceCuotas).getCodPrestamo()));
            cliAux = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(prestamoAux.getCodCliente()));
            valor = Funciones.capitalize(cliAux.getUltimoPagoFecha())+" - $"+cliAux.getUltimoPagoImporte();
        }
        else if ("direccion".equals(jrf.getName())){
            if (Principal.opcionesExtra.getX1().equals("SI")) {
                valor = "No disponible";
            } else {
                prestamoAux = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, Integer.parseInt(cuotas.get(indiceCuotas).getCodPrestamo()));
                cliAux = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(prestamoAux.getCodCliente()));
                Localidad loc = Funciones.getLocalidadPorCodInt(cliAux.getLocalidad());
                String locStr = Funciones.capitalize(loc.getNombre());
                valor = Funciones.capitalize(cliAux.getDireccion()+" - "+locStr);
            }
            
        }
        else if ("telefono".equals(jrf.getName())){
            if (Principal.opcionesExtra.getX3().equals("SI")) {
                valor = "No disponible";
            } else {
                prestamoAux = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, Integer.parseInt(cuotas.get(indiceCuotas).getCodPrestamo()));
                cliAux = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(prestamoAux.getCodCliente()));
                valor = cliAux.getTelefono() + " - " + cliAux.getCelular();
            }
            
        }
        else if ("numcuota".equals(jrf.getName())){
            prestamoAux = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, Integer.parseInt(cuotas.get(indiceCuotas).getCodPrestamo()));
            Plan pn = Funciones.getPlanPorNombre(prestamoAux.getPlan());
            valor = cuotas.get(indiceCuotas).getNumCuota()+"/"+pn.getCantCuotas();
        }
        else if ("fechadepago".equals(jrf.getName())){
            valor = cuotas.get(indiceCuotas).getFechaPagar();
        }
        else if ("fechapagado".equals(jrf.getName())){
            if (cuotas.get(indiceCuotas).getFechaPagado().equals("01/01/2099")) {
                valor = "";
            } else {
                valor = cuotas.get(indiceCuotas).getFechaPagado();
            }
            
        }
        else if ("importe".equals(jrf.getName())){
            valor = "$ " + cuotas.get(indiceCuotas).getImporteTotal();
        }
        else if ("importepagado".equals(jrf.getName())){
            valor = "$ " + cuotas.get(indiceCuotas).getImportePagado();
        }
        else if ("estado".equals(jrf.getName())){
            valor = Funciones.getEstadoCuota(cuotas.get(indiceCuotas));
        }
       else if ("interes".equals(jrf.getName())){
           valor = "$ " + Double.toString(Funciones.formatearDecimales((cuotas.get(indiceCuotas).getInteresCalculado()),2)); 
        }
        else if ("retraso".equals(jrf.getName())){
            valor = "0";
            if (Funciones.getEstadoCuota(cuotas.get(indiceCuotas)).equals("PENDIENTE") || Funciones.getEstadoCuota(cuotas.get(indiceCuotas)).equals("PARCIAL")|| Funciones.getEstadoCuota(cuotas.get(indiceCuotas)).equals("ATRASADA")) {
                valor = (cuotas.get(indiceCuotas).getDiasAtrasoHastaHoy()+"");
            }
            if (Funciones.getEstadoCuota(cuotas.get(indiceCuotas)).equals("PAGADA")) {
                valor = (cuotas.get(indiceCuotas).getDiasAtrasoPago())+"";
            }
        }
       else if ("usuario".equals(jrf.getName())){
            valor = Funciones.capitalize(Principal.usuario);
       } 
       else if ("fechaactual".equals(jrf.getName())){
            valor = Funciones.devolverFechaActualStr() + " " + Funciones.devolverHoraActualStr() ;
       } 
        else if ("cobrador".equals(jrf.getName())){
            valor = Funciones.capitalize(cobrador);
       }
        return valor;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers;


import controlador.Conector;
import controlador.Funciones;
import controlador.Main;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerAct extends SwingWorker<Double, Integer> {
   

    
   
    
    @Override
    protected Double doInBackground() throws Exception {
      Conector con = new Conector();
        Ventana.lbLiq.setText("Actualizando base de datos...");
        Acceso.loadWindow.setAlwaysOnTop(true);
        for (int i=0; i < 5; i++) {

            double porc = ((i+1)*100)/(4);    
            int porcAux = (int) porc;
            
            switch(i) {
            case 0: {
                Ventana.leyenda.setText("GUARDANDO COPIA DE SEGURIDAD");
                try {
                long ms = System.currentTimeMillis();

                Path origenPath = FileSystems.getDefault().getPath("C:\\Prestamos\\base.db");
                Path destinoPath = FileSystems.getDefault().getPath("C:\\Prestamos\\base.bak");
                //sobreescribir el fichero de destino si existe y lo copia
                Files.copy(origenPath, destinoPath, StandardCopyOption.REPLACE_EXISTING);

            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null,"Error al intentar guardar una copia de la BD. \n" + ex.getMessage(),"Error",1);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null,"Error al intentar guardar una copia de la BD. \n" + ex.getMessage(),"Error",1);
            }
                break;                
            }
            case 1: {
                Ventana.leyenda.setText("ACTUALIZANDO PRESTAMOS"); 
                con.conectar();
                con.crearColumnasEnPrestamos();
                con.cerrar();
                break;
            }
            case 2: {
                 Ventana.leyenda.setText("ACTUALIZANDO CUOTAS"); 
                con.conectar();
                con.crearColumnasEnCuotas();
                con.cerrar();
                break;                
            }
            case 3: {
                 Ventana.leyenda.setText("ACTUALIZANDO CLIENTES"); 
                con.conectar();
                con.crearColumnasEnClientes();
                con.cerrar();
                break;                
            }
           

        }
            
            publish(porcAux);

            
            
            }
        
            //javax.swing.JOptionPane.showMessageDialog(null,"Liquidación guardada correctamente.","Guardada",1);
            
            
            

            // Se pasa valor para la barra de progreso. ESto llamara al metodo
            // process() en el hilo de despacho de eventos.
            
        Ventana.lbLiq.setText("Finalizado.");
        Ventana.leyenda.setText("");
        Acceso.loadWindow.dispose();
        Acceso.p.setVisible(true);
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()
    private JProgressBar progreso;

    @Override
    protected void process(List<Integer> chunks) {

        progreso.setValue(chunks.get(0));
    }
    
    
    public void setProgreso(JProgressBar prog) {
        this.progreso = prog;
    }
    
    
}

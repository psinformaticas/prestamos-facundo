/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers;


import controlador.Conector;
import controlador.Funciones;
import controlador.Main;
import java.awt.Color;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cliente;
import modelo.Cuota;
import modelo.Plan;
import modelo.Prestamo;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PatternFormatting;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerPlanillaXLSCuotasFecha extends SwingWorker<Double, Integer> {
   

    boolean todasLasCuotas;
    boolean todasLasFechas;
    
    
    @Override
    protected Double doInBackground() throws Exception {
        
        
      Ventana.lbLiq.setText("Generando planilla...");  
        ArrayList<Cuota> cuotas = new ArrayList<>();
        
        for (Cuota cx1: Principal.todasLasCuotas) {
            if (!todasLasFechas) {
                //SI LA CUOTA NO SE PAGO
                if (todasLasCuotas) {
                    //SI LA FECHA DE PAGO ES ANTERIOR AL DIA ACTUAL
                    if (cx1.getFechaPagarD().before(new Date()) || cx1.getFechaPagarD().equals(new Date()))  {
                        //SI LA CUOTA NO ES ANTICIPO NI INTERES
                        if (Integer.parseInt(cx1.getNumCuota()) > 0 && Integer.parseInt(cx1.getNumCuota()) < 999) {
                            cuotas.add(cx1);
                        }
                    }
                } else {
                    if (!cx1.yaSePago()) {
                        //SI LA FECHA DE PAGO ES ANTERIOR AL DIA ACTUAL
                        if (cx1.getFechaPagarD().before(new Date()) || cx1.getFechaPagarD().equals(new Date()))  {
                            //SI LA CUOTA NO ES ANTICIPO NI INTERES
                            if (Integer.parseInt(cx1.getNumCuota()) > 0 && Integer.parseInt(cx1.getNumCuota()) < 999) {
                                cuotas.add(cx1);
                            }
                        }
                    }    
                }
            } else {
                //SI TODAS LAS FECHAS
                if (!cx1.yaSePago()) {
                    //SI LA CUOTA NO ES ANTICIPO NI INTERES
                    if (Integer.parseInt(cx1.getNumCuota()) > 0 && Integer.parseInt(cx1.getNumCuota()) < 999) {
                        cuotas.add(cx1);
                    }
                }
            }
            
            
        }
        
        Collections.sort(cuotas);
        
//        for (Cuota cx: cuotas) {
//            System.out.println(cx.getFechaPagar());
//        }
//        
        //Datos para crear hoja
        int cantLineas = cuotas.size();
        
        
        /*La ruta donde se creará el archivo*/
        String fecha = Funciones.devolverFechaActualStr().replace("/", "-");
        String rutaArchivo = System.getProperty("user.home")+"/cuotas_"+fecha+".xls";
        /*Se crea el objeto de tipo File con la ruta del archivo*/
        File archivoXLS = new File(rutaArchivo);
        /*Si el archivo existe se elimina*/
        if(archivoXLS.exists()) archivoXLS.delete();
        /*Se crea el archivo*/
        archivoXLS.createNewFile();
        
        /*Se crea el libro de excel usando el objeto de tipo Workbook*/
        Workbook libro = new HSSFWorkbook();
        /*Se inicializa el flujo de datos con el archivo xls*/
        FileOutputStream archivo = new FileOutputStream(archivoXLS);
        
        /*Utilizamos la clase Sheet para crear una nueva hoja de trabajo dentro del libro que creamos anteriormente*/
        Sheet hoja = libro.createSheet("Planilla Cuotas");
        hoja.getPrintSetup().setLandscape(true);
        hoja.getPrintSetup().setPaperSize(HSSFPrintSetup.A4_PAPERSIZE); 
        
        
        CellStyle styleCabecera = libro.createCellStyle();
        Font font = libro.createFont();
        font.setColor(IndexedColors.BLACK.getIndex());
        font.setBold(true);
        font.setFontName("Calibri");
        styleCabecera.setBorderTop(BorderStyle.THIN);
        styleCabecera.setBorderBottom(BorderStyle.THIN);
        styleCabecera.setBorderLeft(BorderStyle.THIN);
        styleCabecera.setBorderRight(BorderStyle.THIN);   
        styleCabecera.setAlignment(HorizontalAlignment.CENTER);
        styleCabecera.setFont(font);
        styleCabecera.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleCabecera.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        
        CellStyle styleNormal = libro.createCellStyle();
        Font font1 = libro.createFont();
        font.setColor(IndexedColors.BLACK.getIndex());
        font.setFontName("Calibri");
        styleNormal.setBorderTop(BorderStyle.THIN);
        styleNormal.setBorderBottom(BorderStyle.THIN);
        styleNormal.setBorderLeft(BorderStyle.THIN);
        styleNormal.setBorderRight(BorderStyle.THIN);   
        styleNormal.setFont(font1);
        styleNormal.setAlignment(HorizontalAlignment.CENTER);
        styleNormal.setVerticalAlignment(VerticalAlignment.CENTER);
        
                
        CellStyle styleMoney = libro.createCellStyle();
        styleMoney.setFont(font1);
        styleMoney.setAlignment(HorizontalAlignment.CENTER);
        styleMoney.setVerticalAlignment(VerticalAlignment.CENTER);
        styleMoney.setBorderTop(BorderStyle.THIN);
        styleMoney.setBorderBottom(BorderStyle.THIN);
        styleMoney.setBorderLeft(BorderStyle.THIN);
        styleMoney.setBorderRight(BorderStyle.THIN);  
        styleMoney.setDataFormat((short)8); //8 = "($#,##0.00_);[Red]($#,##0.00)"
        /*Hacemos un ciclo para inicializar los valores de 10 filas de celdas*/
        
        hoja.setColumnWidth(0, 1700);
        hoja.setColumnWidth(1, 4500);
        hoja.setColumnWidth(2, 5500);
        hoja.setColumnWidth(3, 3000);
        hoja.setColumnWidth(4, 3300);
        hoja.setColumnWidth(5, 3300);
        hoja.setColumnWidth(6, 3300);
        hoja.setColumnWidth(7, 3300);
        hoja.setColumnWidth(8, 3300);
        hoja.setColumnWidth(9, 3300);
        hoja.setColumnWidth(10, 3300);
        hoja.setColumnWidth(11, 3300);
        hoja.setColumnWidth(12, 3300);
        hoja.setColumnWidth(13, 3300);
        hoja.setColumnWidth(14, 3300);
        hoja.setColumnWidth(15, 3300);
        hoja.setColumnWidth(16, 3300);
       
        for(int f=0;f<cantLineas+1;f++){
            double porc = ((f+1)*100)/(cantLineas+1);    
            int porcAux = (int) porc;
            
            /*La clase Row nos permitirá crear las filas*/
            Row fila = hoja.createRow(f);
            if (f>0) {
                fila.setHeight((short)400);
            }
            

            /*Cada fila tendrá 5 celdas de datos*/
            for(int c=0;c<17;c++){
                /*Creamos la celda a partir de la fila actual*/
                Cell celda = fila.createCell(c);
                
                /*Si la fila es la número 0, estableceremos los encabezados*/
                if(f==0){
                    celda.setCellStyle(styleCabecera);
                    if (c==0) {
                        celda.setCellValue("ID");
                    }
                    if (c==1) {
                        celda.setCellValue("Cobrador");
                    }
                    if (c==2) {
                        celda.setCellValue("Nombre");
                    }
                    if (c==3) {
                        celda.setCellValue("Cuota");
                    }
                    if (c==4) {
                        celda.setCellValue("Monto");
                    }
                    if (c==5) {
                        celda.setCellValue("Compromiso");
                    }
                    if (c==6) {
                        celda.setCellValue("Últ. Pago");
                    }
                    if (c==7) {
                        celda.setCellValue("Fecha de pago");
                    }
                    if (c==8) {
                        celda.setCellValue("S");
                    }
                    if (c==9) {
                        celda.setCellValue("R. x Día");
                    }
                    if (c==10) {
                        celda.setCellValue("R. Total");
                    }
                    if (c==11) {
                        celda.setCellValue("Zona");
                    }
                    if (c==12) {
                        celda.setCellValue("Dirección");
                    }
                    if (c==13) {
                        celda.setCellValue("M. de Pago");
                    }
                    if (c==14) {
                        celda.setCellValue("F. entrega");
                    }
                    if (c==15) {
                        celda.setCellValue("Teléfono");
                    }
                    if (c==16) {
                        celda.setCellValue("Valor Créd.");
                    }
                    
                   
                    
                }else{
                    celda.setCellStyle(styleNormal);
                    //Carga vehiculo en el ciclo
                    Cuota cuota = cuotas.get(f-1);
                    Cliente cli = cuota.getCliente();
                    Plan plan = Funciones.getPlanPorNombre(cuota.getNombrePlan());
                    Prestamo prestamo = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, cuota.getCodPrestamoInt());
                    if (c==0) {
                        celda.setCellValue(cuota.getCod());
                    }
                    if (c==1) {
                        celda.setCellValue(Funciones.capitalize(cuota.getCobrador().getNombre()));
                    }
                    if (c==2) {
                        celda.setCellValue(Funciones.capitalize(cuota.getCliente().getNombre()));
                    }
                    if (c==3) {
                        celda.setCellValue(cuota.getNumCuota()+" DE "+plan.getCantCuotas());
                    }
                    if (c==4) {
                        celda.setCellStyle(styleMoney);
                        celda.setCellValue(cuota.getImporteAdeudadoD());
                    }
                    if (c==5) {
                        celda.setCellValue(cuota.getFechaPagar());
                    }
                    if (c==6) {
                        if (cli.getUltimoPagoFecha().equalsIgnoreCase("01/01/2099")) {
                            celda.setCellValue("No registrado");
                        } else {
                            celda.setCellValue(cli.getUltimoPagoFecha()+" - $"+cli.getUltimoPagoImporte());
                        }
                    }
                    if (c==7) {
                        celda.setCellValue(Funciones.devolverFechaActualStr());
                    }
                    if (c==8) {
                        Date fechaPagoAux = Funciones.stringADate(cuota.getFechaPagar());
                        Date fechaHoyAux = new Date();
                        int diasDif = Funciones.calcularDiasDeDiferencia(Funciones.dateAString(fechaHoyAux), Funciones.dateAString(fechaPagoAux));
                        if (diasDif<0) {
                            diasDif = 0;
                        }
                        celda.setCellValue(diasDif);
                    }
                    if (c==9) {
                        celda.setCellStyle(styleMoney);
                        double cuotaTotalAux = cuota.getImporteTotalD();
                        double porcIntDia = Principal.opciones.getInteresdia();
                        double valorIntDia = (cuotaTotalAux * porcIntDia) / 100;

                        
                        celda.setCellValue(Funciones.formatearDecimales(valorIntDia,2));
                    }
                    if (c==10) {
                        celda.setCellStyle(styleMoney);
                        double cuotaTotalAux = cuota.getImporteTotalD();
                        double porcIntDia = Principal.opciones.getInteresdia();
                        double valorIntDia = (cuotaTotalAux * porcIntDia) / 100;

                        Date fechaPagoAux = Funciones.stringADate(cuota.getFechaPagar());
                        Date fechaHoyAux = new Date();
                        int diasDif = Funciones.calcularDiasDeDiferencia(Funciones.dateAString(fechaHoyAux), Funciones.dateAString(fechaPagoAux));
                        if (diasDif<0) {
                            diasDif = 0;
                        }
                        double totalIntAux = valorIntDia * diasDif;

                        celda.setCellValue(Funciones.formatearDecimales(totalIntAux,2));
                    }
                    if (c==11) {
                        celda.setCellValue(Funciones.capitalize(Funciones.getLocalidadPorCodInt(cli.getLocalidad()).getNombre()));
                    }
                    if (c==12) {
                        celda.setCellValue(Funciones.capitalize(cli.getDireccion()));
                    }
                    if (c==13) {
                        String tipo = "";
                        if (plan.getTipo().equalsIgnoreCase("semanal")) {
                            tipo = "S";
                        } else if (plan.getTipo().equalsIgnoreCase("mensual")) {
                            tipo = "M";
                        } else if (plan.getTipo().contains("p-")) {
                            String [] aux = plan.getTipo().split("-");
                            
                            int cantDias = Integer.parseInt(aux[1]);
                            if (cantDias == 14 || cantDias == 15) {
                                tipo = "Q";
                            } else {
                                tipo = aux[1] + " días";
                            }
                            
                        }
                        celda.setCellValue(tipo);
                    }
                    if (c==14) {
                        celda.setCellValue(prestamo.getFechaGenerado());
                    }
                    if (c==15) {
                        celda.setCellValue(cli.getTelefono());
                    }
                    if (c==16) {
                        celda.setCellStyle(styleMoney);
                        celda.setCellValue(Funciones.formatearDecimales(Double.parseDouble(plan.getImportePrestar()),2));
                    }
                    /*Si no es la primera fila establecemos un valor*/
//                    hoja.autoSizeColumn(c);
                }
            }
                publish(porcAux);
                
        }
        
       
       
        
            
            

            
        Ventana.lbLiq.setText("Finalizado.");
        Ventana.leyenda.setText("");
        Principal.loadWindow.dispose();
        Acceso.p.setVisible(true);
        
        /*Escribimos en el libro*/
        libro.write(archivo);
        /*Cerramos el flujo de datos*/
        archivo.close();
        /*Y abrimos el archivo con la clase Desktop*/
        Desktop.getDesktop().open(archivoXLS);
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()
    private JProgressBar progreso;

    @Override
    protected void process(List<Integer> chunks) {
        progreso.setValue(chunks.get(0));
    }
    
    
    public void setProgreso(JProgressBar prog) {
        this.progreso = prog;
    }
    
    public void setTodasLasCuotas(boolean booleano) {
        this.todasLasCuotas = booleano;
    }
    
    public void setTodasLasFechas(boolean booleano) {
        this.todasLasFechas = booleano;
    }
    
}

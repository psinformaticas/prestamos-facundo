/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers;


import controlador.Conector;
import controlador.Funciones;
import controlador.Main;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerRefresh extends SwingWorker<Double, Integer> {
   

    
    
    
    @Override
    protected Double doInBackground() throws Exception {
      
        Principal.panelCarga.setBackground(new Color(255,102,0));
        Principal.lbCargando1.setText("CARGANDO ...");
        Principal.lbCargando3.setText("Espere por favor...");
        long inicio = System.currentTimeMillis();
    
        for (int i=0; i < 7; i++) {

            double porc = ((i+1)*100)/(6);    
            int porcAux = (int) porc;
            
            switch(i) {
            
            
            case 0: {
                Principal.lbCargando2.setText("USUARIOS");
                Principal.cargarUsuarios();
                break;                
            }
            case 1: {
                Principal.lbCargando2.setText("RECALCULANDO VALORES");
                Principal.recalcularValorPlan();
                break;                
            }
            case 2: {
                Principal.lbCargando2.setText("COBRADORES");
                Principal.cargarCobradores();
                break;                
            }
            case 3: {
                Principal.lbCargando2.setText("CLIENTES");
                Principal.cargarClientes();
                break;                
            }
            case 4: {
                Principal.lbCargando2.setText("PLANES");
                Principal.cargarPlanes();
                break;                
            }
            case 5: {
                Principal.lbCargando2.setText("PRÉSTAMOS Y CUOTAS");
                Principal.cargarPrestamosYCuotas();
                break;                
            }
            case 6: {
                Principal.lbCargando2.setText("CLIENTES Y PRÉSTAMOS");
                Principal.cargarClientesPrestamos();
                break;                
            }
            
           

        }
            

            
            
            }
        
            //javax.swing.JOptionPane.showMessageDialog(null,"Liquidación guardada correctamente.","Guardada",1);
            
            
            

            // Se pasa valor para la barra de progreso. ESto llamara al metodo
            // process() en el hilo de despacho de eventos.
            
        Principal.lbCargando1.setText("CARGA");
        Principal.lbCargando2.setText("FINALIZADA");
        
        Principal.panelCarga.setBackground(new Color(0,153,0));
        
        long fin = System.currentTimeMillis();
        
        double tiempo = (double) ((fin - inicio)/1000);
        
        Principal.lbCargando3.setText(tiempo +" segundos");
        
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()

    @Override
    protected void process(List<Integer> chunks) {

    }
    
    
    public void setProgreso(JProgressBar prog) {
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers;


import controlador.Conector;
import controlador.Funciones;
import controlador.Main;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;
import static vista.Principal.cargarClientesPrestamos;


/**
 *
 * @author Will
 */
public class WorkerCarga extends SwingWorker<Double, Integer> {
   

    
    
    
    @Override
    protected Double doInBackground() throws Exception {
      
        Principal.panelCarga.setBackground(new Color(255,102,0));
        Principal.lbCargando1.setText("CARGANDO ...");
        Principal.lbCargando3.setText("Espere por favor...");
        long inicio = System.currentTimeMillis();
        
        
        //Ventana.lbLiq.setText("Cargando...");
        for (int i=0; i < 13; i++) {

            double porc = ((i+1)*100)/(13);    
            int porcAux = (int) porc;
            
            switch(i) {
            case 0: {
                //Ventana.leyenda.setText("OPCIONES Y TABLAS");
                Principal.lbCargando2.setText("OPCIONES Y TABLAS");
                Principal.cargarOpciones();
                Principal.cargarOpcionesExtra();
                Principal.cargarTableModels();
                break;                
            }
            case 1: {
                Principal.lbCargando2.setText("LICENCIA");
                Principal.comprobarActivado();
                break;
            }
            case 2: {
                Principal.lbCargando2.setText("PERMISOS");
                Principal.comprobarPermisos();
                break;                
            }
            case 3: {
                Principal.lbCargando2.setText("VALORES INICIALES");
                Principal.cargarValoresIniciales();
                break;                
            }
            case 4: {
                Principal.lbCargando2.setText("USUARIOS");
                Principal.cargarUsuarios();
                break;          
                
            }
            case 5: {
                Principal.lbCargando2.setText("COBRADORES");
                Principal.cargarCobradores();
                break;                
            }
            case 6: {
                Principal.lbCargando2.setText("RECALCULANDO VALORES");
                Principal.recalcularValorPlan();
                break;                
            }
            case 7: {
                if (Principal.opcionesExtra.getMarcaFinalizados().equals("SI")) {
                    Principal.lbCargando2.setText("FINALIZANDO");
                    Conector c = new Conector();

                    c.conectar();
                    ArrayList<Prestamo> pSinFinalizar= c.getPrestamosSinFinalizar();
                    c.cerrar();

                    for (Prestamo p: pSinFinalizar) {
                        ArrayList<Cuota> cs = p.getCuotas();
                        boolean termino = true;
                        for (Cuota cx: cs) {
                            termino = termino && cx.yaSePago();
                        }
                        if (termino) {
                            //System.out.println("PRESTAMO FINALIZADO / ACTUALIZANDO...");
                            c.conectar();
                            c.marcarPrestamoFinalizado(p);
                            c.cerrar();
                            for (Cuota cx: cs) {
                                c.conectar();
                                c.marcarCuotaFinalizada(cx);
                                c.cerrar();
                            }
                        }
                    }

                } else {
                    //Ventana.leyenda.setText("FUNCIÓN DESHABILITADA");
                }
                
                
                break;                
            }
            case 8: {
                Principal.lbCargando2.setText("CLIENTES");
                Principal.cargarClientes();
                cargarClientesPrestamos();
                break;                
            }
            case 9: {
                Principal.lbCargando2.setText("PLANES");
                Principal.cargarPlanes();
                break;                
            }
            case 10: {
                Principal.lbCargando2.setText("PRÉSTAMOS Y CUOTAS");
                Principal.cargarPrestamosYCuotas();
                System.out.println("LLEGAMOS");
                break;                
            }
            case 11: {
                if (true) {
                    
                Principal.lbCargando2.setText("USUARIOS APP");
                Principal.cargarUsuariosApp();
                break;                
                }
            }
            

        }
            
           // publish(porcAux);

            
            
            }
        
            //javax.swing.JOptionPane.showMessageDialog(null,"Liquidación guardada correctamente.","Guardada",1);
            
            
            

            // Se pasa valor para la barra de progreso. ESto llamara al metodo
            // process() en el hilo de despacho de eventos.
            
        //Ventana.lbLiq.setText("Finalizado.");
        //Ventana.leyenda.setText("");
        //Principal.loadWindow.dispose();
        //Acceso.p.setVisible(true);
        // Supuesto resultado de la tarea que tarda mucho.
        Principal.lbCargando1.setText("CARGA");
        Principal.lbCargando2.setText("FINALIZADA");
        
        Principal.panelCarga.setBackground(new Color(0,153,0));
        
        long fin = System.currentTimeMillis();
        
        double tiempo = (double) ((fin - inicio)/1000);
        
        Principal.lbCargando3.setText(tiempo +" segundos");

        
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()
    //private JProgressBar progreso;

    @Override
    protected void process(List<Integer> chunks) {

//        progreso.setValue(chunks.get(0));
    }
    
    
    public void setProgreso(JProgressBar prog) {
       // this.progreso = prog;
    }
    
    
}

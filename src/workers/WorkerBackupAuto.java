/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers;


import controlador.Conector;
import static controlador.FuncionesArchivos.copiarCarpeta;
import controlador.FuncionesMySQL;
import controlador.Reloj;
import java.io.File;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import vista.Acceso;
import vista.Principal;
import static vista.Principal.opcionesExtra;


/**
 *
 * @author Will
 */
public class WorkerBackupAuto extends SwingWorker<Double, Integer> {
   

    
    
    
    @Override
    protected Double doInBackground() throws Exception {
      
        Ventana.lbLiq.setText("Realizando backup automático...");
        for (int i=0; i < 1; i++) {

            double porc = 50;    
            int porcAux = (int) porc;
            
         
            try {
                FuncionesMySQL.backup();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Falló copia de seguridad local.\n\n"+e.toString(), "INFO", 0);
            }
            
            try {
                copiarCarpeta(new File("C:\\prestav2\\backup\\"), new File(opcionesExtra.getX6()));
                copiarCarpeta(new File("C:\\prestav2\\res\\fotos"), new File(opcionesExtra.getX6()+"\\fotos"));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Falló copia de seguridad hacia carpeta Drive.\n\n"+e.toString(), "INFO", 0);
            }
            
            

      
            publish(porcAux);

            
            
            }
        
            //javax.swing.JOptionPane.showMessageDialog(null,"Liquidación guardada correctamente.","Guardada",1);
            
            
            

            // Se pasa valor para la barra de progreso. ESto llamara al metodo
            // process() en el hilo de despacho de eventos.
            
        Ventana.lbLiq.setText("Finalizado.");
        Reloj.haciendoBkp = false;
        Ventana.leyenda.setText("");
        Principal.loadWindow.dispose();
        Acceso.p.setVisible(true);
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()
    private JProgressBar progreso;

    @Override
    protected void process(List<Integer> chunks) {

        progreso.setValue(chunks.get(0));
    }
    
    
    public void setProgreso(JProgressBar prog) {
        this.progreso = prog;
    }
    
    
}

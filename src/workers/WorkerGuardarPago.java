/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers;


import aplicacion.WorkersApp;
import controlador.Conector;
import controlador.Funciones;
import controlador.Main;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cliente;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Acceso;
import vista.GenerarComprobante;
import vista.ListadoPorCob;
import vista.ListadoPorLocalidad;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerGuardarPago extends SwingWorker<Double, Integer> {
   

    
    private Cliente cliente;
    private ArrayList<Cuota> csAux;
    private Cuota cuota;
    private String fechaPago;
    private String impPagar;
    private String cobradorSel;
    private Prestamo prestamo;
    
    public void setCliente(Cliente c) {
        cliente = c;
    }
    
    public void setCobradorSel(String cob) {
        cobradorSel = cob;
    }
    
    public void setCuota(Cuota c) {
        cuota = c;
    }
    
    public void setFPago(String fp) {
        fechaPago = fp;
    }
    
    public void setImpPagar(String imp) {
        impPagar = imp;
    }
    
    public void setPrestamo(Prestamo p) {
        prestamo = p;
    }
    
    
    
    @Override
    protected Double doInBackground() throws Exception {
      
        Principal.panelCarga.setBackground(new Color(255,102,0));
        Principal.lbCargando1.setText("REGISTRANDO PAGO ...");
        Principal.lbCargando2.setText(Funciones.capitalize(cliente.getNombre()));
        Principal.lbCargando3.setText("Espere por favor.");
        long inicio = System.currentTimeMillis();
        
        
    
        
        
  
       // import static vista.GuardarPago.blanqueaListaCuotas;
        
        Conector con = new Conector();
        //COMPRUEBA IMPORTE
        con.conectar();
        csAux = con.getCuotasPorCodPrestamo(Integer.toString(prestamo.getCod()));
        con.cerrar();
        
        //IMPORTE ADEUDADO TOTAL SI ES CUOTA COMUN
        double importeTotalAdeudado = 0.0;
        for (Cuota cuota1: csAux) {
            if (!cuota1.getNumCuota().equals("999")) {
             importeTotalAdeudado += cuota1.getImporteAdeudadoD();
            }
        }
        
        //IMPORTE ADEUDADO
        if (cuota.getNumCuota().equals("999")) {
            importeTotalAdeudado = cuota.getImporteAdeudadoD();
        }
        
        if (Double.parseDouble(impPagar)>importeTotalAdeudado) {
            if (cuota.getNumCuota().equals("999")) {
                JOptionPane.showMessageDialog(null,"El importe ingresado supera el monto de interés adeudado en el préstamo.\n\nEl monto máximo a pagar por interés es: $"+importeTotalAdeudado,"**** INFO ****",1);
            } else {
                JOptionPane.showMessageDialog(null,"El importe ingresado supera el monto adeudado en el préstamo.\n\nEl monto máximo a pagar por las cuotas es: $"+importeTotalAdeudado,"**** INFO ****",1);    
            }
            
        } else {
            
        
        
        //DEFINE SI SE COBRA INT
        boolean seCobraInteres;
        if (Principal.opcionesExtra.getX9().equals("SI")) {
            seCobraInteres = true;
        } else {
            seCobraInteres = false;
        }
         
        //DEFINE RUTA COBRADOR
            String ruta = "xxx";
            if (!cobradorSel.equals("XXX - Sin especificar")) {
                    String[] arr1 = cobradorSel.split("-");
                    arr1[0] = arr1[0].replaceAll(" ", "");

                    ruta = arr1[0];
            }
        
        con.conectar();
        if (!cuota.getNumCuota().equals("999")) {
            //CARGA CUOTAS DEL PRESTAMO A ARREGLO AUXILIAR
            csAux = con.getCuotasPorCodPrestamo(Integer.toString(prestamo.getCod()));
            double dineroSobrante = Double.parseDouble(impPagar);
            double restanteAux = 0.0;
            int cuotaSel = Integer.parseInt(cuota.getNumCuota());
            int cantCuotasQueSePagaran = 0;

            for (Cuota cuotaCiclo: csAux) {
                int numCuota = Integer.parseInt(cuotaCiclo.getNumCuota());
                //SI LA CUOTA NO ES ANTICIPO, NI CUOTA INT Y ES MAYOR O IGUAL A LA CUOTA SEL Y NO SE PAGO
                if (numCuota > 0 && numCuota >= cuotaSel && numCuota != 999 && !cuotaCiclo.yaSePago()) {
                    if (dineroSobrante>0) {
                        restanteAux = cuotaCiclo.realizaPagoCuotaYDevuelveResto(ruta, Double.toString(dineroSobrante), seCobraInteres,fechaPago);
                        con.actualizarCuota(cuotaCiclo);
                        dineroSobrante = restanteAux;
                        cantCuotasQueSePagaran++;
                    }
                }
            }
        } else {
            cuota.realizaPagoCuotaYDevuelveResto(ruta, impPagar.replace(",","."), seCobraInteres,fechaPago);
            con.actualizarCuota(cuota);
        }
        
        con.actualizarUltimoPago(cliente, fechaPago+"-"+Funciones.devolverHoraActualStr()+"-"+impPagar);
        con.guardarPagoParcial(Principal.usuario, prestamo, Double.parseDouble(impPagar), Funciones.stringADate(fechaPago), cuota.getCod());
        con.cerrar();
            Principal.lbCargando1.setText("CARGANDO DATOS ...");
            Principal.cargarPrestamosYCuotas();
            Principal.cargarClientes();

//            blanqueaListaCuotas();

            if (Principal.opcionesExtra.getImpresion().equals("SI")) {
                int dialogResult = JOptionPane.showConfirmDialog(null, "¿Desea imprimir un comprobante de la operaciónn?", "¡¡¡ATENCIÓN!!!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if(dialogResult == JOptionPane.YES_OPTION){
                        GenerarComprobante gen = new GenerarComprobante();
                        gen.setTitle("Seleccione tipo de comprobante");
                        gen.setVisible(true);
                } else {
//                    ImpresionTicket.imprimirTicketPago(prestamo, GuardarPago.cuota, GuardarPago.totalIntPorDia.getText(), GuardarPago.impPagar.getText(), GuardarPago.retrasoPago.getText(), false);
                }
            } else {
//                ImpresionTicket.imprimirTicketPago(prestamo, GuardarPago.cuota, GuardarPago.totalIntPorDia.getText(), GuardarPago.impPagar.getText(), GuardarPago.retrasoPago.getText(), false);
            }

            if (Principal.lCobrador != null) {//si existe una venta, la cierra.
                ListadoPorCob.cargarLista();
            }
            if (Principal.lLocalidades != null) {//si existe una venta, la cierra.
                ListadoPorLocalidad.cargarLista();
            }

        }
        
        Workers.workerRefresh();
        //GUARDA EN LA APP
        WorkersApp.guardarPrestamo(prestamo);
        
        
        
        
        
        
        
        
        
//        Principal.lbCargando1.setText("PAGO REGISTRADO");
//        Principal.lbCargando2.setText(Funciones.capitalize(cliente.getNombre()));
//        Principal.lbCargando3.setText("Operación finalizada.");
        
        Principal.panelCarga.setBackground(new Color(0,153,0));
        
        long fin = System.currentTimeMillis();
        
        double tiempo = (double) ((fin - inicio)/1000);
        
//        Principal.lbCargando3.setText(tiempo +" segundos");

        
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()
    //private JProgressBar progreso;

    @Override
    protected void process(List<Integer> chunks) {

//        progreso.setValue(chunks.get(0));
    }
    
    
    public void setProgreso(JProgressBar prog) {
       // this.progreso = prog;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers;


import controlador.Conector;
import controlador.ConectorApp;
import controlador.Funciones;
import controlador.Main;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerSincronizarCuotas extends SwingWorker<Double, Integer> {
   

    
   
    
    
    @Override
    protected Double doInBackground() throws Exception {
        Ventana.lbLiq.setText("Sincronizando cuotas...");
        Conector con = new Conector();
        con.conectar();
        ArrayList<Cuota> cuotasOff = con.getCuotas();
        con.cerrar();
        
//        Principal.loadWindow.setAlwaysOnTop(true);
        ConectorApp conApp = new ConectorApp();
        conApp.conectar();
        
        for (int i=0; i < cuotasOff.size(); i++) {
            Ventana.leyenda.setText("Sincronizando cuota "+Integer.toString(i+1)+" de "+Integer.toString(cuotasOff.size()));
            System.out.println("Sincronizando cuota "+Integer.toString(i+1)+" de "+Integer.toString(cuotasOff.size()));
            double porc = ((i+1)*100)/(cuotasOff.size());    
            int porcAux = (int) porc;
            conApp.actualizarCuota(cuotasOff.get(i));
            publish(porcAux);
        }
        conApp.cerrar();
        
            javax.swing.JOptionPane.showMessageDialog(null,"Sincronización finalizada.","OK",1);
            
            
            

            // Se pasa valor para la barra de progreso. ESto llamara al metodo
            // process() en el hilo de despacho de eventos.
            
        Ventana.lbLiq.setText("Finalizado.");
        Ventana.leyenda.setText("");
        Principal.loadWindow.dispose();
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()
    private JProgressBar progreso;

    @Override
    protected void process(List<Integer> chunks) {

        progreso.setValue(chunks.get(0));
    }
    
    
    public void setProgreso(JProgressBar prog) {
        this.progreso = prog;
    }
    
    
}

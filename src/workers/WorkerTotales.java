/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers;


import controlador.Estadisticas;
import controlador.Funciones;
import controlador.Main;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cuota;
import vista.Acceso;
import vista.Principal;
import vista.Totales;


/**
 *
 * @author Will
 */
public class WorkerTotales extends SwingWorker<Double, Integer> {
   

    
    
    
    @Override
    protected Double doInBackground() throws Exception {
      
        ArrayList<Cuota> cuotasAux = new ArrayList<>();
        Ventana.lbLiq.setText("Cargando...");
        for (int i=0; i < 7; i++) {

            double porc = ((i+1)*100)/(9);    
            int porcAux = (int) porc;
            
            switch(i) {
            case 0: {
                Ventana.leyenda.setText("CUOTAS");
                cuotasAux = Estadisticas.getCuotasEntreFechas(Principal.inicioTotal.getDate(), Principal.finTotal.getDate());
                Totales.cuotas = cuotasAux;
                break;                
            }
            case 1: {
                Ventana.leyenda.setText("ADEUDADO");
                Totales.adeudado.setText("$"+Double.toString(Funciones.formatearDecimales(Estadisticas.getImporteAdeudadoEntreFechas(Principal.inicioTotal.getDate(), Principal.finTotal.getDate()),2)));
                break;
            }
            case 2: {
                Ventana.leyenda.setText("PRESTADO");
                Totales.prestado.setText("$"+Double.toString(Funciones.formatearDecimales(Estadisticas.getImportePrestadoEntreFechas(Principal.inicioTotal.getDate(), Principal.finTotal.getDate()),2)));
                break;                
            }
            case 3: {
                Ventana.leyenda.setText("COBRADO");
                Totales.cobrado.setText("$"+Double.toString(Funciones.formatearDecimales(Estadisticas.getImporteCobradoEntreFechas(Principal.inicioTotal.getDate(), Principal.finTotal.getDate()),2)));
                break;                
            }
            case 4: {
                Ventana.leyenda.setText("PERÍODO");
                Totales.lbPeriodo.setText(Funciones.dateAString(Principal.inicioTotal.getDate()) +" al "+ Funciones.dateAString(Principal.finTotal.getDate()));
                break;                
            }
          
            case 5: {
                Ventana.leyenda.setText("CANT CUOTAS PAGAS");
                Totales.lbCuotas.setText(Integer.toString(cuotasAux.size()));
                break;                
            }
           case 6: {
                Ventana.leyenda.setText("IMPORTE INTERËS");
                Totales.lbInt.setText("$"+Double.toString(Funciones.formatearDecimales(Estadisticas.getImporteInteresEntreFechas(Principal.inicioTotal.getDate(), Principal.finTotal.getDate()),2)));
                break;                
            }

        }
            
            publish(porcAux);

            
            
            }
        
            //javax.swing.JOptionPane.showMessageDialog(null,"Liquidación guardada correctamente.","Guardada",1);
            
            
            

            // Se pasa valor para la barra de progreso. ESto llamara al metodo
            // process() en el hilo de despacho de eventos.
            
        Ventana.lbLiq.setText("Finalizado.");
        Ventana.leyenda.setText("");
        Totales.loading.dispose();
        Principal.totales.setVisible(true);
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()
    private JProgressBar progreso;

    @Override
    protected void process(List<Integer> chunks) {

        progreso.setValue(chunks.get(0));
    }
    
    
    public void setProgreso(JProgressBar prog) {
        this.progreso = prog;
    }
    
    
}

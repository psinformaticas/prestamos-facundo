/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers;


import controlador.Conector;
import controlador.Funciones;
import controlador.Main;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerCarga_Old extends SwingWorker<Double, Integer> {
   

    
    
    
    @Override
    protected Double doInBackground() throws Exception {
      
        Ventana.lbLiq.setText("Cargando...");
        for (int i=0; i < 13; i++) {

            double porc = ((i+1)*100)/(13);    
            int porcAux = (int) porc;
            
            switch(i) {
            case 0: {
                Ventana.leyenda.setText("OPCIONES");
                Principal.cargarOpciones();
                Principal.cargarOpcionesExtra();
                break;                
            }
            case 1: {
                Ventana.leyenda.setText("LICENCIA");
                Principal.comprobarActivado();
                break;
            }
            case 2: {
                Ventana.leyenda.setText("PERMISOS");
                Principal.comprobarPermisos();
                break;                
            }
            case 3: {
                Ventana.leyenda.setText("VALORES INICIALES");
                Principal.cargarValoresIniciales();
                break;                
            }
            case 4: {
                Ventana.leyenda.setText("USUARIOS");
                Principal.cargarUsuarios();
                break;          
                
            }
            case 5: {
                Ventana.leyenda.setText("COBRADORES");
                Principal.cargarCobradores();
                break;                
            }
            case 6: {
                Ventana.leyenda.setText("RECALCULANDO VALORES");
                Principal.recalcularValorPlan();
                break;                
            }
            case 7: {
                if (Principal.opcionesExtra.getMarcaFinalizados().equals("SI")) {
                    Ventana.leyenda.setText("FINALIZANDO PRÉSTAMOS PAGADOS");
                    Conector c = new Conector();

                    c.conectar();
                    ArrayList<Prestamo> pSinFinalizar= c.getPrestamosSinFinalizar();
                    c.cerrar();

                    for (Prestamo p: pSinFinalizar) {
                        ArrayList<Cuota> cs = p.getCuotas();
                        boolean termino = true;
                        for (Cuota cx: cs) {
                            termino = termino && cx.yaSePago();
                        }
                        if (termino) {
                            System.out.println("PRESTAMO FINALIZADO / ACTUALIZANDO...");
                            c.conectar();
                            c.marcarPrestamoFinalizado(p);
                            c.cerrar();
                            for (Cuota cx: cs) {
                                c.conectar();
                                c.marcarCuotaFinalizada(cx);
                                c.cerrar();
                            }
                        }
                    }

                } else {
                    Ventana.leyenda.setText("FUNCIÓN DESHABILITADA");
                }
                
                
                break;                
            }
            case 8: {
                Ventana.leyenda.setText("CLIENTES");
                Principal.cargarClientes();
                break;                
            }
            case 9: {
                Ventana.leyenda.setText("PLANES");
                Principal.cargarPlanes();
                break;                
            }
            case 10: {
                Ventana.leyenda.setText("PRÉSTAMOS Y CUOTAS");
                Principal.cargarPrestamosYCuotas();
                break;                
            }
            case 11: {
                if (true) {
                    
                Ventana.leyenda.setText("USUARIOS APP");
                Principal.cargarUsuariosApp();
                break;                
                }
            }
            

        }
            
            publish(porcAux);

            
            
            }
        
            //javax.swing.JOptionPane.showMessageDialog(null,"Liquidación guardada correctamente.","Guardada",1);
            
            
            

            // Se pasa valor para la barra de progreso. ESto llamara al metodo
            // process() en el hilo de despacho de eventos.
            
        Ventana.lbLiq.setText("Finalizado.");
        Ventana.leyenda.setText("");
        Principal.loadWindow.dispose();
        Acceso.p.setVisible(true);
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()
    private JProgressBar progreso;

    @Override
    protected void process(List<Integer> chunks) {

        progreso.setValue(chunks.get(0));
    }
    
    
    public void setProgreso(JProgressBar prog) {
        this.progreso = prog;
    }
    
    
}

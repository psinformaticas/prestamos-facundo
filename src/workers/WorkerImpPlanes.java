/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers;


import controlador.Conector;
import controlador.Funciones;
import controlador.Main;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cuota;
import modelo.Plan;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerImpPlanes extends SwingWorker<Double, Integer> {
   

    
    private ArrayList<Plan> ps; 
    
    @Override
    protected Double doInBackground() throws Exception {
      
        Ventana.lbLiq.setText("Importando");
        Principal.con.conectar();
        for (int i=0; i < ps.size(); i++) {

            double porc = ((i+1)*100)/(ps.size());    
            int porcAux = (int) porc;
          
            Ventana.leyenda.setText("PLAN "+Integer.toString(i+1)+" DE "+ Integer.toString(ps.size()));
            ArrayList<Plan> psAux = Principal.con.getPlanes();
            boolean existe = false;
            for (Plan p: psAux) {
                if (p.getNombre().toLowerCase().equals(ps.get(i).getNombre().toLowerCase())) {
                    existe = true;
                }
            }
            if (!existe) {
                Principal.con.guardarPlan(ps.get(i));
            } else {
                JOptionPane.showMessageDialog(null, "El plan "+ps.get(i).getNombre().toUpperCase()+" ya se encuentra en la base de datos.", "Info",1);
            }
            

        
            publish(porcAux);

            
            
        }
        Principal.con.cerrar();
            //javax.swing.JOptionPane.showMessageDialog(null,"Liquidación guardada correctamente.","Guardada",1);
            
            
            

            // Se pasa valor para la barra de progreso. ESto llamara al metodo
            // process() en el hilo de despacho de eventos.
        
        Ventana.lbLiq.setText("Finalizado.");
        Ventana.leyenda.setText("");
        Principal.loadWindow.dispose();
        JOptionPane.showMessageDialog(null, "Actualice el listado para ver los nuevos planes importados.", "Info",1);
        
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()
    private JProgressBar progreso;

    @Override
    protected void process(List<Integer> chunks) {

        progreso.setValue(chunks.get(0));
    }
    
    
    public void setProgreso(JProgressBar prog) {
        this.progreso = prog;
    }
   
     public void setPlanes(ArrayList<Plan> planes) {
        this.ps = planes;
    }
    
}

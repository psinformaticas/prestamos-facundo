/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers;


import controlador.Conector;
import controlador.Funciones;
import controlador.Main;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerActRutaPrestamos extends SwingWorker<Double, Integer> {
   

    private ArrayList<Prestamo> ps;
    private String ruta;
    
    @Override
    protected Double doInBackground() throws Exception {
      Conector con = new Conector();
        Ventana.lbLiq.setText("Actualizando rutas...");
        
        for (int i=0; i < ps.size(); i++) {

            double porc = ((i+1)*100)/(ps.size());    
            int porcAux = (int) porc;
            
            Prestamo px = ps.get(i);
            con.conectar();
            px.setCodCobrador(ruta);
            
           
            con.actualizarPrestamo(px);
            con.cerrar();
            Ventana.leyenda.setText("RUTA "+Integer.toString(i)+" DE "+Integer.toString(ps.size()));
            
            
           

            publish(porcAux);

            
            
            }
        
            //javax.swing.JOptionPane.showMessageDialog(null,"Liquidación guardada correctamente.","Guardada",1);
            
            
            

            // Se pasa valor para la barra de progreso. ESto llamara al metodo
            // process() en el hilo de despacho de eventos.
            
        Ventana.lbLiq.setText("Finalizado.");
        Ventana.leyenda.setText("");
        Principal.loadWindow.dispose();
        Acceso.p.setVisible(true);
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()
    private JProgressBar progreso;

    @Override
    protected void process(List<Integer> chunks) {

        progreso.setValue(chunks.get(0));
    }
    
    public void setPrestamos(ArrayList<Prestamo> ps) {
        this.ps = ps;
    }
    
    public void setRuta(String ruta) {
        this.ruta = ruta;
    }
    
    
    public void setProgreso(JProgressBar prog) {
        this.progreso = prog;
    }
    
    
}

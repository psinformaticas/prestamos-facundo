/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers;

import aplicacion.WorkersApp;
import controlador.Conector;
import controlador.Funciones;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import modelo.Cliente;
import modelo.Cuota;
import modelo.Prestamo;
import vista.GenerarComprobante;

import vista.ListadoPorCob;
import vista.ListadoPorLocalidad;
import vista.Principal;
import static vista.Principal.loadWindow;

/**
 *
 * @author Usuario
 */
public class Workers {
    
    public static void workerRefresh() {
        WorkerRefresh actu = new WorkerRefresh();
                
            actu.execute();
    }
    
    
    public static void workerGuardarPago(Cliente clienteAux, String cobradorSel, Cuota cuotaAux, String fPagoAux,
        String impPagarAux, Prestamo prestAux) {
       
        WorkerGuardarPago actu = new WorkerGuardarPago();

        actu.setCliente(clienteAux);
        actu.setCobradorSel(cobradorSel);
        actu.setCuota(cuotaAux);
        actu.setFPago(fPagoAux);
        actu.setImpPagar(impPagarAux);
        actu.setPrestamo(prestAux);

        actu.execute();
        
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers;


import controlador.Conector;
import controlador.Funciones;
import controlador.Main;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cliente;
import modelo.Cobrador;
import modelo.Cuota;
import modelo.Localidad;
import modelo.Plan;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerImpVarios extends SwingWorker<Double, Integer> {
   

    
    private ArrayList<Cobrador> cobradores; 
    private ArrayList<Localidad> localidades; 
    private ArrayList<Cliente> clientes; 
    
    @Override
    protected Double doInBackground() throws Exception {
      
        Ventana.lbLiq.setText("Importando");
        Principal.con.conectar();
        for (int i=0; i < 3; i++) {

            double porc = ((i+1)*100)/(3);    
            int porcAux = (int) porc;
          
            if (i==0) {
                //AGREGA COBRADOERS
                Ventana.leyenda.setText("Agregando cobradores...");
                for (Cobrador c: cobradores) {
                    boolean esta = false;
                    for (Cobrador cx: Principal.cobradores) {
                        if (c.getCodInt().equals(cx.getCodInt())) {
                            esta = true;
                        }
                    }
                    if(!esta) {
                        Principal.con.conectar();
                        Principal.con.guardarCobrador(c);
                        Principal.con.cerrar();
                    }
                }
           }
           if (i==1) {
                //AGREGA LOCALIDADES
                Ventana.leyenda.setText("Agregando localidades...");
                for (Localidad l: localidades) {
                    boolean esta = false;
                    for (Localidad lx: Principal.localidades) {
                        if (l.getCodInt().equals(lx.getCodInt())) {
                            esta = true;
                        }
                    }
                    if(!esta) {
                        Principal.con.conectar();
                        Principal.con.guardarLocalidad(l);
                        Principal.con.cerrar();
                    }
                }
           } 
           if (i==2) {
                //AGREGA CLIENTES
                Ventana.leyenda.setText("Agregando clientes...");
                for (Cliente c: clientes) {
                    boolean esta = false;
                    for (Cliente cx: Principal.clientes) {
                        if (c.getNombre().equals(cx.getNombre())) {
                            esta = true;
                        }
                    }
                    if(!esta) {
                        Principal.con.conectar();
                        Principal.con.guardarCliente(c);
                        Principal.con.cerrar();
                    }
                }
           } 
            
            
        
            publish(porcAux);

            
            
        }
        Principal.con.cerrar();
            //javax.swing.JOptionPane.showMessageDialog(null,"Liquidación guardada correctamente.","Guardada",1);
            
            
            

            // Se pasa valor para la barra de progreso. ESto llamara al metodo
            // process() en el hilo de despacho de eventos.
        
        Ventana.lbLiq.setText("Finalizado.");
        Ventana.leyenda.setText("");
        Principal.loadWindow.dispose();
        JOptionPane.showMessageDialog(null, "Actualice el listado para ver los nuevos planes importados.", "Info",1);
        
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()
    private JProgressBar progreso;

    @Override
    protected void process(List<Integer> chunks) {

        progreso.setValue(chunks.get(0));
    }
    
    
    public void setProgreso(JProgressBar prog) {
        this.progreso = prog;
    }
   
     public void setCobradores(ArrayList<Cobrador> cs) {
        this.cobradores = cs;
    }
    
    public void setLocalidades(ArrayList<Localidad> cs) {
        this.localidades = cs;
    }
     
    public void setClientes(ArrayList<Cliente> cs) {
        this.clientes = cs;
    } 
}

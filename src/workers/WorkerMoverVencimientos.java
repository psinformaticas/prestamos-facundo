/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workers;


import auxiliares.FuncionesCovir;
import controlador.Conector;
import controlador.ConectorApp;
import controlador.Funciones;
import controlador.FuncionesMySQL;
import controlador.Main;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import modelo.Cliente;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Acceso;
import vista.Principal;


/**
 *
 * @author Will
 */
public class WorkerMoverVencimientos extends SwingWorker<Double, Integer> {
   

    
    ArrayList<Prestamo> prestamos;
    ArrayList<Cuota> cuotas;
    ArrayList<Cliente> clientesModif;
    Conector con;
    Date fechaInicial;
    int dias; 
    boolean todosLosClientes;
    
    public void setPrestamos(ArrayList<Prestamo> prestamos) {
        this.prestamos = prestamos;
    }

    public void setCuotas(ArrayList<Cuota> cuotas) {
        this.cuotas = cuotas;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public void setTodosLosClientes(boolean todosLosClientes) {
        this.todosLosClientes = todosLosClientes;
    }

    public void setClientesModif(ArrayList<Cliente> clientesModif) {
        this.clientesModif = clientesModif;
    }
    
    
    
    
    
    
    @Override
    protected Double doInBackground() throws Exception {
        Ventana.lbLiq.setText("Guardando copia de seguridad...");
        FuncionesMySQL.backup();
        Ventana.lbLiq.setText("Adelantando vencimientos...");
        Conector con = new Conector();
        System.out.println("SE ADELANTAN TODOS LOS CLIENTES: "+todosLosClientes);
        //ELIMINA PRESTAMOS DE CLIENTES QUE NO INTERESAN
        if (!todosLosClientes) {
            Iterator<Prestamo> it = prestamos.iterator();
            while(it.hasNext()) {
                Prestamo pAux = (Prestamo) it.next();
                boolean seModifica = false;
                for (Cliente cli: clientesModif) {
                    if (Integer.parseInt(pAux.getCodCliente())==cli.getCod()) {
                        seModifica = true;
                    }
                }
                
                System.out.println("CREDITO DE: "+pAux.getCodCliente()+"  SE MODIFICA: "+seModifica);
                if (!seModifica) {
                    it.remove();
                }
            
            }
            
            //ELIMINA CUOTAS QUE NO PERTENECEN A ESOS CREDITOS
            Iterator<Cuota> itCuotas = cuotas.iterator();
            while(itCuotas.hasNext()) {
                Cuota cAux = (Cuota) itCuotas.next();
                boolean pertenece = false;
                for (Prestamo p: prestamos) {
                    if (cAux.getCodPrestamoInt()==p.getCod()) {
                        pertenece = true;
                    }
                }
                if (!pertenece) {
                    itCuotas.remove();
                }
            }
            
           
        }
        
        
        for (int i=0; i < prestamos.size(); i++) {
            Ventana.leyenda.setText("Actualizando préstamo "+Integer.toString(i+1)+" de "+Integer.toString(prestamos.size()));
            
            double porc = ((i+1)*100)/(prestamos.size());    
            int porcAux = (int) porc;
            FuncionesCovir.adelantaPagoCuotasPrestamoXDias(con, dias, fechaInicial, prestamos.get(i), cuotas, prestamos, true);
            publish(porcAux);
        }
        
        javax.swing.JOptionPane.showMessageDialog(null,"Actualización finalizada.\n\nVerifique que los datos sean correctos y en caso de ser asi,\nsincronice las cuotas con la aplicación desde el menú.","OK",1);
            

            // Se pasa valor para la barra de progreso. ESto llamara al metodo
            // process() en el hilo de despacho de eventos.
            
        Ventana.lbLiq.setText("Finalizado.");
        Ventana.leyenda.setText("");
        Principal.loadWindow.dispose();
        // Supuesto resultado de la tarea que tarda mucho.
        return 100.0;
    }
    
    // Esta JProgressBar la recibiremos en el constructor o en
    // un parametro setProgreso()
    private JProgressBar progreso;

    @Override
    protected void process(List<Integer> chunks) {

        progreso.setValue(chunks.get(0));
    }
    
    
    public void setProgreso(JProgressBar prog) {
        this.progreso = prog;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;



/**
 *
 * @author Will
 */
public class Opciones {
    
    String nombre, direccion, telefono, mostrarpagados, mostrarvencidos, serial, extra1,  extra2, extra3, extra4, extra5, extra6, ultimoBackup;
    double interesdia;

    public Opciones() {
        this.nombre = "";
        this.direccion = "";
        this.telefono = "";
        this.mostrarpagados = "";
        this.mostrarvencidos = "";
        this.serial = "";
        this.extra1 = "";
        this.extra2 = "";
        this.extra3 = "";
        this.extra4 = "";
        this.extra5 = "";
        this.extra6 = "";
        this.ultimoBackup = "";
        this.interesdia = 0.0;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMostrarpagados() {
        return mostrarpagados;
    }

    public void setMostrarpagados(String mostrarpagados) {
        this.mostrarpagados = mostrarpagados;
    }

    public String getMostrarvencidos() {
        return mostrarvencidos;
    }

    public void setMostrarvencidos(String mostrarvencidos) {
        this.mostrarvencidos = mostrarvencidos;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getExtra1() {
        return extra1;
    }

    public void setExtra1(String extra1) {
        this.extra1 = extra1;
    }

    public String getExtra2() {
        return extra2;
    }

    public void setExtra2(String extra2) {
        this.extra2 = extra2;
    }

    public String getExtra3() {
        return extra3;
    }

    public void setExtra3(String extra3) {
        this.extra3 = extra3;
    }

    public String getExtra4() {
        return extra4;
    }

    public void setExtra4(String extra4) {
        this.extra4 = extra4;
    }

    public String getExtra5() {
        return extra5;
    }

    public void setExtra5(String extra5) {
        this.extra5 = extra5;
    }

    public String getExtra6() {
        return extra6;
    }

    public void setExtra6(String extra6) {
        this.extra6 = extra6;
    }

    public double getInteresdia() {
        return interesdia;
    }

    public void setInteresdia(double interesdia) {
        this.interesdia = interesdia;
    }

    public String getUltimoBackup() {
        return ultimoBackup;
    }

    public void setUltimoBackup(String ultimoBackup) {
        this.ultimoBackup = ultimoBackup;
    }
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import modelo.Cliente;
import modelo.Cuota;
import modelo.LineaSMS;
import modelo.Prestamo;
import vista.Principal;

/**
 *
 * @author Usuario
 */
public class FuncionesReportes {
    
    public static ArrayList<LineaSMS> getDatosParaEnvioSMS() {
        ArrayList<LineaSMS> ret = new ArrayList<>();
        LineaSMS linea = new LineaSMS();
        
        for (Cliente cli: Principal.clientes) {
            linea = new LineaSMS();
            
            double totalConInt = Funciones.getDeudaTotalPorCliente(cli, true);
            double totalSinInt = Funciones.getDeudaTotalPorCliente(cli, false);
            double interes = totalConInt - totalSinInt;
            interes = Funciones.formatearDecimales(interes, 2);
            
            linea.setDeudaCuotas(Double.toString(totalSinInt));
            linea.setDeudaRecargos(Double.toString(interes));
            linea.setFechaHoy(Funciones.devolverFechaActualStr());
            linea.setNombre(Funciones.capitalize(cli.getNombre()));
            linea.setTelefono(cli.getCelular());
            if (totalSinInt > 1 || interes > 1) {
                ret.add(linea);    
            }
            
        }
        return ret;
    }
    
    public static ArrayList<Cuota> getArrayCuotasQueSonLasUltimasAdeudadas() {
        ArrayList<Cuota> cuotasRet = new ArrayList<>();
        for (Cliente cli: Principal.clientes) {
            ArrayList<Cuota> adeudadas = cli.getCuotasAdeudadas();
            if (adeudadas.size() == 1) {
                cuotasRet.addAll(adeudadas);
            }
        }
        
        Collections.sort(cuotasRet);
        return cuotasRet;
    }
    
    public static ArrayList<Cliente> getClientesTodoPagoSinRenovar() {
        ArrayList<Cliente> clientesRet = new ArrayList<>();
        
        for (Cliente cli: Principal.clientes) {
            if (cli.tieneTodasLasCuotasPagasSinContarInt() && cli.getPrestamos().size()>0) {
                clientesRet.add(cli);
            }
        }
        
        return clientesRet;
    }       
    
    public static ArrayList<Prestamo> getPrestamosEntreFechas(Date inicio, Date fin) {
        ArrayList<Prestamo> prestamosRet = new ArrayList<>();
        
        Principal.todosLosPrestamos.stream().filter((c) -> ((c.getDateGenerado().equals(inicio) || c.getDateGenerado().after(inicio)) && (c.getDateGenerado().equals(fin) || c.getDateGenerado().before(fin)))).forEachOrdered((c) -> {
            prestamosRet.add(c);
        });
        
        
        return prestamosRet;
    }       
}

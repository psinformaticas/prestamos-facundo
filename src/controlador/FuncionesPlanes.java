/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import modelo.Plan;
import vista.Principal;

/**
 *
 * @author Usuario
 */
public class FuncionesPlanes {
    
    public static String validaNombrePlan(String nombrePlan) {
        boolean valido = true;
        String nombre = nombrePlan;
        
        while (compruebaSiNombrePlanYaExiste(nombre)) {
                int numNuevo = obtenerNumeroDeFinalString(nombre);
                int digitos = 0;
                if (numNuevo == 0) {
                    digitos = 0;
                } else {
                    digitos = Integer.toString(numNuevo).length();
                }
                
                nombre = nombre.substring(0, nombre.length()-digitos)+Integer.toString(numNuevo+1);
        }
        
        return nombre;
    }
    
    public static int obtenerNumeroDeFinalString(String nombre) {
        int lastInd = nombre.length();
        String ultimos = nombre.substring(lastInd-4 ,lastInd);
        if (Funciones.isNumeric(ultimos)) {
            return Integer.parseInt(ultimos);
        } else if (Funciones.isNumeric(ultimos.substring(ultimos.length()-1, ultimos.length()))) {
            return Integer.parseInt(ultimos.substring(ultimos.length()-1, ultimos.length()));
        } else if (Funciones.isNumeric(ultimos.substring(ultimos.length()-2, ultimos.length()))) {
            return Integer.parseInt(ultimos.substring(ultimos.length()-2, ultimos.length()));
        } else if (Funciones.isNumeric(ultimos.substring(ultimos.length()-3, ultimos.length()))) {
            return Integer.parseInt(ultimos.substring(ultimos.length()-3, ultimos.length()));
        } else {
            return 0;
        }
        
    }
    
    public static boolean compruebaSiNombrePlanYaExiste(String nombrePlan) {
        boolean existe = false;
        for (Plan p: Principal.planes) {
            if (p.getNombre().equalsIgnoreCase(nombrePlan)) {
                existe = true;
            }
        }
        return existe;
    }
}
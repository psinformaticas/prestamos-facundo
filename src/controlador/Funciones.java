/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.List;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Math.toIntExact;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.imageio.ImageIO;
import modelo.*;
import vista.*;

/**
 *
 * @author Will
 */
public class Funciones {
    

    
    public static OpcionesDB leerOptDB() {
      OpcionesDB opt = new OpcionesDB();
      File archivo = null;
      FileReader fr = null;
      BufferedReader br = null;
      String linea = "";
      String ubicaBD = "";
      try {
         // Apertura del fichero y creacion de BufferedReader para poder
         // hacer una lectura comoda (disponer del metodo readLine()).
         archivo = new File ("location.ini");
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);
         String tipo = "TIPO=";
         String usr = "USR=";
         String pass = "PASS=";
         String srv = "SRV=";
         String bd = "BD=";
         String port = "PORT=";
         
         // Lectura del fichero
        while ((linea = br.readLine()) != null) {
            if (Funciones.contiene(linea, tipo)) {
                opt.setTipo(linea.replace("TIPO=",""));
            }
            if (Funciones.contiene(linea, usr)) {
                opt.setUsr(linea.replace("USR=",""));
            }
            if (Funciones.contiene(linea, pass)) {
                opt.setPass(linea.replace("PASS=",""));
            }
            if (Funciones.contiene(linea, srv)) {
                opt.setSrv(linea.replace("SRV=",""));
            }
            if (Funciones.contiene(linea, bd)) {
                opt.setBd(linea.replace("BD=",""));
            }
            if (Funciones.contiene(linea, port)) {
                opt.setPort(linea.replace("PORT=",""));
            }
         }       
          
          
      }
      catch(Exception e){
         OpcionesDB op = new OpcionesDB();
         op.setTipo("SERVIDOR");
         op.setBd("base");
         op.setPass("root");
         op.setUsr("root");
         op.setSrv("localhost");
         op.setPort("3306");
         guardaOptBD(op);
         e.printStackTrace();
      }finally{
         // En el finally cerramos el fichero, para asegurarnos
         // que se cierra tanto si todo va bien como si salta 
         // una excepcion.
         try{                    
            if( null != fr ){   
               fr.close();     
            }                  
         }catch (Exception e2){ 
            e2.printStackTrace();
         }
      }
      //System.out.println(ubicaBD);
      return opt;
    }
    
    public static OpcionesDB leerOptDBApp() {
      OpcionesDB opt = new OpcionesDB();
      File archivo = null;
      FileReader fr = null;
      BufferedReader br = null;
      String linea = "";
      String ubicaBD = "";
      try {
         // Apertura del fichero y creacion de BufferedReader para poder
         // hacer una lectura comoda (disponer del metodo readLine()).
         archivo = new File ("locationapp.ini");
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);
         String tipo = "TIPO=";
         String usr = "USR=";
         String pass = "PASS=";
         String srv = "SRV=";
         String bd = "BD=";
         String port = "PORT=";
         
         // Lectura del fichero
        while ((linea = br.readLine()) != null) {
            if (Funciones.contiene(linea, usr)) {
                opt.setUsr(linea.replace("USR=",""));
            }
            if (Funciones.contiene(linea, pass)) {
                opt.setPass(linea.replace("PASS=",""));
            }
            if (Funciones.contiene(linea, srv)) {
                opt.setSrv(linea.replace("SRV=",""));
            }
            if (Funciones.contiene(linea, bd)) {
                opt.setBd(linea.replace("BD=",""));
            }
            if (Funciones.contiene(linea, port)) {
                opt.setPort(linea.replace("PORT=",""));
            }
         }       
        opt.setTipo("APP");
          
          
      }
      catch(Exception e){
         OpcionesDB op = new OpcionesDB();
         op.setTipo("APP");
         op.setBd("base");
         op.setPass("root");
         op.setUsr("root");
         op.setSrv("localhost");
         op.setPort("3306");
         guardaOptBD(op);
         e.printStackTrace();
      }finally{
         // En el finally cerramos el fichero, para asegurarnos
         // que se cierra tanto si todo va bien como si salta 
         // una excepcion.
         try{                    
            if( null != fr ){   
               fr.close();     
            }                  
         }catch (Exception e2){ 
            e2.printStackTrace();
         }
      }
      //System.out.println(ubicaBD);
      return opt;
    }
    
    public static void guardaOptBD(OpcionesDB opt){
        FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter("location.ini");
            pw = new PrintWriter(fichero);
            pw.println("[CFG-DB]");
            pw.println("TIPO="+opt.getTipo());
            pw.println("USR="+opt.getUsr());
            pw.println("PASS="+opt.getPass());
            pw.println("SRV="+opt.getSrv());
            pw.println("BD="+opt.getBd());
            pw.println("PORT="+opt.getPort());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
    
    }
    
    public static void guardarArchOmitirWelc(){
        FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter("skip.psi");
            pw = new PrintWriter(fichero);
            pw.println("[OMITE]");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
    
    }
    
    public static boolean ocultaFinalizados() {
      String linea = "";
      File archivo = null;
      FileReader fr = null;
      BufferedReader br = null;
      String ubicaBD = "";
      try {
         // Apertura del fichero y creacion de BufferedReader para poder
         // hacer una lectura comoda (disponer del metodo readLine()).
         archivo = new File ("adic.ini");
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);
         String ubica = "OCULTA=";
         
         // Lectura del fichero
          //linea=br.readLine();
        while ((linea = br.readLine()) != null) {
            if (Funciones.contiene(linea, ubica)) {
                ubicaBD = linea.replace("OCULTA=","");
            }
         }       
          
          
      }
      catch(Exception e){
         e.printStackTrace();
      }finally{
         // En el finally cerramos el fichero, para asegurarnos
         // que se cierra tanto si todo va bien como si salta 
         // una excepcion.
         try{                    
            if( null != fr ){   
               fr.close();     
            }                  
         }catch (Exception e2){ 
            e2.printStackTrace();
         }
      }
      //System.out.println(ubicaBD);
      return ubicaBD.equals("SI");
    }
    
    public static void guardarOcultaFinalizados(boolean oculta){
        FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter("adic.ini");
            pw = new PrintWriter(fichero);
            pw.println("[CFG]");
            if (oculta) {
                pw.println("OCULTA=SI");
            } else {
                pw.println("OCULTA=NO");
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
    
    }
    
    public static OpcionesExtra leerOptExtra() {
      OpcionesExtra opt = new OpcionesExtra();
      File archivo = null;
      FileReader fr = null;
      BufferedReader br = null;
      String linea = "";
      String ubicaBD = "";
      try {
         // Apertura del fichero y creacion de BufferedReader para poder
         // hacer una lectura comoda (disponer del metodo readLine()).
         archivo = new File ("cfgextra.ini");
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);
         String marcaFinalizados = "MARCAFINALIZADOS=";
         String impresion = "IMPRESION=";
         String x1 = "X1=";
         String x2 = "X2=";
         String x3 = "X3=";
         String x4 = "X4=";
         String x5 = "X5=";
         String x6 = "X6=";
         String x7 = "X7=";
         String x8 = "X8=";
         String x9 = "X9=";
         String x10 = "X10=";
         
         // Lectura del fichero
        while ((linea = br.readLine()) != null) {
            if (Funciones.contiene(linea, marcaFinalizados)) {
                opt.setMarcaFinalizados(linea.replace("MARCAFINALIZADOS=",""));
            }
            if (Funciones.contiene(linea, impresion)) {
                opt.setImpresion(linea.replace("IMPRESION=",""));
            }
            if (Funciones.contiene(linea, x1)) {
                opt.setX1(linea.replace("X1=",""));
            }
            if (Funciones.contiene(linea, x2)) {
                opt.setX2(linea.replace("X2=",""));
            }
            if (Funciones.contiene(linea, x3)) {
                opt.setX3(linea.replace("X3=",""));
            }
            if (Funciones.contiene(linea, x4)) {
                opt.setX4(linea.replace("X4=",""));
            }
            if (Funciones.contiene(linea, x5)) {
                opt.setX5(linea.replace("X5=",""));
            }
            if (Funciones.contiene(linea, x6)) {
                opt.setX6(linea.replace("X6=",""));
            }
            if (Funciones.contiene(linea, x7)) {
                opt.setX7(linea.replace("X7=",""));
            }
            if (Funciones.contiene(linea, x8)) {
                opt.setX8(linea.replace("X8=",""));
            }
            if (Funciones.contiene(linea, x9)) {
                opt.setX9(linea.replace("X9=",""));
            }
            if (Funciones.contiene(linea, x10)) {
                opt.setX10(linea.replace("X10=",""));
            }
         }       
          
          
      }
      catch(Exception e){
         e.printStackTrace();
      }finally{
         // En el finally cerramos el fichero, para asegurarnos
         // que se cierra tanto si todo va bien como si salta 
         // una excepcion.
         try{                    
            if( null != fr ){   
               fr.close();     
            }                  
         }catch (Exception e2){ 
            e2.printStackTrace();
         }
      }
      //System.out.println(ubicaBD);
      return opt;
    }
    
    public static void guardaOptExtra(OpcionesExtra opt){
        FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter("cfgextra.ini");
            pw = new PrintWriter(fichero);
            pw.println("[CFG-EXTRA]");
            pw.println("MARCAFINALIZADOS="+opt.getMarcaFinalizados());
            pw.println("IMPRESION="+opt.getImpresion());
            pw.println("X1="+opt.getX1());
            pw.println("X2="+opt.getX2());
            pw.println("X3="+opt.getX3());
            pw.println("X4="+opt.getX4());
            pw.println("X5="+opt.getX5());
            pw.println("X6="+opt.getX6());
            pw.println("X7="+opt.getX7());
            pw.println("X8="+opt.getX8());
            pw.println("X9="+opt.getX9());
            pw.println("X10="+opt.getX10());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
    
    }
   
    public static String leerObsA4() {
      String notas = "";
      File archivo = null;
      FileReader fr = null;
      BufferedReader br = null;
      String notasAux = "";
      try {
         // Apertura del fichero y creacion de BufferedReader para poder
         // hacer una lectura comoda (disponer del metodo readLine()).
         archivo = new File ("obsa4.psi");
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);
         
         // Lectura del fichero
          //linea=br.readLine();
        while ((notas = br.readLine()) != null) {
            if (notasAux.equals("")) {
                notasAux += notas;    
            }
            else {
                notasAux += "\n" + notas;
            }
            
         }       
          
          
      }
      catch(Exception e){
         e.printStackTrace();
      }finally{
         // En el finally cerramos el fichero, para asegurarnos
         // que se cierra tanto si todo va bien como si salta 
         // una excepcion.
         try{                    
            if( null != fr ){   
               fr.close();     
            }                  
         }catch (Exception e2){ 
            e2.printStackTrace();
         }
      }
      //System.out.println(ubicaBD);
      
      return notasAux;
    }
    
    public static void guardarObsA4(String notas){
        FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter("obsa4.psi");
            pw = new PrintWriter(fichero);
            pw.println(notas);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
    
    }
    
       public static BufferedImage getScaledImage(String ruta, int w, int h) throws IOException {
        File file = new File(ruta);
        BufferedImage src=ImageIO.read(file);
        int original_width = src.getWidth();
        int original_height = src.getHeight();
        int bound_width = w;
        int bound_height = h;
        int new_width = original_width;
        int new_height = original_height;

        // first check if we need to scale width
        if (original_width > bound_width) {
            //scale width to fit
            new_width = bound_width;
            //scale height to maintain aspect ratio
            new_height = (new_width * original_height) / original_width;
        }

        // then check if we need to scale even with the new height
        if (new_height > bound_height) {
            //scale height to fit instead
            new_height = bound_height;
            //scale width to maintain aspect ratio
            new_width = (new_height * original_width) / original_height;
        }

        BufferedImage resizedImg = new BufferedImage(new_width, new_height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = resizedImg.createGraphics();
        g2.setBackground(Color.WHITE);
        g2.clearRect(0,0,new_width, new_height);
        g2.drawImage(src, 0, 0, new_width, new_height, null);
        g2.dispose();
        return resizedImg;
    }
    
    
    
    //FUNCIONES VALIDAR
    public static boolean validarFormatoRuta(String ruta) {
        boolean valido = true;
        
        if (ruta.length() != 3) {
            valido = false;
        }
        
        if (!strEsNumero(ruta)) {
            
            valido = false;
        }
        
        return valido;
    }
    
    public static boolean isNumeric(String cadena) {

        boolean resultado;

        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }
    
    public static boolean strEsNumero(String cadena) {

        boolean resultado;

        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }
    
    //FUNCIONES LOCALIDAD
    public static Localidad getLocalidadPorCodInt(String codInt) {
        Localidad l = new Localidad();
        if (codInt.equals("XXX")) {
            return new Localidad();
        }
        for (Localidad lx: Principal.localidades) {
            if (lx.getCodInt().equals(codInt)) {
                l = lx;
            }
        }
        return l;
    }
   
    
    //FUNCIONES CUOTAS
    public static ArrayList<Cuota> getCuotasPorCodPrestamo(ArrayList<Cuota> cuotasTodas, String cod) {
        ArrayList<Cuota> cuotas = new ArrayList<Cuota>();
        for (Cuota cuotaX: cuotasTodas) {
            if (cuotaX.getCodPrestamo().equals(cod)){
                cuotas.add(cuotaX);
            }
        }
        
        
        return cuotas;
    }
    
    public static int calcularDiasDeDiferencia(String fechaAct, String fechaAnt) {
        if (fechaAct.equals("01/01/2099") || fechaAnt.equals("01/01/2099")) {
            return 0;
        }
        Date fechaFinal = Funciones.stringADate(fechaAct);
        Date fechaInicial = Funciones.stringADate(fechaAnt);
        
        int dias=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/86400000);
        
        if (dias < 0) {
            return 0;
        }
            
        return dias;
    }
    
    public static String getEstadoCuota(Cuota cuota) {
        String estado = "PENDIENTE";
        double importe = Double.parseDouble(cuota.getImporteTotal());
        double pagado = Double.parseDouble(cuota.getImportePagado());
        
        if (cuota.getNumCuota().equals("999")) {
            importe = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, cuota.getCodPrestamoInt()).getInteresTotalAlDiaDeHoy();
            pagado = Double.parseDouble(cuota.getImportePagado());
        }
        
        double restante = importe - pagado;
        boolean pagada = false;
        boolean parcial = false;
        
        if (restante<1) {
            estado = "PAGADA";
            pagada = true;
        }
        if (restante == importe && Funciones.stringADate(cuota.getFechaPagar()).before(Funciones.stringADate(Funciones.devolverFechaActualStr()))) {
            estado = "ATRASADA";
        }
        if (restante>1 && restante<importe) {
            estado = "PARCIAL";
            parcial = true;
        }
        if (Funciones.stringADate(cuota.getFechaPagar()).after(Funciones.stringADate(Funciones.devolverFechaActualStr())) && !pagada && !parcial) {
            estado = "PENDIENTE";
        }
        if (cuota.getNumCuota().equals("999")) {
            estado = "INTERÉS";
        }
        if (cuota.getNumCuota().equals("999") && restante<1) {
            estado = "PAGADA";
        }
        
        return estado;
    }
    
    public static Cuota getCuotaPorCod(int cod) {
        for (Cuota cuotaX: Principal.todasLasCuotas) {
            if (cuotaX.getCod() == cod) {
                return cuotaX;
            }
        }
        return new Cuota();
    }
    
    public static Cuota getCuotaInteresPorCuota(Cuota c) {
        Prestamo p = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, c.getCod());
        for (Cuota cx: p.getCuotas()) {
            if(cx.getNumCuota().equals("999")) {
                return cx;
            }
        }
        return new Cuota();
    }
    
    public static double getImportePrestadoCuota(Cuota cuota) {
        int codPrestamo = Integer.parseInt(cuota.getCodPrestamo());
        Prestamo nP = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, codPrestamo);
        Plan plan = Funciones.getPlanPorNombre(nP.getPlan());
        return Double.parseDouble(plan.getImportePrestar()) / Double.parseDouble(plan.getCantCuotas());
    }
            
    public static double getInteresCalculadoPorDiasAtrasoPorcentaje(Cuota cuota) {
        Date fechaPagoAux = Funciones.stringADate(cuota.getFechaPagar());
        Date fechaHoyAux = Funciones.stringADate(devolverFechaActualStr());
        int diasDif = Funciones.calcularDiasDeDiferencia(Funciones.dateAString(fechaHoyAux), Funciones.dateAString(fechaPagoAux));
        if (diasDif<0) {
            diasDif = 0;
        }
        
        double impRestante = Double.parseDouble(cuota.getImporteTotal()) - Double.parseDouble(cuota.getImportePagado());
        
        double valorIntAux = 0.0;
        double totalIntAux = 0.0;
        
        
            
        //PORCENTAJE
        double porcIntDia = Principal.opciones.getInteresdia();
        double valorIntDia = 0;
        if (impRestante>0) {
            valorIntDia = (impRestante * porcIntDia) / 100;
        } else {
            valorIntDia = (cuota.getImporteTotalD() * porcIntDia) / 100;
        }
        

        totalIntAux = valorIntDia * diasDif;
            
        return totalIntAux;   
               
    }
    
    public static double getInteresCalculadoPorDiasAtrasoHastaHoy(Cuota cuota) {
        Date fechaPagoAux = Funciones.stringADate(cuota.getFechaPagar());
        Date fechaHoyAux = Funciones.stringADate(devolverFechaActualStr());
        int diasDif = Funciones.calcularDiasDeDiferencia(Funciones.dateAString(fechaHoyAux), Funciones.dateAString(fechaPagoAux));
        if (diasDif<0) {
            diasDif = 0;
        }
        
        double impRestante = Double.parseDouble(cuota.getImporteTotal()) - Double.parseDouble(cuota.getImportePagado());
        
        double valorIntAux = 0.0;
        double totalIntAux = 0.0;
        
        
            
        //PORCENTAJE
        double porcIntDia = Principal.opciones.getInteresdia();
        double valorIntDia = (impRestante * porcIntDia) / 100;

        totalIntAux = valorIntDia * diasDif;
            
        return totalIntAux;   
               
    }
    
    public static double getInteresCalculadoPorDiasAtrasoHastaHoy(Cuota cuota, Date fecha) {
        Date fechaPagoAux = Funciones.stringADate(cuota.getFechaPagar());
        Date fechaHoyAux = fecha;
        int diasDif = Funciones.calcularDiasDeDiferencia(Funciones.dateAString(fechaHoyAux), Funciones.dateAString(fechaPagoAux));
        if (diasDif<0) {
            diasDif = 0;
        }
        
        double impRestante = Double.parseDouble(cuota.getImporteTotal()) - Double.parseDouble(cuota.getImportePagado());
        
        double valorIntAux = 0.0;
        double totalIntAux = 0.0;
        
        
            
        //PORCENTAJE
        double porcIntDia = Principal.opciones.getInteresdia();
        double valorIntDia = (impRestante * porcIntDia) / 100;

        totalIntAux = valorIntDia * diasDif;
            
        return totalIntAux;   
               
    }
    
    public static double getInteresCalculadoPorDiasAtrasoHastaFechaPagada(Cuota cuota) {
        Date fechaPagoAux = Funciones.stringADate(cuota.getFechaPagar());
        Date fechaHoyAux = Funciones.stringADate(cuota.getFechaPagado());
        int diasDif = Funciones.calcularDiasDeDiferencia(Funciones.dateAString(fechaHoyAux), Funciones.dateAString(fechaPagoAux));
        if (diasDif<0) {
            diasDif = 0;
            
        }
        
        
        
        double impRestante = Double.parseDouble(cuota.getImportePagado());
        
        double valorIntAux = 0.0;
        double totalIntAux = 0.0;
        
        
            
        //PORCENTAJE
        double porcIntDia = Principal.opciones.getInteresdia();
        double valorIntDia = (impRestante * porcIntDia) / 100;

        totalIntAux = valorIntDia * diasDif;
            
        return totalIntAux;   
               
    }
    
    public static ArrayList<Cuota> getCuotasPorCobradorCobradasEntreFechas(String cob, Date inicio, Date fin) {
        ArrayList<Cuota> cuotas = new ArrayList<>();
        Principal.todasLasCuotas.stream().filter((c) -> (c.getA1().equals(cob))).filter((c) -> ((c.getFPago().equals(inicio) || c.getFPago().after(inicio)) && (c.getFPago().equals(fin) || c.getFPago().before(fin)))).filter((c) -> (c.getImportePagadoD()>0)).forEachOrdered((c) -> {
            cuotas.add(c);
        });
        return cuotas;
    }       
    
    public static ArrayList<Cuota> getCuotasGlobalesCobradasEntreFechas(Date inicio, Date fin) {
        ArrayList<Cuota> cuotas = new ArrayList<>();
        Principal.todasLasCuotas.stream().filter((c) -> ((c.getFPago().equals(inicio) || c.getFPago().after(inicio)) && (c.getFPago().equals(fin) || c.getFPago().before(fin)))).filter((c) -> (c.getImportePagadoD()>0)).forEachOrdered((c) -> {
            cuotas.add(c);
        });
        return cuotas;
    }   

    //FUNCIONES PLANES
    public static Plan getPlanPorNombre(String nombre) {
        for (Plan plan: Principal.planes) {
            if (plan.getNombre().equalsIgnoreCase(nombre)) {
                return plan;
            }
        }
        return new Plan();
    }
    
    public static Plan getPlanPorCod(int cod) {
        for (Plan plan: Principal.planes) {
            if (plan.getCod() == cod) {
                return plan;
            }
        }
        return new Plan();
    }
    
    //FUNCIONES CLIENTES
    public static boolean contiene(String cadena, String subcadena) {
	return cadena.toLowerCase().contains(subcadena.toLowerCase());
    }
    
    public static ArrayList<Cliente> getClientesPorNombre(String nombre) {
        ArrayList<Cliente> cliRet = new ArrayList<Cliente>();
        for (Cliente cli: Principal.clientes) {
            if (contiene(cli.getNombre(),nombre)) {
                cliRet.add(cli);
            }
        }
        
        return cliRet;
    }
    
    public static ArrayList<Cliente> getClientesPorDni(String dni) {
        ArrayList<Cliente> cliRet = new ArrayList<Cliente>();
        for (Cliente cli: Principal.clientes) {
            if (contiene(cli.getDni(),dni)) {
                cliRet.add(cli);
            }
        }
        
        return cliRet;
    }
    
    public static Cliente getClientePorDni(String dni) {
        Cliente cliRet = new Cliente();
        for (Cliente cli: Principal.clientes) {
            if (cli.getDni().equalsIgnoreCase(dni)) {
                return cli;
            }
        }
        
        return cliRet;
    }
    
    public static Cliente getClientePorCod(ArrayList<Cliente> clientes, int cod) {
        Cliente cliRet = new Cliente();
        for (Cliente cli: clientes) {
            if (cli.getCod() == cod) {
                cliRet = cli;
            }
        }
        
        return cliRet;
    }
    
    public static Cobrador getCobradorPorCod(ArrayList<Cobrador> cobradores, int cod) {
        Cobrador cliRet = new Cobrador();
        for (Cobrador cli: cobradores) {
            if (cli.getCod() == cod) {
                cliRet = cli;
            }
        }
        
        return cliRet;
    }
    
    public static Cobrador getCobradorPorRutaYNom(ArrayList<Cobrador> cobradores, String rutaYnom) {
        Cobrador cliRet = new Cobrador();
        for (Cobrador cli: cobradores) {
            String aux = cli.getCodInt()+" - "+capitalize(cli.getNombre());
            if (aux.equals(rutaYnom)) {
                cliRet = cli;
            }
        }
        
        return cliRet;
    }
    
    public static Cobrador getCobradorPorRuta(ArrayList<Cobrador> cobradores, String ruta) {
        Cobrador cliRet = new Cobrador();
        for (Cobrador cli: cobradores) {
            String aux = cli.getCodInt();
            if (aux.equals(ruta)) {
                cliRet = cli;
            }
        }
        
        return cliRet;
    }
        
    public static Cobrador getCobradorPorNombre(ArrayList<Cobrador> cobradores, String nom) {
        Cobrador cliRet = new Cobrador();
        for (Cobrador cli: cobradores) {
            if (cli.getNombre().equals(nom.toLowerCase())) {
                cliRet = cli;
            }
        }
        
        return cliRet;
    }
    
    public static Cliente getClientePorNombre(ArrayList<Cliente> clientes, String nom) {
        Cliente cliRet = new Cliente();
        for (Cliente cli: clientes) {
            if (cli.getNombre().equals(nom.toLowerCase())) {
                cliRet = cli;
            }
        }
        
        return cliRet;
    }
    
    public static double getDeudaTotalPorCliente(Cliente cli, boolean conInteres) {
        double total = 0.0;
        ArrayList<Prestamo> prestamos = Funciones.getPrestamosPorCli(Principal.todosLosPrestamos, Integer.toString(cli.getCod()));
                
        for (Prestamo p: prestamos) {
            for (Cuota c: p.getCuotas()) {
                if (!c.yaSePago() && (c.getFechaPagarD().before(new Date()) || c.getFechaPagarD().equals(new Date()))) {
                    total += c.getImporteTotalD()-c.getImportePagadoD();
                    if (conInteres) {
                        total += Funciones.getInteresCalculadoPorDiasAtrasoPorcentaje(c);
                    }
                }
            }
        }
        
        return Funciones.formatearDecimales(total, 2);
    }
    
    
    
    //FUNCIONES PRESTAMOS
    public static Prestamo getPrestamoPorCod(ArrayList<Prestamo> prestamos, int cod) {
        Prestamo prestRet = new Prestamo();
        for (Prestamo prestX: prestamos) {
            if (prestX.getCod() == cod) {
                prestRet = prestX;
            }
        }
        
        return prestRet;
    }
    
    public static ArrayList<Prestamo> getPrestamosPorCli(ArrayList<Prestamo> prestamos, String cod) {
        ArrayList<Prestamo> prestamosRet = new ArrayList<Prestamo>();
        for (Prestamo prestX: prestamos) {
            
            if (prestX.getCodCliente().equals(cod)) {
                
                prestamosRet.add(prestX);
            }
        }
        return prestamosRet;
    }
                
    public static boolean prestamoTieneTodasCuotasPagasYFaltaPagarInt(Prestamo p) {
        boolean cuotasPagas = true;
        boolean faltaPagarInt = false;
        for (Cuota c: p.getCuotas()) {
            if (c.getNumCuota().equals("999")) {
                faltaPagarInt = !c.yaSePago();
            } else {
                cuotasPagas = cuotasPagas && c.yaSePago();
            }
        }
        System.out.println("TIENE CUOTAS PAGAS: "+cuotasPagas);
        System.out.println("FALTA PAGAR INT: "+faltaPagarInt);
        if (cuotasPagas && faltaPagarInt) {
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean prestamoTieneTodasCuotasPagasSinContarInt(Prestamo p) {
        boolean cuotasPagas = true;
        for (Cuota c: p.getCuotas()) {
            if (!c.getNumCuota().equals("999")) {
                cuotasPagas = cuotasPagas && c.yaSePago();
            }
        }
        System.out.println("TIENE CUOTAS PAGAS: "+cuotasPagas);
        if (cuotasPagas) {
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean tieneUnSoloPrestamo(Cuota c) {
        boolean unSoloPrestamo = false;
        Prestamo prestamo = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, c.getCodPrestamoInt());
        Cliente cliente = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(prestamo.getCodCliente()));
        
        ArrayList<Prestamo> prestamos = Funciones.getPrestamosPorCli(Principal.todosLosPrestamos, Integer.toString(cliente.getCod()));
        
        if (prestamos.size() == 1) {
            unSoloPrestamo = true;
        }
        return unSoloPrestamo;
    }
                
    //FUNCIONES STRINGS
    public static String capitalize(String str) {
        boolean prevWasWhiteSp = true;
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (Character.isLetter(chars[i])) {
                if (prevWasWhiteSp) {
                    chars[i] = Character.toUpperCase(chars[i]);    
                }
                prevWasWhiteSp = false;
        } else {
            prevWasWhiteSp = Character.isWhitespace(chars[i]);
        }
    }
    return new String(chars);
    }
    
    public static int getCodPorPrestamoStr(String plan) {
        int cod = 0;
        if (plan != null) {
            int indiceFinal = plan.indexOf("]");
            String codFinal = plan.substring(1, indiceFinal);
            cod = Integer.parseInt(codFinal);
        }
        
        
        return cod;
    }
    
    public static String ponerPuntos(String dato){
        if(dato==null) return "0";
        if(dato.equals("0")) return "0";
        if(dato.equals("0")) return "0";
        if(dato.equals(null)) return "0";
        String dato1="";
        char []aux1=dato.toCharArray();
        for(int i=0;i<aux1.length;i++){
        if(aux1[i]!='.' && aux1[i]!=',')
        dato1=dato1+String.valueOf(aux1[i]);
        else
        i=aux1.length+2;
        }
        char []aux=dato1.toCharArray();
        String str=dato1;
        String salida="";
        int longitud = str.length();
        if(longitud <4) return dato1;
        if (longitud ==4){
        String sub2=str.substring(0,1);
        String sub1=str.substring(1,4);
        salida=sub2+"."+sub1;
        }
        if (longitud ==5){
        String sub2=str.substring(0,2);
        String sub1=str.substring(2,5);
        salida=sub2+"."+sub1;
        }
        if (longitud ==6){
        String sub2=str.substring(0,3);
        String sub1=str.substring(3,6);
        salida=sub2+"."+sub1;
        }
        if (longitud ==7 ){
        String sub2=str.substring(0,1);
        String sub1=str.substring(1,4);
        String sub0=str.substring(4,7);
        salida=sub2+"."+sub1+"."+sub0;
        }
        if (longitud ==8 ){
        String sub2=str.substring(0,2);
        String sub1=str.substring(2,5);
        String sub0=str.substring(5,8);
        salida=sub2+"."+sub1+"."+sub0;
        }
        if (longitud ==9 ){
        String sub2=str.substring(0,3);
        String sub1=str.substring(3,6);
        String sub0=str.substring(6,9);
        salida=sub2+"."+sub1+"."+sub0;
        }
        if (longitud ==10 ){
        String sub2=str.substring(0,1);
        String sub1=str.substring(1,4);
        String sub0=str.substring(4,7);
        String sub=str.substring(7,10);
        salida=sub2+"."+sub1+"."+sub0+"."+sub;
        }
        if (longitud ==11 ){
        String sub2=str.substring(0,2);
        String sub1=str.substring(2,5);
        String sub0=str.substring(5,8);
        String sub=str.substring(8,11);
        salida=sub2+"."+sub1+"."+sub0+"."+sub;
        }
        if (longitud ==12 ){
        String sub2=str.substring(0,3);
        String sub1=str.substring(3,6);
        String sub0=str.substring(6,9);
        String sub=str.substring(9,12);
        salida=sub2+"."+sub1+"."+sub0+"."+sub;
        }
        if (longitud ==13 ){
        String sub2=str.substring(0,1);
        String sub1=str.substring(1,4);
        String sub0=str.substring(4,7);
        String sub=str.substring(7,10);
        String su=str.substring(10,13);
        salida=sub2+"."+sub1+"."+sub0+"."+sub+"."+su;
        }
        return salida;
        }
    
    public static Color rgbAColor(String rgb) {
        int r = 0;
        int g = 0;
        int b = 0;
        String[] color = rgb.split("-");
        
        r = Integer.parseInt(color[0]);
        g = Integer.parseInt(color[1]);
        b = Integer.parseInt(color[2]);
        Color ret = new Color(r,g,b);
        
        return ret;
    }
    
    public static String pMayus(String str) {
        if (!str.equals("")) {
            return str.toUpperCase().charAt(0) + str.substring(1, str.length()).toLowerCase();
        } else {
            return str;
        }
         
    }
    
    public static String formatearNumRecibo(String num) {
        String ret = "";
        if (num.length()==1) {
            ret = "00000"+num;
        }
        if (num.length()==2) {
            ret = "0000"+num;
        }
        if (num.length()==3) {
            ret = "000"+num;
        }
        if (num.length()==4) {
            ret = "00"+num;
        }
        if (num.length()==5) {
            ret = "0"+num;
        }
        
        
        
        
        return ret;
    }
    
    public static String getAlphaNumericString(int n) 
    { 
  
        // chose a Character random from this String 
        String AlphaNumericString = "0123456789"
                                    + "abcdefghijklmnopqrstuvxyz"; 
  
        // create StringBuffer size of AlphaNumericString 
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            // generate a random number between 
            // 0 to AlphaNumericString variable length 
            int index 
                = (int)(AlphaNumericString.length() 
                        * Math.random()); 
  
            // add Character one by one in end of sb 
            sb.append(AlphaNumericString 
                          .charAt(index)); 
        } 
  
        return sb.toString(); 
    } 
    
    //FUNCIONES NUMEROS
    public static Double formatearDecimales(Double numero, Integer numeroDecimales) {
        return Math.round(numero * Math.pow(10, numeroDecimales-1)) / Math.pow(10, numeroDecimales-1);
    }
    
    public static String importeDoubleALetras(double imp) {
        n2t numero;
        int importe = (int) imp;
	numero = new n2t(importe);
	return numero.convertirLetras(importe);
    }
    
    // FUNCIONESFECHAS 
    
    public static String getFechaTexto(Date fecha) {
        return getDiaSemana(fecha) +" "+ dateAString(fecha).substring(0,2) + " de " + getMes(fecha) + " de " + dateAString(fecha).substring(6,10);
    }
    
    public static String getDiaSemana(Date fecha) {
        String Valor_dia = null;
        GregorianCalendar cal = new GregorianCalendar();
	cal.setTime(fecha);
        
        int diaSemana = cal.get(Calendar.DAY_OF_WEEK);		
        
        GregorianCalendar fechaCalendario = new GregorianCalendar();
        fechaCalendario.setTime(fecha);
        if (diaSemana == 1) {
            Valor_dia = "Domingo";
        } else if (diaSemana == 2) {
            Valor_dia = "Lunes";
        } else if (diaSemana == 3) {
            Valor_dia = "Martes";
        } else if (diaSemana == 4) {
            Valor_dia = "Miércoles";
        } else if (diaSemana == 5) {
            Valor_dia = "Jueves";
        } else if (diaSemana == 6) {
            Valor_dia = "Viernes";
        } else if (diaSemana == 7) {
            Valor_dia = "Sábado";
        }
        return Valor_dia;
    }
    
    public static String getMes(Date fecha) {
        String Valor_Mes = null;
        GregorianCalendar cal = new GregorianCalendar();
	cal.setTime(fecha);
        
        String f = dateAString(fecha);
        String mes = f.substring(3, 5);
        int mesInt = Integer.parseInt(mes);		
        
        GregorianCalendar fechaCalendario = new GregorianCalendar();
        fechaCalendario.setTime(fecha);
        if (mesInt == 1) {
            Valor_Mes = "Enero";
        } else if (mesInt == 2) {
            Valor_Mes = "Febrero";
        } else if (mesInt == 3) {
            Valor_Mes = "Marzo";
        } else if (mesInt == 4) {
            Valor_Mes = "Abril";
        } else if (mesInt == 5) {
            Valor_Mes = "Mayo";
        } else if (mesInt == 6) {
            Valor_Mes = "Junio";
        } else if (mesInt == 7) {
            Valor_Mes = "Julio";
        } else if (mesInt == 8) {
            Valor_Mes = "Agosto";
        } else if (mesInt == 9) {
            Valor_Mes = "Septiembre";
        } else if (mesInt == 10) {
            Valor_Mes = "Octubre";
        } else if (mesInt == 11) {
            Valor_Mes = "Noviembre";
        } else if (mesInt == 12) {
            Valor_Mes = "Diciembre";
        }
        
        return Valor_Mes;
    }
    
    public static String getFechaCartonTresLetras(String fx) {
        Date fecha = stringADate(fx);
        String Valor_Mes = null;
        GregorianCalendar cal = new GregorianCalendar();
	cal.setTime(fecha);
        
        String f = dateAString(fecha);
        String mes = f.substring(3, 5);
        int mesInt = Integer.parseInt(mes);		
        
        GregorianCalendar fechaCalendario = new GregorianCalendar();
        fechaCalendario.setTime(fecha);
        if (mesInt == 1) {
            Valor_Mes = "Ene";
        } else if (mesInt == 2) {
            Valor_Mes = "Feb";
        } else if (mesInt == 3) {
            Valor_Mes = "Mar";
        } else if (mesInt == 4) {
            Valor_Mes = "Abr";
        } else if (mesInt == 5) {
            Valor_Mes = "May";
        } else if (mesInt == 6) {
            Valor_Mes = "Jun";
        } else if (mesInt == 7) {
            Valor_Mes = "Jul";
        } else if (mesInt == 8) {
            Valor_Mes = "Ago";
        } else if (mesInt == 9) {
            Valor_Mes = "Sep";
        } else if (mesInt == 10) {
            Valor_Mes = "Oct";
        } else if (mesInt == 11) {
            Valor_Mes = "Nov";
        } else if (mesInt == 12) {
            Valor_Mes = "Dic";
        }
        
        return fx.substring(0, 2)+"-"+Valor_Mes;
    }
    
    // FUNCIONESFECHAS 
    public static String devolverHoraActualStr() {
        Date hour = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
        return hourFormat.format(hour);
    }
    
    public static String devolverFechaActualStr() {
        Date date = new Date();
        SimpleDateFormat f= new SimpleDateFormat("dd/MM/yyyy");
        return f.format(date);
    }
    
    public static Date stringADate(String fecha) {
        
        SimpleDateFormat f= new SimpleDateFormat("dd/MM/yyyy");
        Date fechaTemp = new Date();
        try {
        fechaTemp = f.parse(fecha);
        } catch (ParseException ex) {
        ex.printStackTrace();
        }
        
        return  fechaTemp;
    }
    
    public static String dateAString(Date fecha) {
        
        SimpleDateFormat f= new SimpleDateFormat("dd/MM/yyyy");
        return  f.format(fecha);
    }
    
    public static Date sumarDias(Date fecha, int dias){
      if (dias==0) return fecha;
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(fecha); 
      calendar.add(Calendar.DAY_OF_YEAR, dias);  
      return calendar.getTime(); 
    }
    
    public static Date sumarMeses(Date fecha, int meses){
      if (meses==0) return fecha;
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(fecha); 
      calendar.add(Calendar.MONTH, meses);  
      
      return calendar.getTime(); 
    }
    

    public static boolean caeDomingo(Date d){
	boolean caeFindeX = false;
        GregorianCalendar cal = new GregorianCalendar();
	cal.setTime(d);
        
        int dia = cal.get(Calendar.DAY_OF_WEEK);		
        
        if (dia == 1) {
            caeFindeX = true;
        }
        
	return caeFindeX;
    }
    
    public static boolean caeSabado(Date d){
	boolean caeFindeX = false;
        GregorianCalendar cal = new GregorianCalendar();
	cal.setTime(d);
        
        int dia = cal.get(Calendar.DAY_OF_WEEK);		
        
        if (dia == 7) {
            caeFindeX = true;
        }
        
	return caeFindeX;
    }
    
    public static int ultimoDiaMes(String f) {
        String mes = f.substring(3, 5);
        String ano = f.substring(6, 10);
        
        Calendar calendario=Calendar.getInstance();
        calendario.set(Integer.parseInt(ano), Integer.parseInt(mes)-1, 1);
        return calendario.getActualMaximum(Calendar.DAY_OF_MONTH);
    }    
    
    public static boolean horaValida(String hh, String mm) {
        boolean valida = true;
        
        if (Integer.parseInt(hh)>23) {
            valida = false;
        }
        if (Integer.parseInt(mm)>59) {
            valida = false;
        }
        
        if (hh.length()>2 || hh.length()<=1) {
            valida = false;
        }
        
        if (mm.length()>2 || mm.length()<=1) {
            valida = false;
        }
       
        for (int i = 0; i<hh.length();i++) {
            if (!Character.isDigit(hh.charAt(i))) {
                valida = false;
            }
        }
        for (int i = 0; i<mm.length();i++) {
            if (!Character.isDigit(mm.charAt(i))) {
                valida = false;
            }
        }
        
        return valida;
        
    }
    
    //
    // FUNCIONES USUARIOS
    //
    
    public static String tipoUsuario(ArrayList<Usuario> usuarios, String nombre) {
        String tipo = "";
        for (Usuario usuario: usuarios) {
            if (usuario.getNombre().toLowerCase().equals(nombre.toLowerCase())) {
                tipo = usuario.getTipo();
            }
        }
        return tipo;
    }
    
    public static Boolean checkPassword(ArrayList<Usuario> usuarios, String nombre, String password) {
        for (Usuario usuario: usuarios) {
            if (usuario.getNombre().toLowerCase().equals(nombre.toLowerCase()) && usuario.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
        
    }
    
    public static Boolean serialValido(String serial) {
        Boolean valido = false;
        if (serial.equals(Principal.zaracatunga) || serial.equals(Principal.serialretail)) {
            valido = true;
        }
        return valido;
    }
    
    
    public static int returnEpochActual() {
        long epochTime = 1;
        Date today = new Date();
 
		// Constructs a SimpleDateFormat using the given pattern
		SimpleDateFormat crunchifyFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
 
		// format() formats a Date into a date/time string.
		String currentTime = crunchifyFormat.format(today);
	
 
		try {
 
			// parse() parses text from the beginning of the given string to produce a date.
			Date date = crunchifyFormat.parse(currentTime);
 
			// getTime() returns the number of milliseconds since January 1, 1970, 00:00:00 GMT represented by this Date object.
			 epochTime = date.getTime();
 
			
 
		} catch (ParseException e) {
			e.printStackTrace();
		}
                
                return toIntExact(epochTime/1000l);
        
    }
  
}

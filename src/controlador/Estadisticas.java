/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.ArrayList;
import java.util.Date;
import modelo.Cuota;
import modelo.Prestamo;
import vista.Principal;

/**
 *
 * @author Will
 */
public class Estadisticas {
    
    public static ArrayList<Cuota> getCuotasEntreFechas(Date inicio, Date fin) {
        ArrayList<Cuota> cuotas = new ArrayList<>();
        for (Cuota c: Principal.todasLasCuotas) {
            if ((c.getFecha().before(fin) && c.getFecha().after(inicio)) || (c.getFecha().equals(fin) || c.getFecha().equals(inicio))) {
                        cuotas.add(c);
            }
            
        }
        
        return cuotas;
        
    }
    
    public static Double getImporteCobradoEntreFechas(Date inicio, Date fin) {
        Double totalCobrado = 0.0;
        for (Cuota c: Principal.todasLasCuotas) {
            if ((c.getFecha().before(fin) && c.getFecha().after(inicio)) || (c.getFecha().equals(fin) || c.getFecha().equals(inicio))) {
                        totalCobrado += Double.parseDouble(c.getImportePagado());
            }
            
        }
        
        return totalCobrado;
        
    }
    
    public static Double getImportePrestadoEntreFechas(Date inicio, Date fin) {
        Double totalPrestado = 0.0;
        for (Prestamo p: Principal.todosLosPrestamos) {
            
            if ((p.getFecha().before(fin) && p.getFecha().after(inicio)) || p.getFecha().equals(fin) || p.getFecha().equals(inicio)) {
                totalPrestado += Double.parseDouble(Funciones.getPlanPorNombre(p.getPlan()).getImportePrestar());
            }
        }
        
        return totalPrestado;
        
    }
    
    public static Double getImporteInteresEntreFechas(Date inicio, Date fin) {
        Double totalInt = 0.0;
        for (Prestamo p: Principal.todosLosPrestamos) {
            
            if ((p.getFecha().before(fin) && p.getFecha().after(inicio)) || p.getFecha().equals(fin) || p.getFecha().equals(inicio)) {
                totalInt += Double.parseDouble(Funciones.getPlanPorNombre(p.getPlan()).getImporteInteres());
            }
        }
        
        return totalInt;
        
    }
   
    public static Double getImporteAdeudadoEntreFechas(Date inicio, Date fin) {
        Double totalDeuda = 0.0;
        for (Cuota c: Principal.todasLasCuotas) {
            
            if ((c.getFechaPagarDate().before(fin) && c.getFechaPagarDate().after(inicio)) || (c.getFechaPagarDate().equals(fin) || c.getFechaPagarDate().equals(inicio))) {
                        
            
                        Double temp = Double.parseDouble(c.getImporteTotal())-Double.parseDouble(c.getImportePagado());
                        if (temp>0) {
                           totalDeuda += temp;
                        }
                        
            }
        }
        
        return totalDeuda;
        
    }
}

package controlador;

import java.io.File;
import java.io.PrintStream;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import jxl.Sheet;
import jxl.Workbook;

import modelo.Plan;
import vista.Principal;





public class ImportarPlanesXLS
{
  
 
    
          
  public static void leerArchivoExcel(String archivoDestino)
  {
    
    ArrayList<Plan> planes = new ArrayList<Plan>();
    

    Plan plan = new Plan();
    try
    {
      Workbook archivoExcel = Workbook.getWorkbook(new File(archivoDestino));
      System.out.println("Número de Hojas\t" + archivoExcel.getNumberOfSheets());
      for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets(); sheetNo++)
      {


        Sheet hoja = archivoExcel.getSheet(sheetNo);
        int numColumnas = hoja.getColumns();
        int numFilas = hoja.getRows();
        
        System.out.println("Nombre de la Hoja\t" + archivoExcel
          .getSheet(sheetNo).getName());
        for (int fila = 1; fila < numFilas; fila++)
        {
           
          plan = new Plan();

          for (int columna = 0; columna < numColumnas; columna++)
          {
            
            if (columna == 0) {
                plan.setNombre(hoja.getCell(columna, fila).getContents().toLowerCase());
            }
            if (columna == 1) {
              if(hoja.getCell(columna, fila).getContents().equals("1")) {
                  plan.setCalculaPor("porc");
              }
            }
            if (columna == 2) {
              if(hoja.getCell(columna, fila).getContents().equals("1")) {
                  plan.setCalculaPor("imp");
              }
            }
            if (columna == 3) {
              plan.setImportePrestar(hoja.getCell(columna, fila).getContents());
            }
            if (columna == 4) {
              plan.setImporteDevolver(hoja.getCell(columna, fila).getContents());
            }
            if (columna == 5) {
              if (plan.getPorcentajeInteres().equals("porc")) {
                  plan.setPorcentajeInteres(hoja.getCell(columna, fila).getContents());
              }
            }
            if (columna == 6) {
              if(hoja.getCell(columna, fila).getContents().equals("1")) {
                  plan.setTipo("diario");
              }
            }
            if (columna == 7) {
              if(hoja.getCell(columna, fila).getContents().equals("1")) {
                  plan.setTipo("semanal");
              }
            }
            if (columna == 8) {
              if(hoja.getCell(columna, fila).getContents().equals("1")) {
                  plan.setTipo("mensual");
              }
            }
            if (columna == 9) {
              plan.setCantCuotas(hoja.getCell(columna, fila).getContents());
                  
            }
            
            
          }
          
          
          plan.calcularImporteInt();
          plan.calcularPorcInt();
          planes.add(plan);
          
        }
      }
      
      Principal.workerImpPlanes(planes);
      
      
    } catch (Exception ioe) {
      ioe.printStackTrace();
    }
  }
}

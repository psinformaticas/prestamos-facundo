/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import aplicacion.ActFallidaUsuario;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Blob;
import modelo.*;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import vista.Principal;
import static vista.Principal.planes;

/**
 *
 * @author Will
 */
public class Conector {
    
    	
	Connection conexion;
	private OpcionesDB opt;
        
      public Conector() {
          opt = Funciones.leerOptDB();
          opt = Funciones.leerOptDB();
          
      }
      
      public void cargarOpcionesIni() {
          opt = Funciones.leerOptDB();
          opt = Funciones.leerOptDB();
      }
      
    public OpcionesDB getOpt() {
        return opt;
    }

    public void setOpt(OpcionesDB opt) {
        this.opt = opt;
    }
        
      
    public void conectar(){

        	 try {
                 Class.forName("com.mysql.cj.jdbc.Driver");
                    conexion = DriverManager.getConnection("jdbc:mysql://"+opt.getSrv()+":"+opt.getPort()+"/" + opt.getBd(), opt.getUsr(), opt.getPass());
                    //System.out.println("Se ha iniciado la conexión con el servidor de forma exitosa");
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
//                    opt.setActivo("no");
//                    Funciones.guardaOptWeb(opt);
                    JOptionPane.showMessageDialog(null,"Imposible conectar. \nRevise la conexión a la red.\n\nES PROBABLE QUE DEBA REINICIAR EL PROGRAMA", "Error de conexion", JOptionPane.ERROR_MESSAGE);	
                    Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
//                    System.exit(0);
                 }
        
    }
    
    public void cerrar(){
		    try {
                        conexion.close();
                        //System.out.println("Se ha finalizado la conexión con el servidor");
                    } catch (SQLException ex) {
                        Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
                    }
	}
    
    public void borrarPrestamosYCuotas() {
        try {
                PreparedStatement st;
                st = conexion.prepareStatement("truncate table prestamos");
                st.execute();
                
                st = conexion.prepareStatement("truncate table cuotas");
                st.execute();
                
                    //LOGGEA
                    try {
                        Log log = new Log();
                        log.setAccion("Borra todos los prestamos y cuotas");
                        
                        guardarLog(log);
                    } catch (Exception e) {
                        
                    }
                    
                JOptionPane.showMessageDialog(null,"Préstamos y cuotas borrados correctamente.", "OK!", 1);
        }
	catch (SQLException ex) {
        	JOptionPane.showMessageDialog(null,"Error al borrar prestamos y cuotas, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    }
        
    public void borrarPlanes() {
        try {
                PreparedStatement st;
                st = conexion.prepareStatement("truncate table planes");
                st.execute();
              
                                    //LOGGEA
                    try {
                        Log log = new Log();
                        log.setAccion("Borra todos los planes");
                        
                        guardarLog(log);
                    } catch (Exception e) {
                        
                    }

                    
                JOptionPane.showMessageDialog(null,"Planes borrados correctamente.", "OK!", 1);
        }
	catch (SQLException ex) {
        	JOptionPane.showMessageDialog(null,"Error al borrar planes, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    }
    
    public void borrarLogs() {
        try {
                PreparedStatement st;
                st = conexion.prepareStatement("truncate table logs");
                st.execute();
              
                //LOGGEA
                    try {
                        Log log = new Log();
                        log.setAccion("Vacia historial de actividades");
                        
                        guardarLog(log);
                    } catch (Exception e) {
                        
                    }

                    
                JOptionPane.showMessageDialog(null,"Historial borrado correctamente.", "OK!", 1);
        }
	catch (SQLException ex) {
        	JOptionPane.showMessageDialog(null,"Error al borrar historial, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    }
    
    public void borrarLocalidades() {
        try {
                PreparedStatement st;
                st = conexion.prepareStatement("truncate table localidades");
                st.execute();
                
                                    //LOGGEA
                    try {
                        Log log = new Log();
                        log.setAccion("Borra todas las localidades");
                        
                        guardarLog(log);
                    } catch (Exception e) {
                        
                    }

                    
                JOptionPane.showMessageDialog(null,"Localidades borradas correctamente.", "OK!", 1);
        }
	catch (SQLException ex) {
        	JOptionPane.showMessageDialog(null,"Error al borrar localidades, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    }
    
    public void borrarCobradores() {
        try {
                PreparedStatement st;
                st = conexion.prepareStatement("truncate table cobradores");
                st.execute();
                
                                    //LOGGEA
                    try {
                        Log log = new Log();
                        log.setAccion("Borra todos los cobradores");
                        
                        guardarLog(log);
                    } catch (Exception e) {
                        
                    }

                    
                JOptionPane.showMessageDialog(null,"Cobradores borrados correctamente.", "OK!", 1);
        }
	catch (SQLException ex) {
        	JOptionPane.showMessageDialog(null,"Error al borrar cobradores, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    }
    
    public void borrarClientes() {
        try {
                PreparedStatement st;
                st = conexion.prepareStatement("truncate table clientes");
                st.execute();
                
                                    //LOGGEA
                    try {
                        Log log = new Log();
                        log.setAccion("Borra todos los clientes");
                        
                        guardarLog(log);
                    } catch (Exception e) {
                        
                    }

                
                JOptionPane.showMessageDialog(null,"Clientes borrados correctamente.", "OK!", 1);
                
        }
	catch (SQLException ex) {
        	JOptionPane.showMessageDialog(null,"Error al borrar clientes, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    }
    
    public void vaciarLaBDCuidado() {
        try {
                borrarPrestamosYCuotas();
                borrarPlanes();
                borrarClientes();
                PreparedStatement st;
                st = conexion.prepareStatement("truncate table usuarios");
                st.execute();
                
                JOptionPane.showMessageDialog(null,"Usuarios borrados correctamente.", "OK!", 1);
                
        }
	catch (SQLException ex) {
        	JOptionPane.showMessageDialog(null,"Error al borrar clientes, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    }
        
     //
    //  FUNC. CLIENTES
    //
    
    public void guardarCliente(Cliente cliente){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("insert into clientes(nombre, fechanac, dni, direccion, localidad, telfijo, telcel, email, observacion, codprestamos, fechaAlta, ref1, ref2, ref3, ref4, a1, a2, a3, esclientenuevo) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		    //Guarda en BD strings en minusculas
                    st.setString(1, cliente.getNombre().toLowerCase());
		    st.setString(2, cliente.getFechaNac());
		    st.setString(3, cliente.getDni());
                    st.setString(4, cliente.getDireccion().toLowerCase());
		    st.setString(5, cliente.getLocalidad().toLowerCase());
                    st.setString(6, cliente.getTelefono().toLowerCase());
                    st.setString(7, cliente.getCelular().toLowerCase());
                    st.setString(8, cliente.getEmail().toLowerCase());
                    st.setString(9, cliente.getObservaciones().toLowerCase());
                    st.setString(10, cliente.getCodPrestamos());
                    st.setString(11, Funciones.devolverFechaActualStr());
                    st.setString(12, cliente.getRef1().toLowerCase());
                    st.setString(13, cliente.getRef2().toLowerCase());
                    st.setString(14, cliente.getRef3().toLowerCase());
                    st.setString(15, cliente.getRef4().toLowerCase());
                    st.setString(16, cliente.getA1().toLowerCase());
                    st.setString(17, cliente.getA2().toLowerCase());
                    st.setString(18, cliente.getA3().toLowerCase());
                    st.setString(19, cliente.getEsClienteNuevo());
		    st.execute();
		    
                    
                    
                    //LOGGEA
                    if (!Principal.adminPsi) {
                        try {
                            Log l = new Log();
                            l.setAccion("Nuevo Cliente");
                            l.setDetalles("CLIENTE: "+cliente.getNombre());

                            guardarLog(l);
                        } catch (Exception e) {

                        }
                    }
                }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al guardar cliente, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    
    public Cliente getClientePorCod(int cod){
        Cliente cliente = new Cliente();     
		ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from clientes where cod = '"+ cod + "'");
            result = st.executeQuery();
            
            while (result.next()) {
                    cliente.setCod(result.getInt("cod"));
                    cliente.setNombre(result.getString("nombre"));
		    cliente.setFechaNac(result.getString("fechanac"));
		    cliente.setDni(result.getString("dni"));
                    cliente.setDireccion(result.getString("direccion"));
		    cliente.setLocalidad(result.getString("localidad"));
                    cliente.setTelefono(result.getString("telfijo"));
                    cliente.setCelular(result.getString("telcel"));
                    cliente.setEmail(result.getString("email"));
                    cliente.setObservaciones(result.getString("observacion"));
                    cliente.setCodPrestamos(result.getString("codprestamos"));
                    cliente.setFechaAlta(result.getString("fechaAlta"));
                    cliente.setRef1(result.getString("ref1"));
                    cliente.setRef2(result.getString("ref2"));
                    cliente.setRef3(result.getString("ref3"));
                    cliente.setRef4(result.getString("ref4"));
                    cliente.setA1(result.getString("a1"));
                    cliente.setA2(result.getString("a2"));
                    cliente.setA3(result.getString("a3"));
                    cliente.setUltimoPago(result.getString("ultimopago"));
                    cliente.setEsClienteNuevo(result.getString("esclientenuevo"));
            }
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error al leer cliente por codigo, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return cliente;
    }
    
    public Cliente getClientePorNombre(String nombre){
        Cliente cliente = null;     
		ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from clientes where nombre = '"+ nombre.toLowerCase() + "'");
            result = st.executeQuery();
            
            while (result.next()) {
                cliente = new Cliente();
                    cliente.setCod(result.getInt("cod"));
                    cliente.setNombre(result.getString("nombre"));
		    cliente.setFechaNac(result.getString("fechanac"));
		    cliente.setDni(result.getString("dni"));
                    cliente.setDireccion(result.getString("direccion"));
		    cliente.setLocalidad(result.getString("localidad"));
                    cliente.setTelefono(result.getString("telfijo"));
                    cliente.setCelular(result.getString("telcel"));
                    cliente.setEmail(result.getString("email"));
                    cliente.setObservaciones(result.getString("observacion"));
                    cliente.setCodPrestamos(result.getString("codprestamos"));
                    cliente.setFechaAlta(result.getString("fechaAlta"));
                    cliente.setRef1(result.getString("ref1"));
                    cliente.setRef2(result.getString("ref2"));
                    cliente.setRef3(result.getString("ref3"));
                    cliente.setRef4(result.getString("ref4"));
                    cliente.setA1(result.getString("a1"));
                    cliente.setA2(result.getString("a2"));
                    cliente.setA3(result.getString("a3"));
                    cliente.setUltimoPago(result.getString("ultimopago"));
                    cliente.setEsClienteNuevo(result.getString("esclientenuevo"));
                }
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error al leer cliente por nombre, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return cliente;
    }
    
    public void eliminarClientePorCod(int cod) {
		try {
                        
                    //LOGGEA
                    if (!Principal.adminPsi) {
                        try {
                            Cliente c = Funciones.getClientePorCod(Principal.clientes, cod);
                            Log l = new Log();
                            l.setAccion("Elimina Cliente");
                            l.setDetalles("CLIENTE: "+c.getNombre());

                            guardarLog(l);
                        } catch (Exception e) {

                        }
                    }
                    PreparedStatement st = conexion.prepareStatement("delete from clientes where cod = '"+ cod + "'");
                    st.executeUpdate();
		} catch (SQLException ex) {
            		JOptionPane.showMessageDialog(null,"Error al eliminar cliente, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
    
    public void actualizarCliente(Cliente cliente){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("update clientes set nombre = ?, fechanac = ?, dni = ?, direccion = ?, localidad = ?,"
                            + " telfijo = ?, telcel = ?, email = ?, observacion = ?, codprestamos = ?, fechaAlta = ?, ref1 = ?, ref2 = ?, ref3 = ?, ref4 = ?, a1 = ?, a2 = ?, a3 = ?, esclientenuevo = ? where cod = '"+ cliente.getCod() + "'");
		    
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, cliente.getNombre().toLowerCase());
		    st.setString(2, cliente.getFechaNac());
		    st.setString(3, cliente.getDni());
                    st.setString(4, cliente.getDireccion().toLowerCase());
		    st.setString(5, cliente.getLocalidad().toLowerCase());
                    st.setString(6, cliente.getTelefono().toLowerCase());
                    st.setString(7, cliente.getCelular().toLowerCase());
                    st.setString(8, cliente.getEmail().toLowerCase());
                    st.setString(9, cliente.getObservaciones().toLowerCase());
                    st.setString(10, cliente.getCodPrestamos());
                    st.setString(11, Funciones.devolverFechaActualStr());
                    st.setString(12, cliente.getRef1().toLowerCase());
                    st.setString(13, cliente.getRef2().toLowerCase());
                    st.setString(14, cliente.getRef3().toLowerCase());
                    st.setString(15, cliente.getRef4().toLowerCase());
                    st.setString(16, cliente.getA1().toLowerCase());
                    st.setString(17, cliente.getA2().toLowerCase());
                    st.setString(18, cliente.getA3().toLowerCase());
                    st.setString(19, cliente.getEsClienteNuevo());
                    
		    st.execute();
		    
                    //LOGGEA
                    if (!Principal.adminPsi) {
                        try {
                            Log l = new Log();
                            l.setAccion("Actualiza datos cliente");
                            l.setDetalles("CLIENTE: "+cliente.getNombre());

                            guardarLog(l);
                        } catch (Exception e) {

                        }
                    }
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al actualizar cliente, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    public void actualizarRefCliente(Cliente cliente){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("update clientes set ref1 = ?, ref2 = ?, ref3 = ?, ref4 = ?  where cod = '"+ cliente.getCod() + "'");
		    
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, cliente.getRef1().toLowerCase());
                    st.setString(2, cliente.getRef2().toLowerCase());
                    st.setString(3, cliente.getRef3().toLowerCase());
                    st.setString(4, cliente.getRef4().toLowerCase());
		    st.execute();
		    
                    //LOGGEA
                    if (!Principal.adminPsi) {
                        try {
                            Log l = new Log();
                            l.setAccion("Actualiza referencias cliente");
                            l.setDetalles("CLIENTE: "+cliente.getNombre());

                            guardarLog(l);
                        } catch (Exception e) {

                        }
                    }
                    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al actualizar cliente, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    public void actualizarUltimoPago(Cliente cliente, String ultimoPago){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("update clientes set ultimopago = ? where cod = '"+ cliente.getCod() + "'");
		    
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, ultimoPago);
                    
		    st.execute();
		    
                    //LOGGEA
                    if (!Principal.adminPsi) {
                        try {
                            Log l = new Log();
                            l.setAccion("Actualiza último pago de cliente");
                            l.setDetalles("CLIENTE: "+cliente.getNombre()+" ULT. PAGO: "+ultimoPago);

                            guardarLog(l);
                        } catch (Exception e) {

                        }
                    }
                    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al actualizar ultimo pago de cliente, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    
    public ArrayList<Cliente> getClientes(){
            ArrayList<Cliente> clientes = new ArrayList<Cliente>();
                    ResultSet result = null;
            try {
                PreparedStatement st = conexion.prepareStatement("select * from clientes");
                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {
                    Cliente cliente = new Cliente();         	

                    cliente.setCod(result.getInt("cod"));
                    cliente.setNombre(result.getString("nombre"));
		    cliente.setFechaNac(result.getString("fechanac"));
		    cliente.setDni(result.getString("dni"));
                    cliente.setDireccion(result.getString("direccion"));
		    cliente.setLocalidad(result.getString("localidad"));
                    cliente.setTelefono(result.getString("telfijo"));
                    cliente.setCelular(result.getString("telcel"));
                    cliente.setEmail(result.getString("email"));
                    cliente.setObservaciones(result.getString("observacion"));
                    cliente.setCodPrestamos(result.getString("codprestamos"));
                    cliente.setFechaAlta(result.getString("fechaAlta"));
                    cliente.setRef1(result.getString("ref1"));
                    cliente.setRef2(result.getString("ref2"));
                    cliente.setRef3(result.getString("ref3"));
                    cliente.setRef4(result.getString("ref4"));                                    
                    cliente.setA1(result.getString("a1"));
                    cliente.setA2(result.getString("a2"));
                    cliente.setA3(result.getString("a3"));
                    cliente.setUltimoPago(result.getString("ultimopago"));
                    cliente.setEsClienteNuevo(result.getString("esclientenuevo"));
                    
                    clientes.add(cliente);
            }

            } catch (SQLException ex) {
                		JOptionPane.showMessageDialog(null,"Error al cargar clientes, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return clientes;
    }
    
    public boolean clienteEstaGuardado(String nombre) {
        ArrayList<Cliente> clientes = new ArrayList<Cliente>();
        Cliente cliente = new Cliente();
        clientes = getClientes();
        
        for(int i = 0; i < clientes.size(); i++) {
            cliente = clientes.get(i);
            if (cliente.getNombre().equals(nombre)) {
                return true;
            }
        }
        
        return false;
    }
    
    
    
    //
    //  FUNC. COBRADORES
    //
    
    public void guardarCobrador(Cobrador c){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("insert into cobradores(nombre, codint, telefono, obs, a1, a2, a3, a4, nombre_app) values (?,?,?,?,?,?,?,?,?)");
		    //Guarda en BD strings en minusculas
                    st.setString(1, c.getNombre().toLowerCase());
		    st.setString(2, c.getCodInt());
		    st.setString(3, c.getTelefono());
                    st.setString(4, c.getObs());
                    st.setString(5, c.getA1());
                    st.setString(6, c.getA2());
                    st.setString(7, c.getA3());
                    st.setString(8, c.getA4());
                    st.setString(9, c.getNombreApp());
		    st.execute();
		    
                    //LOGGEA
                    try {
                        Log l = new Log();
                        l.setAccion("Guarda nuevo cobrador");
                        l.setDetalles("NOMCOB: "+c.getNombre());
                        
                        guardarLog(l);
                    } catch (Exception e) {
                        
                    }

                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al guardar cobrador, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    
    public Cobrador getCobradorPorCod(int cod){
        Cobrador c = new Cobrador();     
		ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from cobradores where cod = '"+ cod + "'");
            result = st.executeQuery();
            
            while (result.next()) {
                    c.setCod(result.getInt("cod"));
                    c.setNombre(result.getString("nombre"));
                    c.setCodInt(result.getString("codint"));
                    c.setTelefono(result.getString("telefono"));
                    c.setObs(result.getString("obs"));
                    c.setA1(result.getString("a1"));
                    c.setA2(result.getString("a2"));
                    c.setA3(result.getString("a3"));
                    c.setA4(result.getString("a4"));
                    c.setNombreApp(result.getString("nombre_app"));
            }
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error al leer cobrador por codigo, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return c;
    }
    
    public Cobrador getCobradorPorNombre(String nombre){
        Cobrador c = new Cobrador();     
		ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from cobradores where nombre = '"+ nombre.toLowerCase() + "'");
            result = st.executeQuery();
            
            while (result.next()) {
                    c.setCod(result.getInt("cod"));
                    c.setNombre(result.getString("nombre"));
                    c.setCodInt(result.getString("codint"));
                    c.setTelefono(result.getString("telefono"));
                    c.setObs(result.getString("obs"));
                    c.setA1(result.getString("a1"));
                    c.setA2(result.getString("a2"));
                    c.setA3(result.getString("a3"));
                    c.setA4(result.getString("a4"));
                    c.setNombreApp(result.getString("nombre_app"));
            }
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error al leer cobrador por nombre, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return c;
    }
    
    public void eliminarCobradorPorCod(int cod) {
		try {
                    //LOGGEA
                    try {
                        Cobrador c = Funciones.getCobradorPorCod(Principal.cobradores, cod);
                        Log l = new Log();
                        l.setAccion("Guarda nuevo cobrador");
                        l.setDetalles("NOMCOB: "+c.getNombre());
                        
                        guardarLog(l);
                    } catch (Exception e) {
                        
                    }	
                    
                    PreparedStatement st = conexion.prepareStatement("delete from cobradores where cod = '"+ cod + "'");
			st.executeUpdate();
                        
                        
		} catch (SQLException ex) {
            		JOptionPane.showMessageDialog(null,"Error al eliminar cobrador, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
    
    public void actualizarCobrador(Cobrador c){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("update cobradores set nombre = ?, codint = ?, telefono = ?, obs = ?, a1 = ?,"
                            + " a2 = ?, a3 = ?, a4 = ?, nombre_app = ? where cod = '"+ c.getCod() + "'");
		    
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, c.getNombre().toLowerCase());
		    st.setString(2, c.getCodInt());
		    st.setString(3, c.getTelefono());
                    st.setString(4, c.getObs());
                    st.setString(5, c.getA1());
                    st.setString(6, c.getA2());
                    st.setString(7, c.getA3());
                    st.setString(8, c.getA4());
                    st.setString(9, c.getNombreApp());
		    st.execute();
		    
                    //LOGGEA
                    try {
                        Log l = new Log();
                        l.setAccion("Actualiza datos cobrador");
                        l.setDetalles("NOMCOB: "+c.getNombre());
                        
                        guardarLog(l);
                    } catch (Exception e) {
                        
                    }	
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al actualizar cobrador, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    public ArrayList<Cobrador> getCobradores(){
            ArrayList<Cobrador> cs = new ArrayList<Cobrador>();
                    ResultSet result = null;
            try {
                PreparedStatement st = conexion.prepareStatement("select * from cobradores");
                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {
                    Cobrador c = new Cobrador();         	

                    c.setCod(result.getInt("cod"));
                    c.setNombre(result.getString("nombre"));
                    c.setCodInt(result.getString("codint"));
                    c.setTelefono(result.getString("telefono"));
                    c.setObs(result.getString("obs"));
                    c.setA1(result.getString("a1"));
                    c.setA2(result.getString("a2"));
                    c.setA3(result.getString("a3"));
                    c.setA4(result.getString("a4"));
                    c.setNombreApp(result.getString("nombre_app"));
                    
                    cs.add(c);
            }

            } catch (SQLException ex) {
                		JOptionPane.showMessageDialog(null,"Error al cargar cobradores, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return cs;
    }
    
    public boolean cobradorEstaGuardado(String nombre, String codInt) {
        ArrayList<Cobrador> cs = new ArrayList<Cobrador>();
        Cobrador c = new Cobrador();
        cs = getCobradores();
        
        for(int i = 0; i < cs.size(); i++) {
            c = cs.get(i);
            if (c.getNombre().equals(nombre)) {
                return true;
            }
            if (c.getCodInt().equals(codInt)) {
                return true;
            }
        }
        
        return false;
    }
    
    
    //
    //  FUNC. PLANES
    //
    
    public void guardarPlan(Plan plan){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("insert into planes(nombre, importeprestar, importedevolver, importeinteres, calculapor, tipo, cantcuotas, interesporc, porccobrador, a1, a2, a3, a4, orden) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		    //Guarda en BD strings en minusculas
                    st.setString(1, plan.getNombre().toLowerCase());
		    st.setString(2, plan.getImportePrestar());
                    st.setString(3, plan.getImporteDevolver());
                    st.setString(4, plan.getImporteInteres());
                    st.setString(5, plan.getCalculaPor());
                    st.setString(6, plan.getTipo());
                    st.setString(7, plan.getCantCuotas());
                    st.setString(8, plan.getPorcentajeInteres());
                    st.setString(9, plan.getPorcentajeCobrador());
                    st.setString(10, plan.getA1());
                    st.setString(11, plan.getA2());
                    st.setString(12, plan.getA3());
                    st.setString(13, plan.getA4());
                     int maxOrden = 1;
                    for (Plan px: planes) {
                        if (!px.getA4().equalsIgnoreCase("personalizado")) {
                            maxOrden ++;
                        }
                    }
                    st.setInt(14, maxOrden);
		    st.execute();
		    
                    //LOGGEA
                    try {
                        Log l = new Log();
                        if (plan.getA4().equalsIgnoreCase("personalizado")) {
                            l.setAccion("Guarda nuevo plan personalizado");
                        } else {
                            l.setAccion("Guarda nuevo plan");
                        }
                        
                        l.setDetalles("PLAN: "+plan.getNombre()+ " IMPPRESTAR: "+plan.getImportePrestar()+ " IMPDEVOLVER: "+plan.getImporteDevolver()+ " CUOTAS: "+plan.getCantCuotas());
                        
                        guardarLog(l);
                    } catch (Exception e) {
                        
                    }	
                    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al guardar plan, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    
    public Plan getPlanPorCod(int cod){
        Plan plan = new Plan();     
		ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from planes where cod = '"+ cod + "'");
            result = st.executeQuery();
            while (result.next()) {
                    plan.setCod(result.getInt("cod"));
                    plan.setNombre(result.getString("nombre"));
                    plan.setImportePrestar(result.getString("importeprestar"));
                    plan.setImporteDevolver(result.getString("importedevolver"));
                    plan.setImporteInteres(result.getString("importeinteres"));
                    plan.setCalculaPor(result.getString("calculapor"));
                    plan.setTipo(result.getString("tipo"));
                    plan.setCantCuotas(result.getString("cantcuotas"));
                    plan.setPorcentajeInteres(result.getString("interesporc"));
                    plan.setPorcentajeCobrador(result.getString("porccobrador"));
                    plan.setA1(result.getString("a1"));
                    plan.setA2(result.getString("a2"));
                    plan.setA3(result.getString("a3"));
                    plan.setA4(result.getString("a4"));
                    plan.setOrden(result.getInt("orden"));
            }        
        
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error al leer plan por codigo, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return plan;
    }
    
    public Plan getPlanPorNombre(String nom){
        Plan plan = new Plan();     
		ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from planes where nombre = '"+ nom.toLowerCase() + "'");
            result = st.executeQuery();
            
            while (result.next()) {
                    plan.setCod(result.getInt("cod"));
                    plan.setNombre(result.getString("nombre"));
                    plan.setImportePrestar(result.getString("importeprestar"));
                    plan.setImporteDevolver(result.getString("importedevolver"));
                    plan.setImporteInteres(result.getString("importeinteres"));
                    plan.setCalculaPor(result.getString("calculapor"));
                    plan.setTipo(result.getString("tipo"));
                    plan.setCantCuotas(result.getString("cantcuotas"));
                    plan.setPorcentajeInteres(result.getString("interesporc"));
                    plan.setPorcentajeCobrador(result.getString("porccobrador"));
                    plan.setA1(result.getString("a1"));
                    plan.setA2(result.getString("a2"));
                    plan.setA3(result.getString("a3"));
                    plan.setA4(result.getString("a4"));
                   
                    plan.setOrden(result.getInt("orden"));
            }
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error al leer plan por nombre, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return plan;
    }
    
    public void eliminarPlanPorCod(int cod) {
		try {
                    //LOGGEA
                    try {
                        Plan p = Funciones.getPlanPorCod(cod);
                        Log l = new Log();
                        l.setAccion("Elimina plan");
                        l.setDetalles("PLAN: "+p.getNombre()+ " IMPPRESTAR: "+p.getImportePrestar()+ " IMPDEVOLVER: "+p.getImporteDevolver()+ " CUOTAS: "+p.getCantCuotas());
                        
                        guardarLog(l);
                    } catch (Exception e) {
                        
                    }	
                    
                    PreparedStatement st = conexion.prepareStatement("delete from planes where cod = '"+ cod + "'");
			st.executeUpdate();
                        
                        
		} catch (SQLException ex) {
            		JOptionPane.showMessageDialog(null,"Error al eliminar plan, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
    
    public void eliminarPlanPorNombre(String nom) {
		try {
		//LOGGEA
                    try {
                        Plan p = Funciones.getPlanPorNombre(nom);
                        Log l = new Log();
                        l.setAccion("Elimina plan");
                        l.setDetalles("PLAN: "+p.getNombre()+ " IMPPRESTAR: "+p.getImportePrestar()+ " IMPDEVOLVER: "+p.getImporteDevolver()+ " CUOTAS: "+p.getCantCuotas());
                        
                        guardarLog(l);
                    } catch (Exception e) {
                        
                    }	
                    
                    PreparedStatement st = conexion.prepareStatement("delete from planes where nombre = '"+ nom.toLowerCase() + "'");
			st.executeUpdate();
                        
                        
		} catch (SQLException ex) {
            		JOptionPane.showMessageDialog(null,"Error al eliminar plan, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
    
    public void actualizarPlan(Plan plan){
		try {
                    PreparedStatement st = conexion.prepareStatement("update planes set nombre = ?, importeprestar = ?, importedevolver = ?, importeinteres = ?, calculapor = ?, tipo = ?, cantcuotas = ?, interesporc = ?, porccobrador = ?, a1 = ?, a2 = ?, a3 = ?, a4 = ?, orden = ? where cod = '"+ plan.getCod() + "'");
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, plan.getNombre().toLowerCase());
		    st.setString(2, plan.getImportePrestar());
                    st.setString(3, plan.getImporteDevolver());
                    st.setString(4, plan.getImporteInteres());
                    st.setString(5, plan.getCalculaPor());
                    st.setString(6, plan.getTipo());
                    st.setString(7, plan.getCantCuotas());
                    st.setString(8, plan.getPorcentajeInteres());
                    st.setString(9, plan.getPorcentajeCobrador());
                    st.setString(10, plan.getA1());
                    st.setString(11, plan.getA2());
                    st.setString(12, plan.getA3());
                    st.setString(13, plan.getA4());
                    st.setInt(14, plan.getOrden());
		    st.execute();
		    
                    //LOGGEA
                    try {
                        Log l = new Log();
                        if (plan.getA4().equalsIgnoreCase("personalizado")) {
                            l.setAccion("Actualiza datos plan personalizado");
                        } else {
                            l.setAccion("Actualiza datos plan");
                        }
                        
                        l.setDetalles("PLAN: "+plan.getNombre()+ " IMPPRESTAR: "+plan.getImportePrestar()+ " IMPDEVOLVER: "+plan.getImporteDevolver()+ " CUOTAS: "+plan.getCantCuotas());
                        
                        guardarLog(l);
                    } catch (Exception e) {
                        
                    }	
                    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al actualizar plan, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    public void actualizarOrdenPlan(Plan plan){
		try {
                    PreparedStatement st = conexion.prepareStatement("update planes set orden = ? where cod = '"+ plan.getCod() + "'");
                    //Guarda en BD strings en minusculas
                    st.setInt(1, plan.getOrden());
		    st.execute();
                    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al actualizar orden, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    public ArrayList<Plan> getPlanes(){
            ArrayList<Plan> planes = new ArrayList<Plan>();
                    ResultSet result = null;
            try {
                PreparedStatement st = conexion.prepareStatement("select * from planes");
                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {
                    Plan plan = new Plan();         	

                    plan.setCod(result.getInt("cod"));
                    plan.setNombre(result.getString("nombre"));
                    plan.setImportePrestar(result.getString("importeprestar"));
                    plan.setImporteDevolver(result.getString("importedevolver"));
                    plan.setImporteInteres(result.getString("importeinteres"));
                    plan.setCalculaPor(result.getString("calculapor"));
                    plan.setTipo(result.getString("tipo"));                                  
                    plan.setCantCuotas(result.getString("cantcuotas"));
                    plan.setPorcentajeInteres(result.getString("interesporc"));
                    plan.setPorcentajeCobrador(result.getString("porccobrador"));
                    plan.setA1(result.getString("a1"));
                    plan.setA2(result.getString("a2"));
                    plan.setA3(result.getString("a3"));
                    plan.setA4(result.getString("a4"));
                    plan.setOrden(result.getInt("orden"));
                    planes.add(plan);
            }

            } catch (SQLException ex) {
                		JOptionPane.showMessageDialog(null,"Error al cargar planes, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return planes;
    }
    
    //
    //  FUNC. PRESTAMOS
    //
    
    public void guardarPrestamo(Prestamo prestamo){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("insert into prestamos(plan, observacion, codcuotas, fechagenerado, codcliente, finalizado, codcobrador, a1, a2, a3, a4, a5, a6) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
		    //Guarda en BD strings en minusculas
                    st.setString(1, prestamo.getPlan().toLowerCase());
		    st.setString(2, prestamo.getObservacion());
                    st.setString(3, prestamo.getCodCuotasStr());
                    st.setString(4, prestamo.getFechaGenerado());
                    st.setString(5, prestamo.getCodCliente());
                    st.setInt(6, prestamo.getFinalizado());
                    st.setString(7, prestamo.getCodCobrador());
                    st.setString(8, prestamo.getImporteDescuento());
                    st.setString(9, prestamo.getA2());
                    st.setString(10, prestamo.getA3());
                    st.setString(11, prestamo.getA4());
                    st.setString(12, prestamo.getA5());
                    st.setString(13, prestamo.getA6());
                    
                    
		    st.execute();
		    
                    //LOGGEA
                    try {
                        Log l = new Log();
                        l.setAccion("Guarda nuevo prestamo");
                        l.setDetalles("CLIENTE: "+Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(prestamo.getCodCliente())).getNombre()+" PLAN: "+prestamo.getPlan());
                        
                        guardarLog(l);
                    } catch (Exception e) {
                        
                    }	
                    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al guardar prestamo, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    
    public Prestamo getPrestamoPorCod(int cod){
        Prestamo prestamo = new Prestamo();     
		ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from prestamos where cod = '"+ cod + "'");
            result = st.executeQuery();
            
            while (result.next()) {
                    prestamo.setCod(result.getInt("cod"));
                    prestamo.setPlan(result.getString("plan"));
                    prestamo.setObservacion(result.getString("observacion"));
                    prestamo.cargarCuotasPorStr(result.getString("codcuotas"));
                    prestamo.setFechaGenerado(result.getString("fechagenerado"));
                    prestamo.setCodCliente(result.getString("codcliente"));
                    prestamo.setFinalizado(result.getInt("finalizado"));
                    prestamo.setCodCobrador(result.getString("codcobrador"));
                    prestamo.setImporteDescuento(result.getString("a1"));
                    prestamo.setA2(result.getString("a2"));
                    prestamo.setA3(result.getString("a3"));
                    prestamo.setA4(result.getString("a4"));
                    prestamo.setA5(result.getString("a5"));
                    prestamo.setA6(result.getString("a6"));
            }        

        
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error al leer prestamo por codigo, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return prestamo;
    }
    
    public void eliminarPrestamoPorCod(int cod) {
		try {
		//LOGGEA
                    try {
                        Prestamo prestamo = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, cod);
                        Log l = new Log();
                        l.setAccion("Elimina prestamo");
                        l.setDetalles("CLIENTE: "+Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(prestamo.getCodCliente())).getNombre()+" PLAN: "+prestamo.getPlan());
                        
                        guardarLog(l);
                    } catch (Exception e) {
                        
                    }		
                    PreparedStatement st = conexion.prepareStatement("delete from prestamos where cod = '"+ cod + "'");
			st.executeUpdate();
                        
                        
		} catch (SQLException ex) {
            		JOptionPane.showMessageDialog(null,"Error al eliminar prestamo, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
    
    public ArrayList<Prestamo> getPrestamosPorCli(String codCli){
            ArrayList<Prestamo> prestamos = new ArrayList<Prestamo>();
                    ResultSet result = null;
            try {
                PreparedStatement st;
                if (Funciones.ocultaFinalizados()) {
                    st = conexion.prepareStatement("select * from prestamos where codcliente = '"+ codCli + "' and finalizado = 0");
                } else {
                    st = conexion.prepareStatement("select * from prestamos where codcliente = '"+ codCli + "'");
                }
                

                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {
                    Prestamo prestamo = new Prestamo();         	

                    prestamo.setCod(result.getInt("cod"));
                    prestamo.setPlan(result.getString("plan"));
                    prestamo.setObservacion(result.getString("observacion"));
                    prestamo.cargarCuotasPorStr(result.getString("codcuotas"));
                    prestamo.setFechaGenerado(result.getString("fechagenerado"));
                    prestamo.setCodCliente(result.getString("codcliente"));
                    prestamo.setFinalizado(result.getInt("finalizado"));
                    prestamo.setCodCobrador(result.getString("codcobrador"));
                    prestamo.setImporteDescuento(result.getString("a1"));
                    prestamo.setA2(result.getString("a2"));
                    prestamo.setA3(result.getString("a3"));
                    prestamo.setA4(result.getString("a4"));
                    prestamo.setA5(result.getString("a5"));
                    prestamo.setA6(result.getString("a6"));
                    
                    prestamos.add(prestamo);
            }

            } catch (SQLException ex) {
                		JOptionPane.showMessageDialog(null,"Error al cargar prestamos, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return prestamos;
    }
    
    public Prestamo getUltimoPrestamoPorCli(String codCli){
            Prestamo prestamo = new Prestamo();
                    ResultSet result = null;
            try {
                PreparedStatement st;
                if (Funciones.ocultaFinalizados()) {
                    st = conexion.prepareStatement("select * from prestamos where codcliente = '"+ codCli + "' and finalizado = 0 order by cod asc");
                } else {
                    st = conexion.prepareStatement("select * from prestamos where codcliente = '"+ codCli + "' order by cod asc");
                }
                

                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {

                    prestamo.setCod(result.getInt("cod"));
                    prestamo.setPlan(result.getString("plan"));
                    prestamo.setObservacion(result.getString("observacion"));
                    prestamo.cargarCuotasPorStr(result.getString("codcuotas"));
                    prestamo.setFechaGenerado(result.getString("fechagenerado"));
                    prestamo.setCodCliente(result.getString("codcliente"));
                    prestamo.setFinalizado(result.getInt("finalizado"));
                    prestamo.setCodCobrador(result.getString("codcobrador"));
                    prestamo.setImporteDescuento(result.getString("a1"));
                    prestamo.setA2(result.getString("a2"));
                    prestamo.setA3(result.getString("a3"));
                    prestamo.setA4(result.getString("a4"));
                    prestamo.setA5(result.getString("a5"));
                    prestamo.setA6(result.getString("a6"));
                    
            }

            } catch (SQLException ex) {
                		JOptionPane.showMessageDialog(null,"Error al cargar ultimo préstamo, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return prestamo;
    }
    
    public ArrayList<Prestamo> getPrestamos(){
            ArrayList<Prestamo> prestamos = new ArrayList<Prestamo>();
                    ResultSet result = null;
            try {
                PreparedStatement st;
                if (Funciones.ocultaFinalizados()) {
                    st = conexion.prepareStatement("select * from prestamos where finalizado = 0");
                } else {
                    st = conexion.prepareStatement("select * from prestamos");
                }
                
                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {
                    Prestamo prestamo = new Prestamo();         	

                    prestamo.setCod(result.getInt("cod"));
                    prestamo.setPlan(result.getString("plan"));
                    prestamo.setObservacion(result.getString("observacion"));
                    prestamo.cargarCuotasPorStr(result.getString("codcuotas"));
                    prestamo.setFechaGenerado(result.getString("fechagenerado"));
                    prestamo.setCodCliente(result.getString("codcliente"));
                    prestamo.setFinalizado(result.getInt("finalizado"));
                    prestamo.setCodCobrador(result.getString("codcobrador"));
                    prestamo.setImporteDescuento(result.getString("a1"));
                    prestamo.setA2(result.getString("a2"));
                    prestamo.setA3(result.getString("a3"));
                    prestamo.setA4(result.getString("a4"));
                    prestamo.setA5(result.getString("a5"));
                    prestamo.setA6(result.getString("a6"));
                    
                    prestamos.add(prestamo);
            }

            } catch (SQLException ex) {
                		JOptionPane.showMessageDialog(null,"Error al cargar prestamos, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return prestamos;
    }
    
    public ArrayList<Prestamo> getPrestamosSinFinalizar(){
            ArrayList<Prestamo> prestamos = new ArrayList<Prestamo>();
                    ResultSet result = null;
            try {
                PreparedStatement st = conexion.prepareStatement("select * from prestamos where finalizado = 0");
                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {
                    Prestamo prestamo = new Prestamo();         	

                    prestamo.setCod(result.getInt("cod"));
                    prestamo.setPlan(result.getString("plan"));
                    prestamo.setObservacion(result.getString("observacion"));
                    prestamo.cargarCuotasPorStr(result.getString("codcuotas"));
                    prestamo.setFechaGenerado(result.getString("fechagenerado"));
                    prestamo.setCodCliente(result.getString("codcliente"));
                    prestamo.setFinalizado(result.getInt("finalizado"));
                    prestamo.setCodCobrador(result.getString("codcobrador"));
                    prestamo.setImporteDescuento(result.getString("a1"));
                    prestamo.setA2(result.getString("a2"));
                    prestamo.setA3(result.getString("a3"));
                    prestamo.setA4(result.getString("a4"));
                    prestamo.setA5(result.getString("a5"));
                    prestamo.setA6(result.getString("a6"));
                    
                    prestamos.add(prestamo);
            }

            } catch (SQLException ex) {
                		JOptionPane.showMessageDialog(null,"Error al cargar prestamos sin finalizar, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return prestamos;
    }
    
    //REVISAR
    public Prestamo getUltimoPrestamo(){
        Prestamo prestamo = new Prestamo();     
	ResultSet result = null;
        int ultimoCod = 0;
        try {
//            PreparedStatement st = conexion.prepareStatement("select seq from sqlite_sequence where name='prestamos'");
//            result = st.executeQuery();
//            ultimoCod = result.getInt("seq");
//                    
            PreparedStatement st = conexion.prepareStatement("SELECT cod FROM prestamos ORDER BY cod DESC LIMIT 1");
            result = st.executeQuery();
            while (result.next()) {
                ultimoCod = result.getInt("cod");
            }
            
            
            st = conexion.prepareStatement("select * from prestamos where cod = '"+ ultimoCod + "'");
            result = st.executeQuery();
            while (result.next()) {
            prestamo.setCod(result.getInt("cod"));
            prestamo.setPlan(result.getString("plan"));
            prestamo.setObservacion(result.getString("observacion"));
            prestamo.cargarCuotasPorStr(result.getString("codcuotas"));
            prestamo.setFechaGenerado(result.getString("fechagenerado"));
            prestamo.setCodCliente(result.getString("codcliente"));
            prestamo.setFinalizado(result.getInt("finalizado"));
            prestamo.setCodCobrador(result.getString("codcobrador"));
            prestamo.setImporteDescuento(result.getString("a1"));
            prestamo.setA2(result.getString("a2"));
            prestamo.setA3(result.getString("a3"));
            prestamo.setA4(result.getString("a4"));
            prestamo.setA5(result.getString("a5"));
            prestamo.setA6(result.getString("a6"));
            }
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error al leer ultimo prestamo, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return prestamo;
    }
    
    public void actualizarPrestamo(Prestamo prestamo){
		try {
                    PreparedStatement st = conexion.prepareStatement("update prestamos set plan = ?, observacion = ?, codcuotas = ?, fechagenerado = ?, codcliente = ?, finalizado = ?, codcobrador = ?, a1 = ?, a2 = ?, a3 = ?, a4 = ?, a5 = ?, a6 = ? where cod = '"+ prestamo.getCod() + "'");
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, prestamo.getPlan().toLowerCase());
		    st.setString(2, prestamo.getObservacion());
                    st.setString(3, prestamo.getCodCuotasStr());
                    st.setString(4, prestamo.getFechaGenerado());
                    st.setString(5, prestamo.getCodCliente());
                    st.setInt(6, prestamo.getFinalizado());
                    st.setString(7, prestamo.getCodCobrador());
                    st.setString(8, prestamo.getImporteDescuento());
                    st.setString(9, prestamo.getA2());
                    st.setString(10, prestamo.getA3());
                    st.setString(11, prestamo.getA4());
                    st.setString(12, prestamo.getA5());
                    st.setString(13, prestamo.getA6());
                    
		    st.execute();
		    
                    //LOGGEA
                    try {
                        Log l = new Log();
                        l.setAccion("Actualiza datos prestamo");
                        l.setDetalles("CLIENTE: "+Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(prestamo.getCodCliente())).getNombre()+" PLAN: "+prestamo.getPlan());
                        
                        guardarLog(l);
                    } catch (Exception e) {
                        
                    }		
                    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al actualizar prestamo, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    public void marcarPrestamoFinalizado(Prestamo prestamo){
		try {
                    PreparedStatement st = conexion.prepareStatement("update prestamos set finalizado = ? where cod = '"+ prestamo.getCod() + "'");
                    
                    //Guarda en BD strings en minusculas
                    st.setInt(1, 1);
                    
		    st.execute();
		    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al marcar prestamo finalizado, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    
    //
    //  FUNC. LOCALIDADES
    //
    
    public void guardarLocalidad(Localidad l){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("insert into localidades(codint, nombre, a1, a2) values (?,?,?,?)");
		    //Guarda en BD strings en minusculas
                    st.setString(1, l.getCodInt());
                    st.setString(2, l.getNombre());
                    st.setString(3, l.getA1());
                    st.setString(4, l.getA2());
                    
		    st.execute();
		    
                    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al guardar localidador reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    
    public Localidad getLocalidadPorCod(int cod){
        Localidad l = new Localidad();     
		ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from localidades where cod = '"+ cod + "'");
            result = st.executeQuery();
            while (result.next()) {
                    l.setCod(result.getInt("cod"));
                    l.setCodInt(result.getString("codint"));
                    l.setNombre(result.getString("nombre"));
                    l.setA1(result.getString("a1"));
                    l.setA2(result.getString("a2"));
            }        
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error al leer localidad por codigo, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return l;
    }
    
    public ArrayList<Localidad> getLocalidades(){
            ArrayList<Localidad> localidades = new ArrayList<Localidad>();
            ResultSet result = null;
            try {
                PreparedStatement st;
                st = conexion.prepareStatement("select * from localidades");
                               
                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {
                    Localidad l = new Localidad();         	

                    l.setCod(result.getInt("cod"));
                    l.setCodInt(result.getString("codint"));
                    l.setNombre(result.getString("nombre"));
                    l.setA1(result.getString("a1"));
                    l.setA2(result.getString("a2"));
                    
                    localidades.add(l);
            }

            } catch (SQLException ex) {
                		JOptionPane.showMessageDialog(null,"Error al cargar localidades, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return localidades;
    }
    
    public void actualizarLocalidad(Localidad l){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("update localidades set codint = ?, nombre = ?, a1 = ?, a2 = ? where cod = '"+ l.getCod() + "'");
		    
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, l.getCodInt());
		    st.setString(2, l.getNombre());
		    st.setString(3, l.getA1());
                    st.setString(3, l.getA2());
		    st.execute();
		    
                    //LOGGEA
                    try {
                        
                        Log log = new Log();
                        log.setAccion("Actualiza localidad");
                        log.setDetalles("LOC: "+l.getNombre());
                        
                        guardarLog(log);
                    } catch (Exception e) {
                        
                    }		
                    }
		catch (SQLException ex) {
				JOptionPane.showMessageDialog(null,"Error al leer la base de datos, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    
        public void eliminarLocalidadPorCod(int cod) {
		try {
			PreparedStatement st = conexion.prepareStatement("delete from localidades where cod = '"+ cod + "'");
			st.executeUpdate();
                        
                        //LOGGEA
                    try {
                        Localidad l = Funciones.getLocalidadPorCodInt(Integer.toString(cod));
                        Log log = new Log();
                        log.setAccion("Elimina localidad");
                        log.setDetalles("LOC: "+l.getNombre());
                        
                        guardarLog(log);
                    } catch (Exception e) {
                        
                    }
		} catch (SQLException ex) {
            		JOptionPane.showMessageDialog(null,"Error al eliminar localidad, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
    //
    //  FUNC. CUOTAS
    //
    
    public void guardarCuota(Cuota cuota){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("insert into cuotas(codprestamo, numcuota, fechapagar, fechapagado, importetotal, importepagado, observacion, finalizada, a1, a2, a3) values (?,?,?,?,?,?,?,?,?,?,?)");
		    //Guarda en BD strings en minusculas
                    st.setString(1, cuota.getCodPrestamo());
                    st.setString(2, cuota.getNumCuota());
                    st.setString(3, cuota.getFechaPagar());
                    st.setString(4, cuota.getFechaPagado());
                    st.setString(5, cuota.getImporteTotal());
                    st.setString(6, cuota.getImportePagado());
                    st.setString(7, cuota.getObservacion());
                    st.setInt(8, cuota.getFinalizada());
                    st.setString(9, cuota.getA1());
                    st.setString(10, cuota.getA2());
                    st.setString(11, cuota.getA3());
                    
                    
		    st.execute();
		    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al guardar cuota, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    
    public void actualizarCuota(Cuota cuota){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("update cuotas set codprestamo = ?, numcuota = ?, fechapagar = ?, fechapagado = ?, importetotal = ?, importepagado = ?, observacion = ?, finalizada = ?, a1 = ?, a2 = ?, a3 = ?, importeinteres = ?  where cod = '"+ cuota.getCod() + "'");
		    
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, cuota.getCodPrestamo());
                    st.setString(2, cuota.getNumCuota());
                    st.setString(3, cuota.getFechaPagar());
                    st.setString(4, cuota.getFechaPagado());
                    st.setString(5, cuota.getImporteTotal());
                    st.setString(6, cuota.getImportePagado());
                    st.setString(7, cuota.getObservacion());
                    st.setInt(8, cuota.getFinalizada());
                    st.setString(9, cuota.getA1());
                    st.setString(10, cuota.getA2());
                    st.setString(11, cuota.getA3());
                    st.setString(12, cuota.getImporteInteres());
		    st.execute();
		    
                    //LOGGEA
                    try {
                        Cuota c = Funciones.getCuotaPorCod(cuota.getCod());
                        Prestamo p = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, c.getCodPrestamoInt());
                        Cliente cli = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(p.getCodCliente()));
                        Log log = new Log();
                        log.setAccion("Registra pago de cuota");
                        log.setDetalles("NUMCUOTA: "+c.getNumCuota()+" PAGADO: "+cuota.getImportePagado()+" PLAN: "+Funciones.getPrestamoPorCod(Principal.todosLosPrestamos,Integer.parseInt(c.getCodPrestamo())).getPlan()+" CLIENTE: "+cli.getNombre());
                        
                        guardarLog(log);
                    } catch (Exception e) {
                        
                    }

                    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al actualizar cliente, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    
    public Cuota getCuotaPorCod(int cod){
        Cuota cuota = new Cuota();     
		ResultSet result = null;
        try {


            PreparedStatement st = conexion.prepareStatement("select * from cuotas where cod = '"+ cod + "'");
            st.execute();
            result = st.getResultSet();
            while (result.next()) {
                    cuota.setCod(result.getInt("cod"));
                    cuota.setCodPrestamo(result.getString("codprestamo"));
                    cuota.setNumCuota(result.getString("numcuota"));
                    cuota.setFechaPagar(result.getString("fechapagar"));
                    cuota.setFechaPagado(result.getString("fechapagado"));
                    cuota.setImporteTotal(result.getString("importetotal"));
                    cuota.setImportePagado(result.getString("importepagado"));
                    cuota.setObservacion(result.getString("observacion"));
                    cuota.setFinalizada(result.getInt("finalizada"));
                    cuota.setA1(result.getString("a1"));
                    cuota.setA2(result.getString("a2"));
                    cuota.setA3(result.getString("a3"));
                    cuota.setImporteInteres(result.getString("importeinteres"));
            }
        } catch (SQLException ex) {
//           		JOptionPane.showMessageDialog(null,"Error al leer cuota por codigo, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
System.out.println("Error al leer cuota por codigo");
        }
        return cuota;
    }
    
    public void eliminarCuotaPorCodPrestamo(String cod) {
		try {
			PreparedStatement st = conexion.prepareStatement("delete from cuotas where codprestamo = '"+ cod + "'");
			st.executeUpdate();
                        
                        
		} catch (SQLException ex) {
            		JOptionPane.showMessageDialog(null,"Error al eliminar cuota, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
    
    public void setImportePagadoPorCod(int cod, String importe, String fecha, String importeInteres){
		try {
                    PreparedStatement st = conexion.prepareStatement("update cuotas set importepagado = ?, fechapagado = ?, a2 = ?, importeinteres = ? where cod = '"+ cod + "'");
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, importe);
                    st.setString(2, fecha);
                    st.setString(3, Principal.usuario.toLowerCase());
                    st.setString(4, importeInteres);
		    st.execute();
		    
                        //LOGGEA
                    try {
                        Cuota c = Funciones.getCuotaPorCod(cod);
                        Prestamo p = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, c.getCodPrestamoInt());
                        Cliente cli = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(p.getCodCliente()));
                        Log log = new Log();
                        log.setAccion("Registra pago de cuota");
                        log.setDetalles("NUMCUOTA: "+c.getNumCuota()+" PAGADO: "+importe+" PLAN: "+Funciones.getPrestamoPorCod(Principal.todosLosPrestamos,Integer.parseInt(c.getCodPrestamo())).getPlan()+" CLIENTE: "+cli.getNombre());
                        
                        guardarLog(log);
                    } catch (Exception e) {
                        
                    }
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al actualizar pago de cuota, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    public void setImporteTotalNuevoPorCod(int cod, String importe){
		try {
                    PreparedStatement st = conexion.prepareStatement("update cuotas set importetotal = ? where cod = '"+ cod + "'");
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, importe);
                    
		    st.execute();
		    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al actualizar importe total, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    public void setCobPorCod(int cod, String cob){
		try {
                    PreparedStatement st = conexion.prepareStatement("update cuotas set a1 = ? where cod = '"+ cod + "'");
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, cob);
                    
		    st.execute();
		    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al actualizar pago de cuota, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    public void setFechaVencPorCod(int cod, String fecha){
		try {
                    PreparedStatement st = conexion.prepareStatement("update cuotas set fechapagar = ? where cod = '" + cod + "'");
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, fecha);
		    st.execute();
		    
                        //LOGGEA
                    try {
                        Cuota c = Funciones.getCuotaPorCod(cod);
                        
                        Log log = new Log();
                        log.setAccion("Modifica fecha de vencimiento");
                        log.setDetalles("NUMCUOTA: "+c.getNumCuota()+" NVAFECHA: "+fecha+" PLAN: "+Funciones.getPrestamoPorCod(Principal.todosLosPrestamos,Integer.parseInt(c.getCodPrestamo())).getPlan());
                        
                        guardarLog(log);
                    } catch (Exception e) {
                        
                    }
                    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al actualizar fecha de pago, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    public void anularPagoCuota(int cod){
		try {
                    Cuota c = Funciones.getCuotaPorCod(cod);
                    Cuota cuotaInt = Funciones.getCuotaInteresPorCuota(c);
                    PreparedStatement st = conexion.prepareStatement("update cuotas set importepagado = ?, fechapagado = ?, importeinteres = ? where cod = '"+ cod + "'");
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, "0.0");
                    st.setString(2, "01/01/2099");
                    st.setString(3, "0.0");
		    st.execute();
		    
                    //MODIFICA IMPORTE INTERES
                    cuotaInt.setImporteTotalD(cuotaInt.getImporteTotalD()-c.getImporteInteresD());
                    setImporteTotalNuevoPorCod(cuotaInt.getCod(), cuotaInt.getImporteTotal());
                    
                         //LOGGEA
                    try {
                        
                        Prestamo p = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, c.getCodPrestamoInt());
                        Cliente cli = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(p.getCodCliente()));
                        Log log = new Log();
                        log.setAccion("Anula pago de cuota");
                        log.setDetalles("NUMCUOTA: "+c.getNumCuota()+" CLIENTE: "+cli.getNombre()+" PLAN: "+Funciones.getPrestamoPorCod(Principal.todosLosPrestamos,Integer.parseInt(c.getCodPrestamo())).getPlan()+" IMPPAGADO: "+c.getImportePagado());
                        
                        guardarLog(log);
                    } catch (Exception e) {
                        
                    }
                    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al anular pago de cuota, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    public ArrayList<Cuota> getCuotas(){
            ArrayList<Cuota> cuotas = new ArrayList<Cuota>();
            ResultSet result = null;
            try {
                PreparedStatement st;
                if (Funciones.ocultaFinalizados()) {
                    st = conexion.prepareStatement("select * from cuotas where finalizada = 0");
                } else {
                    st = conexion.prepareStatement("select * from cuotas");
                }
                

                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {
                    Cuota cuota = new Cuota();         	

                    cuota.setCod(result.getInt("cod"));
                    cuota.setCodPrestamo(result.getString("codprestamo"));
                    cuota.setNumCuota(result.getString("numcuota"));
                    cuota.setFechaPagar(result.getString("fechapagar"));
                    cuota.setFechaPagado(result.getString("fechapagado"));
                    cuota.setImporteTotal(result.getString("importetotal"));
                    cuota.setImportePagado(result.getString("importepagado"));
                    cuota.setObservacion(result.getString("observacion"));
                    cuota.setFinalizada(result.getInt("finalizada"));
                    cuota.setA1(result.getString("a1"));
                    cuota.setA2(result.getString("a2"));
                    cuota.setA3(result.getString("a3"));
                    cuota.setImporteInteres(result.getString("importeinteres"));
                    
                    cuotas.add(cuota);
            }

            } catch (SQLException ex) {
                		JOptionPane.showMessageDialog(null,"Error al cargar cuotas, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return cuotas;
    }
    
    public ArrayList<Cuota> getCuotasPorCodPrestamo(String cod){
            ArrayList<Cuota> cuotas = new ArrayList<>();
            ResultSet result = null;
            try {
                PreparedStatement st;
                st = conexion.prepareStatement("select * from cuotas where codprestamo = '"+cod+"'");
                

                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {
                    Cuota cuota = new Cuota();         	

                    cuota.setCod(result.getInt("cod"));
                    cuota.setCodPrestamo(result.getString("codprestamo"));
                    cuota.setNumCuota(result.getString("numcuota"));
                    cuota.setFechaPagar(result.getString("fechapagar"));
                    cuota.setFechaPagado(result.getString("fechapagado"));
                    cuota.setImporteTotal(result.getString("importetotal"));
                    cuota.setImportePagado(result.getString("importepagado"));
                    cuota.setObservacion(result.getString("observacion"));
                    cuota.setFinalizada(result.getInt("finalizada"));
                    cuota.setA1(result.getString("a1"));
                    cuota.setA2(result.getString("a2"));
                    cuota.setA3(result.getString("a3"));
                    cuota.setImporteInteres(result.getString("importeinteres"));
                    
                    cuotas.add(cuota);
            }

            } catch (SQLException ex) {
                		JOptionPane.showMessageDialog(null,"Error al cargar cuotas, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return cuotas;
    }
    
    public Cuota getUltimaCuota(){
        Cuota cuota = new Cuota();     
	ResultSet result = null;
        int ultimoCod = 0;
        try {
            
            PreparedStatement st = conexion.prepareStatement("SELECT cod FROM cuotas ORDER BY cod DESC LIMIT 1");
            result = st.executeQuery();
            while (result.next()) {
            ultimoCod = result.getInt("cod");
            }
            
            st = conexion.prepareStatement("select * from cuotas where cod = '"+ ultimoCod + "'");
            result = st.executeQuery();
            while (result.next()) {
            cuota.setCod(result.getInt("cod"));
            cuota.setCodPrestamo(result.getString("codprestamo"));
            cuota.setNumCuota(result.getString("numcuota"));
            cuota.setFechaPagar(result.getString("fechapagar"));
            cuota.setFechaPagado(result.getString("fechapagado"));
            cuota.setImporteTotal(result.getString("importetotal"));
            cuota.setImportePagado(result.getString("importepagado"));
            cuota.setObservacion(result.getString("observacion"));
            cuota.setFinalizada(result.getInt("finalizada"));
            cuota.setA1(result.getString("a1"));
            cuota.setA2(result.getString("a2"));
            cuota.setA3(result.getString("a3"));
            cuota.setImporteInteres(result.getString("importeinteres"));
            }
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error al leer ultima cuota, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return cuota;
    }
    
    public void marcarCuotaFinalizada(Cuota c){
		try {
                    PreparedStatement st = conexion.prepareStatement("update cuotas set finalizada = ? where cod = '"+ c.getCod() + "'");
                    
                    //Guarda en BD strings en minusculas
                    st.setInt(1, 1);
                    
		    st.execute();
		    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al actualizar cuota, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
     //************************
    //* CÒDIGO PARA USUARIOS *   
    //************************ 
    
    public void guardarUsuario(Usuario usuario){
	try {
                    
            PreparedStatement st = conexion.prepareStatement("insert into usuarios(nombre, password, tipo) values (?,?,?)");
            //Guarda en BD strings en minusculas
            st.setString(1, usuario.getNombre());
            st.setString(2, usuario.getPassword());
            st.setString(3, usuario.getTipo().toLowerCase());

	    st.execute();
		    
        } catch (SQLException ex) {
			JOptionPane.showMessageDialog(null,"Error al leer la base de datos, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    } 
    
    public void actualizarUsuario(Usuario usuario){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("update usuarios set nombre = ?, password = ?, tipo = ? where cod = '"+ usuario.getCod() + "'");
		    
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, usuario.getNombre());
		    st.setString(2, usuario.getPassword());
		    st.setString(3, usuario.getTipo().toLowerCase());
		    st.execute();
		    
                    }
		catch (SQLException ex) {
				JOptionPane.showMessageDialog(null,"Error al leer la base de datos, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    
    public ArrayList<Usuario> getUsuarios(){
            ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
                    ResultSet result = null;
            try {
                PreparedStatement st = conexion.prepareStatement("select * from usuarios");
                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {
                    Usuario usuario = new Usuario();         	

                    usuario.setCod(result.getInt("cod"));
                    usuario.setNombre(result.getString("nombre"));
		    usuario.setPassword(result.getString("password"));
		    usuario.setTipo(result.getString("tipo"));

                    usuarios.add(usuario);
                }

            } catch (SQLException ex) {
               		JOptionPane.showMessageDialog(null,"Error al leer la base de datos, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return usuarios;
    }
        
    public Usuario getUsuarioPorCod(int cod){
        Usuario usuario = new Usuario();     
		ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from usuarios where cod = '"+ cod + "'");
            result = st.executeQuery();
            while (result.next()) {
                    usuario.setCod(result.getInt("cod"));
                    usuario.setNombre(result.getString("nombre"));
		    usuario.setPassword(result.getString("password"));
		    usuario.setTipo(result.getString("tipo"));
            }
        
        } catch (SQLException ex) {
		JOptionPane.showMessageDialog(null,"Error al leer la base de datos, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return usuario;
    }
    
    public void eliminarUsuarioPorCod(int cod) {
		try {
			PreparedStatement st = conexion.prepareStatement("delete from usuarios where cod = '"+ cod + "'");
			st.executeUpdate();
		} catch (SQLException ex) {
 		JOptionPane.showMessageDialog(null,"Error al leer la base de datos, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
 
    //********************************
    //* FUNCIONES PARA CONFIGURACION *
    //********************************
    
    public void guardarOpciones(Opciones opciones){
		try {
                    PreparedStatement st = conexion.prepareStatement("update opciones set nombre = ?, direccion = ?, telefono = ?, interesdia = ?, mostrarpagados = ?, mostrarvencidos = ?, serial = ?, extra1 = ?, extra2 = ?, extra3 = ?, extra4 = ?, extra5 = ?, extra6 = ?");
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, opciones.getNombre());
                    st.setString(2, opciones.getDireccion().toLowerCase());
                    st.setString(3, opciones.getTelefono().toLowerCase());
                    st.setString(4, Double.toString(opciones.getInteresdia()));
                    st.setString(5, opciones.getMostrarpagados().toLowerCase());
                    st.setString(6, opciones.getMostrarvencidos().toLowerCase());
                    st.setString(7, opciones.getSerial());
                    st.setString(8, opciones.getExtra1());
                    st.setString(9, opciones.getExtra2());
                    st.setString(10, opciones.getExtra3());
                    st.setString(11, opciones.getExtra4());
                    st.setString(12, opciones.getExtra5());
                    st.setString(13, opciones.getExtra6());                    
                    
		    st.execute();
                    }
		catch (SQLException ex) {
			JOptionPane.showMessageDialog(null,"Error al guardar opciones, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);
		}
    }
    
    public Opciones getOpciones(){
        int cod = 1;
        Opciones opciones = new Opciones();
		ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from opciones");
            result = st.executeQuery();

            while (result.next()) {

                opciones.setNombre(result.getString("nombre"));
                opciones.setDireccion(result.getString("direccion"));
                opciones.setTelefono(result.getString("telefono"));
                opciones.setInteresdia(Double.parseDouble(result.getString("interesdia")));
                opciones.setMostrarpagados(result.getString("mostrarpagados"));
                opciones.setMostrarvencidos(result.getString("mostrarvencidos"));
                opciones.setSerial(result.getString("serial"));
                opciones.setExtra1(result.getString("extra1"));
                opciones.setExtra2(result.getString("extra2"));
                opciones.setExtra3(result.getString("extra3"));
                opciones.setExtra4(result.getString("extra4"));
                opciones.setExtra5(result.getString("extra5"));
                opciones.setExtra6(result.getString("extra6"));
                opciones.setUltimoBackup(result.getString("ultimobackup"));
            }
                
            
            
                 
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Error al cargar opciones, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);
        }
        return opciones;
    }
    
    //*************************
    //* FUNCIONES PARA LOGGER *
    //*************************
    
    public void guardarLog(Log log){
	try {
                    
            PreparedStatement st = conexion.prepareStatement("insert into logs(fecha, hora, usuario, accion, detalles, a1, a2, a3) values (?,?,?,?,?,?,?,?)");
            //Guarda en BD strings en minusculas
            st.setString(1, log.getFecha());
            st.setString(2, log.getHora());
            st.setString(3, log.getUsuario());
            st.setString(4, log.getAccion());
            st.setString(5, log.getDetalles());
            st.setString(6, log.getA1());
            st.setString(7, log.getA2());
            st.setString(8, log.getA3());
            
            
	    st.execute();
		    
        } catch (SQLException ex) {
			JOptionPane.showMessageDialog(null,"Error al guardar log en BD." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    } 
    
    public void guardarLogLogin() {
        if (!Principal.adminPsi) {
            Log l = new Log();
            l.setAccion("Inicio de sesion");
            guardarLog(l);
        }
        
    }
    
    public ArrayList<Log> getLogs(String cant){
            ArrayList<Log> logs = new ArrayList<>();
                    ResultSet result = null;
            try {
                
                PreparedStatement st = conexion.prepareStatement("SELECT * FROM logs ORDER BY cod DESC LIMIT "+cant+"");
                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {
                    Log l = new Log();         	

                    l.setCod(result.getInt("cod"));
                    l.setFecha(result.getString("fecha"));
                    l.setHora(result.getString("hora"));
                    l.setUsuario(result.getString("usuario"));
                    l.setAccion(result.getString("accion"));
                    l.setDetalles(result.getString("detalles"));
                    

                    logs.add(l);
                }

            } catch (SQLException ex) {
               		JOptionPane.showMessageDialog(null,"Error al leer la base de datos, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return logs;
    }
    
    //**********************************
    //* FUNCIONES PARA ACTUALIZACIONES *
    //**********************************
    
    //FINALIZADOS Y COL A1, A2 Y A3 Chequea columna "finalizado" en prestamos
    public boolean existeColumnaFinalizado() {
            boolean existe = false;     
            try {
                DatabaseMetaData md = conexion.getMetaData();
                ResultSet rs = md.getColumns(null, null, "prestamos", "finalizado");
                if (rs.next()) {
                    existe = true;
                }
            }
            catch (SQLException ex) {
                JOptionPane.showMessageDialog(null,"Error al chequear columna finalizado." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);
            }
            return existe;
    }
    
    public void crearColumnasEnPrestamos() {
        try {
            PreparedStatement st;
                         
            st = conexion.prepareStatement("PRAGMA foreign_keys = 0");
            st.execute();
            st = conexion.prepareStatement("CREATE TABLE tabla_act AS SELECT * FROM prestamos");
            st.execute();
            st = conexion.prepareStatement("DROP TABLE prestamos");
            st.execute();
            st = conexion.prepareStatement("CREATE TABLE prestamos (\n" +
            "    cod           INTEGER       PRIMARY KEY AUTOINCREMENT,\n" +
            "    plan        VARCHAR (50),\n" +
            "    observacion   VARCHAR (360),\n" +
            "    codcuotas     VARCHAR (2000),\n" +
            "    fechagenerado VARCHAR (12),\n" +
            "    codcliente    VARCHAR (6),\n" +
            "    finalizado    INTEGER (1)   DEFAULT (0),\n" +
            "    a1            VARCHAR (240) DEFAULT (' '),\n" +
            "    a2            VARCHAR (240) DEFAULT (' '),\n" +
            "    a3            VARCHAR (240) DEFAULT (' ')\n" +
            ")");
            st.execute();
            st = conexion.prepareStatement("INSERT INTO prestamos (\n" +
            "                      cod,\n" +
            "                      plan,\n" +
            "                      observacion,\n" +
            "                      codcuotas,\n" +
            "                      fechagenerado,\n" +
            "                      codcliente\n" +

            "                  )\n" +
            "                  SELECT cod,\n" +
            "                         plan,\n" +
            "                         observacion,\n" +
            "                         codcuotas,\n" +
            "                         fechagenerado,\n" +
            "                         codcliente\n" +
            "                    FROM tabla_act");
            st.execute();
            st = conexion.prepareStatement("DROP TABLE tabla_act");
            st.execute();
            st = conexion.prepareStatement("PRAGMA foreign_keys = 1");
            st.execute();


             }
	catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Error al actualizar." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
            
    }
    
    public void crearColumnasEnClientes() {
        try {
            PreparedStatement st;
                         
            st = conexion.prepareStatement("PRAGMA foreign_keys = 0");
            st.execute();
            st = conexion.prepareStatement("CREATE TABLE tabla_act_cli AS SELECT * FROM clientes");
            st.execute();
            st = conexion.prepareStatement("DROP TABLE clientes");
            st.execute();
            st = conexion.prepareStatement("CREATE TABLE clientes (\n" +
            "    cod           INTEGER       PRIMARY KEY AUTOINCREMENT,\n" +
            "    nombre        VARCHAR (50),\n" +
            "    fechanac   VARCHAR (20),\n" +
            "    dni     VARCHAR (20),\n" +
            "    direccion VARCHAR (120),\n" +
            "    localidad VARCHAR (40),\n" +
            "    telfijo VARCHAR (80),\n" +
            "    telcel VARCHAR (80),\n" +
            "    email VARCHAR (80),\n" +
            "    observacion VARCHAR (360),\n" +
            "    codprestamos VARCHAR (2000),\n" +
            "    fechaAlta VARCHAR (12),\n" +
            "    ref1 VARCHAR (300),\n" +
            "    ref2 VARCHAR (300),\n" +
            "    ref3 VARCHAR (300),\n" +
            "    ref4 VARCHAR (300),\n" +
            "    a1            VARCHAR (240) DEFAULT (' '),\n" +
            "    a2            VARCHAR (240) DEFAULT (' '),\n" +
            "    a3            VARCHAR (240) DEFAULT (' ')\n" +
            ")");
            st.execute();
            st = conexion.prepareStatement("INSERT INTO clientes (\n" +
            "                      cod,\n" +
            "                      nombre,\n" +
            "                      fechanac,\n" +
            "                      dni,\n" +
            "                      direccion,\n" +
            "                      localidad,\n" +
            "                      telfijo,\n" +
            "                      telcel,\n" +
            "                      email,\n" +
            "                      observacion,\n" +
            "                      codprestamos,\n" +
            "                      fechaAlta,\n" +
            "                      ref1,\n" +
            "                      ref2,\n" +
            "                      ref3,\n" +
            "                      ref4\n" +

            "                  )\n" +
            "                  SELECT cod,\n" +
            "                      nombre,\n" +
            "                      fechanac,\n" +
            "                      dni,\n" +
            "                      direccion,\n" +
            "                      localidad,\n" +
            "                      telfijo,\n" +
            "                      telcel,\n" +
            "                      email,\n" +
            "                      observacion,\n" +
            "                      codprestamos,\n" +
            "                      fechaAlta,\n" +
            "                      ref1,\n" +
            "                      ref2,\n" +
            "                      ref3,\n" +
            "                      ref4\n" +

            "                    FROM tabla_act_cli");
            st.execute();
            st = conexion.prepareStatement("DROP TABLE tabla_act_cli");
            st.execute();
            st = conexion.prepareStatement("PRAGMA foreign_keys = 1");
            st.execute();


             }
	catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Error al actualizar." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
            
    }
    
    public void crearColumnasEnCuotas() {
        try {
            PreparedStatement st;
                         
            st = conexion.prepareStatement("PRAGMA foreign_keys = 0");
            st.execute();
            st = conexion.prepareStatement("CREATE TABLE tabla_act_cuotas AS SELECT * FROM cuotas");
            st.execute();
            st = conexion.prepareStatement("DROP TABLE cuotas");
            st.execute();
            st = conexion.prepareStatement("CREATE TABLE cuotas (\n" +
            "    cod           INTEGER       PRIMARY KEY AUTOINCREMENT,\n" +
            "    codprestamo        VARCHAR (10),\n" +
            "    numcuota   VARCHAR (10),\n" +
            "    fechapagar     VARCHAR (12),\n" +
            "    fechapagado VARCHAR (12),\n" +
            "    importetotal VARCHAR (20),\n" +
            "    importepagado VARCHAR (20),\n" +        
            "    observacion    VARCHAR (360),\n" +
            "    finalizada    INTEGER (1)   DEFAULT (0),\n" +
            "    a1            VARCHAR (240) DEFAULT (' '),\n" +
            "    a2            VARCHAR (240) DEFAULT (' '),\n" +
            "    a3            VARCHAR (240) DEFAULT (' ')\n" +
            ")");
            st.execute();
            st = conexion.prepareStatement("INSERT INTO cuotas (\n" +
            "                      cod,\n" +
            "                      codprestamo,\n" +
            "                      numcuota,\n" +
            "                      fechapagar,\n" +
            "                      fechapagado,\n" +
            "                      importetotal,\n" +        
            "                      importepagado,\n" +        
            "                      observacion\n" +        

            "                  )\n" +
            "                  SELECT cod,\n" +
            "                      codprestamo,\n" +
            "                      numcuota,\n" +
            "                      fechapagar,\n" +
            "                      fechapagado,\n" +
            "                      importetotal,\n" +        
            "                      importepagado,\n" +        
            "                      observacion\n" +        

            "                    FROM tabla_act_cuotas");
            st.execute();
            st = conexion.prepareStatement("DROP TABLE tabla_act_cuotas");
            st.execute();
            st = conexion.prepareStatement("PRAGMA foreign_keys = 1");
            st.execute();


             }
	catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Error al actualizar." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
            
    }
    
    
    //
    //  FUNC. CARGAS APP FALLIDAS
    //
    
    //PRESTAMOS
    public void guardarActFallidaPrestamo(int codPrest){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("insert into app_fallidas_prestamos(codprest) values (?)");
		    //Guarda en BD strings en minusculas
                    st.setInt(1, codPrest);
		    st.execute();
		    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al guardar actualización fallida a la app, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    public ArrayList<Integer> getActFallidasPrestamos(){
            ArrayList<Integer> fallidas = new ArrayList<>();
                    ResultSet result = null;
            try {
                PreparedStatement st = conexion.prepareStatement("select * from app_fallidas_prestamos");
                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {
                    fallidas.add(result.getInt("codprest"));
            }

            } catch (SQLException ex) {
                		JOptionPane.showMessageDialog(null,"Error al cargar act fallidas app, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return fallidas;
    }
    public void eliminarActPorCodPrest(int cod) {
		try {
			PreparedStatement st = conexion.prepareStatement("delete from app_fallidas_prestamos where codprest = '"+ cod + "'");
			st.executeUpdate();
		} catch (SQLException ex) {
 		JOptionPane.showMessageDialog(null,"Error al leer la base de datos, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
    
    //USUARIOS
    public void guardarActFallidaUsuario(String dni, String accion){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("insert into app_fallidas_usuarios(usuario, accion) values (?,?)");
		    //Guarda en BD strings en minusculas
                    st.setString(1, dni);
                    st.setString(2, accion);
		    st.execute();
		    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error al guardar actualización fallida de usuario a la app, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    public ArrayList<ActFallidaUsuario> getActFallidasUsuarios(){
            ArrayList<ActFallidaUsuario> fallidas = new ArrayList<>();
                    ResultSet result = null;
            try {
                PreparedStatement st = conexion.prepareStatement("select * from app_fallidas_usuarios");
                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {
                    ActFallidaUsuario act = new ActFallidaUsuario();
                    act.setDni(result.getString("usuario"));
                    act.setAccion(result.getString("accion"));
                    
                    fallidas.add(act);
            }

            } catch (SQLException ex) {
                		JOptionPane.showMessageDialog(null,"Error al cargar act fallidas usuario app, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return fallidas;
    }
    public void eliminarActUserPorUsuario(String user) {
		try {
			PreparedStatement st = conexion.prepareStatement("delete from app_fallidas_usuarios where usuario = '"+ user + "'");
			st.executeUpdate();
		} catch (SQLException ex) {
 		JOptionPane.showMessageDialog(null,"Error al leer la base de datos, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
    
    
    
    //*****************************
    //* FUNCIONES PAGOS PARCIALES *
    //*****************************
    
    public void guardarPagoParcial(String usuario, Prestamo prestamo, double importe, Date fecha, int codCuota) {
            try {
                PreparedStatement st = conexion.prepareStatement("insert into pagos_parciales(cod_cliente, cod_cuota, cod_prestamo, importe, usuario, fecha, obj_prestamo, cobrador) values (?,?,?,?,?,?,?,?)");
                //Guarda en BD strings en minusculas
                st.setInt(1, Integer.parseInt(prestamo.getCodCliente()));
                st.setInt(2, codCuota);
                st.setInt(3, prestamo.getCod());
                st.setDouble(4, importe);
                st.setString(5, usuario);
                st.setDate(6, FuncionesMySQL.dateToSQLDate(fecha));
                
                ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
                ObjectOutputStream oos;
                try {
                    oos = new ObjectOutputStream(byteArray);
                    oos.writeObject(prestamo);
                } catch (IOException ex) {
                    Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                st.setBytes(7, byteArray.toByteArray());
                st.setInt(8, Integer.parseInt(prestamo.getCodCobrador()));
                st.execute();
            } catch (SQLException ex) {
                Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
    public ArrayList<PagoParcial> getPagosParcialesCond(Date dateInicio, Date dateFin, int codCliente, int codPrestamo, int codCobrador){
            ArrayList<PagoParcial> pagos = new ArrayList<>();
            ResultSet result = null;
            String where = " WHERE ";
            //FECHA 
            int cantCondiciones = 0;
            if (dateInicio != null && dateFin != null) {
                String dateSqlIni = FuncionesMySQL.dateAStringMySQL(dateInicio);
                String dateSqlFin = FuncionesMySQL.dateAStringMySQL(dateFin);
                where += "DATE(fecha) BETWEEN '"+dateSqlIni+"' AND '"+dateSqlFin+"'";
                cantCondiciones++;
            }
            
            if (codCliente != 0) {
                if (cantCondiciones>0) {
                    where += " AND ";
                }
                where += "cod_cliente = '"+ codCliente +"'";
                cantCondiciones++;
            }
            
            if (codPrestamo != 0) {
                if (cantCondiciones>0) {
                    where += " AND ";
                }
                where += "cod_prestamo = '"+ codPrestamo +"'";
                cantCondiciones++;
            }
            
            if (codCobrador != 0) {
                if (cantCondiciones>0) {
                    where += " AND ";
                }
                where += "cobrador = '"+ codCobrador +"'";
                cantCondiciones++;
            }
            
            if (where.equalsIgnoreCase(" WHERE ")) {
                return new ArrayList<>();
            }
            try {
                PreparedStatement st = conexion.prepareStatement("SELECT * FROM pagos_parciales"+where);
                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {
                    PagoParcial p = new PagoParcial();         	

                    p.setCod(result.getInt("cod"));
                    p.setCodCliente(result.getInt("cod_cliente"));
                    p.setCodCuota(result.getInt("cod_cuota"));
                    p.setCodPrestamo(result.getInt("cod_prestamo"));
                    p.setImporte(result.getDouble("importe"));
                    p.setUsuario(result.getString("usuario"));
                    p.setFecha(result.getDate("fecha"));
                    
                    
                    // Se obtiene el campo blob
                    Blob blob = result.getBlob("obj_prestamo");

                    // Se reconstruye el objeto con un ObjectInputStream
                    ObjectInputStream ois;
                    try {
                        ois = new ObjectInputStream(blob.getBinaryStream());
                        Prestamo prestamo  = (Prestamo) ois.readObject();
                        p.setPrestamo(prestamo);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    p.setCodCobrador(result.getInt("cobrador"));
                    
                    pagos.add(p);
                }

            } catch (SQLException ex) {
               		JOptionPane.showMessageDialog(null,"Error al cargar pagos parciales de la BD.\n\n" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return pagos;
    }
    
    public void eliminarPagoParcialPorCod(int cod) {
		try {
			PreparedStatement st = conexion.prepareStatement("delete from pagos_parciales where cod = '"+ cod + "'");
			st.executeUpdate();
		} catch (SQLException ex) {
 		JOptionPane.showMessageDialog(null,"Error al leer la base de datos, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.ArrayList;
import modelo.Plan;

/**
 *
 * @author Usuario
 */
public class FuncionesAux {
    public static void agregarNumOrdenAPlanes() {
        ArrayList<Plan> planes = new ArrayList<>();
        
        Conector con = new Conector();
        
        con.conectar();
        planes = con.getPlanes();
        
        
        int ultimoOrden = 0;
        
        for (Plan p: planes) {
            if (p.getNombre().contains("-app") || p.getA4().equals("personalizado")) {
                //IGNORAR
            } else {
                ultimoOrden++;
                p.setOrden(ultimoOrden);
                con.actualizarOrdenPlan(p);
            }
        }
        con.cerrar();
        
        
    }
}

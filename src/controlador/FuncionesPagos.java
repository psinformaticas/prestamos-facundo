/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import javax.swing.JOptionPane;
import modelo.Cliente;
import modelo.Cuota;
import modelo.Plan;
import modelo.Prestamo;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import reportes.ReciboA4DataSource;
import vista.GuardarPago;
import vista.OpcionesInteres;
import vista.Principal;
import static vista.Principal.guardarPago;
import static vista.Principal.optIntWindow;
import static vista.Principal.todosLosPrestamos;

/**
 *
 * @author Usuario
 */
public class FuncionesPagos {
    
    public static void cobrarCuota(Cuota cuotaAux) {
                Prestamo prestamo = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, Integer.parseInt(cuotaAux.getCodPrestamo()));
                if (prestamo.tieneCuotasAnterioresAEstaSinPagar(cuotaAux)) {
                    JOptionPane.showMessageDialog(null,"Existen cuotas ANTERIORES pendientes de pago.","*** INFO ***",1);
                } else {
                    
                
                //CARGA IMPORTE CUOTA INT A PAGAR
                if (cuotaAux.getNumCuota().equals("999")) {
                    cuotaAux.setImporteTotal(Double.toString(prestamo.getInteresTotalAlDiaDeHoy()));
                }
                
                double imptotalaux = Double.parseDouble(cuotaAux.getImporteTotal());
                double imppagadoaux = Double.parseDouble(cuotaAux.getImportePagado());
                double diferenciaaux = imptotalaux - imppagadoaux;
                
                if (diferenciaaux < 1) {
                    int dialogResult = JOptionPane.showConfirmDialog(null, "La cuota ya fue cobrada.\n\n¿Desea imprimir el comprobante de pago?","¡¡¡ATENCIÓN!!!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if(dialogResult == JOptionPane.YES_OPTION){
//                    File ticketFile = new File("tickets/"+Integer.toString(codCuota)+".psi");
//
//                    if (ticketFile.exists()) { 
//                        ImprimirComprobanteAnt iC = new ImprimirComprobanteAnt(Integer.toString(codCuota));
//                        iC.setTitle("Imprimir comprobante");
//                        iC.setVisible(true);
//                    } else {
//                        JOptionPane.showMessageDialog(null,"El ticket no se encuentra guardado.", "Error", 1);
//                    }

                    String fecha = Funciones.getFechaTexto(Funciones.stringADate(cuotaAux.getFechaPagado()));
                    Cliente cx = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, cuotaAux.getCodPrestamoInt()).getCodCliente()));
                    String cliente = Funciones.capitalize(cx.getNombre());
                    String cantidad = Funciones.importeDoubleALetras(cuotaAux.getImportePagadoD())+" pesos.";
                    Prestamo p = Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, cuotaAux.getCodPrestamoInt());
                    Plan pn = Funciones.getPlanPorNombre(p.getPlan());

                    String concepto = "";
                    if (cuotaAux.getNumCuota().equals("0")) {
                        concepto = "ANTICIPO - "+pn.getNombre().toUpperCase();
                    } else {
                        concepto = "CUOTA N°"+cuotaAux.getNumCuota()+" de "+pn.getCantCuotas()+"  -  "+pn.getNombre().toUpperCase();
                    }

                    String impefectivo = "$"+cuotaAux.getImportePagadoD();
                    String impTotal1 = "$"+cuotaAux.getImportePagadoD();
                    String obs1 = "Observaciones: "+Funciones.leerObsA4();

                    InputStream inputStream = null;
                    JasperPrint jasperPrint= null;
                    ReciboA4DataSource datasource = new ReciboA4DataSource();

                    datasource.setCantidad(cantidad);
                    datasource.setCliente(cliente);
                    datasource.setConcepto(concepto);
                    datasource.setFecha(fecha);
                    datasource.setImpefectivo(impefectivo);
                    datasource.setImptotal(impTotal1);
                    datasource.setObs(obs1);


                   try {
                        inputStream = new FileInputStream ("reporteRecibo1.jrxml");
                    } catch (FileNotFoundException ex) {
                       javax.swing.JOptionPane.showMessageDialog(null,"Error al leer el fichero de carga reporteRecibo1\n "+ex.getMessage());
                    }

                    try{
                        JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
                        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                        jasperPrint = JasperFillManager.fillReport(jasperReport, null, datasource);

                        //JasperExportManager.exportReportToPdfFile(jasperPrint, "src/agencia/reporte01.pdf");
                        //JasperViewer.viewReport(jasperPrint, false);
                        JasperViewer jv = new JasperViewer(jasperPrint, false);
                        jv.setTitle("Recibo A4");
                        jv.setVisible(true);

                    }catch (JRException em){
                        javax.swing.JOptionPane.showMessageDialog(null,"Error al cargar fichero jrml jasper report "+em.getMessage());
                    }


                      
                    }
                } else {
                    //SI NO FUE PAGADA
                    Prestamo p = Funciones.getPrestamoPorCod(todosLosPrestamos, cuotaAux.getCodPrestamoInt());

                    if (cuotaAux.getNumCuota().equals("999")) {
                        if (Funciones.prestamoTieneTodasCuotasPagasYFaltaPagarInt(p)) {
                            if (optIntWindow != null) {//si existe una venta, la cierra.
                                optIntWindow.dispose();
                            }

                            optIntWindow = new OpcionesInteres(cuotaAux.getCod());
                            optIntWindow.setTitle("Pago de interés acumulado");
                            optIntWindow.setVisible(true);
                        } else {
                            JOptionPane.showMessageDialog(null,"Todavía hay cuotas pendientes de pago.","*** INFO ***",1);
                        }
                        
                    } else {
                        if (guardarPago != null) {//si existe una venta, la cierra.
                            guardarPago.dispose();
                        }

                        guardarPago = new GuardarPago(cuotaAux.getCod());
                        guardarPago.setTitle("Registrar pago");
                        guardarPago.setVisible(true);
                    }
                }
            }
                }
}

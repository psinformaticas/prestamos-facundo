/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Usuario
 */
public class FuncionesMySQL {
    
    public static void backup() {
      try {
         Process p = Runtime
               .getRuntime()
               .exec("C:/prestav2/mysql/bin/mysqldump -uroot -proot base");

         
         InputStream is = p.getInputStream();
         FileOutputStream fos = new FileOutputStream("backup/backup_"+devolverFechaActualStr()+"__"+devolverHoraActualStr()+".sql");
         byte[] buffer = new byte[1000];

         int leido = is.read(buffer);
         while (leido > 0) {
            fos.write(buffer, 0, leido);
            leido = is.read(buffer);
         }

         fos.close();

      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   public static void restore(String ruta) {
      try {
         Process p = Runtime
               .getRuntime()
               .exec("C:/prestav2/mysql/bin/mysql -uroot -proot base");

         
         OutputStream os = p.getOutputStream();
         FileInputStream fis = new FileInputStream(ruta);
         byte[] buffer = new byte[1000];

         int leido = fis.read(buffer);
         while (leido > 0) {
            os.write(buffer, 0, leido);
            leido = fis.read(buffer);
         }

         os.flush();
         os.close();
         fis.close();

      } catch (Exception e) {
         e.printStackTrace();
      }
   }
   
   
   public static String devolverHoraActualStr() {
        Date hour = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        SimpleDateFormat hourFormat = new SimpleDateFormat("HH_mm");
        return hourFormat.format(hour);
    }
    
    public static String devolverFechaActualStr() {
        Date date = new Date();
        SimpleDateFormat f= new SimpleDateFormat("dd_MM_yyyy");
        return f.format(date);
    }
    
     public static java.sql.Date dateToSQLDate(java.util.Date uDate) {
        java.sql.Date sqlDate = new java.sql.Date(uDate.getTime());
        return sqlDate;
    }
     
     public static String dateAStringMySQL(Date fecha) {
        
        SimpleDateFormat f= new SimpleDateFormat("yyyy-MM-dd");
        return  f.format(fecha);
    }
}

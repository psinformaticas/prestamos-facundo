/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author Usuario
 */
public class FuncionesImagenes {
    
    public static ImageIcon getImageIconResized(String ruta, int ancho, int alto) {
        
         BufferedImage imgBuff = null;
        try {
            imgBuff = ImageIO.read(new File(ruta));
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        Image dimg = imgBuff.getScaledInstance(ancho, alto, BufferedImage.SCALE_SMOOTH);
        
        ImageIcon imageIcon = new ImageIcon(dimg);
        
        return imageIcon;
    }
    
     public static BufferedImage getScaledImage(String ruta, int w, int h) throws IOException {
        File file = new File(ruta);
        BufferedImage src=ImageIO.read(file);
        int original_width = src.getWidth();
        int original_height = src.getHeight();
        int bound_width = w;
        int bound_height = h;
        int new_width = original_width;
        int new_height = original_height;

        // first check if we need to scale width
        if (original_width > bound_width) {
            //scale width to fit
            new_width = bound_width;
            //scale height to maintain aspect ratio
            new_height = (new_width * original_height) / original_width;
        }

        // then check if we need to scale even with the new height
        if (new_height > bound_height) {
            //scale height to fit instead
            new_height = bound_height;
            //scale width to maintain aspect ratio
            new_width = (new_height * original_width) / original_height;
        }

        BufferedImage resizedImg = new BufferedImage(new_width, new_height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = resizedImg.createGraphics();
        g2.setBackground(Color.WHITE);
        g2.clearRect(0,0,new_width, new_height);
        g2.drawImage(src, 0, 0, new_width, new_height, null);
        g2.dispose();
        return resizedImg;
    }
            
         
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.*;
/**
 *
 * @author Will
 */
public class OpcionesExtra {
    
    private String marcaFinalizados, impresion, x1, x2, x3,x4,x5,x6,x7,x8,x9,x10;

    public OpcionesExtra() {
        this.marcaFinalizados = "";
        this.impresion = "";
        //X1 OCULTA DIRECCIONES
        this.x1 = "";
        //X2 RECIBOS A4 XLS
        this.x2 = "";
        //X3 OCULTA TELS
        this.x3 = "";
        //X4 TIPO DE RECIBO
        this.x4 = "";
        //X5 PANTALLA BIENVENIDA
        this.x5 = "";
        //X6 CARPETA DRIVE
        this.x6 = "";
        //HORA BKP DIARIO
        this.x7 = "";
        //BKP DIARIO ACTIVADO
        this.x8 = "";
        //COBRAR INT POR ATRASO SELECCIONADO
        this.x9 = "";
        this.x10 = "";
        
    }

    public String getMarcaFinalizados() {
        return marcaFinalizados;
    }

    public void setMarcaFinalizados(String marcaFinalizados) {
        this.marcaFinalizados = marcaFinalizados;
    }

    public String getImpresion() {
        return impresion;
    }

    public void setImpresion(String impresion) {
        this.impresion = impresion;
    }

    public String getX1() {
        return x1;
    }

    public void setX1(String x1) {
        this.x1 = x1;
    }

    public String getX2() {
        return x2;
    }

    public void setX2(String x2) {
        this.x2 = x2;
    }

    public String getX3() {
        return x3;
    }

    public void setX3(String x3) {
        this.x3 = x3;
    }

    public String getX4() {
        return x4;
    }

    public void setX4(String x4) {
        this.x4 = x4;
    }

    public String getX5() {
        return x5;
    }

    public void setX5(String x5) {
        this.x5 = x5;
    }

    public String getX6() {
        return x6;
    }

    public void setX6(String x6) {
        this.x6 = x6;
    }

    public String getX7() {
        return x7;
    }

    public void setX7(String x7) {
        this.x7 = x7;
    }

    public String getX8() {
        return x8;
    }

    public void setX8(String x8) {
        this.x8 = x8;
    }

    public String getX9() {
        return x9;
    }

    public void setX9(String x9) {
        this.x9 = x9;
    }

    public String getX10() {
        return x10;
    }

    public void setX10(String x10) {
        this.x10 = x10;
    }

    
    


}

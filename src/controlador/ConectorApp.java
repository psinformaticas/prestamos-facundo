/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import aplicacion.OpcionesApp;
import modelo.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import vista.Acceso;
import vista.Principal;

/**
 *
 * @author Will
 */
public class ConectorApp {
    
    	
	Connection conexion;
	private OpcionesDB opt;
        
      public ConectorApp() {
          opt = Funciones.leerOptDBApp();
          opt = Funciones.leerOptDBApp();
          
      }
      
      public void cargarOpcionesIni() {
          opt = Funciones.leerOptDBApp();
          opt = Funciones.leerOptDBApp();
      }
   
    public OpcionesDB getOpt() {
        return opt;
    }

    public void setOpt(OpcionesDB opt) {
        this.opt = opt;
    }
        
      
    public void conectar(){

        	 try {
                 Class.forName("com.mysql.cj.jdbc.Driver");
                    conexion = DriverManager.getConnection("jdbc:mysql://"+opt.getSrv()+":"+opt.getPort()+"/" + opt.getBd(), opt.getUsr(), opt.getPass());
                    //System.out.println("Se ha iniciado la conexión con el servidor de forma exitosa");
                    Principal.lbEstadoAppAbajo.setText("APP: CONECTADA");
                    Principal.btnActualizarApp.setEnabled(true);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ConectorApp.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    if (Acceso.p != null) {
                        Principal.lbEstadoAppAbajo.setText("APP: ERROR AL CONECTAR");
                        Principal.btnActualizarApp.setEnabled(false);
                    }
                    Logger.getLogger(ConectorApp.class.getName()).log(Level.SEVERE, null, ex);
                }
    }
    
    public void cerrar(){
		    try {
                        conexion.close();
                        //System.out.println("Se ha finalizado la conexión con el servidor");
                    } catch (SQLException ex) {
                        Logger.getLogger(ConectorApp.class.getName()).log(Level.SEVERE, null, ex);
                    }
	}
    
    public void borrarPrestamosYCuotas() {
        try {
                PreparedStatement st;
                st = conexion.prepareStatement("truncate table prestamos");
                st.execute();
                
                st = conexion.prepareStatement("truncate table cuotas");
                st.execute();
                
                JOptionPane.showMessageDialog(null,"Préstamos y cuotas borrados correctamente.", "OK!", 1);
        }
	catch (SQLException ex) {
        	JOptionPane.showMessageDialog(null,"Error al borrar prestamos y cuotas, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    }
        
    public void borrarPlanes() {
        try {
                PreparedStatement st;
                st = conexion.prepareStatement("truncate table planes");
                st.execute();
                    
                JOptionPane.showMessageDialog(null,"Planes borrados correctamente.", "OK!", 1);
        }
	catch (SQLException ex) {
        	JOptionPane.showMessageDialog(null,"Error al borrar planes, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    }
    
    
    public void borrarCobradores() {
        try {
                PreparedStatement st;
                st = conexion.prepareStatement("truncate table cobradores");
                st.execute();
                
                JOptionPane.showMessageDialog(null,"Cobradores borrados correctamente.", "OK!", 1);
        }
	catch (SQLException ex) {
        	JOptionPane.showMessageDialog(null,"Error al borrar cobradores, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    }
    
    public void borrarClientes() {
        try {
                PreparedStatement st;
                st = conexion.prepareStatement("truncate table clientes");
                st.execute();
                
                JOptionPane.showMessageDialog(null,"Clientes borrados correctamente.", "OK!", 1);
                
        }
	catch (SQLException ex) {
        	JOptionPane.showMessageDialog(null,"Error al borrar clientes, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    }
    
    public void vaciarLaBDCuidado() {
        try {
                borrarPrestamosYCuotas();
                borrarPlanes();
                borrarClientes();
                PreparedStatement st;
                st = conexion.prepareStatement("truncate table usuarios");
                st.execute();
                
                JOptionPane.showMessageDialog(null,"Usuarios borrados correctamente.", "OK!", 1);
                
        }
	catch (SQLException ex) {
        	JOptionPane.showMessageDialog(null,"Error al borrar clientes, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    }
    
    //
    //  FUNC. COBRADORES
    //
    
    public ArrayList<UsuarioApp> getUsuariosCobradoresApp(){
            ArrayList<UsuarioApp> usuarios = new ArrayList<>();
            UsuarioApp usuario = new UsuarioApp();
            ResultSet result = null;
            
            try {
                PreparedStatement st = conexion.prepareStatement("select * from presta_users where groupid = '3'");
                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {

                    usuario = new UsuarioApp(); 
                    usuario.setUserid(result.getInt("userid"));
                    usuario.setUsername(result.getString("username"));
                    usuario.setDisplayName(result.getString("display_name"));
                    usuario.setPassword(result.getString("password"));
                    usuario.setEmail(result.getString("email"));
                    usuario.setKey(result.getString("key"));
                    usuario.setValidated(result.getString("key"));

                    usuario.setGroupid(result.getInt("groupid"));
                    usuario.setLastActive(result.getInt("lastactive"));
                    usuario.setShowAvt(result.getInt("showavt"));
                    usuario.setBanned(result.getInt("banned"));
                    usuario.setRegTime(result.getInt("regtime"));
                    usuario.setFbProfile(result.getString("fbprofile"));
                    usuario.setLocalidad(result.getString("localidad"));
                    usuario.setToken(result.getString("token"));
                    if (usuario.getUserid()!=1) {
                        usuarios.add(usuario);
                    }
                    
                    usuario.setCodCobPC(result.getInt("cod_cob_pc"));
                }

            } catch (SQLException ex) {
               		JOptionPane.showMessageDialog(null,"Error getUsuariosCobradoresApp() " + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return usuarios;
    }
        
    public void guardarUsuarioCobradorApp(UsuarioApp usuario){
	try {
                    
            PreparedStatement st = conexion.prepareStatement("insert into presta_users(username, display_name, password, email, groupid, lastactive, regtime, codpc, localidad, token,cod_cob_pc) values (?,?,?,?,?,?,?,?,?,?,?)");
            //Guarda en BD strings en minusculas
            st.setString(1, usuario.getUsername());
            st.setString(2, usuario.getDisplayName());
            st.setString(3, usuario.getPassword());
            st.setString(4, usuario.getEmail());
            st.setInt(5, 3);
            st.setInt(6, Funciones.returnEpochActual());
            st.setInt(7, Funciones.returnEpochActual());
            st.setInt(8, usuario.getCodPC());
            st.setString(9, usuario.getLocalidad());
            st.setString(10, Funciones.getAlphaNumericString(16));
            st.setInt(11, usuario.getCodCobPC());
	    st.execute();
		    
        } catch (SQLException ex) {
			JOptionPane.showMessageDialog(null,"Error guardarUsuarioCobradorApp().\n\n" + ex.getMessage()+"\n\n"+ex.getStackTrace().toString()+"\n\n", "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    } 
    

    //----------------

    public void guardarCobrador(Cobrador c){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("insert into cobradores(cod,nombre, codint, telefono, obs, a1, a2, a3, a4, nombre_app) values (?,?,?,?,?,?,?,?,?,?)");
		    //Guarda en BD strings en minusculas
                    st.setInt(1, c.getCod());
                    st.setString(2, c.getNombre().toLowerCase());
		    st.setString(3, c.getCodInt());
		    st.setString(4, c.getTelefono());
                    st.setString(5, c.getObs());
                    st.setString(6, c.getA1());
                    st.setString(7, c.getA2());
                    st.setString(8, c.getA3());
                    st.setString(9, c.getA4());
                    st.setString(10, c.getNombreApp());
		    st.execute();
		    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error guardarCobrador() App" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    
    public Cobrador getCobradorPorCod(int cod){
        Cobrador c = null;     
		ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from cobradores where cod = '"+ cod + "'");
            result = st.executeQuery();
            
            while (result.next()) {
                    c = new Cobrador(); 
                    c.setCod(result.getInt("cod"));
                    c.setNombre(result.getString("nombre"));
                    c.setCodInt(result.getString("codint"));
                    c.setTelefono(result.getString("telefono"));
                    c.setObs(result.getString("obs"));
                    c.setA1(result.getString("a1"));
                    c.setA2(result.getString("a2"));
                    c.setA3(result.getString("a3"));
                    c.setA4(result.getString("a4"));
            }
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error getCobradorPorCod() App" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return c;
    }
    
    public Cobrador getCobradorPorNombre(String nombre){
        Cobrador c = new Cobrador();     
		ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from cobradores where nombre = '"+ nombre.toLowerCase() + "'");
            result = st.executeQuery();
            
            while (result.next()) {
                    c.setCod(result.getInt("cod"));
                    c.setNombre(result.getString("nombre"));
                    c.setCodInt(result.getString("codint"));
                    c.setTelefono(result.getString("telefono"));
                    c.setObs(result.getString("obs"));
                    c.setA1(result.getString("a1"));
                    c.setA2(result.getString("a2"));
                    c.setA3(result.getString("a3"));
                    c.setA4(result.getString("a4"));
            }
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error getCobradorPorNombre() App." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return c;
    }
    
    public void eliminarCobradorPorCod(int cod) {
		try {
                 
                    PreparedStatement st = conexion.prepareStatement("delete from cobradores where cod = '"+ cod + "'");
			st.executeUpdate();
                        
                        
		} catch (SQLException ex) {
            		JOptionPane.showMessageDialog(null,"Error eliminarCobradorPorCod() App." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
    
    public void actualizarCobrador(Cobrador c){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("update cobradores set nombre = ?, codint = ?, telefono = ?, obs = ?, a1 = ?,"
                            + " a2 = ?, a3 = ?, a4 = ?, nombre_app = ? where cod = '"+ c.getCod() + "'");
		    
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, c.getNombre().toLowerCase());
		    st.setString(2, c.getCodInt());
		    st.setString(3, c.getTelefono());
                    st.setString(4, c.getObs());
                    st.setString(5, c.getA1());
                    st.setString(6, c.getA2());
                    st.setString(7, c.getA3());
                    st.setString(8, c.getA4());
                    st.setString(9, c.getNombreApp());
		    st.execute();
		    
                  
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error actualizarCobrador() App" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    public ArrayList<Cobrador> getCobradores(){
            ArrayList<Cobrador> cs = new ArrayList<Cobrador>();
                    ResultSet result = null;
            try {
                PreparedStatement st = conexion.prepareStatement("select * from cobradores");
                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {
                    Cobrador c = new Cobrador();         	

                    c.setCod(result.getInt("cod"));
                    c.setNombre(result.getString("nombre"));
                    c.setCodInt(result.getString("codint"));
                    c.setTelefono(result.getString("telefono"));
                    c.setObs(result.getString("obs"));
                    c.setA1(result.getString("a1"));
                    c.setA2(result.getString("a2"));
                    c.setA3(result.getString("a3"));
                    c.setA4(result.getString("a4"));
                    
                    cs.add(c);
            }

            } catch (SQLException ex) {
                		JOptionPane.showMessageDialog(null,"Error getCobradores() App." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return cs;
    }
    
    public boolean cobradorEstaGuardado(String nombre, String codInt) {
        ArrayList<Cobrador> cs = new ArrayList<Cobrador>();
        Cobrador c = new Cobrador();
        cs = getCobradores();
        
        for(int i = 0; i < cs.size(); i++) {
            c = cs.get(i);
            if (c.getNombre().equals(nombre)) {
                return true;
            }
            if (c.getCodInt().equals(codInt)) {
                return true;
            }
        }
        
        return false;
    }
    
    
    //
    //  FUNC. PLANES
    //
    
    public void guardarPlan(Plan plan){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("insert into planes(cod, nombre, importeprestar, importedevolver, importeinteres, calculapor, tipo, cantcuotas, interesporc, porccobrador, a1, a2, a3, a4) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		    //Guarda en BD strings en minusculas
                    st.setInt(1, plan.getCod());
                    st.setString(2, plan.getNombre().toLowerCase());
		    st.setString(3, plan.getImportePrestar());
                    st.setString(4, plan.getImporteDevolver());
                    st.setString(5, plan.getImporteInteres());
                    st.setString(6, plan.getCalculaPor());
                    st.setString(7, plan.getTipo());
                    st.setString(8, plan.getCantCuotas());
                    st.setString(9, plan.getPorcentajeInteres());
                    st.setString(10, plan.getPorcentajeCobrador());
                    st.setString(11, plan.getA1());
                    st.setString(12, plan.getA2());
                    st.setString(13, plan.getA3());
                    st.setString(14, plan.getA4());
    
		    st.execute();
		    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error guardarPlan() App." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    
    public Plan getPlanPorNombre(String nom){
        Plan plan = null;     
	ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from planes where nombre = '"+ nom.toLowerCase() + "'");
            result = st.executeQuery();
            
            while (result.next()) {
                    plan = new Plan();     
                    plan.setCod(result.getInt("cod"));
                    plan.setNombre(result.getString("nombre"));
                    plan.setImportePrestar(result.getString("importeprestar"));
                    plan.setImporteDevolver(result.getString("importedevolver"));
                    plan.setImporteInteres(result.getString("importeinteres"));
                    plan.setCalculaPor(result.getString("calculapor"));
                    plan.setTipo(result.getString("tipo"));
                    plan.setCantCuotas(result.getString("cantcuotas"));
                    plan.setPorcentajeInteres(result.getString("interesporc"));
                    plan.setPorcentajeCobrador(result.getString("porccobrador"));
                    plan.setA1(result.getString("a1"));
                    plan.setA2(result.getString("a2"));
                    plan.setA3(result.getString("a3"));
                    plan.setA4(result.getString("a4"));
            }
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error getPlanPorNombre() App." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return plan;
    }
    
    public void actualizarPlan(Plan plan){
		try {
                    PreparedStatement st = conexion.prepareStatement("update planes set nombre = ?, importeprestar = ?, importedevolver = ?, importeinteres = ?, calculapor = ?, tipo = ?, cantcuotas = ?, interesporc = ?, porccobrador = ?, a1 = ?, a2 = ?, a3 = ?, a4 = ? where cod = '"+ plan.getCod() + "'");
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, plan.getNombre().toLowerCase());
		    st.setString(2, plan.getImportePrestar());
                    st.setString(3, plan.getImporteDevolver());
                    st.setString(4, plan.getImporteInteres());
                    st.setString(5, plan.getCalculaPor());
                    st.setString(6, plan.getTipo());
                    st.setString(7, plan.getCantCuotas());
                    st.setString(8, plan.getPorcentajeInteres());
                    st.setString(9, plan.getPorcentajeCobrador());
                    st.setString(10, plan.getA1());
                    st.setString(11, plan.getA2());
                    st.setString(12, plan.getA3());
                    st.setString(13, plan.getA4());
		    st.execute();
		    
                   
                    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error actualizarPlan() App." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    public void eliminarPlanPorNombre(String plan) {
		try {
                 
                    PreparedStatement st = conexion.prepareStatement("delete from planes where nombre = '"+ plan + "'");
			st.executeUpdate();
                        
                        
		} catch (SQLException ex) {
            		JOptionPane.showMessageDialog(null,"Error eliminarPlanPorNombre() App, por favor reinicie el programa." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
    
    //
    //  FUNC. PRESTAMOS
    //
    
    public void guardarPrestamo(Prestamo prestamo){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("insert into prestamos(cod, plan, observacion, codcuotas, fechagenerado, codcliente, finalizado, codcobrador, a1, a2, a3, a4, a5, a6, nom_cobrador, cod_cobrador) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		    //Guarda en BD strings en minusculas
                    st.setInt(1, prestamo.getCod());
                    st.setString(2, prestamo.getPlan().toLowerCase());
		    st.setString(3, prestamo.getObservacion());
                    st.setString(4, prestamo.getCodCuotasStr());
                    st.setString(5, prestamo.getFechaGenerado());
                    st.setString(6, prestamo.getCodCliente());
                    st.setInt(7, prestamo.getFinalizado());
                    st.setString(8, prestamo.getCodCobrador());
                    st.setString(9, prestamo.getImporteDescuento());
                    st.setString(10, prestamo.getA2());
                    st.setString(11, prestamo.getA3());
                    st.setString(12, prestamo.getA4());
                    st.setString(13, prestamo.getA5());
                    st.setString(14, prestamo.getA6());
                    Cobrador c = Funciones.getCobradorPorRuta(Principal.cobradores, prestamo.getCodCobrador());
                    st.setString(15, Funciones.capitalize(c.getNombreApp()));
                    st.setInt(16, c.getCod());
                    
                    
		    st.execute();
		 
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error guardarPrestamo() App." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    
    public Prestamo getPrestamoPorCod(int cod){
        Prestamo prestamo = null;     
	ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from prestamos where cod = '"+ cod + "'");
            result = st.executeQuery();
            
            while (result.next()) {
                    prestamo = new Prestamo();
                    prestamo.setCod(result.getInt("cod"));
                    prestamo.setPlan(result.getString("plan"));
                    prestamo.setObservacion(result.getString("observacion"));
                    prestamo.cargarCuotasPorStr(result.getString("codcuotas"));
                    prestamo.setFechaGenerado(result.getString("fechagenerado"));
                    prestamo.setCodCliente(result.getString("codcliente"));
                    prestamo.setFinalizado(result.getInt("finalizado"));
                    prestamo.setCodCobrador(result.getString("codcobrador"));
                    prestamo.setImporteDescuento(result.getString("a1"));
                    prestamo.setA2(result.getString("a2"));
                    prestamo.setA3(result.getString("a3"));
                    prestamo.setA4(result.getString("a4"));
                    prestamo.setA5(result.getString("a5"));
                    prestamo.setA6(result.getString("a6"));
            }        

        
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error getPrestamoPorCod() App." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return prestamo;
    }
    
    public ArrayList<Prestamo> getPrestamosPorPlan(String plan){
        ArrayList<Prestamo> prestamos = new ArrayList<>();     
	ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from prestamos where plan = '"+ plan + "'");
            result = st.executeQuery();
            
            while (result.next()) {
                    Prestamo prestamo = new Prestamo();
                    prestamo = new Prestamo();
                    prestamo.setCod(result.getInt("cod"));
                    prestamo.setPlan(result.getString("plan"));
                    prestamo.setObservacion(result.getString("observacion"));
                    prestamo.cargarCuotasPorStr(result.getString("codcuotas"));
                    prestamo.setFechaGenerado(result.getString("fechagenerado"));
                    prestamo.setCodCliente(result.getString("codcliente"));
                    prestamo.setFinalizado(result.getInt("finalizado"));
                    prestamo.setCodCobrador(result.getString("codcobrador"));
                    prestamo.setImporteDescuento(result.getString("a1"));
                    prestamo.setA2(result.getString("a2"));
                    prestamo.setA3(result.getString("a3"));
                    prestamo.setA4(result.getString("a4"));
                    prestamo.setA5(result.getString("a5"));
                    prestamo.setA6(result.getString("a6"));
                    
                    prestamos.add(prestamo);
            }        

        
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error getPrestamosPorPlan() App." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return prestamos;
    }
    
    public void eliminarPrestamoPorCod(int cod) {
		try {
                 
                    PreparedStatement st = conexion.prepareStatement("delete from prestamos where cod = '"+ cod + "'");
			st.executeUpdate();
                        
                        
		} catch (SQLException ex) {
            		JOptionPane.showMessageDialog(null,"Error eliminarPrestamoPorCod() App." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
    
    //REVISAR
    public Prestamo getUltimoPrestamo(){
        Prestamo prestamo = new Prestamo();     
	ResultSet result = null;
        int ultimoCod = 0;
        try {
//            PreparedStatement st = conexion.prepareStatement("select seq from sqlite_sequence where name='prestamos'");
//            result = st.executeQuery();
//            ultimoCod = result.getInt("seq");
//                    
            PreparedStatement st = conexion.prepareStatement("SELECT cod FROM prestamos ORDER BY cod DESC LIMIT 1");
            result = st.executeQuery();
            while (result.next()) {
                ultimoCod = result.getInt("cod");
            }
            
            
            st = conexion.prepareStatement("select * from prestamos where cod = '"+ ultimoCod + "'");
            result = st.executeQuery();
            while (result.next()) {
            prestamo.setCod(result.getInt("cod"));
            prestamo.setPlan(result.getString("plan"));
            prestamo.setObservacion(result.getString("observacion"));
            prestamo.cargarCuotasPorStr(result.getString("codcuotas"));
            prestamo.setFechaGenerado(result.getString("fechagenerado"));
            prestamo.setCodCliente(result.getString("codcliente"));
            prestamo.setFinalizado(result.getInt("finalizado"));
            prestamo.setCodCobrador(result.getString("codcobrador"));
            prestamo.setImporteDescuento(result.getString("a1"));
            prestamo.setA2(result.getString("a2"));
            prestamo.setA3(result.getString("a3"));
            prestamo.setA4(result.getString("a4"));
            prestamo.setA5(result.getString("a5"));
            prestamo.setA6(result.getString("a6"));
            }
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error getUltimoPrestamo() App." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return prestamo;
    }
    
    public void actualizarPrestamo(Prestamo prestamo){
		try {
                    PreparedStatement st = conexion.prepareStatement("update prestamos set plan = ?, observacion = ?, codcuotas = ?, fechagenerado = ?, codcliente = ?, finalizado = ?, codcobrador = ?, a1 = ?, a2 = ?, a3 = ?, a4 = ?, a5 = ?, a6 = ?, nom_cobrador = ?, cod_cobrador = ? where cod = '"+ prestamo.getCod() + "'");
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, prestamo.getPlan().toLowerCase());
		    st.setString(2, prestamo.getObservacion());
                    st.setString(3, prestamo.getCodCuotasStr());
                    st.setString(4, prestamo.getFechaGenerado());
                    st.setString(5, prestamo.getCodCliente());
                    st.setInt(6, prestamo.getFinalizado());
                    st.setString(7, prestamo.getCodCobrador());
                    st.setString(8, prestamo.getImporteDescuento());
                    st.setString(9, prestamo.getA2());
                    st.setString(10, prestamo.getA3());
                    st.setString(11, prestamo.getA4());
                    st.setString(12, prestamo.getA5());
                    st.setString(13, prestamo.getA6());
                    st.setString(13, prestamo.getA6());
                    Cobrador c = Funciones.getCobradorPorRuta(Principal.cobradores, prestamo.getCodCobrador());
                    st.setString(14, Funciones.capitalize(c.getNombreApp()));
                    st.setInt(15, c.getCod());
                    st.execute();    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error actualizarPrestamo() App" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    public void marcarPrestamoFinalizado(Prestamo prestamo){
		try {
                    PreparedStatement st = conexion.prepareStatement("update prestamos set finalizado = ? where cod = '"+ prestamo.getCod() + "'");
                    
                    //Guarda en BD strings en minusculas
                    st.setInt(1, 1);
                    
		    st.execute();
		    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error marcarPrestamoFinalizado() App." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    
    
    //
    //  FUNC. CUOTAS
    //
    
    public void guardarCuota(Cuota cuota){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("insert into cuotas(cod, codprestamo, numcuota, fechapagar, fechapagado, importetotal, importepagado, observacion, finalizada, a1, a2, a3) values (?,?,?,?,?,?,?,?,?,?,?,?)");
		    //Guarda en BD strings en minusculas
                    st.setInt(1, cuota.getCod());
                    st.setString(2, cuota.getCodPrestamo());
                    st.setString(3, cuota.getNumCuota());
                    st.setString(4, cuota.getFechaPagar());
                    st.setString(5, cuota.getFechaPagado());
                    st.setString(6, cuota.getImporteTotal());
                    st.setString(7, cuota.getImportePagado());
                    st.setString(8, cuota.getObservacion());
                    st.setInt(9, cuota.getFinalizada());
                    st.setString(10, cuota.getA1());
                    st.setString(11, cuota.getA2());
                    st.setString(12, cuota.getA3());
                    
                    
		    st.execute();
		    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error guardarCuota()" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    
    public void actualizarCuota(Cuota cuota){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("update cuotas set codprestamo = ?, numcuota = ?, fechapagar = ?, fechapagado = ?, importetotal = ?, importepagado = ?, observacion = ?, finalizada = ?, a1 = ?, a2 = ?, a3 = ?, importeinteres = ?  where cod = '"+ cuota.getCod() + "'");
		    
                    
                    //Guarda en BD strings en minusculas
                    st.setString(1, cuota.getCodPrestamo());
                    st.setString(2, cuota.getNumCuota());
                    st.setString(3, cuota.getFechaPagar());
                    st.setString(4, cuota.getFechaPagado());
                    st.setString(5, cuota.getImporteTotal());
                    st.setString(6, cuota.getImportePagado());
                    st.setString(7, cuota.getObservacion());
                    st.setInt(8, cuota.getFinalizada());
                    st.setString(9, cuota.getA1());
                    st.setString(10, cuota.getA2());
                    st.setString(11, cuota.getA3());
                    st.setString(12, cuota.getImporteInteres());
		    st.execute();
                    
                    }
		catch (SQLException ex) {
					JOptionPane.showMessageDialog(null,"Error actualizarCuota() App" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
	}
    
    public Cuota getCuotaPorCod(int cod){
        Cuota cuota = null;    
        ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from cuotas where cod = '"+ cod + "'");
            result = st.executeQuery();
            while (result.next()) {
                    cuota = new Cuota();  
                    cuota.setCod(result.getInt("cod"));
                    cuota.setCodPrestamo(result.getString("codprestamo"));
                    cuota.setNumCuota(result.getString("numcuota"));
                    cuota.setFechaPagar(result.getString("fechapagar"));
                    cuota.setFechaPagado(result.getString("fechapagado"));
                    cuota.setImporteTotal(result.getString("importetotal"));
                    cuota.setImportePagado(result.getString("importepagado"));
                    cuota.setObservacion(result.getString("observacion"));
                    cuota.setFinalizada(result.getInt("finalizada"));
                    cuota.setA1(result.getString("a1"));
                    cuota.setA2(result.getString("a2"));
                    cuota.setA3(result.getString("a3"));
                    cuota.setImporteInteres(result.getString("importeinteres"));
            }
        } catch (SQLException ex) {
           		JOptionPane.showMessageDialog(null,"Error getCuotaPorCod() App" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return cuota;
    }
    
    public void eliminarCuotasPorCodPrestamo(int codPrest) {
		try {
                 
                    PreparedStatement st = conexion.prepareStatement("delete from cuotas where codprestamo = '"+ codPrest + "'");
			st.executeUpdate();
                        
                        
		} catch (SQLException ex) {
            		JOptionPane.showMessageDialog(null,"Error eliminarCuotasPorCodPrestamo() App " + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
    
     //************************
    //* CÒDIGO PARA USUARIOS *   
    //************************ 
    
    public void guardarUsuarioApp(UsuarioApp usuario){
	try {
                    
            PreparedStatement st = conexion.prepareStatement("insert into presta_users(username, display_name, password, email, lastactive, regtime, codpc, localidad, token) values (?,?,?,?,?,?,?,?,?)");
            //Guarda en BD strings en minusculas
            st.setString(1, usuario.getUsername());
            st.setString(2, usuario.getDisplayName());
            st.setString(3, usuario.getPassword());
            st.setString(4, usuario.getEmail());
            st.setInt(5, Funciones.returnEpochActual());
            st.setInt(6, Funciones.returnEpochActual());
            st.setInt(7, usuario.getCodPC());
            st.setString(8, usuario.getLocalidad());
            st.setString(9, Funciones.getAlphaNumericString(16));
	    st.execute();
		    
        } catch (SQLException ex) {
			JOptionPane.showMessageDialog(null,"Error guardarUsuarioApp() App\n\n" + ex.getMessage()+"\n\n"+ex.getStackTrace().toString()+"\n\n", "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    } 
    
    public void actualizarUsuario(UsuarioApp usuario){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("update presta_users set display_name = ?, localidad = ? where username = '"+ usuario.getUsername()+ "'");
		    
                    
                    //Guarda en BD strings en minusculas
		    st.setString(1, usuario.getDisplayName());
                    st.setString(2, usuario.getLocalidad());
		    st.execute();
		    
                    }
		catch (SQLException ex) {
				JOptionPane.showMessageDialog(null,"Error actualizarUsuario() App" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    
    public void blanquearPassUsuarioPorUsername(String username){
		try {
                    PreparedStatement st = conexion.prepareStatement("update presta_users set password = ? where username = '"+ username + "'");
		    
                    //Guarda en BD strings en minusculas
                    st.setString(1, "");
		    st.execute();
                    }
		catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null,"Error blanquearPassUsuarioPorUsername()" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    
    public ArrayList<UsuarioApp> getUsuariosApp(){
            ArrayList<UsuarioApp> usuarios = new ArrayList<>();
            UsuarioApp usuario = new UsuarioApp();
            ResultSet result = null;
            
            try {
                PreparedStatement st = conexion.prepareStatement("select * from presta_users where groupid = '2'");
                result = st.executeQuery();

                //Lista todos los autos 
                while (result.next()) {

                    usuario = new UsuarioApp(); 
                    usuario.setUserid(result.getInt("userid"));
                    usuario.setUsername(result.getString("username"));
                    usuario.setDisplayName(result.getString("display_name"));
                    usuario.setPassword(result.getString("password"));
                    usuario.setEmail(result.getString("email"));
                    usuario.setKey(result.getString("key"));
                    usuario.setValidated(result.getString("key"));

                    usuario.setGroupid(result.getInt("groupid"));
                    usuario.setLastActive(result.getInt("lastactive"));
                    usuario.setShowAvt(result.getInt("showavt"));
                    usuario.setBanned(result.getInt("banned"));
                    usuario.setRegTime(result.getInt("regtime"));
                    usuario.setFbProfile(result.getString("fbprofile"));
                    usuario.setLocalidad(result.getString("localidad"));
                    usuario.setToken(result.getString("token"));
                    if (usuario.getGroupid() !=4 && usuario.getGroupid() != 3) {
                        usuarios.add(usuario);
                    }
                    
                }

            } catch (SQLException ex) {
               		JOptionPane.showMessageDialog(null,"Error getUsuariosApp() App" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
            }
            return usuarios;
    }
        
    public UsuarioApp getUsuarioAppPorID(int cod){
        UsuarioApp usuario = new UsuarioApp();     
		ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from usuarios where userid = '"+ cod + "'");
            result = st.executeQuery();
            while (result.next()) {
                    usuario = new UsuarioApp(); 
                    usuario.setUserid(result.getInt("userid"));
                    usuario.setUsername(result.getString("username"));
                    usuario.setDisplayName(result.getString("display_name"));
                    usuario.setPassword(result.getString("password"));
                    usuario.setEmail(result.getString("email"));
                    usuario.setKey(result.getString("key"));
                    usuario.setValidated(result.getString("key"));

                    usuario.setGroupid(result.getInt("groupid"));
                    usuario.setLastActive(result.getInt("lastactive"));
                    usuario.setShowAvt(result.getInt("showavt"));
                    usuario.setBanned(result.getInt("banned"));
                    usuario.setRegTime(result.getInt("regtime"));
                    usuario.setFbProfile(result.getString("fbprofile"));
                    usuario.setLocalidad(result.getString("localidad"));
                    usuario.setToken(result.getString("token"));
            }
        
        } catch (SQLException ex) {
		JOptionPane.showMessageDialog(null,"Error getUsuarioAppPorID() App" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return usuario;
    }
    
    public UsuarioApp getUsuarioAppPorUsername(String username){
        UsuarioApp usuario = null;     
	ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from presta_users where username = '"+ username + "'");
            result = st.executeQuery();
            while (result.next()) {
                usuario = new UsuarioApp(); 
                usuario.setUserid(result.getInt("userid"));
                usuario.setUsername(result.getString("username"));
                usuario.setDisplayName(result.getString("display_name"));
                usuario.setPassword(result.getString("password"));
                usuario.setEmail(result.getString("email"));
                usuario.setKey(result.getString("key"));
                usuario.setValidated(result.getString("key"));
                
                usuario.setGroupid(result.getInt("groupid"));
                usuario.setLastActive(result.getInt("lastactive"));
                usuario.setShowAvt(result.getInt("showavt"));
                usuario.setBanned(result.getInt("banned"));
                usuario.setRegTime(result.getInt("regtime"));
                usuario.setFbProfile(result.getString("fbprofile"));
                usuario.setLocalidad(result.getString("localidad"));
                usuario.setToken(result.getString("token"));
            }
        
        } catch (SQLException ex) {
		JOptionPane.showMessageDialog(null,"Error getUsuarioAppPorUsername() App." + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return usuario;
    }
    
    public void eliminarUsuarioAppPorUsername(String username) {
		try {
			PreparedStatement st = conexion.prepareStatement("delete from presta_users where username = '"+ username + "'");
			st.executeUpdate();
		} catch (SQLException ex) {
 		JOptionPane.showMessageDialog(null,"Error eliminarUsuarioAppPorUsername() App " + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
 
    public void eliminarPrivacidadUser(int cod) {
		try {
			PreparedStatement st = conexion.prepareStatement("delete from presta_privacy where userid = '"+ cod + "'");
			st.executeUpdate();
		} catch (SQLException ex) {
 		JOptionPane.showMessageDialog(null,"Error eliminarPrivacidadUser() App" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
    }
    
    public void guardarPrivacidadUser(int userId){
     try {
                    
            PreparedStatement st = conexion.prepareStatement("insert into presta_privacy(userid, email) values (?,?)");
            //Guarda en BD strings en minusculas
            st.setInt(1, userId);
            st.setInt(2, 0);
        
            
	    st.execute();
        
		    
        } catch (SQLException ex) {
			JOptionPane.showMessageDialog(null,"Error guardarPrivacidadUser() App \n" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
	}
    } 
 
    //***********************
    //* OPCIONES APLICACION *   
    //***********************
    
    public void actualizarAjustes(OpcionesApp opc){
		try {
                    
                    PreparedStatement st = conexion.prepareStatement("update opciones set num_whatsapp = ?, num_comunicarse = ?, novedades = ?, permitir_login = ? where cod = '1'");
		    
                    
                    //Guarda en BD strings en minusculas
		    st.setString(1, opc.getNumWhatsapp());
                    st.setString(2, opc.getNumComunicarse());
                    st.setString(3, opc.getNovedades());
                    st.setInt(4, opc.getPermitirLogin());
		    st.execute();
		    
                    }
		catch (SQLException ex) {
				JOptionPane.showMessageDialog(null,"Error actualizarAjustes() App\n" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
		}
    }
    
        
    public OpcionesApp getAjustesApp(){
        OpcionesApp opc = new OpcionesApp();     
	ResultSet result = null;
        try {
            PreparedStatement st = conexion.prepareStatement("select * from opciones where cod = '1'");
            result = st.executeQuery();
            while (result.next()) {
                    opc = new OpcionesApp();  
                    opc.setNumWhatsapp(result.getString("num_whatsapp"));
                    opc.setNumComunicarse(result.getString("num_comunicarse"));
                    opc.setNovedades(result.getString("novedades"));
                    opc.setPermitirLogin(result.getInt("permitir_login"));
            }
        
        } catch (SQLException ex) {
		JOptionPane.showMessageDialog(null,"Error getAjustesApp() App\n" + ex.getMessage(), "Error, reinicie el programa", JOptionPane.ERROR_MESSAGE);	
        }
        return opc;
    }
    
}

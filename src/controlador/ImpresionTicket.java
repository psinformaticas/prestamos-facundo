/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import br.com.adilson.util.Extenso;
import br.com.adilson.util.PrinterMatrix;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.JOptionPane;
import modelo.Cliente;
import modelo.Cuota;
import modelo.DatosComprobante;
import modelo.Plan;
import modelo.Prestamo;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import reportes.ReciboA4DataSource;
import reportes.TestExcel;
import vista.Principal;

/**
 *
 * @author Will
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Administrador
 */
public class ImpresionTicket {
    
public static int getPosCentrado(String linea) {
    int mitad = linea.length()/2;
    int pos = 20 - mitad;
    
    return pos;
    
}    

public static void imprimirTicketAnt(String cod) {
    FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("tickets/"+cod+".psi");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();


        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.err.println("No existen impresoras instaladas");
        }
        
}

public static void imprimirTicketPago(Prestamo p, Cuota c, String interes, String impTotal, String dias, boolean imprime){
        Principal.con.conectar();
        Cliente cli = Principal.con.getClientePorCod(Integer.parseInt(p.getCodCliente()));
        Principal.con.cerrar();
        
        PrinterMatrix printer = new PrinterMatrix();
        int total = 0;
        Extenso e = new Extenso();
       
        e.setNumber(101.85);

        int tam = 50;
        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(tam, 40);
        //Imprimir * de la 2da linea a 25 en la columna 1;
       // printer.printCharAtLin(2, 25, 1, "*");
       

       //*********************
       //* IMPRIMIR CABECERA *
       //*********************
       int i=1;
       printer.printTextWrap(i, i, getPosCentrado(Principal.opciones.nombre), 40, Principal.opciones.nombre.toUpperCase());
       i++;
       printer.printTextWrap(i, i, 14, 40, "");
       i++;
       printer.printTextWrap(i, i, getPosCentrado(Principal.opciones.direccion), 40, Principal.opciones.direccion.toUpperCase());
       i++;
       printer.printTextWrap(i, i, getPosCentrado(Principal.opciones.telefono), 40, Principal.opciones.telefono.toUpperCase());

       
       i++;
       printer.printTextWrap(i, i, 10, 40, "");
       i++;
       printer.printCharAtCol(i, 1, 40, "=");
       i++;
       printer.printTextWrap(i, i, getPosCentrado("RECIBO DE PAGO"), 40, "RECIBO DE PAGO");
       i++;
       printer.printTextWrap(i, i, getPosCentrado("Documento no valido como factura"), 40, "Documento no valido como factura");
       i++;
       i++;
       i++;
       printer.printCharAtCol(i, 1, 40, "=");
     
       i++;
       //Imprimir DATOS
       printer.printTextWrap(i, i, 1, 40, "Fecha: " + Funciones.devolverFechaActualStr());       
       printer.printTextWrap(i, i, 26, 40, "Cod: " + (Integer.toString(c.getCod())));       
       i++;
       printer.printTextWrap(i, i, 1, 40, "Hora: " + Funciones.devolverHoraActualStr());       
       i++;
       printer.printTextWrap(i, i, 1, 40, "Generado por: " + Funciones.capitalize(Principal.usuario));       
       i++;
       printer.printTextWrap(i, i, 1, 40, "");      
       i++;
        
       //Imprimir EMPRESA
       printer.printTextWrap(i, i, 1, 40, "Cliente: " + Funciones.capitalize(cli.getNombre()));       
       i++;
       printer.printTextWrap(i, i, 1, 40, "DNI: " + cli.getDni());       
       i++;
       printer.printTextWrap(i, i, 1, 40, "Direccion: " + Funciones.capitalize(cli.getDireccion()));       
       i++;
       printer.printTextWrap(i, i, 1, 40, "Telefono: " + Funciones.capitalize(cli.getTelefono()));       
       i++;
       i++;
       ArrayList<Cuota> cs = Funciones.getCuotasPorCodPrestamo(Principal.todasLasCuotas, p.getCodCuotasStr());
       printer.printTextWrap(i, i, 1, 40, "Prestamo: " + Funciones.capitalize(p.getPlan()));       
       i++;
//       printer.printTextWrap(i, i, 1, 40, "Importe prestado: $" + Funciones.getPlanPorNombre(p.getPlan()).getImportePrestar());      
//       i++;
//       printer.printTextWrap(i, i, 1, 40, "Importe a devolver: $" + Funciones.getPlanPorNombre(p.getPlan()).getImporteDevolver());       
//       i++;
       i++;
       printer.printTextWrap(i, i, 1, 40, "Cuota: " + c.getNumCuota()+" de "+ Integer.toString(p.getCuotas().size())); 
       i++;
       printer.printTextWrap(i, i, 1, 40, "Vencimiento: " + c.getFechaPagar());
       i++;
       printer.printTextWrap(i, i, 1, 40, ""); 
       i++;
       printer.printTextWrap(i, i, 1, 40, "Importe cuota: $" + c.getImporteTotal());       
       i++;
       if (dias.contains("-")) {
           printer.printTextWrap(i, i, 1, 40, "Interes: $" + interes + " (0) dias"); 
       } else {
           printer.printTextWrap(i, i, 1, 40, "Interes: $" + interes + " ("+dias+") dias"); 
       }
       i++;
       printer.printTextWrap(i, i, 1, 40, "Importe total: $" + impTotal);
       
       i++;
        i++;
       printer.printTextWrap(i, i, 1, 40, ""); 
       i++;
       printer.printCharAtCol(i, 1, 40, "=");
       i++;
       //Imprimir TOTAL Y FIRMA
       i++;
       printer.printTextWrap(i, i, 1, 40, "_______________"); 
       printer.printTextWrap(i, i, 23, 40, "________________________"); 
       i++;
       printer.printTextWrap(i, i, 1, 40, "Firma");
       printer.printTextWrap(i, i, 23, 40, "Aclaracion");

       printer.toFile("tickets/"+Integer.toString(c.getCod())+".psi");

       if (imprime) {
           
                if(Funciones.leerOptExtra().getX4().equals("80MM")) {
                        FileInputStream inputStream = null;
                         try {
                             inputStream = new FileInputStream("tickets/"+Integer.toString(c.getCod())+".psi");
                         } catch (FileNotFoundException ex) {
                             ex.printStackTrace();
                         }
                         if (inputStream == null) {
                             return;
                         }

                         DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
                         Doc document = new SimpleDoc(inputStream, docFormat, null);

                         PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

                         PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();


                         if (defaultPrintService != null) {
                             DocPrintJob printJob = defaultPrintService.createPrintJob();
                             try {
                                 printJob.print(document, attributeSet);

                             } catch (Exception ex) {
                                 ex.printStackTrace();
                             }
                         } else {
                             System.err.println("No existen impresoras instaladas");
                         }
                 }
                if(Funciones.leerOptExtra().getX4().equals("A4")) {
                            Cuota cX = c;

                            String fecha = Funciones.getFechaTexto(Funciones.stringADate(cX.getFechaPagado()));
                            Cliente cx = Funciones.getClientePorCod(Principal.clientes, Integer.parseInt(Funciones.getPrestamoPorCod(Principal.todosLosPrestamos, cX.getCodPrestamoInt()).getCodCliente()));
                            String cliente = Funciones.capitalize(cx.getNombre());
                            String cantidad = Funciones.importeDoubleALetras(cX.getImportePagadoD())+" pesos.";
                            
                            Plan pn = Funciones.getPlanPorNombre(p.getPlan());

                            String concepto = "";
                            if (cX.getNumCuota().equals("0")) {
                                concepto = "ANTICIPO - "+pn.getNombre().toUpperCase();
                            } else {
                                concepto = "CUOTA N°"+cX.getNumCuota()+" de "+pn.getCantCuotas()+"  -  "+pn.getNombre().toUpperCase();
                            }

                            String impefectivo = "$"+cX.getImportePagadoD();
                            String impTotal1 = "$"+cX.getImportePagadoD();
                            String obs1 = "Observaciones: "+Funciones.leerObsA4();

                            if (cX.getImportePagadoD()>0.0) {
                                if (Principal.opcionesExtra.getX2().equals("SI")) {
                                    //XLS
                                    DatosComprobante d = new DatosComprobante();

                                    d.setCant1("1");
                                    d.setDetalle1(concepto);
                                    d.setMonto1(impTotal1);

                                    d.setDireccion(Funciones.capitalize(cx.getDireccion()));
                                    d.setFecha(cX.getFechaPagado());
                                    d.setNombre(cliente);
                                    d.setNumero("0001-"+Funciones.formatearNumRecibo(Integer.toString(cX.getCod())));
                                    d.setObs(obs1);
                                    d.setTelefono(cx.getTelefono()+ "   "+cx.getCelular());
                                    d.setTipo("RECIBO");
                                    d.setTotal(impTotal1);

                                    TestExcel.rellenarXls(d);

                                } else {
                                    //PDF
                                    InputStream inputStream = null;
                                    JasperPrint jasperPrint= null;
                                    ReciboA4DataSource datasource = new ReciboA4DataSource();

                                    datasource.setCantidad(cantidad);
                                    datasource.setCliente(cliente);
                                    datasource.setConcepto(concepto);
                                    datasource.setFecha(fecha);
                                    datasource.setImpefectivo(impefectivo);
                                    datasource.setImptotal(impTotal1);
                                    datasource.setObs(obs1);


                                   try {
                                        inputStream = new FileInputStream ("reporteRecibo1.jrxml");
                                    } catch (FileNotFoundException ex) {
                                       javax.swing.JOptionPane.showMessageDialog(null,"Error al leer el fichero de carga reporteRecibo1\n "+ex.getMessage());
                                    }

                                    try{
                                        JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
                                        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                                        jasperPrint = JasperFillManager.fillReport(jasperReport, null, datasource);

                                        //JasperExportManager.exportReportToPdfFile(jasperPrint, "src/agencia/reporte01.pdf");
                                        //JasperViewer.viewReport(jasperPrint, false);
                                        JasperViewer jv = new JasperViewer(jasperPrint, false);
                                        jv.setTitle("Recibo A4");
                                        jv.setVisible(true);

                                    }catch (JRException exc){
                                        javax.swing.JOptionPane.showMessageDialog(null,"Error al cargar fichero jrml jasper report "+exc.getMessage());
                                    }
                                }


                            } else {
                                JOptionPane.showMessageDialog(null,"La cuota aún no fue pagada.","Información",0);

                            }
                }
        }
        //inputStream.close();

    }

public static void imprimirTicketPrueba(){
        
        PrinterMatrix printer = new PrinterMatrix();
        int total = 0;
        Extenso e = new Extenso();
       
        e.setNumber(101.85);

        int tam = 35;
        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(tam, 40);
        //Imprimir * de la 2da linea a 25 en la columna 1;
       // printer.printCharAtLin(2, 25, 1, "*");
       

       //*********************
       //* IMPRIMIR CABECERA *
       //*********************
       int i=1;
       printer.printTextWrap(i, i, getPosCentrado("Prontas Soluciones Informaticas"), 40, "Prontas Soluciones Informaticas");
       i++;
       printer.printTextWrap(i, i, getPosCentrado("www.psinformaticas.com.ar"), 40, "www.psinformaticas.com.ar");
       i++;
       printer.printTextWrap(i, i, getPosCentrado("soporte@psinformaticas.com.ar"), 40, "soporte@psinformaticas.com.ar");
       
       i++;
       printer.printTextWrap(i, i, 10, 40, "");
       i++;
       printer.printCharAtCol(i, 1, 40, "=");
       i++;
       printer.printTextWrap(i, i, getPosCentrado("Gestion de prestamos"), 40, "Gestion de prestamos");
       i++;
       printer.printTextWrap(i, i, getPosCentrado("www.psinformaticas.com.ar/prestamos"), 40, "www.psinformaticas.com.ar/prestamos");
       i++;
       i++;
       i++;
       printer.printCharAtCol(i, 1, 40, "=");
     
       i++;
       //Imprimir DATOS
       printer.printTextWrap(i, i, 1, 40, "Fecha: " + Funciones.devolverFechaActualStr());       
       i++;
       printer.printTextWrap(i, i, 1, 40, "Hora: " + Funciones.devolverHoraActualStr());       
       i++;
       printer.printTextWrap(i, i, 1, 40, "Generado por: " + Funciones.capitalize(Principal.usuario));       
       i++;
       printer.printTextWrap(i, i, 1, 40, "");      
       i++;
        
       //Imprimir EMPRESA
       printer.printTextWrap(i, i, 1, 40, "Si puede ver este ticket correctamente");       
       i++;
       printer.printTextWrap(i, i, 1, 40, "la impresora se encuentra instalada y ");       
       i++;
       printer.printTextWrap(i, i, 1, 40, "configurada sin problemas.");       
       i++;
       printer.printTextWrap(i, i, 1, 40, "Ya puede utilizar su impresora!");       
       i++;
       i++;
       i++;
   

       printer.printTextWrap(i, i, 1, 40, "¡Gracias por comprar el programa!");
       

       printer.toFile("prueba.psi");

      FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("prueba.psi");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();


        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.err.println("No existen impresoras instaladas");
        }

        //inputStream.close();

    }

}


    


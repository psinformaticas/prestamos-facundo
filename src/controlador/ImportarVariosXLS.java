package controlador;

import java.io.File;
import java.io.PrintStream;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import jxl.Sheet;
import jxl.Workbook;
import modelo.Cliente;
import modelo.Cobrador;
import modelo.Localidad;

import modelo.Plan;
import vista.Principal;





public class ImportarVariosXLS
{
  
 
    
          
  public static void leerArchivoExcel(String archivoDestino)
  {
    
    ArrayList<Cobrador> cobradores = new ArrayList<Cobrador>();
    ArrayList<Localidad> localidades = new ArrayList<Localidad>();
    ArrayList<Cliente> clientes = new ArrayList<Cliente>();
    

    Cobrador cobrador = new Cobrador();
    Localidad localidad = new Localidad();
    Cliente cliente = new Cliente();
    
    try
    {
      Workbook archivoExcel = Workbook.getWorkbook(new File(archivoDestino));
      System.out.println("Número de Hojas\t" + archivoExcel.getNumberOfSheets());
      
       //HOJA 3 COBRADORES  

            Sheet hoja = archivoExcel.getSheet(2);
            int numColumnas = hoja.getColumns();
            int numFilas = hoja.getRows();

            for (int fila = 1; fila < numFilas; fila++)
            {

              cobrador = new Cobrador();

              for (int columna = 0; columna < numColumnas; columna++)
              {

                if (columna == 0) {
                    
                    String cod = hoja.getCell(columna, fila).getContents().toLowerCase();
                    if (cod.length() == 2) {
                        cod = "0"+cod;
                    }
                    if (cod.length() == 1) {
                        cod = "00"+cod;
                    }
                    
                    cobrador.setCodInt(cod);
                }
                if (columna == 1) {
                 cobrador.setNombre(hoja.getCell(columna, fila).getContents());
                }
                
              }

              cobradores.add(cobrador);
            }
            //FIN HOJA 3 COBRADORES
     
            
            
   //HOJA 2 LOCALIDADES  

            hoja = archivoExcel.getSheet(1);
            numColumnas = hoja.getColumns();
            numFilas = hoja.getRows();

      
            for (int fila = 1; fila < numFilas; fila++)
            {

              localidad = new Localidad();

              for (int columna = 0; columna < numColumnas; columna++)
              {

               if (columna == 0) {
                   String loc = hoja.getCell(columna, fila).getContents().toLowerCase();
                    if (loc.length() == 2) {
                        loc = "0"+loc;
                    }
                    if (loc.length() == 1) {
                        loc = "00"+loc;
                    }
                    localidad.setCodInt(loc);
                }
                if (columna == 1) {
                 localidad.setNombre(hoja.getCell(columna, fila).getContents());
                }


              }
            
              localidades.add(localidad);
            }
            //FIN HOJA 1 CLIENTES
     
            
        //HOJA 1 CLIENTES  

            hoja = archivoExcel.getSheet(0);
            numColumnas = hoja.getColumns();
            numFilas = hoja.getRows();

            for (int fila = 1; fila < numFilas; fila++)
            {

              cliente = new Cliente();

              for (int columna = 0; columna < numColumnas; columna++)
              {

                if (columna == 0) {
                    cliente.setNombre(hoja.getCell(columna, fila).getContents().toLowerCase());
                }
                //REVISAR
                if (columna == 1) {
                    cliente.setFechaNac(hoja.getCell(columna, fila).getContents().toLowerCase());
                }
                if (columna == 2) {
                    cliente.setDni(hoja.getCell(columna, fila).getContents().toLowerCase());
                }
                if (columna == 3) {
                    cliente.setDireccion(hoja.getCell(columna, fila).getContents().toLowerCase());
                }
                if (columna == 4) {
                    String loc = hoja.getCell(columna, fila).getContents().toLowerCase();
                    if (loc.length() == 2) {
                        loc = "0"+loc;
                    }
                    if (loc.length() == 1) {
                        loc = "00"+loc;
                    }
                    
                    for (Localidad l: localidades) {
                        if (l.getCodInt().equals(loc)) {
                            cliente.setLocalidad(loc);
                        }
                    }
                    if (cliente.getLocalidad().equals("")) {
                        cliente.setLocalidad("xxx");
                    }
                    
                }
                if (columna == 5) {
                    cliente.setTelefono(hoja.getCell(columna, fila).getContents().toLowerCase());
                }
                if (columna == 6) {
                    cliente.setCelular(hoja.getCell(columna, fila).getContents().toLowerCase());
                }
                if (columna == 7) {
                    cliente.setEmail(hoja.getCell(columna, fila).getContents().toLowerCase());
                }
                if (columna == 8) {
                    String ruta = hoja.getCell(columna, fila).getContents().toLowerCase();
                    if (ruta.length() == 2) {
                        ruta = "0"+ruta;
                    }
                    if (ruta.length() == 1) {
                        ruta = "00"+ruta;
                    }
                    
                    for (Cobrador c: cobradores) {
                        if (c.getCodInt().equals(ruta)) {
                            cliente.setA1(ruta);
                        }
                    }
                    if (cliente.getA1().equals("")) {
                        cliente.setA1("xxx");
                    }
                    
                }
                
              }


              clientes.add(cliente);
            }
            //FIN HOJA 1 CLIENTES
     
      
            Principal.workerImpPlanes(localidades, cobradores, clientes);
//            Principal.cargarLocalidades();
//            Principal.cargarCobradores();
//            Principal.cargarClientes();
            
      
    } catch (Exception ioe) {
      ioe.printStackTrace();
    }
  }
}

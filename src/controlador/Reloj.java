/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;


import java.util.*;


import vista.*;
import static vista.Principal.loadWindow;
import workers.Ventana;
import workers.WorkerBackupAuto;

public class Reloj extends javax.swing.JFrame implements Runnable{
    String hora;//, minutos, segundos, ampm;
    String fecha;
    String fechaAct;
    String horaAct;
    public static boolean haciendoBkp;
    Calendar calendario;
    Thread h1;
    
    public Reloj() {
        initComponents();
        h1 = new Thread(this);
        h1.start();
        fechaAct = "01/01/2099";
        horaAct = "00:66";
        haciendoBkp = false;
   
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblReloj = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblReloj.setFont(new java.awt.Font("Trebuchet MS", 1, 48)); // NOI18N
        lblReloj.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblReloj.setText("jLabel1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblReloj, javax.swing.GroupLayout.DEFAULT_SIZE, 330, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblReloj, javax.swing.GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblReloj;
    // End of variables declaration//GEN-END:variables

    @Override
    public void run() {
        Thread ct= Thread.currentThread();
        Date hoy = new Date();
        while(ct==h1){
            //calcula();
            hora = Funciones.devolverHoraActualStr();
            fecha = Funciones.devolverFechaActualStr();
            Principal.lbFecha.setText(fecha);//hora+":"+minutos+" "+ampm);
            Principal.lbHora.setText(hora);//hora+":"+minutos+" "+ampm);
            
            
            //COMPRUEBA Y HACE BKP
            
            if (hora.equals(Principal.opcionesExtra.getX7())&& !haciendoBkp) {
                haciendoBkp = true;
                workerBkp();
            }
            //RESERVAS
            //JOptionPane.showMessageDialog(null,"PROCESO DE RELOJ","",1);
//            if (Principal.webActiva()) {
//                int cantViajes = 0;
//                if (Principal.viajesWeb != null) {
//                    cantViajes = Principal.viajesWeb.size();
//                }
//                System.out.println("CANT VIAJES WEB: "+Integer.toString(cantViajes));
//                try {
//                    Principal.cargarViajesWeb();
//                    System.out.println("Principal.cargarViajesWeb();");
//                    if (Principal.viajesWeb.size()>cantViajes && cantViajes != 0) {
//                      System.out.println("Entra al if viajes.size > cantViajes");
//                        if (Principal.avisoSol != null) {//si existe una venta, la cierra.
//                            Principal.avisoSol.dispose();
//                        }
//
//                        Principal.avisoSol = new AvisoSolicitud();
//                        Principal.avisoSol.setTitle("Solicitudes");
//                        Principal.avisoSol.setVisible(true);
//                        System.out.println("Abre ventana solis");
//                    }
//                } catch (ParseException ex) {
//                    Logger.getLogger(Reloj.class.getName()).log(Level.SEVERE, null, ex);
//                    System.out.println("ERRROR");
//                }
//            }            
//            Principal.reservasV.clear();
//            System.out.println("Clear Reservas");
////            JOptionPane.showMessageDialog(null,"RESERVAS V BORRADAS","",1);
//            for (Reserva r: Principal.reservas) {
////                JOptionPane.showMessageDialog(null,"CICLO RESERVAS","",1);
////                System.out.println("RESERVA "+r.getFechaReserva()+"   "+r.getHoraReserva());
//                if (r.getDateReserva().equals(hoy) || r.getDateReserva().before(hoy)) {
////                    System.out.println("* RESERVA DE HOY *");
//                    if (Funciones.reservaAtrasada(r)) {
////                        System.out.println("* RESERVA ATRASADA *");
//                        Principal.reservasV.add(r);
////                        JOptionPane.showMessageDialog(null,"RES AGREGADA","",1);
//                    }
//                }
//            }
//       
//            if (Principal.reservasV.size()>0) {
//                Toolkit.getDefaultToolkit().beep();
//                String[] buttons = { "Entendido!", "Ver listado de pendientes" };
//                String msg = "";
//                if (Principal.reservasV.size()==1) {
//                    msg = "<html><font size='12' color=#ffffdd>Hay 1 reserva pendiente.</font></html>";
//                } else {
//                    msg = "<html><font size='12' color=#ffffdd>Hay reservas pendientes.<br><br>Actualmente hay "+Principal.reservasV.size()+" reservas<br>vencidas o por vencer proximamente.</font></html>";
//                }
//                
//                UIManager.put("OptionPane.background", Color.RED);
//                UIManager.getLookAndFeelDefaults().put("Panel.background", Color.RED);
//
//                int rc = JOptionPane.showOptionDialog(null, msg, "ALERTA!",
//                    JOptionPane.WARNING_MESSAGE, 0, null, buttons, buttons[1]);
//
//                UIManager.put("OptionPane.background", Color.white);
//                UIManager.getLookAndFeelDefaults().put("Panel.background", Color.white);
//                
//                if (rc == 1) {
//                    
//                
//                if (Principal.listaVenc != null) {//si existe una venta, la cierra.
//                    Principal.listaVenc.dispose();
//                }
//
//                Principal.listaVenc = new ListadoVencidas();
//                Principal.listaVenc.setTitle("Reservas próximas");
//                Principal.listaVenc.setVisible(true);
//    }
//                System.out.println(rc);
//                
//            }
        
//        JOptionPane.showMessageDialog(null,"PROCESO DE RELOJ FIN","",1);
        try{
                Thread.sleep(35000);
                
            }catch(InterruptedException e){
            javax.swing.JOptionPane.showMessageDialog(null,"Error al actualizar el reloj, por favor reinicie el programa." + e.getMessage(), "Error, reinicie el programa", javax.swing.JOptionPane.ERROR_MESSAGE);}
        }
    }

    public static void workerBkp() {
        WorkerBackupAuto cargando = new WorkerBackupAuto();
                
            loadWindow = new Ventana();
            loadWindow.setVisible(true);
            cargando.setProgreso(Ventana.barra);
            cargando.execute();
    }
    
//    private void calcula() {
//        Calendar calendario = new GregorianCalendar();
//        Date fechaHoraActual = new Date();
//        
//        calendario.setTime(fechaHoraActual);
//        ampm= calendario.get(Calendar.AM_PM)==Calendar.AM?"AM":"PM";
//        
//        if(ampm.equals("PM")){
//            int h=calendario.get(Calendar.HOUR_OF_DAY)-12;
//            hora = h>9?""+h:"0"+h;
//        }else{
//            hora= calendario.get(Calendar.HOUR_OF_DAY)>9?""+calendario.get(Calendar.HOUR_OF_DAY):"0"+calendario.get(Calendar.HOUR_OF_DAY); 
//        }
//        minutos = calendario.get(Calendar.MINUTE)>9?""+calendario.get(Calendar.MINUTE):"0"+calendario.get(Calendar.MINUTE);
//        segundos = calendario.get(Calendar.SECOND)>9?""+calendario.get(Calendar.SECOND):"0"+calendario.get(Calendar.SECOND);
//    }
}
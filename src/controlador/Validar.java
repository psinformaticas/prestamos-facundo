/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import modelo.Plan;
import vista.Principal;

/**
 *
 * @author Usuario
 */
public class Validar {
    
    public static void nombrePlan(String nombre) {
        if (nombre.equalsIgnoreCase("")) {
            throw new RuntimeException("El nombre del plan no puede estar vacío.");
        }
        if (nombre.length()>40) {
            throw new RuntimeException("El nombre del plan excede el largo permitido (40 caracteres).");
        }
        if (nombre.contains("-app")) {
            throw new RuntimeException("El nombre del plan contiene una secuencia de caracteres no permitida.\n\n-app");
        }
        
        for (Plan p: Principal.planes) {
            if (p.getNombre().equalsIgnoreCase(nombre)) {
                throw new RuntimeException("El nombre del plan ya se encuentra utilizado, modifique el nombre.");
            }
        }
    }
}

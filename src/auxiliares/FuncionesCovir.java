/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auxiliares;

import controlador.Conector;
import controlador.Funciones;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import modelo.Cliente;
import modelo.Cuota;
import modelo.Plan;
import modelo.Prestamo;

/**
 *
 * @author Usuario
 */
public class FuncionesCovir {
 
    
    public static void adelantaPagoCuotasPrestamoXDias(Conector con, int dias, Date fechaInicial, Prestamo p, ArrayList<Cuota> todasLasCuotas, ArrayList<Prestamo> todosLosPrestamos, boolean adelantarParciales) {
            
        //OBTIENE NUM ULTIMA CUOTA SIN PAGAR
        String ultimaCuotaSinPagar = "";
        Cuota cuota = new Cuota();
        boolean sigue = true;
        for (Cuota c: p.getCuotas()) {
            if (!c.getNumCuota().equalsIgnoreCase("999") && !c.getNumCuota().equalsIgnoreCase("0")) {
                if (adelantarParciales) {
                    if (!c.yaSePago() && sigue && (c.getFechaPagarD().equals(fechaInicial) || c.getFechaPagarD().after(fechaInicial)) || 
                            !c.yaSePago() && sigue) {
                        //System.out.println("CAMBIAR A PARTIR DE CUOTA: "+c.getNumCuota());
                        ultimaCuotaSinPagar = c.getNumCuota();
                        cuota = c;
                        sigue = false;
                    }
                } else {
                    if (!c.yaSePago() && sigue && (c.getFechaPagarD().equals(fechaInicial) || c.getFechaPagarD().after(fechaInicial))) {
                        //System.out.println("CAMBIAR A PARTIR DE CUOTA: "+c.getNumCuota());
                        ultimaCuotaSinPagar = c.getNumCuota();
                        cuota = c;
                        sigue = false;
                    }
                }
                
            }
        }
        
        
    
        Date fechaCuota = Funciones.stringADate(cuota.getFechaPagar());
        Date fechaCuotaAdelanto = Funciones.sumarDias(Funciones.stringADate(cuota.getFechaPagar()),dias);
        ArrayList<Cuota>cuotasAux = new ArrayList<Cuota>();
        ArrayList<Cuota>cuotas = new ArrayList<Cuota>();
        cuotas.clear();
        cuotasAux.clear();
        cuotasAux = Funciones.getCuotasPorCodPrestamo(todasLasCuotas, Integer.toString(p.getCod()));
        
        Plan pn = Funciones.getPlanPorNombre(Funciones.getPrestamoPorCod(todosLosPrestamos, p.getCod()).getPlan());
        
        ArrayList<String> fechasMismoDia = new ArrayList<>();
        //AGREGAPRIMERA
        fechasMismoDia.add(Funciones.dateAString(fechaCuotaAdelanto));
        int cont1 = 0;
        //System.out.println("FECHA CUOTA BASE: " + cuota.getFechaPagar());
        for (Cuota c: cuotasAux) {
            //System.out.println("FECHA CUOTA 2: " + c.getFechaPagar());
            if (Funciones.stringADate(c.getFechaPagar()).after(Funciones.stringADate(cuota.getFechaPagar())) || Funciones.stringADate(c.getFechaPagar()).equals(Funciones.stringADate(cuota.getFechaPagar())))  {
                
                Cuota cA = new Cuota();
                cA.setCod(c.getCod());
                cA.setCodPrestamo(c.getCodPrestamo());
                cA.setFechaPagado(c.getFechaPagado());
                cA.setFechaPagar(c.getFechaPagar());
                cA.setNumCuota(c.getNumCuota());
                cA.setImportePagado(c.getImportePagado());
                cA.setImporteTotal(c.getImporteTotal());
                cA.setObservacion(c.getObservacion());
                
                cuotas.add(cA);
                cont1++;
            }
        }    
           // System.out.println("CANT CUOTAS A MODIFICAR: "+cont1);
            
        //AGREGA OTRAS CUOTAS
        if (pn.getTipo().equals("mensual")) {
            for (int x = 0; x < cont1-1; x++) {
                Date dateAg = Funciones.sumarMeses(Funciones.stringADate(fechasMismoDia.get(0)),x+1);
                fechasMismoDia.add(Funciones.dateAString(dateAg));
            }
        } else if (pn.getTipo().equals("semanal")) {
            cont1 = cont1 * 7;
            for (int x = 0; x < cont1-1; x+=7) {
                System.out.println(x+7);
                Date dateAg = Funciones.sumarDias(Funciones.stringADate(fechasMismoDia.get(0)),x+7);
                fechasMismoDia.add(Funciones.dateAString(dateAg));
            }
        } else if (pn.getTipo().contains("p-")) {
            String[] arr = pn.getTipo().split("-");
            int cantDias = Integer.parseInt(arr[1]);
            cont1 = cont1 * cantDias;
            for (int x = 0; x < cont1-1; x+=cantDias) {
                System.out.println(x+7);
                Date dateAg = Funciones.sumarDias(Funciones.stringADate(fechasMismoDia.get(0)),x+cantDias);
                fechasMismoDia.add(Funciones.dateAString(dateAg));
            }
        } else {
            for (int x = 0; x < cont1-1; x++) {
                Date dateAg = Funciones.sumarDias(Funciones.stringADate(fechasMismoDia.get(0)),x+1);
                fechasMismoDia.add(Funciones.dateAString(dateAg));
            }
        }
        
        
        int diasDif = 0;
        diasDif = Funciones.calcularDiasDeDiferencia(Funciones.dateAString(fechaCuotaAdelanto), cuota.getFechaPagar());
        
        int ind = 0;
        for (Cuota cAux: cuotas) {
            Date fechaNv = Funciones.stringADate(fechasMismoDia.get(ind));
            ind++;
//            Date fechaNv = Funciones.sumarDias(Funciones.stringADate(cAux.getFechaPagar()), diasDif);
//            if (mismoDia.isSelected()) {
//                String diaCobro = Funciones.dateAString(fechaCuota.getDate()).substring(0,2);
//                String fA1 = Funciones.dateAString(fechaNv);
//                String fAN = diaCobro + fA1.substring(2, 10);
//                
//                //COMPRUEBA SI SE PASA DIA DEL MES
//                if (Integer.parseInt(diaCobro) > Funciones.ultimoDiaMes(fAN)) {
//                    diaCobro = Integer.toString(Funciones.ultimoDiaMes(fAN));
//                    fAN = diaCobro + fA1.substring(2, 10);
//                }
//                
//                cAux.setFechaPagar(fAN);
////                System.out.println(fAN);
//            } else {
                cAux.setFechaPagar(Funciones.dateAString(fechaNv));
            }
        
        //ACTUALIZA EN BD
        //            System.out.println(diasDif);
            con.conectar();
            for (Cuota cAux: cuotas) {
                if (!cAux.getNumCuota().equals("999")) {
                    con.setFechaVencPorCod(cAux.getCod(), cAux.getFechaPagar());
                }
            }
            con.cerrar();
            
    } 


    
}
